/*
 Test for verifying, that logging workss appropriately.

 */

let app, scatterContainer, card

const messageChecker = {}
messageChecker.log = []
messageChecker.send = function(type, message) {
    this.log.push({ type, message })

    console.log(message, this.log.length)
}
messageChecker.getLastMessage = function() {
    console.log(this.log)
    const message = this.log[this.log.length - 1] || { message: '' }
    return message.message
}

messageChecker.getLength = function() {
    console.log(this.log.length)
    return this.log.length
}

messageChecker.on = function() {}

describe('Load page', function() {
    let defaultTimeout = Cypress.config('defaultCommandTimeout')
    Cypress.config('defaultCommandTimeout', 60000)
    before(function() {
        return new Cypress.Promise(resolve => {
            cy.visit('/../../../index.html?cards=05&resetTimer=false', {
                onLoad: window => {
                    window.ipcRenderer = messageChecker
                    window.importedBootstrap = () => {
                        app = window.app
                        app.onLoaded.once(() => {
                            Cypress.config(
                                'defaultCommandTimeout',
                                defaultTimeout
                            )
                            resolve()

                            window.cards[0].scatter.move({ x: 0, y: 0 })
                        })
                    }
                }
            })
        })
    })
    it('App was created', function() {
        cy.expect(app).to.not.be.null
    })
})

describe('ScatterContainer Is Setup Correctly', () => {
    it('Does Property exists?', function() {
        cy.expect(app).to.have.property('scatterContainer')
        scatterContainer = app.scatterContainer
    })

    it('ScatterContainer Has Children', function() {
        card = scatterContainer.scatter.values().next().value
        cy.expect(card).to.not.equal(null)
        card.moveTo({ x: 10, y: 10 })
    })
})

describe('ScatterContainer Is Setup Correctly', () => {
    it('Does Property exists?', function() {
        cy.expect(app).to.have.property('scatterContainer')
        scatterContainer = app.scatterContainer
    })

    it('ScatterContainer Has Children', function() {
        card = scatterContainer.scatter.values().next().value
        cy.expect(card).to.not.equal(null)
        card.moveTo({ x: 10, y: 10 })
    })
})

describe('Language Logging', function() {
    it('Check English Message', function() {
        cy.get('.cardWrapper .language.en').click()
        let last = messageChecker.getLastMessage()
        // cy.expect(last).to.be.equal('CHANGE_LANGUAGE::CARD:05,FROM:de,TO:en')
        console.log(last)
        cy.expect('CHANGE_LANGUAGE::CARD:05,FROM:de,TO:en').to.be.equal(last)
    })
})
