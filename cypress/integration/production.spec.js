import { DOMCardScatterContainer } from '../../js/card'

let app

describe('Load page', function() {
    let defaultTimeout = Cypress.config('defaultCommandTimeout')
    Cypress.config('defaultCommandTimeout', 60000)
    before(function() {
        return new Cypress.Promise(resolve => {
            cy.visit('/../../../index.html', {
                onLoad: window => {
                    window.importedBootstrap = () => {
                        app = window.app
                        console.log('Bootstrapped')
                        app.onLoaded.once(() => {
                            console.log('APP WAS LOOOOOOODED')
                            Cypress.config(
                                'defaultCommandTimeout',
                                defaultTimeout
                            )
                            resolve()
                        })
                    }
                }
            })
        })
    })

    it('App was created', () => {
        cy.expect(app).to.not.be.null
    })
})

describe('Create Memotry Leak', () => {
    it('Memory leak ', () => {
        app.createMemoryLeak()
    })
})

describe('Vorbedingungen', function() {
    it('Does scatter container exists?', () => {
        cy.expect(app).to.have.property('scatterContainer')
    })
})

describe('Memory Test', function() {
    const cycles = 5
    let i = cycles
    do {
        it(`Karteninteraktion - Zyklus ${cycles - i + 1}`, () => {
            cy.get('.cardWrapper .footerMenuitem:nth-child(1)').each(
                element => {
                    console.log(element)
                    element.click()
                }
            )
            cy.wait(700)

            cy.get('.cardWrapper .footerMenuitem:nth-child(2)').each(
                element => {
                    console.log(element)
                    element.click()
                }
            )
            cy.wait(700)

            cy.get('.cardWrapper .footerMenuitem:nth-child(3)').each(
                element => {
                    console.log(element)
                    element.click()
                }
            )
            cy.wait(700)

            cy.get('.cardWrapper .footerMenuitem:nth-child(3)').each(
                element => {
                    console.log(element)
                    element.click()
                }
            )

            cy.wait(2000)
        })
    } while (--i > 0)
})

// describe('Card Implementation', function () {

//     for (let i = 1; i <= 17; i++) {
//         let key = i.toString().padStart(2, '0')

//         it(`Does card ${key} exist?`, () => {
//             cy.get(`.cardWrapper[data-key=${key}]`).should('to.exist')
//         })
//     }

//     it('Test cards ', () => {
//         app.scatterContainer.scatter.forEach(scatter => {
//             cy.expect(true).equal(true)
//         })
//     })
// })
