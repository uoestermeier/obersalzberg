/**
 * Test the language features of the card. To prevent
 * the feature from failing unexpectedly and assuring
 * flawless operation!
 */

let app

describe('Load page', function() {
    let defaultTimeout = Cypress.config('defaultCommandTimeout')
    Cypress.config('defaultCommandTimeout', 60000)
    before(function() {
        return new Cypress.Promise(resolve => {
            cy.visit('/../../../index.html?cards=01', {
                onLoad: window => {
                    window.importedBootstrap = () => {
                        app = window.app
                        app.onLoaded.once(() => {
                            Cypress.config(
                                'defaultCommandTimeout',
                                defaultTimeout
                            )
                            resolve()
                        })
                    }
                }
            })
        })
    })

    it('App was created', function() {
        cy.expect(app).to.not.be.null
    })
})

describe('Test Buttons', () => {
    it('Languagebar exists', function() {
        cy.get('.languages').should('be.visible')
    })

    it('German button should be visible', function() {
        cy.get('.language.de').should('be.visible')
    })

    it('English button should be visible', function() {
        cy.get('.language.en').should('be.visible')
    })

    it('English button should be active after click', function() {
        cy.get('.language.en').click()
        cy.get('.language.en').should('have.class', 'active')
    })

    it('German button should be inactive', function() {
        cy.get('.language.de').should('not.have.class', 'active')
    })

    it('German button should be active after click', function() {
        cy.get('.language.de').click()
        cy.get('.language.de').should('have.class', 'active')
    })

    it('English button should be inactive', function() {
        cy.get('.language.en').should('not.have.class', 'active')
    })
})

function testLanguageAttributeOnElement(name, selector) {
    describe(name, function() {
        it('Should have data-localizable', function() {
            cy.get(selector).should('have.attr', 'data-localizable')
        })

        it('Should have data-lang-en', function() {
            cy.get(selector).should('have.attr', 'data-lang-en')
        })

        it('Should have data-lang-de', function() {
            cy.get(selector).should('have.attr', 'data-lang-de')
        })

        it('After clicking german, text should be german', function() {
            cy.get('.language.de').click()
            cy.get(selector).then($el => {
                const txt = $el.text()

                cy.get(selector).should('have.attr', 'data-lang-de', txt)
            })
        })

        it('After clicking english, text should be english', function() {
            cy.get('.language.en').click()
            cy.get(selector).then($el => {
                const txt = $el.text()

                cy.get(selector).should('have.attr', 'data-lang-en', txt)
            })
        })
    })
}

testLanguageAttributeOnElement(
    'Title bar should have language attribute',
    '.title'
)

testLanguageAttributeOnElement(
    'Background button should have language attribute',
    '.button.background'
)
testLanguageAttributeOnElement(
    'Details button should have language attribute',
    '.button.details'
)
testLanguageAttributeOnElement(
    'Slider button should have language attribute',
    '.button.slider'
)


describe('Test Details', () => {
    it('Open Details', function() {
        cy.get('.button.background').click()
        cy.get('.cardWrapper').should('have.class', 'expanded')
    })
})
