describe('Load page', function() {
    let defaultTimeout = Cypress.config('defaultCommandTimeout')

    before(function() {
        return new Cypress.Promise(resolve => {
            cy.visit('/../../../index.html', {
                onLoad: window => {
                    window.importedBootstrap = () => {
                        app = window.app
                        console.log('Bootstrapped')
                        app.onLoaded.once(() => {
                            console.log('APP WAS LOOOOOOODED')
                            Cypress.config(
                                'defaultCommandTimeout',
                                defaultTimeout
                            )
                            resolve()
                        })
                    }
                }
            })
        })
    })

    it('App was created', () => {
        cy.expect(app).to.not.be.null
    })
})
