/* eslint-env cy, Cypress */
let app
let scatterContainer
let card

function getStateString(state) {
    return `Testing state ${state}:`
}

function testToggleModeByButtonPress(state) {
    it(`${getStateString(state)} Clicking a button toggles it's mode.`, () => {
        cy.get(`.frontFooter .button.${state}`).click()
        cy.get('.cardWrapper').should('have.attr', 'data-mode', state)
    })
}

function expectModeNone(state) {
    it(`${getStateString(state)} Expect to be in state 'none'.`, () => {
        cy.get('.cardWrapper').should('have.attr', 'data-mode', 'none')
    })
}

function clickCloseButton(state = null) {
    it(`${
        state != null ? getStateString(state) : ' '
    } Click close button to reset.`, () => {
        cy.wait(500)
        cy.get(`.cardWrapper .close-button`).click()
        cy.get('.cardWrapper').should('have.attr', 'data-mode', 'none')
    })
}

describe('Load page', function() {
    let defaultTimeout = Cypress.config('defaultCommandTimeout')
    Cypress.config('defaultCommandTimeout', 60000)
    before(function() {
        return new Cypress.Promise(resolve => {
            cy.visit('/../../../index.html?cards=01&resetTimer=false', {
                onLoad: window => {
                    window.importedBootstrap = () => {
                        app = window.app
                        app.onLoaded.once(() => {
                            Cypress.config(
                                'defaultCommandTimeout',
                                defaultTimeout
                            )
                            resolve()
                        })
                    }
                }
            })
        })
    })

    it('App was created', function() {
        cy.expect(app).to.not.be.null
    })
})

describe('ScatterContainer Is Setup Correctly', () => {
    it('Does Property exists?', function() {
        cy.expect(app).to.have.property('scatterContainer')
        scatterContainer = app.scatterContainer
    })

    it('ScatterContainer Has Children', function() {
        card = scatterContainer.scatter.values().next().value
        cy.expect(card).to.not.equal(null)
        card.moveTo({ x: 10, y: 10 })
    })
})

describe('Card Setup', () => {
    it(`Expect card to be initialized with mode 'none'.`, function() {
        cy.get('.cardWrapper').should('have.attr', 'data-mode', 'none')
    })
})

describe('Menu - States', () => {
    const states = ['slider', 'details', 'background']

    states.forEach(state => {
        it(`Does State Button ${state} exist?`, function() {
            let stateButton = cy.get(`.frontFooter .button.${state}`)
            cy.expect(stateButton).to.not.equal(null)
        })
    })

    states.forEach(state => {
        const stateTestString = `Testing state ${state}:`
        it(`${stateTestString} Expect to be in state 'none'.`, function() {
            cy.get('.cardWrapper').should('have.attr', 'data-mode', 'none')
        })

        testToggleModeByButtonPress(state)

        it(`${stateTestString} Clicking the button twice reset's the card to mode 'none'.`, function() {
            cy.get(`.frontFooter .button.${state}`).click()
            cy.get('.cardWrapper').should('have.attr', 'data-mode', 'none')
        })

        states.forEach(other => {
            if (state !== other) {
                it(`${stateTestString} Clicking anotherbutton to toggle mode '${other}'.`, function() {
                    cy.get(`.frontFooter .button.${other}`).click()
                    cy.get('.cardWrapper').should(
                        'have.attr',
                        'data-mode',
                        other
                    )
                })
            }
        })

        clickCloseButton()
    })
})

/**
 * Test if the buttons are pressed correctly.
 */
describe('Menu - Button Press', () => {
    const clickPositions = {
        slider: [
            [60, 482], //Center
            [15, 472], //TL
            [113, 474], //TR
            [16, 488], // BL
            [112, 487] // BR
        ],
        details: [
            [179, 484], //Center
            [124, 475], //TL
            [218, 474], //TR
            [130, 486], //BL
            [226, 488] //BR
        ],
        background: [
            [284, 482], //Center
            [246, 474], //TL
            [336, 473], //TR
            [236, 488], //BL
            [334, 487] //BR
        ]
    }

    for (let [name, positions] of Object.entries(clickPositions)) {
        positions.forEach(clickPosition => {
            let [x, y] = clickPosition

            it(`${name}@[${x},${y}]: state should be none `, function() {
                cy.get('.cardWrapper').should('have.attr', 'data-mode', 'none')
            })

            it(`Click @[${x},${y}]" should trigger ${name}`, function() {
                cy.get('body').click(x, y)
                cy.wait(500)
                cy.get('.cardWrapper').should('have.attr', 'data-mode', name)
                cy.get('body').click(x, y)
            })
        })
    }
})

describe('Testing Background', () => {
    const state = 'background'
    expectModeNone(state)
    testToggleModeByButtonPress(state)

    it('Background article is visible.', function() {
        cy.get(`.cardWrapper section.${state} `).should('exist')
        cy.get(`.cardWrapper section.${state} `).should('be.visible')
        cy.get(`section:not(.${state})`).should('be.hidden')
    })

    //TODO: Properly implement collapsibles test.
    // it('Test all collapsibles', () => {
    //     let col = cy.get(`section.${state} .collapsible:first-of-type`)
    //     let collapsibleContent = cy.get(
    //         `section.${state} .collapsible:first-of-type + .collapsibleContent`
    //     )

    //     cy.expect(collapsibleContent).to.not.equal(null)
    //     collapsibleContent.should('have.css', 'maxHeight', '0px')

    //     // This should work.
    //     col.click({ force: true })
    //     cy.wait(5000)

    //     collapsibleContent.should('have.css', 'maxHeight', '0px')
    // })

    clickCloseButton()
})

//TODO: Implement Details completely.
describe('Details - Magnifiers', () => {
    const state = 'details'
    expectModeNone(state)
    testToggleModeByButtonPress(state)

    const collapsibleSelector = '.collapsible.mann'
    const collapsibleContentSelector = '.collapsible.mann + .collapsibleContent'
    const magnifierSelector = 'svg g > circle.mann'

    it('Collapsible is closed.', function() {
        cy.get(collapsibleContentSelector).should(
            'has.css',
            'max-height',
            '0px'
        )
    })

    it('Collapsible is not active.', function() {
        cy.get(collapsibleSelector).should('not.have.class', 'active')
    })

    it('Expand a magnifier', function() {
        cy.get(magnifierSelector).click()
        cy.wait(500)
        cy.get(magnifierSelector).should('have.class', 'expanded')
    })

    it('Collapsible is set active.', function() {
        cy.get(collapsibleSelector).should('have.class', 'active')
    })

    it('Collapsible was expanded, too', function() {
        cy.get(collapsibleSelector).should('have.class', 'active')
    })

    it('Collapsible was expanded, too', function() {
        cy.get(collapsibleContentSelector).should(
            'not.have.class',
            'height',
            '0px'
        )
    })

    clickCloseButton()

    it('Collapsible should be closed', function() {
        cy.wait(500)
        cy.get(collapsibleContentSelector).should('has.css', 'height', '0px')
    })

    it('Magnifier should NOT be expanded', function() {
        cy.get(magnifierSelector).should('not.have.class', 'expanded')
    })
})

describe('Slider - Images', () => {
    const state = 'slider'
    expectModeNone(state)
    testToggleModeByButtonPress(state)

    const images = 5
    const stepsPerImage = 100
    const total = (images - 1) * stepsPerImage

    let slides = ['.slide1', '.slide2', '.slide3', '.slide4', '.slide5']
    let states = {}
    slides.forEach((slide, i) => {
        let key = (total * i) / (images - 1)
        states[key] = slide
    })

    for (let value of Object.keys(states)) {
        let percent = Math.floor((value / total) * 100) + '%'

        it(`Test slider change: ${value}.`, function() {
            cy.get('.slider input[type=range]')
                .as('range')
                .invoke('val', value)
                .trigger('input')
                .trigger('change')
        })

        for (let [otherValue, otherClassName] of Object.entries(states)) {
            if (value >= otherValue) {
                it(`@${percent}(${value}) image '${otherClassName}' should be visible.`, () => {
                    cy.get(otherClassName).should('have.css', 'opacity', '1')
                })
            } else {
                it(`@${percent}(${value}) image '${otherClassName}' should NOT be visible.`, () => {
                    cy.get(otherClassName).should('have.css', 'opacity', '0')
                })
            }
        }

        //TODO: Texting the visuals of the slider is currently not working, as
        //      cypress does not compare css percentages.

        // it(`@${percent}(${value}) Test slider visuals.`, () => {
        //     cy.get('.sliderTrackActive').should('have.style', 'width', percent)
        // cy.get('.sliderTrackActive').should(
        //     'have.css',
        //     'width',
        //     percent + '%'
        // )
        // })
    }
})

describe('Slider - Snapping', () => {
    const smallValue = 5

    it('Low value should snap to 0', function() {
        cy.get('.slider input[type=range]')
            .as('range')
            .invoke('val', smallValue)
            .trigger('input')
            .trigger('change')

        cy.wait(200)
        cy.get('.slider input[type=range]').should('have.value', '0')
    })

    it('High value should snap to max', function() {
        cy.get('.slider input[type=range]')
            .as('range')
            .invoke('val', 395)
            .trigger('input')
            .trigger('change')

        cy.wait(200)
        cy.get('.slider input[type=range]').should('have.value', '400')
    })
})
