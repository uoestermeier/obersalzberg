/**
 * Loads the app
 */
let app
describe('Load page', function() {
    let defaultTimeout = Cypress.config('defaultCommandTimeout')
    Cypress.config('defaultCommandTimeout', 60000)
    before(function() {
        return new Cypress.Promise(resolve => {
            cy.visit('/../../../index.html?cards=01', {
                onLoad: window => {
                    window.importedBootstrap = () => {
                        app = window.app
                        app.onLoaded.once(() => {
                            Cypress.config(
                                'defaultCommandTimeout',
                                defaultTimeout
                            )
                            resolve()
                        })
                    }
                }
            })
        })
    })

    it('App was created', function() {
        cy.expect(app).to.not.be.null
    })
})

describe('Lowly Images', function() {
    before(function() {
        cy.get('img').as('img')
    })
    it('Test if all img tags have a lowly image attatched:', function() {
        this.img.each((idx, element) => {
            cy.expect(element).to.have.attr('src')
            cy.expect(element).to.have.attr('data-image')
            cy.expect(element).to.have.attr('data-preview-image')
        })
    })
})
