/**
 * Creates all language files and removes the original files.
 */

const GREEN = '\x1b[32m'
const BLUE = '\x1b[34m'
const RED = '\x1b[31m'

const path = require('path')
const fs = require('fs')

const extension = 'html'
const excluded = ['index']
const localizations = ['de', 'en']

/* eslint-disable no-console */
const log = {}
log.error = function(...msg) {
    console.log(RED, ...msg)
}

log.success = function(...msg) {
    console.log(GREEN, ...msg)
}

log.info = function(...msg) {
    console.log(BLUE, ...msg)
}

log.reset = function(...msg) {
    console.log('\x1b[0m', ...msg)
}
/* eslint-enable no-console */

const config = {
    removeBaseFiles: true,
    forceRemove: false
}

const rootUrl = path.join(__dirname, '..', 'var', 'cards')

let files = fs
    .readdirSync(rootUrl, { withFileTypes: true })
    .filter(dirent => dirent.isDirectory())
    .map(dirent => dirent.name)

files.forEach(dir => {
    let { baseFileNames, baseFiles } = findBaseFilenames(dir)

    let existingLocaleFiles = 0
    let copiedLocaleFiles = []
    baseFileNames.forEach((fileName, index) => {
        localizations.forEach(locale => {
            if (localFileIsMissing(dir, fileName, locale)) {
                let output = createLocaleFile(
                    dir,
                    baseFiles[index],
                    fileName,
                    locale
                )
                copiedLocaleFiles.push({ file: output, locale })
                existingLocaleFiles++
                log.success(
                    `[${dir}][${locale.toUpperCase()}] Created locale  of '${fileName}'.`
                )
            } else {
                existingLocaleFiles++
                log.info(
                    `[${dir}][${locale.toUpperCase()}] Locale file already existed: ${getLocalFileName(
                        fileName,
                        locale
                    )} .`
                )
            }
        })

        if (config.removeBaseFiles) {
            if (existingLocaleFiles > 0) {
                removeBaseFile(dir, fileName)
                log.success(
                    `Removed file, as copies were successfully created for locales: ${copiedLocaleFiles.join(
                        ', '
                    )}.`
                )
            } else if (config.forceRemove) {
                removeBaseFile(rootUrl, baseFiles[index])
                log.success(
                    `Removed file by force: ${path.join(
                        rootUrl,
                        baseFiles[index]
                    )}.`
                )
            } else {
                log.error(
                    `File was not removed to avoid data loss: ${fileName}.`
                )
            }
        }
    })
})

function getAbsoluteDirectoryUri(dir) {
    return path.join(rootUrl, dir)
}

/**
 * Checks if the local file already exists.
 *
 * @param {string} file - Path to the basefile.
 * @param {string} local - Locale identifier.
 */
function localFileIsMissing(dir, file, local) {
    const absoluteDirectoryUrl = getAbsoluteDirectoryUri(dir)
    const localFilename = getLocalFileName(file, local)
    let url = path.join(absoluteDirectoryUrl, localFilename)
    console.error(url)
    return !fs.existsSync(url)
}

/**
 * Creates the local file.
 *
 * @param {string} file - Path to the basefile.
 * @param {string} local - Locale identifier.
 */
function createLocaleFile(directory, original, fileName, local) {
    const absoluteDirectoryUrl = getAbsoluteDirectoryUri(directory)
    const localFilename = getLocalFileName(fileName, local)
    const localUrl = path.join(absoluteDirectoryUrl, localFilename)
    const originalUrl = path.join(absoluteDirectoryUrl, original)
    fs.copyFileSync(originalUrl, localUrl)
    return localUrl
}

/**
 * Finds the 'original' files to create the language files from.
 */
function findBaseFilenames(directory) {
    const absoluteDirectoryUrl = getAbsoluteDirectoryUri(directory)

    const baseFiles = []
    const baseFileNames = []
    let files = fs.readdirSync(absoluteDirectoryUrl).filter(file => {
        return path.extname(file) == `.${extension}`
    })

    files.forEach(file => {
        //Remove extension.
        let fileName = file.substring(0, file.lastIndexOf('.'))

        // Remove localization
        let delimiterPosition = fileName.lastIndexOf('_')
        if (delimiterPosition !== -1)
            fileName = fileName.substring(0, delimiterPosition)

        if (
            baseFileNames.indexOf(fileName) == -1 &&
            excluded.indexOf(fileName) == -1
        ) {
            baseFileNames.push(fileName)
            baseFiles.push(file)
        }
    })

    return { baseFileNames, baseFiles }
}

/**
 * Removes the base file, if it exists.
 * The basefile is a file with no localization attatched.
 */
function removeBaseFile(dir, base) {
    const filepath = path.join(dir, base)
    if (fs.existsSync(filepath)) {
        fs.unlinkSync(filepath)
        log.success(`File was removed: ${filepath}.`)
    }
}

function getLocalFileName(basename, locale) {
    return `${basename}_${locale}.${extension}`
}

log.reset('Language script completed!')
