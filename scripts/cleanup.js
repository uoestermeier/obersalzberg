/* eslint no-console: off */
const path = require('path')
const fs = require('fs')

const cardFolder = path.join(__dirname, '../var/cards')

const walkSync = (dir, callback) => {
    const files = fs.readdirSync(dir)
    files.forEach(file => {
        var filepath = path.join(dir, file)
        const stats = fs.statSync(filepath)
        if (stats.isDirectory()) {
            walkSync(filepath, callback)
        } else if (stats.isFile()) {
            callback(filepath, stats)
        }
    })
}

walkSync(cardFolder, p => {
    if (path.basename(p).startsWith('._')) {
        console.log('Removing hidden file', p)
        fs.unlinkSync(p)
    }
})
