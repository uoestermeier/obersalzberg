/**
 * Updates the cards.json to all existing cards inside the var/cards folder.
 */

const path = require('path')
let fs = require('fs')

const cardsFolder = '../var/cards'
const cardsConfig = '../cards.json'
let output = path.join(__dirname, cardsConfig)
let cards = []

try {
    let directoryContents = fs.readdirSync(path.join(__dirname, cardsFolder))
    directoryContents.forEach(fileName => {
        let result = parseInt(fileName)
        let file = path.join(__dirname, cardsFolder, fileName)
        if (!isNaN(result)) {
            cards.push(fileName)
            console.log(`Card found: ${file}.`)
        } else console.error(`Ignored folder: ${file}`)
    })
    fs.writeFileSync(output, JSON.stringify(cards))
} catch (e) {
    console.error(e)
}
