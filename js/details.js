import ObersalzbergHighlight from './highlight.js'
import Events from '../iwmlib/lib/events.js'
import { Drawer } from './drawer.js'
import { DetailsDrawerSynch } from './details-drawer-sync.js'
import { ObersalzbergCard } from './card.js'
import { ImageReplacer } from './imagereplacer.js'
import Highlight from './highlight.js'
import { InteractionMapper } from '../iwmlib/lib/interaction.js'

/* eslint-disable no-console */
export class Details extends Object {
    /**
     * Creates the details view on initialization.
     * For the new str
     *
     * @param {DomElement} newElement - Newly created element's cardWrapper.
     * @param {DomElement} loadedElement - The loaded element's cardWrapper.
     */
    static create(
        newElement,
        loadedElement,
        { width = 600, height = 800 } = {}
    ) {
        let overlayHighlight = newElement.querySelector('.overlayHighlight')
        let legacyHighlights = loadedElement.querySelector('.overlayHighlight')
        overlayHighlight.innerHTML = legacyHighlights.innerHTML

        this.initializeTheCircleImage(newElement, width, height)
    }

    /**
     * We need to create the circles when the card is created to avoid
     * weird long of the image when the image is created on click.
     *
     */
    static initializeTheCircleImage(wrapper, width, height) {
        let svgRoot = this.getSvgRoot(wrapper)
        svgRoot.setAttribute('viewBox', `0 0 ${width} ${height}`)

        let image = this.getOriginalImage(wrapper)

        Details.getAllHighlights(wrapper).forEach(highlight => {
            Highlight._createSVGMask(highlight, { image, svgRoot })

            highlight.removeAttribute('onclick')

            if (window.config.get('highlightStroke') != null) {
                highlight.setAttribute(
                    'stroke-width',
                    window.config.get('highlightStroke')
                )
            }

            InteractionMapper.on('pointerdown', highlight, () => {
                this.highlightNode(highlight)
            })
        })
    }

    static popupPosition(target, image) {
        let bbox = image.getBBox()
        let width = bbox.width
        let height = bbox.height
        let cx = target.getAttribute('cx')
        let cy = target.getAttribute('cy')
        let r = target.getAttribute('r')
        let radius = r.endsWith('%')
            ? (parseFloat(r) / 100) * Math.max(width, height)
            : parseFloat(r)

        let x = cx.endsWith('%') ? (parseFloat(cx) / 100) * width : cx
        let y = cy.endsWith('%') ? (parseFloat(cy) / 100) * height : cx
        y -= radius * 1.75
        return { x, y }
    }

    static get detailsSelector() {
        return 'svg.overlayHighlight'
    }

    static getSvgRoot(cardWrapper) {
        return cardWrapper.querySelector('svg')
    }

    static getOriginalImage(cardWrapper) {
        return cardWrapper.querySelector(`${this.detailsSelector} > image`)
    }

    static getAllHighlights(cardWrapper) {
        return cardWrapper.querySelectorAll(`${this.detailsSelector} circle`)
    }

    static getNonExpandedHighlightsOfClass(cardWrapper, className) {
        return cardWrapper.querySelectorAll(
            `${this.detailsSelector} g circle.${className}:not(.expanded)`
        )
    }

    static getExpandedHighlightsOfClass(cardWrapper, className) {
        return cardWrapper.querySelectorAll(
            `${this.detailsSelector} g circle.${className}.expanded`
        )
    }

    static closeAll(cardWrapper) {
        for (let svgRoot of cardWrapper.querySelectorAll('svg')) {
            for (let expanded of svgRoot.querySelectorAll('.expanded')) {
                ObersalzbergHighlight.closeHighlight(expanded)
            }
        }
    }

    static closeOthers(wrapper, node) {
        for (let _svgRoot of wrapper.querySelectorAll('svg')) {
            for (let expanded of _svgRoot.querySelectorAll('.expanded')) {
                if (expanded != node) {
                    ObersalzbergHighlight.closeHighlight(expanded)
                }
            }
        }
    }

    static openAllOfClass(wrapper, className, excludedHighlight = null) {
        let highlights = this.getNonExpandedHighlightsOfClass(
            wrapper,
            className
        )
        highlights.forEach(highlight => {
            if (highlight != excludedHighlight) {
                if (window.config.get('highlightScale')) {
                    ObersalzbergHighlight.openHighlight(highlight, {
                        scale: window.config.get('highlightScale')
                    })
                } else {
                    ObersalzbergHighlight.openHighlight(highlight)
                }
            }
        })
    }

    static closeAllOfClass(wrapper, className, excludedHighlight = null) {
        let highlights = this.getExpandedHighlightsOfClass(wrapper, className)
        highlights.forEach(highlight => {
            if (highlight != excludedHighlight)
                ObersalzbergHighlight.closeHighlight(highlight)
        })
    }

    static show(cardWrapper) {
        let overlayHighlight = cardWrapper.querySelector('.overlayHighlight')
        ObersalzbergCard.showNode(overlayHighlight)

        this.getOverlayItems(overlayHighlight).forEach(item => {
            ImageReplacer.replaceWithHighResImage(item, 'href')
        })

        let wrapper = ObersalzbergCard.get(cardWrapper)
        Drawer.ensureContent(cardWrapper, wrapper.data.germanDetails)

        let images = cardWrapper.querySelectorAll('section.details img')
        images.forEach(img => {
            ImageReplacer.replaceWithHighResImage(img).then(() => {
                img.style.opacity = 1
            })
        })
    }

    static exit(cardWrapper) {
        this.hideOverlay(cardWrapper)
        DetailsDrawerSynch.closeAll(cardWrapper)

        let images = cardWrapper.querySelectorAll('section.details img')
        images.forEach(img => {
            img.style.opacity = 0
            ImageReplacer.replaceWithLowResImage(img)
        })
    }

    static hideOverlay(cardWrapper) {
        let overlayHighlight = cardWrapper.querySelector('.overlayHighlight')
        ObersalzbergCard.hideNode(overlayHighlight)

        this.getOverlayItems(overlayHighlight).forEach(item => {
            TweenLite.to(item, 0.5, {
                opacity: 0,
                onComplete: () =>
                    ImageReplacer.replaceWithLowResImage(item, 'href')
            })
        })
    }

    static getOverlayItems(overlayHighlight) {
        let querySelector = ['g image']
        let results = []

        querySelector.forEach(selector => {
            let matches = overlayHighlight.querySelectorAll(selector)
            matches.forEach(match => {
                results.push(match)
            })
        })

        return results
    }

    /**
     * Highlight function that is called from HTML.
     *
     * @param {*} event
     * @param {*} collapsibleSelector
     * @param {*} selector
     */
    // eslint-disable-next-line no-unused-vars
    static highlight(event, collapsibleSelector = null, selector = null) {
        let node = event.target
        this.highlightNode(node)
    }

    static highlightNode(node) {
        let klass = node.classList[0]
        let container = node.closest('.cardWrapper')

        //Closes all but the clicked highlight
        this.closeOthers(container, node)

        if (ObersalzbergHighlight._isExpanded(node)) {
            DetailsDrawerSynch.detailsClosed(node, klass)
        } else {
            DetailsDrawerSynch.detailsOpened(node, klass)
        }

        /* 
        We toggle the Highlight afterwards.
        Then the state 'IsExpanded' is logically
        correct when clicked!
        */
        if (window.config.get('highlightScale')) {
            ObersalzbergHighlight.toggleHighlight(node, {
                scale: window.config.get('highlightScale')
            })
        } else {
            ObersalzbergHighlight.toggleHighlight(node)
        }
    }

    /**
     * Returns associated collapsible of a ObersalzbergHighlight.
     * By accessing the 'data-collapsible' attribute.
     *
     * @param {Element} highlight - The highlight element e.g. svg-circle.
     * @returns If set a string of a class, otherwise null.
     */
    static getCollapsibleClassFromHighlight(highlight) {
        return highlight.getAttribute('data-collapsible')
    }

    static openLink(event) {
        let url = event.target.getAttribute('href')
        Events.stop(event)
        let mainCard = event.target.closest('.cardWrapper')
        Drawer.ensureContent(mainCard, url)
    }
}
