/* global main */
/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { Drawer } from './drawer.js'
import { Details } from './details.js'
import { registerStressTest, startHeartBeat } from './stresstest.js'
import {
    FrontLoader,
    DOMCardScatterContainer,
    ObersalzbergCard
} from './card.js'
import { Menu } from './menu.js'
import { ObersalzbergConfig } from './config.js'
import { EventHandler } from '../iwmlib/lib/pixi/maps/utils.js'
import Localization from './localization.js'
import ErrorWindow from './errorwindow.js'
import ResetTimer from './reset-timer.js'
import Time from './time.js'
import PopupContainer from './popup.js'
import DebugCanvas from './debug/canvas.js'
import { DOMScatter } from '../iwmlib/lib/scatter.js'
import Impressum from './impressum.js'
import { setZoomedLimit } from './zoomlimit.js'

window.ObersalzbergCard = ObersalzbergCard
window.Drawer = Drawer
window.Details = Details
window.Menu = Menu
window.PopupContainer = PopupContainer

// if (window.ipcRenderer) {
//     ;[('log', 'warn', 'error')].forEach(messageType => {
//         console.log(window.Logging)

//          setTimeout(, 10)

//         window.Logging[messageType] = message => {
//             window.ipcRenderer.send(messageType, message)
//             console.log('SENT SHIT MATE!')
//         }
//     })
// } else {
//     console.error('Electron logging is really not possible.')
// }

window.traceElement = function(query) {
    let el = document.querySelector(query)
    if (el) {
        let rect = el.getBoundingClientRect()
        let d = document.createElement('div')
        Object.assign(d.style, {
            position: 'absolute',
            width: rect.width + 'px',
            height: rect.height + 'px',
            backgroundColor: 'red',
            left: rect.x + 'px',
            top: rect.y + 'px',
            zIndex: 1000000000000000000
        })

        document.body.appendChild(d)

        setTimeout(() => {
            document.body.removeChild(d)
        }, 3000)
    } else throw new Error('Invalid Query!')
}

DOMScatter.timeoutThrow()
let overlayAnimation = null
let data = []

function pad(num, size) {
    var s = num + ''
    while (s.length < size) s = '0' + s
    return s
}

class ObersalzbergApp {
    constructor() {
        this.cards = []
        this.progress = 0
        this.onLoaded = new EventHandler('loaded')
        this.onCardLoaded = new EventHandler('onCardLoaded')
    }

    async setup() {
        await this.loadConfig()
        if (this.config.get('debug')) this.setupDebugging()
        this.loadCardData()

        new Impressum({ bottom: '10px', right: '10px' })

        if (this.config.get('display') == 'table') {
            new Impressum({
                top: '10px',
                left: '10px',
                transform: 'rotate(180deg)'
            })
        }
    }

    async loadConfig() {
        let config = new ObersalzbergConfig('config.json')
        await config.load().catch(console.error)
        this.config = window.config = config

        setZoomedLimit(this.config.get('maxZoomedScatters'))
    }

    setupDebugging() {
        let errorWindow = (window.errorWindow = new ErrorWindow())
        window.error = errorWindow.error.bind(errorWindow)

        window.errorWindow.create()
    }

    loadCardData() {
        let dir = './var/cards/'
        //Defined inside the config.js
        const cards = window.config.get('cards')
        console.log(`Load cards: ${cards.join(', ')}`)
        for (let i = 0; i < cards.length; i++) {
            this.createCardData(dir, cards[i])
        }
    }

    loader(url, prefixPath) {
        return new FrontLoader(url, {}, prefixPath)
    }

    createMemoryLeak() {
        setInterval(() => {
            this.evenHandler = []
            this.evenHandler.push(
                new EventHandler('Blah', () => {
                    console.log('Nothing in peticular!')
                })
            )
        }, 100)
    }

    showProgress() {
        if (typeof loadingBar != 'undefined') {
            let percent = parseInt(this.progress)
            loadingBar.style.width = percent + '%'
            // console.log('showProgress', percent)
            if (percent >= 98) {
                loadingBar.style.width = '100%'
                if (overlayAnimation == null) {
                    overlayAnimation = true
                    TweenMax.to(startOverlay, 1.5, {
                        delay: 3,
                        backgroundColor: 'rgba(0, 0, 0, 0)',
                        onComplete: () => {
                            startOverlay.remove()
                            this.setupResetTimer()
                            startHeartBeat()
                        }
                    })
                }
            }
        }
    }

    prepareProgress() {
        let allImages = main.getElementsByTagName('img')
        let allSvgImages = main.getElementsByTagNameNS(
            'http://www.w3.org/2000/svg',
            'image'
        )
        let remaining = 100 - this.progress
        let totalImages = allImages.length + allSvgImages.length
        let step = remaining / totalImages
        console.log('Detected images', totalImages)
        for (let img of allImages) {
            img.onload = () => {
                this.progress += step
                this.showProgress()
                img.onload = null
            }
        }
        for (let img of allSvgImages) {
            img.onload = () => {
                this.progress += step / 2
                this.showProgress()
            }
        }
    }

    run() {
        return new Promise((resolve, reject) => {
            let scatterContainer = new DOMCardScatterContainer(main, {
                // stopEvents: false,
                debugCanvas: null, // new DebugCanvas(),
                useCapture: false
            })
            this.scatterContainer = scatterContainer
            let x = 10
            let y = 10
            let rotation = 0
            let cardPromises = []
            data.forEach(d => {
                let promise = this.createCard(d, scatterContainer).then(
                    element => {
                        if (window.config.get('sortingType') == 'random') {
                            let pos = this.sortRandom(
                                element.scatter.scale,
                                element.scatter.width,
                                element.scatter.height
                            )

                            x = pos.x
                            y = pos.y

                            if (window.config.get('display') == 'table')
                                rotation = pos.rotation
                        } else if (window.config.get('sortingType') == 'line') {
                            x += 100
                            y += 100
                        }

                        const scatter = element.scatter
                        scatter.x = x
                        scatter.y = y
                        scatter.rotation = rotation
                        this.cards.push(element)
                        this.progress += 1
                        this.showProgress()
                    }
                )

                cardPromises.push(promise)
            })

            Promise.all(cardPromises)
                .then(() => {
                    this.onLoaded.call(this)
                    resolve()
                    console.log('All cards loaded')

                    this.prepareProgress()
                })
                .catch(console.error)
            setTimeout(() => {
                reject('Loading cards timed out!')
            }, 30000)

            if (window.ipcRenderer) {
                window.ipcRenderer.on('load-layout', txt => {
                    console.log('LOADED LAYOUT', txt)
                })
            }
            registerStressTest()
        })
    }

    setupResetTimer() {
        if (this.config.get('resetTimer'))
            window.resetTimer = new ResetTimer({
                time: Time.minutes(this.config.get('resetTime')),
                resetFunction: () => {
                    this.resetCards()
                }
            })
    }

    resetCards() {
        let activeImpressums = [...document.getElementsByClassName('impActive')]
        activeImpressums.forEach(activeImp => {
            Impressum.deactivate(activeImp)
        })

        let scale = window.config.get('startScale') || 0.5
        for (let cardScatter of this.scatterContainer.scatter.values()) {
            ObersalzbergCard.changeMode(cardScatter.element, 'none')
            let { x, y, rotation } = this.sortRandom(
                cardScatter.scale,
                cardScatter.width,
                cardScatter.height
            )
            TweenLite.to(cardScatter, window.config.get('sortingDuration'), {
                x,
                y,
                scale,
                directionalRotation: {
                    rotation: rotation + '_short',
                    useRadians: true
                }
            })
        }
    }

    createCardData(dir, idx) {
        let cardIndex = idx
        let key = pad(cardIndex, 2)
        let prefixPath = dir + key + '/'

        let useNonLocalizedCardVersions = window.config.get(
            'useNonLocalizedCardVersions'
        )

        let defaultLanguage = window.config.get('startupLanguage')
        const languages = window.config.get('languages')
        const details = {}
        const background = {}
        const front = prefixPath + 'index.html'
        if (useNonLocalizedCardVersions) {
            details[defaultLanguage] = prefixPath + 'details.html'
            background[defaultLanguage] = prefixPath + 'hintergrund.html'
        } else {
            languages.forEach(lang => {
                details[lang] =
                    prefixPath +
                    Localization.getFileName('details', lang, 'html')
                background[lang] =
                    prefixPath +
                    Localization.getFileName('hintergrund', lang, 'html')
            })
        }

        data.push({
            dir,
            details,
            background,
            front,
            key,
            prefixPath
        })
    }

    async createCard(cardData, scatterContainer) {
        const startScale = window.config.get('startScale') || 0.5

        let cardPromise = ObersalzbergCard.create(
            cardData,
            main,
            scatterContainer,
            {
                startScale
            }
        )

        return cardPromise
    }

    sortRandom(scale, width, height, { border = 40 } = {}) {
        let left = 0 - ((1 - scale) * width) / 2 + border
        let top = 0 - ((1 - scale) * height) / 2 + border

        let right =
            window.innerWidth - width + ((1 - scale) * width) / 2 - border
        let bottom =
            window.innerHeight - height + ((1 - scale) * height) / 2 - border

        let x = left + Math.random() * (right - left)
        let y = top + Math.random() * (bottom - top)

        // Calculate screen center.
        let cx = window.innerWidth / 2
        let cy = window.innerHeight / 2

        let cardCenterX = x + width / 2
        let cardCenterY = y + height / 2

        let dx = cardCenterX - cx
        let dy = cardCenterY - cy

        let rotation =
            window.config.get('display') == 'table'
                ? Math.atan2(dy, dx) - Math.PI / 2
                : 0

        if (
            window.config.get('debug') &&
            window.config.get('debugCardSorting')
        ) {
            // this.debugCanvas.clear()
            // this.debugCanvas.fill = 'red'
            // this.debugCanvas.drawPoint(x, y)

            this.debugCanvas.fill = 'blue'
            this.debugCanvas.drawPoint(cx, cy)

            this.debugCanvas.fill = 'yellow'
            this.debugCanvas.drawPoint(cardCenterX, cardCenterY)

            this.debugCanvas.drawLine(cx, cy, cardCenterX, cardCenterY)
        }

        return { x, y, rotation }
    }
}

const app = new ObersalzbergApp()
app.setup()
    .then(() => {
        app.run()
            .then(() => {
                /**
                 * TODO; Remove when popups are working properly.
                 */
                const debugPopup = false
                if (debugPopup)
                    ObersalzbergCard.changeMode(app.cards[0], 'background')
            })
            .catch(console.error)
    })
    .catch(console.error)

window.app = app
