export default class ErrorWindow {
    constructor() {
        // Queue that get's filled it the window is not created yet.
        this.queue = []

        this.requested = false
    }

    create() {
        this.window = document.createElement('div')
        this.window.id = 'error-window'

        let heading = document.createElement('header')
        this.window.appendChild(heading)

        let icon = document.createElement('i')
        icon.className = 'fas fa-chevron-up'
        heading.appendChild(icon)

        let h3 = document.createElement('h3')
        h3.innerText = 'Error Window'
        heading.appendChild(h3)

        this.counter = document.createElement('span')
        this.counter.className = 'counter'
        heading.appendChild(this.counter)

        let listContainer = document.createElement('div')
        listContainer.className = 'list-wrapper'
        this.window.appendChild(listContainer)

        this.list = document.createElement('div')
        this.list.className = 'list'
        listContainer.appendChild(this.list)

        heading.addEventListener('click', () => {
            this.window.classList.toggle('expanded')
        })

        document.body.appendChild(this.window)
    }

    render() {
        if (this.list) {
            this.list.innerHTML = ''
            this.queue.forEach((element, index) => {
                let messageBox = document.createElement('p')
                messageBox.className = element.type
                if (this.list) {
                    this.list.appendChild(messageBox)
                    messageBox.innerText = element.message
                    messageBox.addEventListener('click', () => {
                        this.queue.splice(index, 1)
                        this.render()
                    })
                    this.updateCount()
                } else console.error('No error window was created.')
                return messageBox
            })
        }
    }

    changed() {
        if (!this.requested) {
            this.requested = true
            setTimeout(() => {
                this.render()
                this.requested = false
            }, 500)
        }
    }

    updateCount() {
        this.counter.innerText = this.queue.length
    }

    add(message, type = 'info', tags = []) {
        let em = new ErrorMessage(message, type, tags)
        this.addErrorMessage(em)
    }

    error(message, tags = []) {
        const em = new ErrorMessage(message, 'error', tags)
        this.addErrorMessage(em)
    }
    addErrorMessage(em) {
        this.queue.push(em)
        this.changed()
    }
}

export class ErrorMessage {
    constructor(message, type, tags = []) {
        this.message = message
        this.type = type
        this.tags = tags
    }

    hasTag(tag) {
        return this.tags.indexOf(tag) != -1
    }
}

