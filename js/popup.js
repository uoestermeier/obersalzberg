import Logging from '../iwmlib/lib/logging.js'
import { Points } from '../iwmlib/lib/utils.js'
import { ExtendedCardWrapper, ObersalzbergCard } from './card.js'
import { Drawer } from './drawer.js'
import ObersalzbergHighlight from './highlight.js'

export default class PopupContainer {
    static handlePopup(target, position) {
        // Is it an Popup? Then open the popup

        let cardWrapper = ExtendedCardWrapper.getCardWrapper(target)

        if (
            target.tagName.toLowerCase() == 'a' ||
            target.tagName.toLowerCase() == 'circle'
        ) {
            if (target.closest('.drawer') != null) {
                let href = target.getAttribute('href')
                if (target.tagName == 'circle') {
                    let a = target.closest('a')
                    if (a) href = a.getAttribute('href')
                }

                const openPopup = cardWrapper.getAttribute('data-popup')
                this.closeAll(cardWrapper, [href])
                if (openPopup != href) {
                    this.openPopup(target, position)

                    if (target.tagName == 'circle') {
                        ObersalzbergHighlight.openHighlight(target, {
                            scale: window.config.get('highlightScale')
                        })
                    }
                } else {
                    this.closeAll(cardWrapper)
                }
            }
        }
    }

    static closeAll(cardWrapper, exclude = []) {
        this.closeAllHighlights(cardWrapper, exclude)
        return this.closeAllPopups(cardWrapper, exclude)
    }

    static closeAllHighlights(cardWrapper, exclude = []) {
        cardWrapper
            .querySelectorAll('article circle.expanded')
            .forEach(highlight => {
                console.error(' TODO: IMPLEMENTED EXCLUDE')
                ObersalzbergHighlight.closeHighlight(highlight)
            })
    }

    static closeAllPopups(wrapper, exclude = []) {
        let closedHrefs = []
        let popups = wrapper.querySelectorAll('.popup')
        popups.forEach(popup => {
            const href = popup.getAttribute('data-href')
            if (exclude.indexOf(href) == -1) {
                this.closePopup(wrapper, popup)
                closedHrefs.push(href)
            }
        })

        return closedHrefs
    }

    static followTarget() {}

    static openPopup(target, position, closedHrefs = []) {
        let href = target.hasAttribute('href')
            ? target.getAttribute('href')
            : target.getAttribute('xlink:href')

        let wrapper = target.closest('.cardWrapper')

        if (wrapper.popupInterval) clearInterval(wrapper.popupInterval)

        // Add a popup if it's not the same as the existing one.
        if (closedHrefs.indexOf(href) == -1) {
            this.load(href)
                .then(htmlText => {
                    if (wrapper.getAttribute('data-popup') !== href) {
                        let domParser = new DOMParser()
                        let html = domParser.parseFromString(
                            htmlText,
                            'text/html'
                        )

                        html.querySelectorAll('img').forEach(img => {
                            let src = img.getAttribute('src')
                            let cardNum = wrapper.getAttribute('data-key')
                            img.setAttribute(
                                'src',
                                `var/cards/${cardNum}/${src}`
                            )
                        })

                        html.querySelectorAll('video').forEach(video => {
                            let src = video.getAttribute('src')
                            let cardNum = wrapper.getAttribute('data-key')
                            video.setAttribute(
                                'src',
                                `var/cards/${cardNum}/${src}`
                            )

                            video.setAttribute('autoplay', true)
                            video.setAttribute('muted', true)
                            video.setAttribute('loop', true)
                        })

                        let popup = document.createElement('div')
                        popup.setAttribute('data-href', href)
                        popup.className = 'popup'

                        let popupContent = document.createElement('div')
                        popup.appendChild(popupContent)
                        popupContent.className = 'popup-content'
                        popupContent.innerHTML = html.body.innerHTML

                        let closeBtn = document.createElement('div')
                        closeBtn.classList.add('button', 'close-btn')
                        closeBtn.addEventListener(
                            'pointerdown',
                            this.closePopup.bind(this, wrapper, popup)
                        )
                        popupContent.appendChild(closeBtn)

                        let closeIcon = document.createElement('div')
                        closeIcon.className = 'close-icon'
                        closeBtn.appendChild(closeIcon)

                        TweenLite.set(popup, { opacity: 0 })
                        TweenLite.to(popup, 0.5, {
                            opacity: 1,
                            delay: 0.1
                        })

                        wrapper.setAttribute('data-popup', href)
                        wrapper.appendChild(popup)

                        const offset = Points.fromPageToNode(target, position)

                        let key = ObersalzbergCard.getKey(wrapper)
                        this.setPopupPosition(wrapper, popup, target, offset)


                        wrapper.popupInterval = setInterval(() => {
                            this.setPopupPosition(
                                wrapper,
                                popup,
                                target,
                                offset
                            )
                        }, 1)

                        Logging.log(`OPEN_POPUP::CARD:${key},POPUP:${href}`)
                    }
                })
                .catch(console.error)
            return true
        } else return false
    }

    static closePopup(cardWrapper, popup) {
        if (cardWrapper.popupInterval) clearInterval(cardWrapper.popupInterval)
        const key = ObersalzbergCard.getKey(cardWrapper)
        const href = popup.getAttribute('data-href')
        Logging.log(`CLOSE_POPUP::CARD:${key},POPUP:${href}`)

        cardWrapper.removeAttribute('data-popup')
        TweenLite.to(popup, 0.5, {
            autoAlpha: 0,
            onComplete: () => {
                if (popup.parentNode) popup.parentNode.removeChild(popup)
            }
        })
    }

    static setPopupPosition(cardWrapper, popup, target, mouseOffset) {
        let scroll = Drawer.getScrollElement(cardWrapper)
        const drawer = cardWrapper.querySelector('.drawer')

        
      

        let offsetLeft, offsetTop
        if (target.offsetLeft) {
            offsetLeft = target.offsetLeft + mouseOffset.x
            offsetTop = target.offsetTop + mouseOffset.y
        } else if (target.getBBox) {
            let overlayWrap = target.closest('.overlayWrap')

            if (!overlayWrap)
                console.error('Every svg must be in an overlayWrap!', target)

            const bbox = target.getBBox()
            offsetLeft =
                overlayWrap.offsetLeft + bbox.x + Math.round(bbox.width / 2) + 5
            offsetTop = overlayWrap.offsetTop + bbox.y
        } else {
            console.error('Could not compute offsetPosition of popup.', target)
        }

        let position = {
            x: drawer.offsetWidth + drawer.offsetLeft + offsetLeft  + 15,
            y: offsetTop - scroll.scrollTop
        }

        const popupAboveArticle = position.y < 0
        const popupBelowArticle = position.y > drawer.offsetHeight

        position.y = popupAboveArticle
            ? 0
            : popupBelowArticle
            ? drawer.offsetHeight
            : position.y


           Object.assign(popup.style, {
            position: 'absolute',
            left: position.x + 'px',
            top: position.y + 'px'
        })

        if (popupAboveArticle || popupBelowArticle) {
            this.closePopup(cardWrapper, popup)
        }
    }

    static async load(href) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest()
            xhr.onreadystatechange = function() {
                if (this.readyState == 4) {
                    if (this.responseText) {
                        resolve(this.responseText)
                    } else {
                        reject(
                            `Response @${href} did not return anything.`,
                            this
                        )
                    }
                }
            }

            xhr.onerror = function() {
                reject('Could not load content: ', this)
            }

            xhr.open('GET', href)
            xhr.send()
        })
    }

    static handlePopupOnClick(event) {
        this.handlePopup(event.target, { x: event.clientX, y: event.clientY })
    }

    static fixPopups(cardWrapper) {
        let popups = cardWrapper.querySelectorAll('article a')
        popups.forEach(popup => {
            popup.setAttribute(
                'onclick',
                'PopupContainer.handlePopupOnClick(event)'
            )
            popup.setAttribute('draggable', false)
            popup.addEventListener('click', event => {
                event.preventDefault()
            })
        })
    }
}
