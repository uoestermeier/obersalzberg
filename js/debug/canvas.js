export default class DebugCanvas {
    constructor({ fill = '#ff0000', line = '#ff00ff', lineWidth = '3' } = {}) {
        this.canvas = document.createElement('canvas')
        document.body.appendChild(this.canvas)
        this.context = this.canvas.getContext('2d')

        Object.assign(this.canvas.style, {
            position: 'fixed',
            left: 0,
            top: 0,
            width: '100vw',
            height: '100vh',
            zIndex: 1000000,
            pointerEvents: 'none'
        })

        this.fill = fill
        this.line = line
        this.lineWidth = lineWidth

        let resizeCanvas = () => {
            this.canvas.width = window.innerWidth
            this.canvas.height = window.innerHeight
        }
        resizeCanvas()
        window.addEventListener('resize', resizeCanvas)
    }

    drawPoint(x, y, r = 10) {
        this.start()
        this.context.arc(x, y, r, 0, 2 * Math.PI)
        this.context.stroke()
        this.context.fill()
    }

    start() {
        this.context.beginPath()
        this.context.fillStyle = this.fill
        this.context.strokeStyle = this.line
        this.context.lineWidth = this.lineWidth
    }

    drawLine(fromX, fromTo, toX, toY) {
        this.start()
        this.context.moveTo(fromX, fromTo)
        this.context.lineTo(toX, toY)
        this.context.stroke()
    }

    clear() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height)
    }
}
