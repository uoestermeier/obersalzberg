import Highlight from '../iwmlib/lib/card/highlight.js'
import Logging from '../iwmlib/lib/logging.js'
import { ObersalzbergCard } from './card.js'
import { ImageReplacer } from './imagereplacer.js'

export default class ObersalzbergHighlight extends Highlight {
    static toggleHighlight(node, options = {}) {
        let wrapper = ObersalzbergCard.getCardWrapper(node)
        let key = ObersalzbergCard.getKey(wrapper)
        Logging.log(
            `TOGGLE_HIGHLIGHT::CARD:${key},HIGHLIGHT:${node.getAttribute(
                'id'
            )}:ACTIVE:${this._isExpanded(node) ? 'close' : 'open'}`
        )
        super.toggleHighlight(node, options)
    }

    static initHighlightFigure(svg) {
        let image = svg.querySelector('image')

        svg.querySelectorAll('circle').forEach(circle => {
            if (window.config.get('highlightStroke') != null) {
                circle.setAttribute(
                    'stroke-width',
                    window.config.get('highlightStroke')
                )
            }
        })

        if (image) {
            svg.querySelectorAll('circle').forEach(circle => {
                this._createSVGMask(circle, { svgRoot: svg, image })

                // Somehow the mask is not working here,
                // when the mask is inside the <defs> tag.
                // So we put it outside! Feel free to fix this, I couldn't. -sopel
                const id = circle.getAttribute('id').replace(/@/g, '')

                let mask = svg.querySelector(`#mask${id}`)
                mask.style.position = 'absolute'
                svg.appendChild(mask)
            })
            image.parentNode.removeChild(image)
        } else if (window.config.get('debug')) {
            window.errorWindow.error(
                'There is a svg specified for image highlights, but there is no image attatched to it!'
            )
        }
    }

    static _createSVGMask(
        element,
        { svgRoot = null, image = null, id = null } = {}
    ) {
        let [mask, maskedImage] = Highlight._createSVGMask(element, {
            svgRoot,
            image,
            id
        })

        let lowly = maskedImage.getAttribute('href')
        let original = lowly.replace('-lowly', '')

        /**
         * In the browser the mask also get's clicked.
         * Making the mask bigger than to original image.
         *
         * We remove all click handlers on the mask to prevent that.
         */
        mask.childNodes.forEach(element => {
            element.removeAttribute('onclick')
        })

        maskedImage.setAttribute('width', '100%')
        maskedImage.setAttribute('height', '100%')
        maskedImage.setAttribute('preserveAspectRatio', 'xMidYMid slice')

        ImageReplacer.setImages(maskedImage, original, lowly)
        ImageReplacer.replaceWithHighResImage(maskedImage, 'href')

        return [mask, maskedImage]
    }
}
