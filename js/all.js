(function () {
    'use strict';

    /* eslint-disable no-console */

    let _HighlightEnabled = true;
    let _CircleIds = 0;

    /** Helper method to round values with one digit precision */
    function round(value) {
        return Math.round(parseFloat(value) * 10) / 10
    }

    /**
     * A namespace with static functions to expand and shrink highlighted image regions.
     * Assumes an SVG image with the following structure:
     *
     * <svg viewbox="0 0 100 100">
     *       <!-- The defs section must be defined and cannot be generated in JavaScript -->
     *      <defs>
     *      </defs>
     *       <image width="100" height="100" xlink:href="../assets/chess.jpg"/>
     *        <circle onclick="Highlight.animateZoom(event)" cx="47" cy="18" r="8" stroke-width="0.5" />
     *       <circle onclick="Highlight.animateZoom(event)" cx="60" cy="67" r="8" stroke-width="0.5" />
     *   </svg>
     *
     * The SVG root element should use a viewbox with 0 0 100 100 to ensure that the positions and size of the
     * circles can be represented in percent.
     *
     * @class Highlight
     * @extends {Object}
     */

    const SCALE = 1.5;

    class Highlight extends Object {
        static disableAnimations() {
            _HighlightEnabled = false;
            let expanded = document.querySelectorAll('.' + this.expandedClass);
            for (let obj of expanded) {
                this.shrink(obj);
            }
        }

        static enableAnimations() {
            _HighlightEnabled = true;
        }

        static removeAnimations(svgRoot) {
            let expanded = svgRoot.querySelectorAll('.' + this.expandedClass);
            for (let obj of expanded) {
                TweenLite.set(obj, { scale: 1 });
                obj.classList.remove('zooming');
                obj.classList.remove(this.expandedClass);
            }
            let defs = svgRoot.querySelector('defs');
            while (defs.firstChild) {
                defs.firstChild.remove();
            }
            let maskImages = svgRoot.querySelectorAll('.addedImage');
            for (let m of maskImages) {
                m.remove();
            }
            let circles = svgRoot.querySelectorAll('circle');
            for (let circle of circles) {
                if (circle.classList.length == 0) {
                    circle.removeAttribute('class');
                }
                if (circle.hasAttribute('id') && circle.getAttribute('id').startsWith('@@')) {
                    circle.removeAttribute('id');
                }
                circle.removeAttribute('data-svg-origin');
                circle.removeAttribute('transform');
                circle.removeAttribute('style');
                let cx = circle.getAttribute('cx');
                let cy = circle.getAttribute('cy');
                let r = circle.getAttribute('r');
                circle.setAttribute('cx', round(cx));
                circle.setAttribute('cy', round(cy));
                circle.setAttribute('r', round(r));
            }
        }

        static expand(obj, { scale = SCALE, duration = 3, stroke = 2, onComplete = null } = {}) {
            if (obj == null) return
            //console.log("expand")
            obj.classList.add('zooming');
            TweenLite.to(obj, duration, {
                scale,
                onUpdate: () => {
                    let scale = obj._gsTransform.scaleX;
                    obj.setAttribute('stroke-width', stroke / scale);
                },
                onComplete: () => {
                    console.log('expand complete');
                    obj.classList.remove('zooming');
                    obj.classList.add(this.expandedClass);
                    obj.setAttribute('stroke-width', stroke / scale);
                    if (onComplete) onComplete();
                },
            });
        }

        static shrink(obj, { duration = 0.5, stroke = 2 } = {}) {
            //console.log("shrink")
            if (obj == null) return
            obj.classList.add('zooming');
            TweenLite.to(obj, duration, {
                scale: 1,
                onUpdate: () => {
                    let scale = obj._gsTransform.scaleX;
                    obj.setAttribute('stroke-width', stroke / scale);
                },
                onComplete: () => {
                    //console.log("shrink complete")
                    obj.classList.remove('zooming');
                    obj.classList.remove(this.expandedClass);
                    obj.setAttribute('stroke-width', stroke);
                },
            });
        }

        static animateCircle(target, callback) {
            console.log('ANIMATE CIRCLE', this);
            // ** DEBUG OUTPUTS **

            let circle = target;
            // We need a unique id to ensure correspondence between circle, mask, and maskImage
            if (!circle.hasAttribute('id')) {
                _CircleIds += 1;
                circle.setAttribute('id', '@@' + _CircleIds);
            }
            let id = circle.getAttribute('id');
            TweenLite.set(circle, { transformOrigin: '50% 50%' });
            /*if (circle.classList.contains('zooming')) {
                    console.log("already zooming")
                    return
                }*/

            let svgRoot = circle.closest('svg');
            let circleGroup = circle.parentNode;
            let image = svgRoot.querySelector('image');

            let stroke = parseFloat(circleGroup.getAttribute('stroke-width') || 6);

            let defs = svgRoot.querySelector('defs');
            if (defs == null) {
                defs = document.createElementNS(svgRoot, 'defs');
                svgRoot.insertBefore(defs, image);
            }

            // // We need direct children, therefore we cannot use querySelectorAll
            let maskImageId = 'maskImage' + id;
            let maskImage = svgRoot.getElementById(maskImageId);

            if (circle.classList.contains(this.expandedClass)) {
                if (!circle.classList.contains('zooming')) {
                    this.shrink(circle, { stroke });
                    this.shrink(maskImage, { stroke });
                    return
                }
                //console.log("animate called while zooming out -> expand")
            } else if (circle.classList.contains('zooming')) {
                //console.log("animate called while zooming in -> shrink")
                this.shrink(circle, { stroke });
                this.shrink(maskImage, { stroke });
                return
            }
            let circles = Array.from(circleGroup.children).filter((e) => e.tagName == 'circle');
            for (let c of circles) {
                //console.log("shrinking all circles")
                this.shrink(c, { stroke });
            }
            let maskImages = circleGroup.querySelectorAll('.addedImage');
            for (let m of maskImages) {
                this.shrink(m, { stroke });
            }

            this._createSVGMask(svgRoot, image, id);

            // TweenLite.set(maskImage, { transformOrigin: `${tx}% ${ty}%` })

            this.expand(circle, { stroke, onComplete: callback });
            this.expand(maskImage);

            return false
        }

        static openHighlight(target, { animation = 0.5, scale = SCALE, onExpanded = null } = {}) {
            if (this._isExpanded(target)) {
                console.log('Target is already expanded!');
                return
            } else {
                let targetId = target.getAttribute('id');
                if (targetId && targetId.startsWith('@@')) {
                    let id = targetId.slice(2);
                    const imageId = '#maskImage' + id;
                    const parent = target.parentNode;
                    if (parent != null) {
                        let image = parent.querySelector(imageId);
                        if (image) {
                            this._bringToFront(image);
                        } else console.error('Could not find corresponding image element.');
                    } else console.log('Element was no parent:', target);
                }
                this._bringToFront(target);
                let svgRoot = target.closest('svg');
                if (svgRoot == null) {
                    return
                }
                let image = svgRoot.querySelector('image');

                // eslint-disable-next-line no-unused-vars
                let [mask, maskImage] = this._getSVGMask(target, {
                    svgRoot,
                    image,
                });
                let center = this._calculateCenterRelativeTo(target, image);
                console.log('_calculateCenterRelativeTo', center);
                TweenLite.set(maskImage, {
                    transformOrigin: `${center.x} ${center.y}`,
                });
                TweenLite.set(target, { transformOrigin: '50% 50%' });

                TweenLite.to([target, maskImage], animation, {
                    scale,
                    onComplete: onExpanded,
                });

                target.classList.add(this.expandedClass);
            }
        }

        static toggleHighlight(node, options = {}) {
            console.log('toggleHighlight', this._isExpanded(node));
            if (this._isExpanded(node)) {
                this.closeHighlight(node, options);
            } else {
                this.openHighlight(node, options);
            }
        }

        static _bringToFront(target) {
            const parent = target.parentNode;
            if (target && parent) {
                parent.removeChild(target);
                parent.appendChild(target);
            } else console.error('Could not bring to front. Either no target or no parent.', target, parent);
        }

        static _getSVGMask(circle, { svgRoot = null, image = null } = {}) {
            const id = this._retrieveId(circle);
            const maskId = 'mask' + id;
            const maskImageId = 'maskImage' + id;

            if (!svgRoot) svgRoot = circle.closest('svg');

            let mask = svgRoot.getElementById(maskId);
            let maskImage = svgRoot.getElementById(maskImageId);

            if (!mask || !maskImage)
                [mask, maskImage] = this._createSVGMask(circle, {
                    svgRoot,
                    image,
                    id,
                });

            return [mask, maskImage]
        }

        /**
         * Creates an SVG mask for a provided svgElement.
         *
         * @static
         * @param {SVGElement} element - Element that should be masked.
         * @param {object} opts - Optional parameters to avoid unnecessary fetching of elements.
         * @param {SVGElement} opts.svgRoot - The root <svg> element of the element.
         * @param {SVGImageElement} opts.image - The image that is used in the mask.
         * @param {number} opts.id - The id of the mask.
         * @returns
         * @memberof Highlight
         */
        static _createSVGMask(element, { svgRoot = null, image = null, id = null } = {}) {
            // We can fetch these values here, but it's more efficient to
            // simply pass them in, as it's likely they were already retrieved beforehand.
            if (svgRoot == null) svgRoot = element.closest('svg');
            if (image == null) image = svgRoot.querySelector('image');
            if (id == null) id = this._retrieveId(element);

            let svg = 'http://www.w3.org/2000/svg';
            let xlink = 'http://www.w3.org/1999/xlink';

            element.parentNode;

            let src = image.getAttributeNS(xlink, 'href');

            let maskId = 'mask' + id;
            let maskImageId = 'maskImage' + id;
            let mask = svgRoot.getElementById(maskId);
            let maskImage = svgRoot.getElementById(maskImageId);

            let defs = svgRoot.querySelector('defs');
            if (defs == null) {
                defs = document.createElementNS(svgRoot, 'defs');
                svgRoot.insertBefore(defs, image);
            }

            if (mask == null) {
                mask = document.createElementNS(svg, 'mask');
                mask.setAttribute('id', maskId);
                let maskCircle = element.cloneNode(true);
                mask.appendChild(maskCircle);
                defs.appendChild(mask);
            }

            let bbox = svgRoot.getElementsByTagName('image')[0].getBBox();
            let width = bbox.width;
            let height = bbox.height;

            if (maskImage == null) {
                maskImage = document.createElementNS(svg, 'image');
                maskImage.style.pointerEvents = 'none';
                maskImage.setAttribute('id', maskImageId);
                maskImage.setAttributeNS(xlink, 'href', src);
                maskImage.setAttribute('width', width);
                maskImage.setAttribute('height', height);
                maskImage.setAttribute('class', 'addedImage');
                TweenLite.set(maskImage, { scale: 1 });
                maskImage.style.mask = 'url(#' + maskId + ')';
            }

            element.insertAdjacentElement('beforebegin', maskImage); // image.nextSibling)
            // svgGroup.appendChild(element)

            return [mask, maskImage]
        }

        static _calculateCenterRelativeTo(target, image) {
            let bbox = image.getBBox();
            let width = bbox.width;
            let height = bbox.height;
            let cx = target.getAttribute('cx');
            let cy = target.getAttribute('cy');
            let x = cx.endsWith('%') ? cx : round((cx / width) * 100) + '%';
            let y = cy.endsWith('%') ? cy : round((cy / height) * 100) + '%';
            return { x, y }
        }

        static _isExpanded(target) {
            return target.classList.contains(this.expandedClass)
        }

        static closeHighlight(target, { animation = 0.5 } = {}) {
            target.classList.remove(this.expandedClass);
            // eslint-disable-next-line no-unused-vars
            let [mask, maskImage] = this._getSVGMask(target);
            // console.log('Close Highlight', maskImage)
            TweenLite.to([target, maskImage], animation, {
                scale: 1,
            });
        }

        static animate(event) {
            if (!_HighlightEnabled) return

            event.stopPropagation();
            this.animateCircle(event.target);

            return false
        }

        static _retrieveId(target) {
            let id = target.getAttribute('id');
            // We need a unique id to ensure correspondence between circle, mask, and maskImage
            if (!id) {
                _CircleIds += 1;
                target.setAttribute('id', '@@' + _CircleIds);
                id = _CircleIds;
            } else {
                id = parseInt(id.substring(2));
            }

            return id
        }
    }

    Highlight.expandedClass = 'expanded';

    /* eslint-disable no-undef */
    /* eslint-disable no-console */
    /* eslint-disable no-unused-vars */
    let ipc = null;
    let logMessages = new Set();
    let logHandlers = {
        log: console.log,
        warn: console.warn,
        error: console.error,
    };

    try {
        ipc = window.ipcRenderer || require('electron').ipcRenderer;
        logHandlers.log = (message) => ipc.send('log', message);
        logHandlers.warn = (message) => ipc.send('warn', message);
        logHandlers.error = (message) => ipc.send('error', message);
    } catch (e) {
        console.log('Cannot use electron logging.');
    }

    /** Basic class for app specific logging requirements.
     * Can be used to implement persistent logging in electron apps.
     * Uses a logMessage cache to prevent error overflows. This is
     * needed since errors may occur very frequently
     * (e.g. display update loops at 60fps, programmatic loops, ...).
     *
     * The logging handlers can be overwritten by calling the static
     * setup method.
     */
    class Logging {
        /** Static log function.
         * @param {*} message
         */
        static log(message) {
            logHandlers.log(message);
        }

        /**
         * Static warn function.
         * Emits each warning only once per session.
         * @param {*} message
         */
        static warn(message) {
            if (!logMessages.has(message)) {
                logMessages.add(message);
                logHandlers.warn(message);
            }
        }

        /**
         * Static error function.
         * Emits each error message only once per session.
         * @param {*} message
         */
        static error(message) {
            if (!logMessages.has(message)) {
                logMessages.add(message);
                logHandlers.error(message);
            }
        }

        static setup({ log = console.log, warn = console.warn, error = console.error } = {}) {
            logHandlers.log = log;
            logHandlers.warn = warn;
            logHandlers.error = error;
        }
    }

    class Events {
        static stop(event) {
            event.preventDefault();
            // I removed this, as it destroys all the Hammer.js events.
            // And also the click event.
            // It seems to have no (apparent) negative impact. -SO
            // event.stopPropagation()
        }

        static extractPoint(event) {
            switch (event.constructor.name) {
                case 'TouchEvent':
                    for (let i = 0; i < event.targetTouches.length; i++) {
                        let t = event.targetTouches[i];
                        return { x: t.clientX, y: t.clientY }
                    }
                    break
                default:
                    return { x: event.clientX, y: event.clientY }
            }
        }

        static isCaptured(event) {
            if (event.__capturedBy) return true
            return false
        }

        static capturedBy(event, obj) {
            event.__capturedBy = obj;
        }

        static isPointerDown(event) {
            // According to
            // https://developer.mozilla.org/en-US/docs/Web/API/Pointer_events
            // pointer events use the buttons feature to represent pressed buttons
            return event.buttons
        }

        static isMouseDown(event) {
            // Attempts to clone the which attribute of events failed in WebKit. May
            // be this is a bug or a security feature. Workaround: we introduce
            // a mouseDownSubstitute attribute that can be assigned to cloned
            // events after instantiation.
            if (Reflect.has(event, 'mouseDownSubstitute')) return event.mouseDownSubstitute
            return event.buttons || event.which
        }

        static isSimulatedEvent(event) {
            return Reflect.has(event, 'mouseDownSubstitute')
        }

        static isMouseRightClick(event) {
            return event.buttons || event.which
        }

        static extractTouches(targets) {
            let touches = [];
            for (let i = 0; i < targets.length; i++) {
                let t = targets[i];
                touches.push({
                    targetSelector: this.selector(t.target),
                    identifier: t.identifier,
                    screenX: t.screenX,
                    screenY: t.screenY,
                    clientX: t.clientX,
                    clientY: t.clientY,
                    pageX: t.pageX,
                    pageY: t.pageY,
                });
            }
            return touches
        }

        static createTouchList(targets) {
            let touches = [];
            for (let i = 0; i < targets.length; i++) {
                let t = targets[i];
                let touchTarget = document.elementFromPoint(t.pageX, t.pageY);
                let touch = new Touch(undefined, touchTarget, t.identifier, t.pageX, t.pageY, t.screenX, t.screenY);
                touches.push(touch);
            }
            return new TouchList(...touches)
        }

        static extractEvent(timestamp, event) {
            let targetSelector = this.selector(event.target);
            let infos = {
                type: event.type,
                time: timestamp,
                constructor: event.constructor,
                data: {
                    targetSelector: targetSelector,
                    view: event.view,
                    mouseDownSubstitute: event.buttons || event.which, // which cannot be cloned directly
                    bubbles: event.bubbles,
                    cancelable: event.cancelable,
                    screenX: event.screenX,
                    screenY: event.screenY,
                    clientX: event.clientX,
                    clientY: event.clientY,
                    layerX: event.layerX,
                    layerY: event.layerY,
                    pageX: event.pageX,
                    pageY: event.pageY,
                    ctrlKey: event.ctrlKey,
                    altKey: event.altKey,
                    shiftKey: event.shiftKey,
                    metaKey: event.metaKey,
                },
            };
            if (event.type.startsWith('touch')) {
                // On Safari-WebKit the TouchEvent has layerX, layerY coordinates
                let data = infos.data;
                data.targetTouches = this.extractTouches(event.targetTouches);
                data.changedTouches = this.extractTouches(event.changedTouches);
                data.touches = this.extractTouches(event.touches);
            }
            if (event.type.startsWith('pointer')) {
                let data = infos.data;
                data.pointerId = event.pointerId;
                data.pointerType = event.pointerType;
            }
            if (Events.debug) {
                Events.extracted.push(this.toLine(event));
            }
            return infos
        }

        static cloneEvent(type, constructor, data) {
            if (type.startsWith('touch')) {
                // We need to find target from layerX, layerY
                //var target = document.querySelector(data.targetSelector)
                // elementFromPoint(data.layerX, data.layerY)
                //data.target = target
                data.targetTouches = this.createTouchList(data.targetTouches);
                data.changedTouches = this.createTouchList(data.changedTouches);
                data.touches = this.createTouchList(data.touches);
            }
            // We need to find target from pageX, pageY which are only
            // available after construction. They seem to getter items.

            let clone = Reflect.construct(constructor, [type, data]);
            clone.mouseDownSubstitute = data.mouseDownSubstitute;
            return clone
        }

        static simulateEvent(type, constructor, data) {
            data.target = document.querySelector(data.targetSelector);
            let clone = this.cloneEvent(type, constructor, data);
            if (data.target != null) {
                data.target.dispatchEvent(clone);
            }
            if (Events.debug) {
                Events.simulated.push(this.toLine(clone));
            }
        }

        static toLine(event) {
            return `${event.type} #${event.target.id} ${event.clientX} ${event.clientY}`
        }

        static compareExtractedWithSimulated() {
            if (this.extracted.length != this.simulated.length) {
                alert(
                    'Unequal length of extracted [' +
                        this.extracted.length +
                        '] and simulated events [' +
                        this.simulated.length +
                        '].'
                );
            } else {
                for (let i = 0; i < this.extracted.length; i++) {
                    var extracted = this.extracted[i];
                    var simulated = this.simulated[i];
                    if (extracted != simulated) {
                        console.log('Events differ:' + extracted + '|' + simulated);
                    }
                }
            }
        }

        static selector(context) {
            return OptimalSelect.select(context)
        }

        static reset() {
            this.extracted = [];
            this.simulated = [];
        }

        static resetSimulated() {
            this.simulated = [];
        }

        static showExtractedEvents(event) {
            if (!event.shiftKey) {
                return
            }
            if (this.popup == null) {
                let element = document.createElement('div');
                Elements.setStyle(element, {
                    position: 'absolute',
                    width: '480px',
                    height: '640px',
                    overflow: 'auto',
                    backgroundColor: 'lightgray',
                });
                document.body.appendChild(element);
                this.popup = element;
            }
            this.popup.innerHTML = '';
            for (let line of this.extracted) {
                let div = document.createElement('div');
                div.innerHTML = line;
                this.popup.appendChild(div);
            }
            let div = document.createElement('div');
            div.innerHTML = '------------ Simulated -----------';
            this.popup.appendChild(div);
            for (let line of this.simulated) {
                let div = document.createElement('div');
                div.innerHTML = line;
                this.popup.appendChild(div);
            }
            Elements.setStyle(this.popup, {
                left: event.clientX + 'px',
                top: event.clientY + 'px',
            });
        }
    }

    Events.popup = null;
    Events.debug = true;
    Events.extracted = [];
    Events.simulated = [];
    Events.simulationRunning = false;

    /* globals */

    /** Static methods to compute 2D points with x and y coordinates.
     */
    class Points {
        static length(a) {
            return Math.sqrt(a.x * a.x + a.y * a.y)
        }

        static normalize(p) {
            let len = this.length(p);
            return this.multiplyScalar(p, 1 / len)
        }

        static mean(a, b) {
            return { x: (a.x + b.x) / 2, y: (a.y + b.y) / 2 }
        }

        static subtract(a, b) {
            return { x: a.x - b.x, y: a.y - b.y }
        }

        static multiply(a, b) {
            return { x: a.x * b.x, y: a.y * b.y }
        }

        static divide(a, b) {
            return { x: a.x / b.x, y: a.y / b.y }
        }

        static multiplyScalar(a, b) {
            return { x: a.x * b, y: a.y * b }
        }

        static add(a, b) {
            return { x: a.x + b.x, y: a.y + b.y }
        }

        static negate(p) {
            return { x: -p.x, y: -p.y }
        }

        static angle(p1, p2) {
            return Math.atan2(p1.y - p2.y, p1.x - p2.x)
        }

        static normalizedAngle(p1, p2) {
            return Angle.normalize(this.angle(p1, p2))
        }

        static normalized2Angle(p1, p2) {
            return Angle.normalize2(this.angle(p1, p2))
        }

        static arc(p, alpha, radius) {
            return {
                x: p.x + radius * Math.cos(alpha),
                y: p.y + radius * Math.sin(alpha),
            }
        }

        static distance(a, b) {
            let dx = a.x - b.x;
            let dy = a.y - b.y;
            return Math.sqrt(dx * dx + dy * dy)
        }

        // Distance == 0.0 indicates an inside relation.
        static distanceToRect(p, r) {
            let dx = 0;
            let dy = 0;
            if (p.x < r.x) dx = p.x - r.x;
            else if (p.x > r.x + r.width) dx = p.x - (r.x + r.width);
            if (p.y < r.y) dy = p.y - r.y;
            else if (p.y > r.y + r.height) dy = p.y - (r.y + r.height);
            return Math.sqrt(dx * dx + dy * dy)
            /* let cx = Math.max(Math.min(p.x, r.x + r.width), r.x)
                let cy = Math.max(Math.min(p.y, r.y + r.height), r.y)
                let result = Math.sqrt((p.x - cx) * (p.x - cx) + (p.y - cy) * (p.y - cy))
                console.log("distanceToRect", p, r, result)
                return result */
        }

        static fromPageToNode(element, p) {
            //    if (window.webkitConvertPointFromPageToNode) {
            //             return window.webkitConvertPointFromPageToNode(element,
            //                                                     new WebKitPoint(p.x, p.y))
            //         }
            return window.convertPointFromPageToNode(element, p.x, p.y)
        }

        static fromNodeToPage(element, p) {
            //  if (window.webkitConvertPointFromNodeToPage) {
            //             return window.webkitConvertPointFromNodeToPage(element,
            //                                                     new WebKitPoint(p.x, p.y))
            //         }
            return window.convertPointFromNodeToPage(element, p.x, p.y)
        }
    }

    /** Static methods to compute angles.
     */
    class Angle {
        static normalize(angle) {
            let TAU = Math.PI * 2.0;
            while (angle > Math.PI) {
                angle -= TAU;
            }
            while (angle < -Math.PI) {
                angle += TAU;
            }
            return angle
        }

        static normalize2(angle) {
            let TAU = Math.PI * 2.0;
            while (angle > TAU) {
                angle -= TAU;
            }
            while (angle < 0) {
                angle += TAU;
            }
            return angle
        }

        static normalizeDegree(angle) {
            let full = 360.0;
            while (angle > 180.0) {
                angle -= full;
            }
            while (angle < -180.0) {
                angle += full;
            }
            return angle
        }

        static normalizedDiff(a, b) {
            return this.normalize(this.diff(a, b))
        }

        static normalized2Diff(a, b) {
            return this.normalize2(this.diff(a, b))
        }

        static diff(a, b) {
            return Math.atan2(Math.sin(a - b), Math.cos(a - b))
        }

        static degree2radian(degree) {
            return (Math.PI * degree) / 180.0
        }

        static radian2degree(rad) {
            return (180.0 / Math.PI) * rad
        }
    }

    class Elements$1 {
        static setStyle(element, styles) {
            for (let key in styles) {
                element.style[key] = styles[key];
            }
        }

        static addClass(element, cssClass) {
            element.classList.add(cssClass);
        }

        static removeClass(element, cssClass) {
            element.classList.remove(cssClass);
        }

        static toggleClass(element, cssClass) {
            element.classList.toggle(cssClass);
        }

        static hasClass(element, cssClass) {
            return element.classList.contains(cssClass)
        }
    }

    class MapProxy {
        /* This class is needed if we want to use the interaction classes
        in Firefox 45.8 and modern Browsers.

        A workaround for https://github.com/babel/babel/issues/2334
      */
        constructor() {
            this.map = new Map();
        }

        get size() {
            return this.map.size
        }

        get(key) {
            return this.map.get(key)
        }

        set(key, value) {
            return this.map.set(key, value)
        }

        delete(key) {
            return this.map.delete(key)
        }

        clear() {
            return this.map.clear()
        }

        has(key) {
            return this.map.has(key)
        }

        keys() {
            return this.map.keys()
        }

        values() {
            return this.map.values()
        }

        entries() {
            return this.map.entries()
        }

        forEach(func) {
            this.map.forEach(func);
        }
    }

    /* Based om https://gist.github.com/cwleonard/e124d63238bda7a3cbfa */
    class Polygon {
        /*
         *  This is the Polygon constructor. All points are center-relative.
         */
        constructor(center) {
            this.points = new Array();
            this.center = center;
        }

        /*
         *  Point x and y values should be relative to the center.
         */
        addPoint(p) {
            this.points.push(p);
        }

        /*
         *  Point x and y values should be absolute coordinates.
         */
        addAbsolutePoint(p) {
            this.points.push({ x: p.x - this.center.x, y: p.y - this.center.y });
        }

        /*
         * Returns the number of sides. Equal to the number of vertices.
         */
        getNumberOfSides() {
            return this.points.length
        }

        /*
         * rotate the polygon by a number of radians
         */
        rotate(rads) {
            for (let i = 0; i < this.points.length; i++) {
                let x = this.points[i].x;
                let y = this.points[i].y;
                this.points[i].x = Math.cos(rads) * x - Math.sin(rads) * y;
                this.points[i].y = Math.sin(rads) * x + Math.cos(rads) * y;
            }
        }

        /*
         *  The draw function takes as a parameter a Context object from
         *  a Canvas element and draws the polygon on it.
         */
        draw(context, { lineWidth = 2, stroke = '#000000', fill = null } = {}) {
            context.beginPath();
            context.moveTo(this.points[0].x + this.center.x, this.points[0].y + this.center.y);
            for (let i = 1; i < this.points.length; i++) {
                context.lineTo(this.points[i].x + this.center.x, this.points[i].y + this.center.y);
            }
            context.closePath();
            context.lineWidth = lineWidth;
            if (stroke) {
                context.strokeStyle = stroke;
                context.stroke();
            }
            if (fill) {
                context.fillStyle = fill;
                context.fill();
            }
        }

        absolutePoints() {
            let result = new Array();
            for (let p of this.points) {
                result.push(Points.add(p, this.center));
            }
            return result
        }

        flatAbsolutePoints() {
            let result = new Array();
            for (let p of this.points) {
                let a = Points.add(p, this.center);
                result.push(a.x);
                result.push(a.y);
            }
            return result
        }

        /*
         *  This function returns true if the given point is inside the polygon,
         *  and false otherwise.
         */
        containsPoint(pnt) {
            let nvert = this.points.length;
            let testx = pnt.x;
            let testy = pnt.y;

            let vertx = new Array();
            for (let q = 0; q < this.points.length; q++) {
                vertx.push(this.points[q].x + this.center.x);
            }

            let verty = new Array();
            for (let w = 0; w < this.points.length; w++) {
                verty.push(this.points[w].y + this.center.y);
            }

            let i,
                j = 0;
            let c = false;
            for (i = 0, j = nvert - 1; i < nvert; j = i++) {
                if (
                    verty[i] > testy != verty[j] > testy &&
                    testx < ((vertx[j] - vertx[i]) * (testy - verty[i])) / (verty[j] - verty[i]) + vertx[i]
                )
                    c = !c;
            }
            return c
        }

        multiplyScalar(scale) {
            let center = Points.multiplyScalar(this.center, scale);
            let clone = new Polygon(center);
            for (let p of this.points) {
                clone.addPoint(Points.multiplyScalar(p, scale));
            }
            return clone
        }

        /*
         *  To detect intersection with another Polygon object, this
         *  function uses the Separating Axis Theorem. It returns false
         *  if there is no intersection, or an object if there is. The object
         *  contains 2 fields, overlap and axis. Moving the polygon by overlap
         *  on axis will get the polygons out of intersection.
         */
        intersectsWith(other) {
            let axis = { x: 0, y: 0 };
            let tmp, minA, maxA, minB, maxB;
            let side, i;
            let smallest = null;
            let overlap = 99999999;

            /* test polygon A's sides */
            for (side = 0; side < this.getNumberOfSides(); side++) {
                /* get the axis that we will project onto */
                if (side == 0) {
                    axis.x = this.points[this.getNumberOfSides() - 1].y - this.points[0].y;
                    axis.y = this.points[0].x - this.points[this.getNumberOfSides() - 1].x;
                } else {
                    axis.x = this.points[side - 1].y - this.points[side].y;
                    axis.y = this.points[side].x - this.points[side - 1].x;
                }

                /* normalize the axis */
                tmp = Math.sqrt(axis.x * axis.x + axis.y * axis.y);
                axis.x /= tmp;
                axis.y /= tmp;

                /* project polygon A onto axis to determine the min/max */
                minA = maxA = this.points[0].x * axis.x + this.points[0].y * axis.y;
                for (i = 1; i < this.getNumberOfSides(); i++) {
                    tmp = this.points[i].x * axis.x + this.points[i].y * axis.y;
                    if (tmp > maxA) maxA = tmp;
                    else if (tmp < minA) minA = tmp;
                }
                /* correct for offset */
                tmp = this.center.x * axis.x + this.center.y * axis.y;
                minA += tmp;
                maxA += tmp;

                /* project polygon B onto axis to determine the min/max */
                minB = maxB = other.points[0].x * axis.x + other.points[0].y * axis.y;
                for (i = 1; i < other.getNumberOfSides(); i++) {
                    tmp = other.points[i].x * axis.x + other.points[i].y * axis.y;
                    if (tmp > maxB) maxB = tmp;
                    else if (tmp < minB) minB = tmp;
                }
                /* correct for offset */
                tmp = other.center.x * axis.x + other.center.y * axis.y;
                minB += tmp;
                maxB += tmp;

                /* test if lines intersect, if not, return false */
                if (maxA < minB || minA > maxB) {
                    return false
                } else {
                    let o = maxA > maxB ? maxB - minA : maxA - minB;
                    if (o < overlap) {
                        overlap = o;
                        smallest = { x: axis.x, y: axis.y };
                    }
                }
            }

            /* test polygon B's sides */
            for (side = 0; side < other.getNumberOfSides(); side++) {
                /* get the axis that we will project onto */
                if (side == 0) {
                    axis.x = other.points[other.getNumberOfSides() - 1].y - other.points[0].y;
                    axis.y = other.points[0].x - other.points[other.getNumberOfSides() - 1].x;
                } else {
                    axis.x = other.points[side - 1].y - other.points[side].y;
                    axis.y = other.points[side].x - other.points[side - 1].x;
                }

                /* normalize the axis */
                tmp = Math.sqrt(axis.x * axis.x + axis.y * axis.y);
                axis.x /= tmp;
                axis.y /= tmp;

                /* project polygon A onto axis to determine the min/max */
                minA = maxA = this.points[0].x * axis.x + this.points[0].y * axis.y;
                for (i = 1; i < this.getNumberOfSides(); i++) {
                    tmp = this.points[i].x * axis.x + this.points[i].y * axis.y;
                    if (tmp > maxA) maxA = tmp;
                    else if (tmp < minA) minA = tmp;
                }
                /* correct for offset */
                tmp = this.center.x * axis.x + this.center.y * axis.y;
                minA += tmp;
                maxA += tmp;

                /* project polygon B onto axis to determine the min/max */
                minB = maxB = other.points[0].x * axis.x + other.points[0].y * axis.y;
                for (i = 1; i < other.getNumberOfSides(); i++) {
                    tmp = other.points[i].x * axis.x + other.points[i].y * axis.y;
                    if (tmp > maxB) maxB = tmp;
                    else if (tmp < minB) minB = tmp;
                }
                /* correct for offset */
                tmp = other.center.x * axis.x + other.center.y * axis.y;
                minB += tmp;
                maxB += tmp;

                /* test if lines intersect, if not, return false */
                if (maxA < minB || minA > maxB) {
                    return false
                } else {
                    let o = maxA > maxB ? maxB - minA : maxA - minB;
                    if (o < overlap) {
                        overlap = o;
                        smallest = { x: axis.x, y: axis.y };
                    }
                }
            }
            return { overlap: overlap + 0.001, axis: smallest }
        }

        static fromPoints(points) {
            let min = { x: Number.MAX_VALUE, y: Number.MAX_VALUE };
            let max = { x: Number.MIN_VALUE, y: Number.MIN_VALUE };
            for (let p of points) {
                min.x = Math.min(p.x, min.x);
                max.x = Math.max(p.x, max.x);
                min.y = Math.min(p.y, min.y);
                max.y = Math.max(p.y, max.y);
            }
            let center = Points.mean(min, max);
            let polygon = new Polygon(center);
            for (let p of points) {
                polygon.addAbsolutePoint(p);
            }
            return polygon
        }
    }

    class LowPassFilter {
        constructor(smoothing = 0.5, bufferMaxSize = 10) {
            this.smoothing = smoothing; // must be smaller than 1
            this.buffer = []; // FIFO queue
            this.bufferMaxSize = bufferMaxSize;
        }

        /**
         * Setup buffer with array of values
         *
         * @param {array} values
         * @returns {array}
         * @access public
         */
        setup(values) {
            for (let i = 0; i < values.length; i++) {
                this.__push(values[i]);
            }
            return this.buffer
        }

        /**
         * Clear buffer to prepare for new values.
         *
         * @access public
         */
        clear() {
            this.buffer = [];
        }

        /**
         * Add new value to buffer (FIFO queue)
         *
         * @param {integer|float} value
         * @returns {integer|float}
         * @access private
         */
        __push(value) {
            let removed = this.buffer.length === this.bufferMaxSize ? this.buffer.shift() : 0;

            this.buffer.push(value);
            return removed
        }

        /**
         * Smooth value from stream
         *
         * @param {integer|float} nextValue
         * @returns {integer|float}
         * @access public
         */
        next(nextValue) {
            // push new value to the end, and remove oldest one
            let removed = this.__push(nextValue);
            // smooth value using all values from buffer
            let result = this.buffer.reduce((last, current) => {
                return this.smoothing * current + (1 - this.smoothing) * last
            }, removed);
            // replace smoothed value
            this.buffer[this.buffer.length - 1] = result;
            return result
        }

        /**
         * Smooth array of values
         *
         * @param {array} values
         * @returns {undefined}
         * @access public
         */
        smoothArray(values) {
            let value = values[0];
            for (let i = 1; i < values.length; i++) {
                let currentValue = values[i];
                value += (currentValue - value) * this.smoothing;
                values[i] = Math.round(value);
            }
            return values
        }
    }

    /* eslint-disable no-console */

    class CardWrapper extends Object {
        constructor(domNode, { triggerSVGClicks = true, allowClickDistance = 44 } = {}) {
            super();
            this.domNode = domNode;
            this.triggerSVGClicks = triggerSVGClicks;
            this.allowClickDistance = allowClickDistance;
            this.tapNodes = new Map();
            this.tapHandler = new Map();
        }

        handleClicks() {
            this.domNode.addEventListener(
                'click',
                (event) => {
                    if (event.isTrusted) {
                        Events.stop(event);
                        if (this.triggerSVGClicks && this.isSVGNode(event.target)) {
                            this.tap(event, 'triggerSVGClicks');
                        }
                    }
                },
                true
            );
        }

        handleClicksAsTaps() {
            this.domNode.addEventListener(
                'click',
                (event) => {
                    if (event.isTrusted) {
                        Events.stop(event);
                    }
                    this.tap(event);
                },
                true
            );
        }

        isClickPrevented(node) {
            if (node == null) {
                return false
            }
            if (node.style && node.style.pointerEvents == 'none') {
                return true
            }
            return this.isClickPrevented(node.parentNode)
        }

        isClickable(node) {
            if (node == null) return false
            //  console.log("isClickable", node, this.isClickPrevented(node))
            if (this.isClickPrevented(node)) {
                return false
            }
            if (node.tagName == 'A' && node.hasAttribute('href')) return true
            if (node.hasAttribute('onclick')) return true
            return false
        }

        hasClickHandler(node) {
            if (node == null) return false
            if (this.tapNodes.has(node)) return true
            for (let [selector, handler] of this.tapHandler.entries()) {
                for (let obj of this.domNode.querySelectorAll(selector)) {
                    if (node == obj) {
                        return true
                    }
                }
            }
            return false
        }

        /**
         * Returns an array of all active nodes.
         * Unfortunately we cannot search for all nodes with an attached 'click' event listener
         * See https://stackoverflow.com/questions/11455515/how-to-check-whether-dynamically-attached-event-listener-exists-or-not
         * Therefore we can only detect the following standard cases:
         * I. All clickable objects like activeNodes
         * II. Objects that have been attached a click handler by the scatter itself via
         */
        activeNodes() {
            let result = [];
            for (let node of this.domNode.querySelectorAll('*')) {
                if (this.isClickable(node)) result.push(node);
                if (this.hasClickHandler(node)) result.push(node);
            }
            return result
        }

        nearestActive(event) {
            let activeNodes = this.activeNodes();
            let globalClick = event.center ? event.center : { x: event.x, y: event.y };
            let distances = [];
            activeNodes.forEach((link) => {
                let rect = link.getBoundingClientRect();
                let distance = Points.distanceToRect(globalClick, rect);
                distances.push(parseInt(distance));
            });
            let closestClickIndex = distances.indexOf(Math.min(...distances));
            let closestClickable = activeNodes[closestClickIndex];
            if (distances[closestClickIndex] < this.allowClickDistance) {
                return closestClickable
            }
            return null
        }

        isSVGNode(node) {
            return node.ownerSVGElement || node.tagName == 'svg'
        }

        simulateClick(node, event) {
            /* https://stackoverflow.com/questions/49564905/is-it-possible-to-use-click-function-on-svg-tags-i-tried-element-click-on-a
             proposes the dispatchEvent solution. But this leads to problems in flippable.html hiding the back page.
            Therefore we use the original click event (see constructor). */
            // if (this.isSVGNode(node)) {
            //     if (this.triggerSVGClicks) {
            //         let click = new Event('click')
            //         node.dispatchEvent(click)
            //     }
            //     return
            // }
            // node.click()

            let click = new Event('click');
            click.clientX = event.clientX;
            click.clientY = event.clientY;
            node.dispatchEvent(click);
        }

        nodeTapped(node, event) {
            if (this.tapNodes.has(node)) {
                let handler = this.tapNodes.get(node);
                handler(event, node);
                return true
            }
            for (let [selector, handler] of this.tapHandler.entries()) {
                console.log('nodeTapped', selector);
                for (let obj of this.domNode.querySelectorAll(selector)) {
                    if (node == obj) {
                        handler(event, node);
                        return true
                    }
                }
            }
            if (this.isClickable(node)) {
                this.simulateClick(node, event);
                return true
            }
            return false
        }

        tap(event, calledBy = 'unknown') {
            if (event.isTrusted) {
                let node = this.nearestActive(event);
                console.log('tap', node);
                this.nodeTapped(node, event);

                /*  let node = document.elementFromPoint(event.clientX, event.clientY)
                if (!this.nodeTapped(node, event)) {
                    node = this.nearestActive(event)
                    this.nodeTapped(node, event)
                } */
            }
        }

        onTap(objOrSelector, handler) {
            if (typeof objOrSelector == 'string') {
                this.tapHandler.set(objOrSelector, handler);
            } else {
                this.tapNodes.set(objOrSelector, handler);
            }
        }
    }

    // In order to test this interface implementation run jsc interface.js

    class Interface {
        // Abstract interface that should be extended in interface subclasses.
        // By convention all interfaces should start with an upper 'I'

        static implementationError(klass) {
            let interfaceKeys = Reflect.ownKeys(this.prototype);
            Reflect.ownKeys(klass.prototype);
            for (let key of interfaceKeys) {
                this.prototype[key];
                let classDesc = klass.prototype[key];
                if (typeof classDesc == 'undefined') return 'Missing ' + key
            }
            return null
        }

        static implementedBy(klass) {
            // In the first step only checks whether the methods of this
            // interface are all implemented by the given class
            let error = this.implementationError(klass);
            return error == null
        }

        // TODO: Specify optional methods
        //     static optionalMethods() {
        //         return [this.onMouseWheel]
        //     }
    }

    /* eslint-disable no-unused-vars */

    /** Interaction patterns

        See interaction.html for explanation
    */

    class IInteractionTarget extends Interface {
        capture(event) {
            return typeof true
        }

        onStart(event, interaction) {}
        onMove(event, interaction) {}
        onEnd(event, interaction) {}

        onMouseWheel(event) {}
    }

    class IInteractionMapperTarget extends Interface {
        capture(event) {
            return typeof true
        }

        findTarget(event, local, global) {
            return IInteractionTarget
        }
    }

    class PointMap extends MapProxy {
        // Collects touch points, mouse coordinates, etc. as key value pairs.
        // Keys are pointer and touch ids, the special "mouse" key.
        // Values are points, i.e. all objects with numeric x and y properties.
        constructor(points = {}) {
            super();
            for (let key in points) {
                this.set(key, points[key]);
            }
        }

        toString() {
            let points = [];
            for (let key of this.keys()) {
                let value = this.get(key);
                points.push(`${key}:{x:${value.x}, y:${value.y}}`);
            }
            let attrs = points.join(', ');
            return `[PointMap ${attrs}]`
        }

        clone() {
            let result = new PointMap();
            for (let key of this.keys()) {
                let value = this.get(key);
                result.set(key, { x: value.x, y: value.y });
            }
            return result
        }

        keyOf(value) {
            for (let key of this.keys()) {
                let p = this.get(key);
                if (p.x == value.x && p.y == value.y) {
                    return key
                }
            }
            return null
        }

        firstKey() {
            for (let key of this.keys()) {
                return key
            }
            return null
        }

        first() {
            for (let key of this.keys()) {
                return this.get(key)
            }
            return null
        }

        farthests() {
            if (this.size == 0) {
                return null
            }
            let pairs = [];
            for (let key of this.keys()) {
                let p = this.get(key);
                p.key = key;
                for (let k of this.keys()) {
                    let q = this.get(k);
                    q.key = k;
                    pairs.push([p, q]);
                }
            }
            let sorted = pairs.sort((a, b) => {
                return Points.distance(b[0], b[1]) - Points.distance(a[0], a[1])
            });
            return sorted[0]
        }

        mean() {
            if (this.size == 0) {
                return null
            }
            let x = 0.0,
                y = 0.0;
            for (let p of this.values()) {
                x += p.x;
                y += p.y;
            }
            return { x: x / this.size, y: y / this.size }
        }
    }

    class InteractionDelta {
        /**
         *Creates an instance of InteractionDelta.
         * @param {*} x
         * @param {*} y
         * @param {*} zoom
         * @param {*} rotate
         * @param {*} about
         * @param {*} number - number of involved pointer
         * @param {*} distance - distance of farthests touch points
         * @memberof InteractionDelta
         */
        constructor(x, y, zoom, rotate, about, number, distance) {
            this.x = x;
            this.y = y;
            this.zoom = zoom;
            this.rotate = rotate;
            this.about = about;
            this.number = number;
            this.distance = distance;
        }

        toString() {
            let values = [];
            for (let key of Object.keys(this)) {
                let value = this[key];
                if (key == 'about') {
                    values.push(`${key}:{x:${value.x}, y:${value.y}}`);
                } else {
                    values.push(`${key}:${value}`);
                }
            }
            let attrs = values.join(', ');
            return `[InteractionDelta ${attrs}]`
        }
    }

    class InteractionPoints {
        constructor(parent = null) {
            this.parent = parent;
            this.current = new PointMap();
            this.previous = new PointMap();
            this.start = new PointMap();
            this.ended = new PointMap();
            this.timestamps = new Map();
        }

        moved(key) {
            let current = this.current.get(key);
            let previous = this.previous.get(key);
            return Points.subtract(current, previous)
        }

        move() {
            let current = this.current.mean();
            let previous = this.previous.mean();
            return Points.subtract(current, previous)
        }

        /**
         * Computes the delta between previous and current angles. Corrects
         * value that are larger than 45°
         * @param {*} a
         * @param {*} b
         * @returns delta
         */
        diffAngle(a, b) {
            let alpha = Math.atan2(Math.sin(a - b), Math.cos(a - b));
            if (Math.abs(alpha) > Math.PI / 4) {
                alpha -= Math.PI;
            }
            return alpha
        }

        /**
         * Computes the delta between interaction points at t and t+1.
         *
         * @returns InteractionDelta
         * @memberof InteractionPoints
         */
        delta() {
            let prev = [];
            let curr = [];
            let cm = { x: 0, y: 0 };
            let pm = { x: 0, y: 0 };
            let count = 0;
            for (let key of this.current.keys()) {
                if (this.previous.has(key)) {
                    let p = this.previous.get(key);
                    let c = this.current.get(key);
                    pm = Points.add(pm, p);
                    cm = Points.add(cm, c);
                    prev.push(p);
                    curr.push(c);
                    count += 1;
                }
            }
            if (count > 0) {
                pm = Points.multiplyScalar(pm, 1 / count);
                cm = Points.multiplyScalar(cm, 1 / count);
                let delta = Points.subtract(cm, pm);
                let scale = 0;
                let scaled = 0;
                let alpha = 0;
                let zoom = 1;
                for (let i = 0; i < count; i++) {
                    let p = prev[i];
                    let c = curr[i];
                    let previousAngle = Points.angle(p, pm);
                    let currentAngle = Points.angle(c, cm);
                    let diff = this.diffAngle(currentAngle, previousAngle);
                    alpha += diff;

                    let distance1 = Points.distance(p, pm);
                    let distance2 = Points.distance(c, cm);
                    if (distance1 != 0 && distance2 != 0) {
                        scale += distance2 / distance1;
                        scaled += 1;
                    }
                }
                if (scaled > 0) {
                    zoom = scale / scaled;
                }
                alpha /= count;

                let current = this.current.farthests();

                let c1 = current[0];
                let c2 = current[1];
                let distance2 = Points.distance(c1, c2);

                return new InteractionDelta(delta.x, delta.y, zoom, alpha, cm, count, distance2)
            } else {
                return null
            }
        }

        /**
         * Computes the delta between interaction points at t and t+1.
         *
         * @returns InteractionDelta
         * @memberof InteractionPoints
         */
        deltaByTwoFarthestsPoints() {
            let csize = this.current.size;
            let psize = this.previous.size;
            if (csize >= 2 && csize == psize) {
                // Reduce to the two farthests points
                let current = this.current.farthests();

                let c1 = current[0];
                let c2 = current[1];

                let p1 = this.previous.get(c1.key);
                let p2 = this.previous.get(c2.key);

                let d1 = Points.subtract(c1, p1);
                let d2 = Points.subtract(c2, p2);
                let cm = Points.mean(c1, c2);

                // Using the mean leads to jumps between time slices with 3 and 2 fingers
                // We use the mean of deltas instead
                let delta = Points.mean(d1, d2);
                let zoom = 1.0;
                let distance1 = Points.distance(p1, p2);
                let distance2 = Points.distance(c1, c2);
                if (distance1 != 0 && distance2 != 0) {
                    zoom = distance2 / distance1;
                }
                let currentAngle = Points.angle(c1, c2);
                let previousAngle = Points.angle(p1, p2);
                let alpha = this.diffAngle(currentAngle, previousAngle);
                return new InteractionDelta(delta.x, delta.y, zoom, alpha, cm, csize, distance2)
            } else if (csize == 1 && psize == 1 && this.current.firstKey() == this.previous.firstKey()) {
                // We need to ensure that the keys are the same, since single points with different keys
                // can jump
                let current = this.current.first();
                let previous = this.previous.first();
                let delta = Points.subtract(current, previous);
                return new InteractionDelta(delta.x, delta.y, 1.0, 0.0, current, csize)
            }
            return null
        }

        started(key, point) {
            this.current.set(key, point);
            this.start.set(key, point);
            this.previous.set(key, point);
            this.timestamps.set(key, performance.now());
        }

        update(key, point) {
            // Returns true iff the key is new
            this.current.set(key, point);
            if (!this.start.has(key)) {
                this.start.set(key, point);
                this.previous.set(key, point);
                this.timestamps.set(key, performance.now());
                return true
            }
            return false
        }

        updatePrevious() {
            for (let key of this.current.keys()) {
                this.previous.set(key, this.current.get(key));
            }
        }

        stop(key, point) {
            if (this.current.has(key)) {
                this.current.delete(key);
                this.previous.delete(key);
                this.ended.set(key, point);
            }
        }

        finish(key, point) {
            this.current.delete(key);
            this.previous.delete(key);
            this.start.delete(key);
            this.timestamps.delete(key);
            this.ended.delete(key);
        }

        isFinished() {
            return this.current.size == 0
        }

        isNoLongerTwoFinger() {
            return this.previous.size > 1 && this.current.size < 2
        }

        isTap(key) {
            return this.parent.isTap(key)
        }

        isDoubleTap(key) {
            return this.parent.isDoubleTap(key)
        }

        isLongPress(key) {
            return this.parent.isLongPress(key)
        }
    }

    class Interaction extends InteractionPoints {
        constructor(tapDistance = 10, tapDuration = 250.0, longPressTime = 500.0) {
            super();
            this.tapDistance = tapDistance;
            this.tapCounts = new Map();
            this.tapPositions = new Map();
            this.tapTimestamps = new Map();
            this.tapDuration = tapDuration;
            this.longPressTime = longPressTime;
            this.targets = new Map();
            this.subInteractions = new Map(); // target:Object : InteractionPoints
        }

        stop(key, point) {
            super.stop(key, point);
            for (let points of this.subInteractions.values()) {
                points.stop(key, point);
            }
        }

        addTarget(key, target) {
            this.targets.set(key, target);
            this.subInteractions.set(target, new InteractionPoints(this));
        }

        removeTarget(key) {
            let target = this.targets.get(key);
            this.targets.delete(key);
            // Only remove target if no keys are refering to the target
            let remove = true;
            for (let t of this.targets.values()) {
                if (target === t) {
                    remove = false;
                }
            }
            if (remove) {
                this.subInteractions.delete(target);
            }
        }

        finish(key, point) {
            super.finish(key, point);
            this.removeTarget(key);
        }

        mapInteraction(points, aspects, mappingFunc) {
            // Map centrally registered points to target interactions
            // Returns an array of [target, updated subInteraction] pairs
            let result = new Map();
            for (let key in points) {
                if (this.targets.has(key)) {
                    let target = this.targets.get(key);
                    if (this.subInteractions.has(target)) {
                        let interaction = this.subInteractions.get(target);
                        for (let aspect of aspects) {
                            let pointMap = this[aspect];
                            let point = pointMap.get(key);
                            let mapped = mappingFunc(point, target);
                            interaction[aspect].set(key, mapped);
                        }
                        result.set(target, interaction);
                    }
                }
            }
            return result
        }

        registerTap(key, point) {
            if (this.tapCounts.has(key)) {
                let count = this.tapCounts.get(key);
                this.tapCounts.set(key, count + 1);
            } else {
                this.tapCounts.set(key, 1);
            }
            this.tapPositions.set(key, point);
            this.tapTimestamps.set(key, performance.now());
        }

        unregisterTap(key) {
            this.tapCounts.delete(key);
            this.tapPositions.delete(key);
            this.tapTimestamps.delete(key);
        }

        isTap(key) {
            let ended = this.ended.get(key);
            let start = this.start.get(key);
            if (start && ended && Points.distance(ended, start) < this.tapDistance) {
                let t1 = this.timestamps.get(key);
                let tookLong = performance.now() > t1 + this.longPressTime;
                if (tookLong) {
                    return false
                }
                return true
            }
            return false
        }

        isDoubleTap(key) {
            let ended = this.ended.get(key);
            if (this.tapCounts.has(key) && this.tapCounts.get(key) > 2) {
                this.unregisterTap(key);
            }
            if (this.tapPositions.has(key)) {
                let pos = this.tapPositions.get(key);
                if (Points.distance(ended, pos) > this.tapDistance) {
                    this.unregisterTap(key);
                }
            }
            if (this.tapTimestamps.has(key) && performance.now() > this.tapTimestamps.get(key) + this.tapDuration) {
                //console.log("tap too long")
                this.unregisterTap(key);
            }
            let result = false;
            if (this.isTap(key)) {
                this.registerTap(key, ended);
                result = this.tapCounts.get(key) == 2;
            } else {
                this.unregisterTap(key);
            }
            //console.log("isDoubleTap", this.tapCounts.get(key), result)
            return result
        }

        isAnyTap() {
            for (let key of this.ended.keys()) {
                if (this.isTap(key)) return true
            }
            return false
        }

        isLongPress(key) {
            let ended = this.ended.get(key);
            let start = this.start.get(key);
            if (start && ended && Points.distance(ended, start) < this.tapDistance) {
                let t1 = this.timestamps.get(key);
                let tookLong = performance.now() > t1 + this.longPressTime;
                if (tookLong) {
                    return true
                }
                return false
            }
            return false
        }

        isAnyLongPress() {
            for (let key of this.ended.keys()) {
                if (this.isLongPress(key)) return true
            }
            return false
        }

        isStylus(key) {
            return key === 'stylus'
        }
    }

    /**
     * This class implements the main delegate functionality: All necessary event handlers are registered for the
     * given element. Uses PointerEvents if available or TouchEvents on iOS. The fallback is on mouse events.
     * Collects the events if the interaction target captures the start event (i.e. declares that
     * the target wants the start event as well as all following move and end evcents.)
     *
     * @export
     * @class InteractionDelegate
     */
    class InteractionDelegate {
        // Long press: http://stackoverflow.com/questions/1930895/how-long-is-the-event-onlongpress-in-the-android
        // Stylus support: https://w3c.github.io/touch-events/

        /**
         * Creates an instance of InteractionDelegate.
         * @param {any} element
         * @param {any} target
         * @param {any} [{ mouseWheelElement = null, useCapture = true, capturePointerEvents = true, debug = false }={}]
         * @memberof InteractionDelegate
         */
        constructor(
            element,
            target,
            {
                mouseWheelElement = null,
                useCapture = true,
                capturePointerEvents = true,
                cancelOnWindowOut = true,
                preventPointerClicks = true,
                debug = false
            } = {}
        ) {
            this.debug = debug;
            this.interaction = new Interaction();
            this.element = element;
            this.mouseWheelElement = mouseWheelElement || element;
            this.target = target;
            this.useCapture = useCapture;
            this.capturePointerEvents = capturePointerEvents;
            this.cancelOnWindowOut = cancelOnWindowOut;
            this.preventPointerClicks = preventPointerClicks;
            this.setupInteraction();
        }

        setupInteraction() {
            if (this.debug) {
                let error = this.targetInterface.implementationError(this.target.constructor);
                if (error != null) {
                    throw new Error('Expected IInteractionTarget: ' + error)
                }
            }
            this.setupTouchInteraction();
            this.setupMouseWheelInteraction();
        }

        get targetInterface() {
            return IInteractionTarget
        }

        setupTouchInteraction() {
            let element = this.element;
            let useCapture = this.useCapture;
            if (window.PointerEvent) {
                if (this.debug) console.log('Pointer API' + window.PointerEvent);
                element.addEventListener(
                    'pointerdown',
                    e => {
                        if (this.debug) console.log('pointerdown', e.pointerId);
                        if (this.capture(e)) {
                            if (this.capturePointerEvents) {
                                try {
                                    element.setPointerCapture(e.pointerId);
                                } catch (e) {
                                    console.warn('Cannot setPointerCapture', e.setPointerCapture);
                                }
                            }
                            this.onStart(e);
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'pointermove',
                    e => {
                        if (this.debug) console.log('pointermove', e.pointerId, e.pointerType);

                        if (e.pointerType == 'touch' || (e.pointerType == 'mouse' && Events.isPointerDown(e))) {
                            // this.capture(e) &&
                            if (this.debug) console.log('pointermove captured', e.pointerId);
                            this.onMove(e);
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'pointerup',
                    e => {
                        if (this.debug) console.log('pointerup', e.pointerId, e.pointerType);
                        this.onEnd(e);
                        if (this.capturePointerEvents) {
                            try {
                                element.releasePointerCapture(e.pointerId);
                            } catch (e) {
                                console.warn('Cannot release pointer');
                            }
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'pointercancel',
                    e => {
                        if (this.debug) console.log('pointercancel', e.pointerId, e.pointerType);
                        this.onEnd(e);
                        if (this.capturePointerEvents) element.releasePointerCapture(e.pointerId);
                    },
                    useCapture
                );

                if (!this.capturePointerEvents) {
                    element.addEventListener(
                        'pointerleave',
                        e => {
                            if (this.debug) console.log('pointerleave', e.pointerId, e.pointerType);
                            if (e.target == element) this.onEnd(e);
                        },
                        useCapture
                    );
                }

                if (!this.capturePointerEvents) {
                    element.addEventListener(
                        'pointerout',
                        e => {
                            if (this.debug) console.log('pointerout', e.pointerId, e.pointerType);
                            if (e.target == element) this.onEnd(e);
                        },
                        useCapture
                    );
                }

                if (this.cancelOnWindowOut) {
                    window.addEventListener(
                        'pointerout',
                        e => {
                            if (this.debug) console.log('pointerout', e.pointerId, e.pointerType, e.target);
                            if (e.target == element) {
                                this.onEnd(e);
                            }
                        },
                        useCapture
                    );
                }
            } else if (window.TouchEvent) {
                if (this.debug) console.log('Touch API');
                element.addEventListener(
                    'touchstart',
                    e => {
                        if (this.debug) console.log('touchstart', this.touchPoints(e));
                        if (this.capture(e)) {
                            for (let touch of e.changedTouches) {
                                this.onStart(touch);
                            }
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'touchmove',
                    e => {
                        if (this.debug) console.log('touchmove', this.touchPoints(e), e);
                        for (let touch of e.changedTouches) {
                            this.onMove(touch);
                        }
                        for (let touch of e.targetTouches) {
                            this.onMove(touch);
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'touchend',
                    e => {
                        if (this.debug) console.log('touchend', this.touchPoints(e));
                        for (let touch of e.changedTouches) {
                            this.onEnd(touch);
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'touchcancel',
                    e => {
                        if (this.debug) console.log('touchcancel', e.targetTouches.length, e.changedTouches.length);
                        for (let touch of e.changedTouches) {
                            this.onEnd(touch);
                        }
                    },
                    useCapture
                );
            } else {
                if (this.debug) console.log('Mouse API');

                element.addEventListener(
                    'mousedown',
                    e => {
                        if (this.debug) console.log('mousedown', e);
                        if (this.capture(e)) {
                            this.onStart(e);
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'mousemove',
                    e => {
                        // Dow we only use move events if the mouse is down?
                        // HOver effects have to be implemented by other means
                        // && Events.isMouseDown(e))

                        if (Events.isMouseDown(e)) {
                            if (this.debug) console.log('mousemove', e);
                            this.onMove(e);
                        }
                    },
                    useCapture
                );
                element.addEventListener(
                    'mouseup',
                    e => {
                        if (this.debug) console.log('mouseup', e);
                        this.onEnd(e);
                    },
                    true
                );

                if (!this.capturePointerEvents) {
                    element.addEventListener(
                        'mouseout',
                        e => {
                            if (e.target == element) {
                                this.onEnd(e);
                                console.warn("Shouldn't happen: mouseout ends interaction");
                            }
                        },
                        useCapture
                    );
                }
                if (this.cancelOnWindowOut) {
                    window.addEventListener(
                        'mouseout',
                        e => {
                            if (e.target == element) {
                                this.onEnd(e);
                            }
                        },
                        useCapture
                    );
                }
            }

            if (this.preventPointerClicks) {
                window.addEventListener(
                    'click',
                    e => {
                        if (e instanceof PointerEvent) {
                            console.warn('Prevented pointer click');
                            e.preventDefault();
                            e.stopImmediatePropagation();
                        }
                    },
                    true
                );
            }
        }

        isDescendant(parent, child) {
            if (parent == child) return true
            let node = child.parentNode;
            while (node != null) {
                if (node == parent) {
                    return true
                }
                node = node.parentNode;
            }
            return false
        }

        touchPoints(event) {
            let result = [];
            for (let touch of event.changedTouches) {
                result.push(this.extractPoint(touch));
            }
            return result
        }

        setupMouseWheelInteraction() {
            this.mouseWheelElement.addEventListener('mousewheel', this.onMouseWheel.bind(this), true);
            this.mouseWheelElement.addEventListener('DOMMouseScroll', this.onMouseWheel.bind(this), true);
        }

        onMouseWheel(event) {
            if (this.capture(event) && this.target.onMouseWheel) {
                this.target.onMouseWheel(event);
            }
        }

        onStart(event) {
            let extracted = this.extractPoint(event);
            this.startInteraction(event, extracted);
            this.target.onStart(event, this.interaction);
        }

        onMove(event) {
            let extracted = this.extractPoint(event, 'all');
            this.updateInteraction(event, extracted);
            this.target.onMove(event, this.interaction);
            this.interaction.updatePrevious();
        }

        onEnd(event) {
            let extracted = this.extractPoint(event, 'changedTouches');
            this.endInteraction(event, extracted);
            this.target.onEnd(event, this.interaction);
            this.finishInteraction(event, extracted);
        }

        /**
         * Asks the target whether the event should be captured
         *
         * @param {any} event
         * @returns {bool}
         * @memberof InteractionDelegate
         */
        capture(event) {
            if (Events.isCaptured(event)) {
                return false
            }

            return this.target.capture ? this.target.capture(event) : false
        }

        getPosition(event) {
            return { x: event.clientX, y: event.clientY }
        }

        extractPoint(event, touchEventKey = 'all') {
            // 'targetTouches'
            let result = {};
            switch (event.constructor.name) {
                case 'MouseEvent': {
                    let buttons = event.buttons || event.which;
                    if (buttons) result['mouse'] = this.getPosition(event);
                    break
                }
                case 'PointerEvent': {
                    result[event.pointerId.toString()] = this.getPosition(event);
                    break
                }
                case 'Touch': {
                    let id = event.touchType === 'stylus' ? 'stylus' : event.identifier.toString();
                    result[id] = this.getPosition(event);
                    break
                }
            }
            return result
        }

        interactionStarted(event, key, point) {
            // Callback: can be overwritten
        }

        interactionEnded(event, key, point) {
            // Callback: can be overwritten
        }

        interactionFinished(event, key, point) {}

        startInteraction(event, extracted) {
            for (let key in extracted) {
                let point = extracted[key];
                this.interaction.started(key, point);
                this.interactionStarted(event, key, point);
            }
        }

        updateInteraction(event, extracted) {
            for (let key in extracted) {
                let point = extracted[key];
                let updated = this.interaction.update(key, point);
                if (updated) {
                    console.warn("new pointer in updateInteraction shouldn't happen", key);
                    this.interactionStarted(event, key, point);
                }
            }
        }

        endInteraction(event, ended) {
            for (let key in ended) {
                let point = ended[key];
                this.interaction.stop(key, point);
                this.interactionEnded(event, key, point);
            }
        }

        finishInteraction(event, ended) {
            for (let key in ended) {
                let point = ended[key];
                this.interaction.finish(key, point);
                this.interactionFinished(event, key, point);
            }
        }
    }
    /**
     * A special InteractionDelegate that maps events to specific parts of
     * the interaction target. The InteractionTarget must implement a findTarget
     * method that returns an object implementing the IInteractionTarget interface.
     *
     * If the InteractionTarget also implements a mapPositionToPoint method this
     * is used to map the points to the local coordinate space of the the target.
     *
     * This makes it easier to lookup elements and relate events to local
     * positions.
     *
     * @export
     * @class InteractionMapper
     * @extends {InteractionDelegate}
     */
    class InteractionMapper extends InteractionDelegate {
        constructor(
            element,
            target,
            {
                tapDistance = 10,
                longPressTime = 500.0,
                useCapture = true,
                capturePointerEvents = true,
                mouseWheelElement = null,
                logInteractionsAbove = 12
            } = {}
        ) {
            super(element, target, {
                tapDistance,
                useCapture,
                capturePointerEvents,
                longPressTime,
                mouseWheelElement
            });
            this.logInteractionsAbove = logInteractionsAbove;
        }

        get targetInterface() {
            return IInteractionMapperTarget
        }

        mapPositionToPoint(point, element = null) {
            if (this.target.mapPositionToPoint) {
                return this.target.mapPositionToPoint(point, element)
            }
            return point
        }

        interactionStarted(event, key, point) {
            if (this.target.findTarget) {
                let local = this.mapPositionToPoint(point);
                let found = this.target.findTarget(event, local, point);
                if (found != null) {
                    this.interaction.addTarget(key, found);
                }
            }
            let size = this.interaction.current.size;
            let limit = this.logInteractionsAbove;
            if (size > limit) {
                Logging.log(`Number of interactions ${size} exceeds ${limit}`);
            }
        }

        onMouseWheel(event) {
            if (this.capture(event)) {
                if (this.target.findTarget) {
                    let point = this.getPosition(event);
                    let local = this.mapPositionToPoint(point);
                    let found = this.target.findTarget(event, local, point);
                    if (found != null && found.onMouseWheel) {
                        found.onMouseWheel(event);
                        return
                    }
                }
                if (this.target.onMouseWheel) {
                    this.target.onMouseWheel(event);
                }
            }
        }

        onStart(event) {
            let extracted = this.extractPoint(event);
            this.startInteraction(event, extracted);
            let mapped = this.interaction.mapInteraction(
                extracted,
                ['current', 'start'],
                this.mapPositionToPoint.bind(this)
            );
            for (let [target, interaction] of mapped.entries()) {
                target.onStart(event, interaction);
            }
        }

        onMove(event) {
            let extracted = this.extractPoint(event, 'all');
            this.updateInteraction(event, extracted);
            let mapped = this.interaction.mapInteraction(
                extracted,
                ['current', 'previous'],
                this.mapPositionToPoint.bind(this)
            );
            for (let [target, interaction] of mapped.entries()) {
                target.onMove(event, interaction);
                interaction.updatePrevious();
            }
            this.interaction.updatePrevious();
        }

        onEnd(event) {
            let extracted = this.extractPoint(event, 'changedTouches');
            this.endInteraction(event, extracted);
            let mapped = this.interaction.mapInteraction(extracted, ['ended'], this.mapPositionToPoint.bind(this));
            for (let [target, interaction] of mapped.entries()) {
                target.onEnd(event, interaction);
            }
            this.finishInteraction(event, extracted);
        }

        /**
         *
         *
         * @static
         * @param {string|array} types - An event type, an array of event types or event types seperated by a space sign. The following
         *     events are possible:
         *         pan, panstart, panmove, panend, pancancel, panleft, panright, panup, pandown
         *         pinch, pinchstart, pinchmove, pinchend, pinchcancel, pinchin, pinchout
         *         press, pressup
         *         rotate, rotatestart, rotatemove, rotateend, rotatecancel
         *         swipe, swipeleft, swiperight, swipeup, swipedown
         *         tap
         * @param {HTMLElement|HTMLElement[]} elements - An HTML element or an array of HTML elements.
         * @param {function} [cb] - The callback. A function which is executed after the event occurs. Receives the event object as the
         *     first paramter
         * @param {object} [opts] - An options object. See the hammer documentation for more details.
         */
        static on(types, elements, cb, opts = {}) {
            opts = Object.assign({}, {}, opts);

            if (typeof Hammer === 'undefined') {
                console.error('Hammer.js not found!');
                return this
            }

            if (typeof Hammer.__hammers === 'undefined') {
                Hammer.__hammers = new Map();
            }

            // convert to array
            types = Array.isArray(types) ? types : types.split(/\s/);
            if (elements instanceof NodeList || elements instanceof HTMLCollection) {
                elements = Array.from(elements);
            }
            elements = Array.isArray(elements) ? elements : [elements];

            for (let i = 0; i < types.length; i++) {
                const type = types[i].toLowerCase();

                // list of hammer events
                const useHammer = /^(tap|doubletap|press|pan|swipe|pinch|rotate).*$/.test(type);

                // if it is a hammer event
                if (useHammer) {
                    for (let j = 0; j < elements.length; j++) {
                        // if(elements[j].tagName == "svg") return false;

                        let hammer = new Hammer(elements[j], opts);

                        if (window.propagating !== 'undefined') {
                            hammer = propagating(hammer);
                        }

                        // recognizers
                        if (type.startsWith('pan')) {
                            hammer.get('pan').set(Object.assign({ direction: Hammer.DIRECTION_ALL }, opts));
                        } else if (type.startsWith('pinch')) {
                            hammer.get('pinch').set(Object.assign({ enable: true }, opts));
                        } else if (type.startsWith('press')) {
                            hammer.get('press').set(opts);
                        } else if (type.startsWith('rotate')) {
                            hammer.get('rotate').set(Object.assign({ enable: true }, opts));
                        } else if (type.startsWith('swipe')) {
                            hammer.get('swipe').set(Object.assign({ direction: Hammer.DIRECTION_ALL }, opts));
                        } else if (type.startsWith('tap')) {
                            hammer.get('tap').set(opts);
                        }

                        hammer.on(type, event => {
                            cb(event);
                        });

                        if (Hammer.__hammers.has(elements[j])) {
                            const elementHammers = Hammer.__hammers.get(elements[j]);
                            elementHammers.push(hammer);
                            Hammer.__hammers.set(elements[j], elementHammers);
                        } else {
                            Hammer.__hammers.set(elements[j], [hammer]);
                        }
                    }
                } else {
                    for (let j = 0; j < elements.length; j++) {
                        Hammer.on(elements[j], type, event => {
                            cb(event);
                        });
                    }
                }
            }
        }

        /**
         *
         *
         * @static
         * @param {HTMLElement|HTMLElement[]} elements - An HTML element or an array of HTML elements.
         */
        static off(elements) {
            if (typeof Hammer === 'undefined') {
                console.error('Hammer.js not found!');
                return this
            }

            // convert to array
            if (elements instanceof NodeList || elements instanceof HTMLCollection) {
                elements = Array.from(elements);
            }
            elements = Array.isArray(elements) ? elements : [elements];

            for (let i = 0; i < elements.length; i++) {
                const element = elements[i];

                if (Hammer.__hammers.has(element)) {
                    const elementHammers = Hammer.__hammers.get(element);
                    elementHammers.forEach(it => it.destroy());
                    Hammer.__hammers.delete(element);
                }
            }
        }
    }

    window.InteractionMapper = InteractionMapper;

    /** Report capabilities with guaranteed values.
     */
    class Capabilities {
        /** Returns the browser userAgent.
        @return {string}
        */
        static get userAgent() {
            return navigator.userAgent || 'Unknown Agent'
        }

        /** Tests whether the app is running on a mobile device.
        Implemented as a readonly attribute.
        @return {boolean}
        */
        static get isMobile() {
            return /Mobi/.test(navigator.userAgent)
        }

        /** Tests whether the app is running on a iOS device.
        Implemented as a readonly attribute.
        @return {boolean}
        */
        static get isIOS() {
            return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream
        }

        /** Tests whether the app is running in a Safari environment.
        See https://stackoverflow.com/questions/7944460/detect-safari-browser
        Implemented as a readonly attribute.
        @return {boolean}
        */
        static get isSafari() {
            return (
                navigator.vendor &&
                navigator.vendor.indexOf('Apple') > -1 &&
                navigator.userAgent &&
                !navigator.userAgent.match('CriOS')
            )
        }

        /**
         * Distincts if the app is running inside electron or not.
         *
         * source: https://github.com/cheton/is-electron
         */
        static get isElectron() {
            // Renderer process
            if (typeof window !== 'undefined' && typeof window.process === 'object' && window.process.type === 'renderer') {
                return true
            }

            // Main process
            if (typeof process !== 'undefined' && typeof process.versions === 'object' && !!process.versions.electron) {
                return true
            }

            // Detect the user agent when the `nodeIntegration` option is set to true
            if (
                typeof navigator === 'object' &&
                typeof navigator.userAgent === 'string' &&
                navigator.userAgent.indexOf('Electron') >= 0
            ) {
                return true
            }

            return false
        }

        /** Returns the display resolution. Necessary for retina displays.
        @return {number}
        */
        static get devicePixelRatio() {
            return window.devicePixelRatio || 1
        }

        /** Returns true if the device is a multi-touch table. This method is currently not universal usable and not sure!
        @return {boolean}
        */
        static get isMultiTouchTable() {
            return (
                Capabilities.devicePixelRatio > 2 &&
                Capabilities.isMobile === false &&
                /Windows/i.test(Capabilities.userAgent)
            )
        }

        /** Returns true if mouse events are supported
        @return {boolean}
        */
        static supportsMouseEvents() {
            return typeof window.MouseEvent != 'undefined'
        }

        /** Returns true if touch events are supported
        @return {boolean}
        */
        static supportsTouchEvents() {
            return typeof window.TouchEvent != 'undefined'
        }

        /** Returns true if pointer events are supported
        @return {boolean}
        */
        static supportsPointerEvents() {
            return typeof window.PointerEvent != 'undefined'
        }

        /** Returns true if DOM templates are supported
        @return {boolean}
        */
        static supportsTemplate() {
            return 'content' in document.createElement('template')
        }
    }

    /** Basic tests for Capabilities.
     */
    class CapabilitiesTests {
        static testConfirm() {
            let bool = confirm('Please confirm');
            document.getElementById('demo').innerHTML = bool ? 'Confirmed' : 'Not confirmed';
        }

        static testPrompt() {
            let person = prompt('Please enter your name', 'Harry Potter');
            if (person != null) {
                demo.innerHTML = 'Hello ' + person + '! How are you today?';
            }
        }

        static testUserAgent() {
            let agent = 'User-agent: ' + Capabilities.userAgent;
            user_agent.innerHTML = agent;
        }

        static testDevicePixelRatio() {
            let value = 'Device Pixel Ratio: ' + Capabilities.devicePixelRatio;
            device_pixel_ratio.innerHTML = value;
        }

        static testMultiTouchTable() {
            let value = 'Is the device a multi-touch table? ' + Capabilities.isMultiTouchTable;
            multi_touch_table.innerHTML = value;
        }

        static testSupportedEvents() {
            let events = [];
            if (Capabilities.supportsMouseEvents()) {
                events.push('MouseEvents');
            }
            if (Capabilities.supportsTouchEvents()) {
                events.push('TouchEvents');
            }
            if (Capabilities.supportsPointerEvents()) {
                events.push('PointerEvents');
            }
            supported_events.innerHTML = 'Supported Events: ' + events.join(', ');
        }

        static testAll() {
            this.testUserAgent();
            this.testDevicePixelRatio();
            this.testMultiTouchTable();
            this.testSupportedEvents();
        }
    }

    /* Optional global variables, needed in DocTests. */
    window.Capabilities = Capabilities;
    window.CapabilitiesTests = CapabilitiesTests;

    /* eslint-disable no-unused-vars */

    /**
     * A base class for scatter specific events.
     *
     * @constructor
     * @param {name} String - The name of the event
     * @param {target} Object - The target of the event
     */
    class BaseEvent {
        constructor(name, target) {
            this.name = name;
            this.target = target;
        }
    }

    // Event types
    const START = 'onStart';
    const UPDATE = 'onUpdate';
    const END = 'onEnd';

    /**
     * A scatter event that describes how the scatter has changed.
     *
     * @constructor
     * @param {target} Object - The target scatter of the event
     * @param {optional} Object - Optional parameter
     */
    class ScatterEvent extends BaseEvent {
        constructor(
            target,
            { translate = { x: 0, y: 0 }, scale = null, rotate = 0, about = null, fast = false, type = null } = {}
        ) {
            super('scatterTransformed', { target: target });
            this.translate = translate;
            this.scale = scale;
            this.rotate = rotate;
            this.about = about;
            this.fast = fast;
            this.type = type;
        }

        toString() {
            return (
                "Event('scatterTransformed', scale: " + this.scale + ' about: ' + this.about.x + ', ' + this.about.y + ')'
            )
        }
    }

    /**
     * A scatter resize event that describes how the scatter has changed.
     *
     * @constructor
     * @param {target} Object - The target scatter of the event
     * @param {optional} Object - Optional parameter
     */
    class ResizeEvent extends BaseEvent {
        constructor(target, { width = 0, height = 0 } = {}) {
            super('scatterResized', { width: width, height: height });
            this.width = width;
            this.height = height;
        }

        toString() {
            return 'Event(scatterResized width: ' + this.width + 'height: ' + this.height + ')'
        }
    }

    /**
     * A abstract base class that implements the throwable behavior of a scatter
     * object.
     *
     * @constructor
     */


    class RequestFrameThrower {
        /** Implemenents the standard throw behavior. */
        animateThrow(throwable) {
            /*** Calls the animateThrow method in sync with the display link. */
            requestAnimationFrame(throwable.animateThrow.bind(throwable));
        }
    }

    class TimeoutThrower {

        constructor(delay=20) {
            this.throwables = new Set();
            this.delay = delay;
            setTimeout(this.animateStep.bind(this), this.delay);
        }

        animateThrow(throwable) {
            this.throwables.add(throwable);
        }

        animateStep() {
            let active = [...this.throwables];
            this.throwables.clear();
            for(let throwable of active) {
                throwable.animateThrow();
            }
            setTimeout(this.animateStep.bind(this), this.delay);
        }
    }

    let thrower = new RequestFrameThrower();

    class Throwable {
        constructor({
            movableX = true,
            movableY = true,
            throwVisibility = 44,
            throwDamping = 0.95,
            autoThrow = true,
            onThrowFinished = null
        } = {}) {
            this.movableX = movableX;
            this.movableY = movableY;
            this.throwVisibility = throwVisibility;
            this.throwDamping = throwDamping;
            this.autoThrow = autoThrow;
            this.velocities = [];
            this.velocity = null;
            this.lastframe = null;
            this.onThrowFinished = onThrowFinished;
        }

        static defaultThrow() {
            thrower = new RequestFrameThrower();
        }

        static timeoutThrow() {
            thrower = new TimeoutThrower();
        }

        observeVelocity() {
            this.lastframe = performance.now();
        }

        addVelocity(delta, buffer = 5) {
            let t = performance.now();
            let dt = t - this.lastframe;
            this.lastframe = t;

            // When number is not set or is zero. Use one instead.
            let number = delta.number ? delta.number : 1;
            if (dt > 0) {
                // Avoid division by zero errors later on
                // and consider the number of involved pointers sind addVelocity will be called by the
                // onMove events
                let velocity = {
                    t: t,
                    dt: dt,
                    dx: delta.x / number,
                    dy: delta.y / number
                };
                this.velocities.push(velocity);
                while (this.velocities.length > buffer) {
                    this.velocities.shift();
                }
            }
        }

        addTestVelocity(delta, dt=20, buffer = 5) {
            let t = performance.now();
            this.lastframe = t;
            let velocity = {
                t: t,
                dt: dt,
                dx: delta.x ,
                dy: delta.y 
            };
            for(let i=0; i<buffer; i++) {
                velocity.t += dt;
                this.velocities.push(velocity);
            }
        }

        meanVelocity(milliseconds = 30) {
         //   this.addVelocity({ x: 0, y: 0, number: 1 })
            let sum = { x: 0, y: 0 };
            let count = 0;
            let t = 0;
            for (let i = this.velocities.length - 1; i > 0; i--) {
                let v = this.velocities[i];
                t += v.dt;
                let nv = { x: v.dx / v.dt, y: v.dy / v.dt };
                sum = Points.add(sum, nv);
                count += 1;
                if (t > milliseconds) {
                    break
                }
            }
            if (count === 0) return sum // empty vector
            return Points.multiplyScalar(sum, 1 / count)
        }

        killAnimation() {
            this.velocity = null;
            this.velocities = [];
            this.lastframe = null;
        }

        startThrow() {
            this.velocity = this.meanVelocity();
            if (this.velocity != null) {
                // Call next velocity to ansure that specializations
                // that use keepOnStage are called
                this.velocity = this.nextVelocity(this.velocity);
                if (this.autoThrow) this.animateThrow(performance.now());
            } else {
                this.onDragComplete();
            }
        }

        _throwDeltaTime() {
            let t = performance.now();
            let dt = t - this.lastframe;
            this.lastframe = t;
            return dt
        }

        recurseAnimateThrow() {
            ThrowableObjects.add(this);
        }

        animateThrow(time) {
            if (this.velocity != null) {
                let dt = this._throwDeltaTime();
                // console.log("animateThrow", dt)
                let next = this.nextVelocity(this.velocity);
                let prevLength = Points.length(this.velocity);
                let nextLength = Points.length(next);
                if (nextLength > prevLength) {
                    let factor = nextLength / prevLength;
                    next = Points.multiplyScalar(next, 1 / factor);
                    // console.log('Prevent acceleration', factor, this.velocity, next)
                }
                this.velocity = next;
                let d = Points.multiplyScalar(this.velocity, dt);
                this._move(d);

                this.onDragUpdate(d);
                if (dt == 0 || this.needsAnimation()) {
                    thrower.animateThrow(this);
                    return
                } else {
                    if (this.isOutside()) {
                        thrower.animateThrow(this);
                        return
                    }
                }
            }
            this.onDragComplete();
            if (this.onThrowFinished != null) {
                this.onThrowFinished();
            }
        }

        needsAnimation() {
            if (this.velocity == null) {
                return false
            }
            return Points.length(this.velocity) > 0.01
        }

        nextVelocity(velocity) {
            // Must be overwritten: computes the changed velocity. Implement
            // damping, collison detection, etc. here
            let next = Points.multiplyScalar(velocity, this.throwDamping);
            return {
                x: this.movableX ? next.x : 0,
                y: this.movableY ? next.y : 0
            }
        }

        _move(delta) {
            // Overwrite if necessary
        }

        onDragComplete() {
            // Overwrite if necessary
        }

        onDragUpdate(delta) {
            // Overwrite if necessary
        }
    }

    class AbstractScatter extends Throwable {
        constructor({
            minScale = 0.1,
            maxScale = 1.0,
            startScale = 1.0,
            autoBringToFront = true,
            autoThrow = true,
            translatable = true,
            scalable = true,
            rotatable = true,
            resizable = false,
            movableX = true,
            movableY = true,
            throwVisibility = 44,
            throwDamping = 0.95,
            overdoScaling = 1,
            mouseZoomFactor = 1.1,
            rotationDegrees = null,
            rotation = null,
            onTransform = null,
            interactive = true,
            onClose = null,
            onThrowFinished = null,
            scaleAutoClose = false,
            scaleCloseThreshold = 0.1,
            scaleCloseBuffer = 0.05,
            maxRotation = Angle.degree2radian(5),
            minInteractionDistance = 0,
            useLowPassFilter = false
        } = {}) {
            if (rotationDegrees != null && rotation != null) {
                throw new Error('Use rotationDegrees or rotation but not both')
            } else if (rotation != null) {
                rotationDegrees = Angle.radian2degree(rotation);
            } else if (rotationDegrees == null) {
                rotationDegrees = 0;
            }
            super({
                movableX,
                movableY,
                throwVisibility,
                throwDamping,
                autoThrow,
                onThrowFinished
            });

            /**
             * Closes the card when the minScale is reached and the
             * card is released. Card can be saved by scaling it up again.
             */
            this.scaleAutoClose = scaleAutoClose;
            this.scaleCloseThreshold = scaleCloseThreshold;
            this.scaleCloseBuffer = scaleCloseBuffer;
            this.scaleAutoCloseTimeout = null;

            this.interactive = interactive;
            this.startRotationDegrees = rotationDegrees;
            this.startScale = startScale; // Needed to reset object
            this.minScale = minScale;
            this.maxScale = maxScale;
            this.maxRotation = maxRotation;
            this.overdoScaling = overdoScaling;
            this.translatable = translatable;
            if (!translatable) {
                this.movableX = false;
                this.movableY = false;
            }
            this.scalable = scalable;
            this.rotatable = rotatable;
            this.resizable = resizable;
            this.mouseZoomFactor = mouseZoomFactor;
            this.autoBringToFront = autoBringToFront;
            this.useLowPassFilter = useLowPassFilter;
            this.minInteractionDistance = minInteractionDistance;
            if (useLowPassFilter) {
                this.rotateLPF = new LowPassFilter();
                this.zoomLPF = new LowPassFilter();
                this.zoomLPF.setup([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
            }
            this.dragging = false;
            this.onTransform = onTransform != null ? [onTransform] : null;
            this.onClose = onClose != null ? [onClose] : null;
        }

        addCloseEventCallback(callback) {
            if (this.onClose == null) {
                this.onClose = [];
            }
            this.onClose.push(callback);
        }

        addTransformEventCallback(callback) {
            if (this.onTransform == null) {
                this.onTransform = [];
            }
            this.onTransform.push(callback);
        }

        startGesture(interaction) {
            this.bringToFront();
            this.killAnimation();
            this.observeVelocity();
            if (this.useLowPassFilter) {
                this.rotateLPF.clear();
                this.zoomLPF.clear();
                this.zoomLPF.setup([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
            }
            return true
        }

        close() {
            this._callCloseCallbacks();
            this._removeCallbacks();
            this._removeSelfFromScatterContainer();
        }

        _callCloseCallbacks() {
            if (this.onClose) {
                this.onClose.forEach(callback => callback(this));
            }
        }

        _removeCallbacks() {
            this.onClose = [];
            this.onTransform = [];
        }

        _removeSelfFromScatterContainer() {
            // Removes self from container when it's closed.
            if (this.container) {
                this.container.remove(this);
            }
        }

        gesture(interaction) {
            let delta = interaction.delta();

            if (delta != null) {
                /* uo: Is this the best place to add velocity? It works with scrollable text in card drawers but
                has to be tested */
                this.addVelocity(delta);
                let rotate = delta.rotate;
                let zoom = delta.zoom;
                if (this.maxRotation != null) {
                    if (Math.abs(rotate) > this.maxRotation) {
                        rotate = 0;
                    }
                }
                if (this.useLowPassFilter) {
                    rotate = this.rotateLPF.next(rotate);
                    zoom = this.zoomLPF.next(zoom);
                }
                if (delta.distance < this.minInteractionDistance) {
                    let ratio = delta.distance / this.minInteractionDistance;
                    rotate *= ratio;
                    let zoomDelta = zoom - 1;
                    zoomDelta *= ratio;
                    zoom = 1 + zoomDelta;
                }
                this.transform(delta, zoom, rotate, delta.about);
                /* uo: This is too late an dangerous. transform sometimes modifies delta
                this.addVelocity(delta) // uo: reverted commit fa0256d782dd498c6d3e51321260ca375ca9f855
                */

                if (zoom != 1) this.interactionAnchor = delta.about;
            }
        }

        get polygon() {
            let w2 = (this.width * this.scale) / 2;
            let h2 = (this.height * this.scale) / 2;
            let center = this.center;
            let polygon = new Polygon(center);
            polygon.addPoint({ x: -w2, y: -h2 });
            polygon.addPoint({ x: w2, y: -h2 });
            polygon.addPoint({ x: w2, y: h2 });
            polygon.addPoint({ x: -w2, y: h2 });
            polygon.rotate(this.rotation);
            return polygon
        }

        isOutside() {
            let stagePolygon = this.containerPolygon;
            if (stagePolygon == null) return false
            let polygon = this.polygon;
            if (polygon == null) return false
            let result = stagePolygon.intersectsWith(polygon);
            return result === false || result.overlap < this.throwVisibility
        }

        recenter() {
            // Return a small vector that guarantees that the scatter is moving
            // towards the center of the stage
            let center = this.center;
            let target = this.container.center;
            let delta = Points.subtract(target, center);
            return Points.normalize(delta)
        }

        nextVelocity(velocity) {
            return this.keepOnStage(velocity)
        }

        bouncing() {
            // Implements the bouncing behavior of the scatter. Moves the scatter
            // to the center of the stage if the scatter is outside the stage or
            // not within the limits of the throwVisibility.

            let stagePolygon = this.containerPolygon;
            let polygon = this.polygon;
            let result = stagePolygon.intersectsWith(polygon);
            if (result === false || result.overlap < this.throwVisibility) {
                let cv = this.recenter();
                let recentered = false;
                while (result === false || result.overlap < this.throwVisibility) {
                    polygon.center.x += cv.x;
                    polygon.center.y += cv.y;
                    this._move(cv);
                    result = stagePolygon.intersectsWith(polygon);
                    recentered = true;
                }
                return recentered
            }
            return false
        }

        keepOnStage(velocity, collision = 0.5) {
            let stagePolygon = this.containerPolygon;
            // UO: since keepOnStage is called in nextVelocity we need to
            // ensure a return value
            if (!stagePolygon) return { x: 0, y: 0 }
            this.polygon;
            let bounced = this.bouncing();
            if (bounced) {
                let stage = this.containerBounds;
                let x = this.center.x;
                let y = this.center.y;
                let dx = this.movableX ? velocity.x : 0;
                let dy = this.movableY ? velocity.y : 0;
                let factor = this.throwDamping;
                // if (recentered) {
                if (x < 0) {
                    dx = -dx;
                    factor = collision;
                }
                if (x > stage.width) {
                    dx = -dx;
                    factor = collision;
                }
                if (y < 0) {
                    dy = -dy;
                    factor = collision;
                }
                if (y > stage.height) {
                    dy = -dy;
                    factor = collision;
                }
                // }
                return Points.multiplyScalar({ x: dx, y: dy }, factor)
            }
            return super.nextVelocity(velocity)
        }

        endGesture(interaction) {
            this.startThrow();
            this._checkAutoClose();
        }

        _checkAutoClose() {
            if (this.scaleAutoClose)
                if (this.scale < this.minScale + this.scaleCloseThreshold - this.scaleCloseBuffer) {
                    this.zoom(this.minScale, {
                        animate: 0.2,
                        onComplete: this.close.bind(this)
                    });
                } else if (this.scale < this.minScale + this.scaleCloseThreshold) {
                    this.zoom(this.minScale + this.scaleCloseThreshold, {
                        animate: 0.4
                    });
                }
        }

        rotateDegrees(degrees, anchor) {
            let rad = Angle.degree2radian(degrees);
            this.rotate(rad, anchor);
        }

        rotate(rad, anchor) {
            this.transform({ x: 0, y: 0 }, 1.0, rad, anchor);
        }

        move(d, { animate = 0 } = {}) {
            if (this.translatable) {
                if (animate > 0) {
                    let startPos = this.position;
                    TweenLite.to(this, animate, {
                        x: '+=' + d.x,
                        y: '+=' + d.y,
                        /* scale: scale, uo: not defined, why was this here? */
                        onUpdate: e => {
                            let p = this.position;
                            let dx = p.x - startPos.x;
                            let dy = p.x - startPos.y;
                            this.onMoved(dx, dy);
                        }
                    });
                } else {
                    this._move(d);
                    this.onMoved(d.x, d.y);
                }
            }
        }

        moveTo(p, { animate = 0 } = {}) {
            let c = this.origin;
            let delta = Points.subtract(p, c);
            this.move(delta, { animate: animate });
        }

        centerAt(p, { animate = 0 } = {}) {
            let c = this.center;
            let delta = Points.subtract(p, c);
            this.move(delta, { animate: animate });
        }

        zoom(scale, { animate = 0, about = null, delay = 0, x = null, y = null, onComplete = null } = {}) {
            let anchor = about || this.center;
            if (scale != this.scale) {
                if (animate > 0) {
                    TweenLite.to(this, animate, {
                        scale: scale,
                        delay: delay,
                        onComplete: onComplete,
                        onUpdate: this.onZoomed.bind(this)
                    });
                } else {
                    this.scale = scale;
                    this.onZoomed(anchor);
                }
            }
        }

        _move(delta) {
            // this.addVelocity(delta) uo: reverted commit fa0256d782dd498c6d3e51321260ca375ca9f855
            this.x += this.movableX ? delta.x : 0;
            this.y += this.movableX ? delta.y : 0;
        }

        transform(translate, zoom, rotate, anchor) {
            let delta = {
                x: this.movableX ? translate.x : 0,
                y: this.movableY ? translate.y : 0
            };
            if (this.resizable) var vzoom = zoom;
            if (!this.translatable) delta = { x: 0, y: 0 };
            if (!this.rotatable) rotate = 0;
            if (!this.scalable) zoom = 1.0;
            if (zoom == 1.0 && rotate == 0) {
                this._move(delta);
                if (this.onTransform != null) {
                    let event = new ScatterEvent(this, {
                        translate: delta,
                        scale: this.scale,
                        rotate: 0,
                        about: anchor,
                        fast: false,
                        type: UPDATE
                    });
                    this.onTransform.forEach(function(f) {
                        f(event);
                    });
                }
                return
            }
            let origin = this.rotationOrigin;
            let beta = Points.angle(origin, anchor);
            let distance = Points.distance(origin, anchor);
            let { scale: newScale, zoom: thresholdedZoom } = this.calculateScale(zoom);

            let newOrigin = Points.arc(anchor, beta + rotate, distance * thresholdedZoom);
            let extra = Points.subtract(newOrigin, origin);
            const anchorOffset = Points.subtract(anchor, origin);
            // this._move(offset)
            this.scale = newScale;
            this.rotation += rotate;
            let offset = Points.negate(anchorOffset);
            offset = Points.add(offset, extra);
            offset = Points.add(offset, translate);
            this._move(Points.add(anchorOffset, offset));

            delta.x += extra.x;
            delta.y += extra.y;
            if (this.onTransform != null) {
                let event = new ScatterEvent(this, {
                    translate: delta,
                    scale: newScale,
                    rotate: rotate,
                    about: anchor
                });
                this.onTransform.forEach(function(f) {
                    f(event);
                });
            }
            if (this.resizable) {
                this.resizeAfterTransform(vzoom);
            }
        }

        /**
         * For a given zoom, a new scale is calculated, taking
         * min and max scale into account.
         *
         * @param {number} zoom - The zoom factor, to scale the object with.
         * @returns {object} - Returns an object containing the a value for a valid scale and the corrected zoom factor.
         */
        calculateScale(zoom) {
            let scale = this.scale * zoom;

            let minScale = this.minScale / this.overdoScaling;
            let maxScale = this.maxScale * this.overdoScaling;
            if (scale < minScale) {
                scale = minScale;
                zoom = scale / this.scale;
            }
            if (scale > maxScale) {
                scale = maxScale;
                zoom = scale / this.scale;
            }

            if (this.scaleAutoClose) this._updateTransparency();

            return { zoom, scale }
        }

        _updateTransparency() {
            if (this.scale < this.minScale + this.scaleCloseThreshold) {
                let transparency = this.calculateScaleTransparency();
                this.element.style.opacity = transparency;
            } else this.element.style.opacity = 1;
        }

        calculateScaleTransparency() {
            let transparency = (this.scale - this.minScale) / this.scaleCloseThreshold;
            transparency = transparency > 1 ? 1 : transparency < 0 ? 0 : transparency;
            return transparency
        }

        resizeAfterTransform(zoom) {
            // Overwrite this in subclasses.
        }

        validScale(scale) {
            scale = Math.max(scale, this.minScale);
            scale = Math.min(scale, this.maxScale);
            return scale
        }

        animateZoomBounce(dt = 1) {
            if (this.zoomAnchor != null) {
                let zoom = 1;
                let amount = Math.min(0.01, (0.3 * dt) / 100000.0);
                if (this.scale < this.minScale) zoom = 1 + amount;
                if (this.scale > this.maxScale) zoom = 1 - amount;
                if (zoom != 1) {
                    this.transform({ x: 0, y: 0 }, zoom, 0, this.zoomAnchor);
                    requestAnimationFrame(dt => {
                        this.animateZoomBounce(dt);
                    });
                    return
                }
                this.zoomAnchor = null;
            }
        }

        checkScaling(about, delay = 0) {
            this.zoomAnchor = about;
            clearTimeout(this.animateZoomBounce.bind(this));
            setTimeout(this.animateZoomBounce.bind(this), delay);
        }

        onMouseWheel(event) {
            if (event.claimedByScatter) {
                if (event.claimedByScatter != this) return
            }
            this.killAnimation();
            this.targetScale = null;
            let direction = event.detail < 0 || event.wheelDelta > 0;
            let globalPoint = { x: event.clientX, y: event.clientY };
            let centerPoint = this.mapPositionToContainerPoint(globalPoint);
            if (event.shiftKey) {
                let degrees = direction ? 5 : -5;
                let rad = Angle.degree2radian(degrees);
                return this.transform({ x: 0, y: 0 }, 1.0, rad, centerPoint)
            }
            const zoomFactor = this.mouseZoomFactor;
            let zoom = direction ? zoomFactor : 1 / zoomFactor;
            this.transform({ x: 0, y: 0 }, zoom, 0, centerPoint);
            this.checkScaling(centerPoint, 200);

            if (this.scaleAutoClose) {
                if (this.scale <= this.minScale + this.scaleCloseThreshold) {
                    if (this.scaleAutoCloseTimeout) clearTimeout(this.scaleAutoCloseTimeout);
                    this.scaleAutoCloseTimeout = setTimeout(() => {
                        this._checkAutoClose();
                    }, 600);
                }
                this._updateTransparency();
            }
        }

        onStart(event, interaction) {
            if (this.startGesture(interaction)) {
                this.dragging = true;
                this.interactionAnchor = null;
            }
            if (this.onTransform != null) {
                let event = new ScatterEvent(this, {
                    translate: { x: 0, y: 0 },
                    scale: this.scale,
                    rotate: 0,
                    about: null,
                    fast: false,
                    type: START
                });
                this.onTransform.forEach(function(f) {
                    f(event);
                });
            }
        }

        onMove(event, interaction) {
            /** As long as mouseout && mouseleave interrupt we cannot be sure that
             * dragging remains correct.
             */
            if (this.dragging) {
                this.gesture(interaction);
            }
        }

        onEnd(event, interaction) {
            //console.log('Scatter.onEnd', this.dragging)
            if (interaction.isFinished()) {
                this.endGesture(interaction);
                this.dragging = false;
                for (let key of interaction.ended.keys()) {
                    if (interaction.isTap(key)) {
                        //console.log('Scatter.isTap')
                        let point = interaction.ended.get(key);
                        this.onTap(event, interaction, point);
                    }
                }
                if (this.onTransform != null) {
                    let event = new ScatterEvent(this, {
                        translate: { x: 0, y: 0 },
                        scale: this.scale,
                        rotate: 0,
                        about: null,
                        fast: false,
                        type: END
                    });
                    this.onTransform.forEach(function(f) {
                        f(event);
                    });
                }
            }
            let about = this.interactionAnchor;
            if (about != null) {
                this.checkScaling(about, 100);
            }
        }

        //onTap(event, interaction, point) {}

        onTap(event, interaction, point) {
            //console.log('AbstractScatter.onTap', this.tapDelegate, interaction)
            if (this.tapDelegate) {
                Events.stop(event);
                this.tapDelegate.tap(event, 'scatter');
            }
        }

        onDragUpdate(delta) {
            if (this.onTransform != null) {
                let event = new ScatterEvent(this, {
                    fast: true,
                    translate: delta,
                    scale: this.scale,
                    about: this.currentAbout,
                    type: null
                });
                this.onTransform.forEach(function(f) {
                    f(event);
                });
            }
        }

        onDragComplete() {
            if (this.onTransform) {
                let event = new ScatterEvent(this, {
                    scale: this.scale,
                    about: this.currentAbout,
                    fast: false,
                    type: null
                });
                this.onTransform.forEach(function(f) {
                    f(event);
                });
            }
        }

        onMoved(dx, dy, about) {
            if (this.onTransform != null) {
                let event = new ScatterEvent(this, {
                    translate: { x: dx, y: dy },
                    about: about,
                    fast: true,
                    type: null
                });
                this.onTransform.forEach(function(f) {
                    f(event);
                });
            }
        }

        onResizing() {
            if (this.onTransform != null) {
                let event = new ScatterEvent(this, {
                    scale: this.scale,
                    fast: false,
                    type: null
                });
                this.onTransform.forEach(function(f) {
                    f(event);
                });
            }
        }

        onZoomed(about) {
            if (this.scaleAutoClose) this._updateTransparency();

            if (this.onTransform != null) {
                let event = new ScatterEvent(this, {
                    scale: this.scale,
                    about: about,
                    fast: false,
                    type: null
                });
                this.onTransform.forEach(function(f) {
                    f(event);
                });
            }
        }
    }

    /** A container for scatter objects, which uses a single InteractionMapper
     * for all children. This reduces the number of registered event handlers
     * and covers the common use case that multiple objects are scattered
     * on the same level.
     */
    class DOMScatterContainer {
        /**
         * @constructor
         * @param {DOM node} element - DOM element that receives events
         * @param {Bool} stopEvents -  Whether events should be stopped or propagated
         * @param {Bool} claimEvents - Whether events should be marked as claimed
         *                             if findTarget return as non-null value.
         * @param {String} [touchAction=none] - CSS to set touch action style, needed to prevent
         *                              pointer cancel events. Use null if the
         *                              the touch action should not be set.
         * @param {DOM node} debugCanvas - Shows debug infos about touches if not null
         */
        constructor(
            element,
            {
                stopEvents = 'auto',
                claimEvents = true,
                useCapture = true,
                capturePointerEvents = true,
                touchAction = 'none',
                debugCanvas = null
            } = {}
        ) {
            this.onCapture = null;
            this.element = element;
            if (stopEvents === 'auto') {
                /*
                The events have to be stopped in Safari, otherwise the whole page will be zoomed with
                a pinch gesture (preventDefault in method preventPinch). In order to enable the
                movement of scatter objects, the touchmove event has to be bound again.
                */
                if (Capabilities.isSafari) {
                    document.addEventListener('touchmove', event => this.preventPinch(event), false);
                    stopEvents = false;
                } else {
                    stopEvents = true;
                }
            }
            this.stopEvents = stopEvents;
            this.claimEvents = claimEvents;
            if (touchAction !== null) {
                Elements$1.setStyle(element, { touchAction });
            }
            this.scatter = new Map();
            this.delegate = new InteractionMapper(element, this, {
                useCapture,
                capturePointerEvents,
                mouseWheelElement: window
            });

            if (debugCanvas !== null) {
                requestAnimationFrame(dt => {
                    this.showTouches(dt, debugCanvas);
                });
            }
        }

        showTouches(dt, canvas) {
            let resolution = window.devicePixelRatio;
            let current = this.delegate.interaction.current;
            let context = canvas.getContext('2d');
            let radius = 20 * resolution;
            context.clearRect(0, 0, canvas.width, canvas.height);
            context.fillStyle = 'rgba(0, 0, 0, 0.3)';
            context.lineWidth = 2;
            context.strokeStyle = '#003300';
            for (let [key, point] of current.entries()) {
                let local = point;
                context.beginPath();
                context.arc(local.x * resolution, local.y * resolution, radius, 0, 2 * Math.PI, false);
                context.fill();
                context.stroke();
            }
            requestAnimationFrame(dt => {
                this.showTouches(dt, canvas);
            });
        }

        preventPinch(event) {
            event = event.originalEvent || event;
            if (event.scale !== 1) {
                event.preventDefault();
            }
        }

        /**
         * Removes an element from the scatter.
         *
         * @param {Scatter} scatter - Element to remove.
         * @memberof DOMScatterContainer
         */
        remove(scatter) {
            this.scatter.delete(scatter.element);
        }

        /**
         * Adds an element to the ScatterContainer.
         *
         * @param {Scatter} scatter - Element to add to the ScatterContainer.
         * @memberof DOMScatterContainer
         */
        add(scatter) {
            this.scatter.set(scatter.element, scatter);
        }

        capture(event) {
            if (this.onCapture) {
                return this.onCapture(event)
            }
            if (event.target == this.element && this.stopEvents) {
                Events.stop(event);
            }
            return true
        }

        mapPositionToPoint(point) {
            return Points.fromPageToNode(this.element, point)
        }

        isDescendant(parent, child, clickable = false) {
            if (parent == child) return true
            let node = child.parentNode;
            while (node != null) {
                if (!clickable && node.onclick) {
                    return false
                }
                if (node == parent) {
                    return true
                }
                node = node.parentNode;
            }
            return false
        }

        findTarget(event, local, global) {
            /*** Note that elementFromPoint works with clientX, clientY, not pageX, pageY
            The important point is that event should not be used, since the TouchEvent
            points are hidden in sub objects.
            ***/
            let found = document.elementFromPoint(global.x, global.y);
            for (let target of this.scatter.values()) {
                if (target.interactive && this.isDescendant(target.element, found)) {
                    if (this.stopEvents) Events.stop(event);
                    if (this.claimEvents) event.claimedByScatter = target;
                    return target
                }
            }
            return null
        }

        get center() {
            let r = this.bounds;
            let w2 = r.width / 2;
            let h2 = r.height / 2;
            return { x: w2, y: h2 }
        }

        get bounds() {
            return this.element.getBoundingClientRect()
        }

        get polygon() {
            let r = this.bounds;
            let w2 = r.width / 2;
            let h2 = r.height / 2;
            let center = { x: w2, y: h2 };
            let polygon = new Polygon(center);
            polygon.addPoint({ x: -w2, y: -h2 });
            polygon.addPoint({ x: w2, y: -h2 });
            polygon.addPoint({ x: w2, y: h2 });
            polygon.addPoint({ x: -w2, y: h2 });
            return polygon
        }
    }

    class DOMScatter extends AbstractScatter {
        constructor(
            element,
            container,
            {
                startScale = 1.0,
                minScale = 0.1,
                maxScale = 1.0,
                overdoScaling = 1.5,
                autoBringToFront = true,
                translatable = true,
                scalable = true,
                rotatable = true,
                movableX = true,
                movableY = true,
                rotationDegrees = null,
                rotation = null,
                onTransform = null,
                transformOrigin = 'center center',
                // extras which are in part needed
                x = 0,
                y = 0,
                width = null, // required
                height = null, // required
                resizable = false,
                tapDelegate = null,
                triggerSVGClicks = false,
                allowClickDistance = 44,
                verbose = true,
                onResize = null,
                touchAction = 'none',
                throwVisibility = 44,
                throwDamping = 0.95,
                autoThrow = true,
                scaleAutoClose = false,
                onClose = null,
                scaleCloseThreshold = 0.1,
                scaleCloseBuffer = 0.05,
                useLowPassFilter = false,
                maxRotation = Angle.degree2radian(15),
                minInteractionDistance = 200
            } = {}
        ) {
            super({
                minScale,
                maxScale,
                startScale,
                overdoScaling,
                autoBringToFront,
                translatable,
                scalable,
                rotatable,
                movableX,
                movableY,
                resizable,
                rotationDegrees,
                rotation,
                onTransform,
                throwVisibility,
                throwDamping,
                autoThrow,
                scaleAutoClose,
                scaleCloseThreshold,
                scaleCloseBuffer,
                onClose,
                useLowPassFilter,
                maxRotation,
                minInteractionDistance
            });
            if (container == null || width == null || height == null) {
                throw new Error('Invalid value: null')
            }
            element.scatter = this;
            this.element = element;
            this.x = x;
            this.y = y;
            this.oldX = 0;
            this.oldY = 0;
            this.meanX = x;
            this.meanY = y;
            this.width = width;
            this.height = height;
            this.throwVisibility = Math.min(width, height, throwVisibility);
            this.container = container;
            this.tapDelegate = tapDelegate;
            this.scale = startScale;
            this.rotationDegrees = this.startRotationDegrees;
            this.transformOrigin = transformOrigin;
            this.initialValues = {
                x: x,
                y: y,
                width: width,
                height: height,
                scale: startScale,
                rotation: this.startRotationDegrees,
                transformOrigin: transformOrigin
            };
            this.tapNodes = new Map();

            // For tweenlite we need initial values in _gsTransform
            TweenLite.set(element, this.initialValues);
            this.onResize = onResize;
            this.verbose = verbose;
            if (touchAction !== null) {
                Elements$1.setStyle(element, { touchAction });
            }
            this.resizeButton = null;
            if (resizable) {
                let button = document.createElement('div');
                button.style.position = 'absolute';
                button.style.right = '0px';
                button.style.bottom = '0px';
                button.style.width = '50px';
                button.style.height = '50px';
                button.className = 'interactiveElement';
                this.element.appendChild(button);

                button.addEventListener('pointerdown', e => {
                    this.startResize(e);
                });

                button.addEventListener('pointermove', e => {
                    this.resize(e);
                });

                button.addEventListener('pointerup', e => {
                    this.stopResize(e);
                });
                this.resizeButton = button;
            }
            if (tapDelegate) {
                tapDelegate.handleClicks();
            }
            container.add(this);
        }

        /** Returns geometry data as object. **/
        getState() {
            return {
                scale: this.scale,
                x: this.x,
                y: this.y,
                rotation: this.rotation
            }
        }

        close() {
            super.close();
            let parent = this.element.parentNode;
            if (parent) parent.removeChild(this.element);
        }

        get rotationOrigin() {
            return this.center
        }

        get x() {
            return this._x
        }

        get y() {
            return this._y
        }

        set x(value) {
            this._x = value;
            TweenLite.set(this.element, { x: value });
        }

        set y(value) {
            this._y = value;
            TweenLite.set(this.element, { y: value });
        }

        get position() {
            let transform = this.element._gsTransform;
            let x = transform.x;
            let y = transform.y;
            return { x, y }
        }

        get origin() {
            let p = this.fromNodeToPage(0, 0);
            return Points.fromPageToNode(this.container.element, p)
        }

        get bounds() {
            let stage = this.container.element.getBoundingClientRect();
            let rect = this.element.getBoundingClientRect();
            return {
                top: rect.top - stage.top,
                left: rect.left - stage.left,
                width: rect.width,
                height: rect.height
            }
        }

        get center() {
            let r = this.bounds;
            let w2 = r.width / 2;
            let h2 = r.height / 2;
            //   if (this.resizable) {
            //             w2 *= this.scale
            //             h2 *= this.scale
            //         }
            var x = r.left + w2;
            var y = r.top + h2;
            return { x, y }
        }

        set rotation(radians) {
            let rad = radians; // Angle.normalize(radians)
            let degrees = Angle.radian2degree(rad);
            TweenLite.set(this.element, { rotation: degrees });
            this._rotation = rad;
        }

        set rotationDegrees(degrees) {
            let deg = degrees; // Angle.normalizeDegree(degrees)
            TweenLite.set(this.element, { rotation: deg });
            this._rotation = Angle.degree2radian(deg);
        }

        get rotation() {
            return this._rotation
        }

        get rotationDegrees() {
            return this._rotation
        }

        set scale(scale) {
            TweenLite.set(this.element, {
                scale: scale,
                transformOrigin: this.transformOrigin
            });
            this._scale = scale;
        }

        get scale() {
            return this._scale
        }

        get containerBounds() {
            return this.container.bounds
        }

        get containerPolygon() {
            return this.container.polygon
        }

        mapPositionToContainerPoint(point) {
            return this.container.mapPositionToPoint(point)
        }

        capture(event) {
            return true
        }

        reset() {
            TweenLite.set(this.element, this.initialValues);
        }

        hide() {
            TweenLite.to(this.element, 0.1, {
                display: 'none',
                onComplete: e => {
                    this.element.parentNode.removeChild(this.element);
                }
            });
        }

        show() {
            TweenLite.set(this.element, { display: 'block' });
        }

        showAt(p, rotationDegrees) {
            TweenLite.set(this.element, {
                display: 'block',
                x: p.x,
                y: p.y,
                rotation: rotationDegrees,
                transformOrigin: this.transformOrigin
            });
        }

        bringToFront() {
            // this.element.parentNode.appendChild(this.element)
            // uo: On Chome and Electon appendChild leads to flicker
            TweenLite.set(this.element, { zIndex: DOMScatter.zIndex++ });
        }

        isDescendant(parent, child) {
            let node = child.parentNode;
            while (node != null) {
                if (node == parent) {
                    return true
                }
                node = node.parentNode;
            }
            return false
        }

        fromPageToNode(x, y) {
            return Points.fromPageToNode(this.element, { x, y })
        }

        fromNodeToPage(x, y) {
            return Points.fromNodeToPage(this.element, { x, y })
        }

        _move(delta) {
            // UO: We need to keep TweenLite's _gsTransform and the private
            // _x and _y attributes aligned
            let x = this.element._gsTransform.x;
            let y = this.element._gsTransform.y;
            if (this.movableX) {
                x += delta.x;
            }
            if (this.movableY) {
                y += delta.y;
            }
            this._x = x;
            this._y = y;
            // this.addVelocity({ x: delta.x, y: delta.y }) uo: reverted commit fa0256d782dd498c6d3e51321260ca375ca9f855
            TweenLite.set(this.element, { x, y });
        }

        resizeAfterTransform(zoom) {
            if (this.onResize) {
                let w = this.width * this.scale;
                let h = this.height * this.scale;
                let event = new ResizeEvent(this, { width: w, height: h });
                this.onResize(event);
            }
        }

        startResize(e) {
            e.preventDefault();
            let event = new CustomEvent('resizeStarted');

            let oldPostition = {
                x: this.element.getBoundingClientRect().left,
                y: this.element.getBoundingClientRect().top
            };
            this.bringToFront();

            this.element.style.transformOrigin = '0% 0%';

            let newPostition = {
                x: this.element.getBoundingClientRect().left,
                y: this.element.getBoundingClientRect().top
            };

            let offset = Points.subtract(oldPostition, newPostition);

            this.oldX = e.clientX;
            this.oldY = e.clientY;

            e.target.setAttribute('resizing', 'true');
            e.target.setPointerCapture(e.pointerId);

            TweenLite.to(this.element, 0, { css: { left: '+=' + offset.x + 'px' } });
            TweenLite.to(this.element, 0, { css: { top: '+=' + offset.y + 'px' } });

            this.element.dispatchEvent(event);
        }

        resize(e) {
            e.preventDefault();

            let rotation = Angle.radian2degree(this.rotation);
            rotation = (rotation + 360) % 360;
            let event = new CustomEvent('resized');
            if (e.target.getAttribute('resizing') == 'true') {
                let deltaX = e.clientX - this.oldX;
                let deltaY = e.clientY - this.oldY;

                let r = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
                let phi = Angle.radian2degree(Math.atan2(deltaX, deltaY));

                phi = (phi + 630) % 360;
                let rot = (rotation + 90 + 630) % 360;

                let diffAngle = (0 + rot + 360) % 360;
                let phiCorrected = (phi + diffAngle + 360) % 360;

                let resizeW = r * Math.cos(Angle.degree2radian(phiCorrected));
                let resizeH = -r * Math.sin(Angle.degree2radian(phiCorrected));

                if (
                    (this.element.offsetWidth + resizeW) / this.scale > (this.width * 0.5) / this.scale &&
                    (this.element.offsetHeight + resizeH) / this.scale > (this.height * 0.3) / this.scale
                )
                    TweenLite.to(this.element, 0, {
                        width: this.element.offsetWidth + resizeW / this.scale,
                        height: this.element.offsetHeight + resizeH / this.scale
                    });

                this.oldX = e.clientX;
                this.oldY = e.clientY;
                this.onResizing();

                this.element.dispatchEvent(event);
            }
        }

        stopResize(e) {
            e.preventDefault();

            let event = new CustomEvent('resizeEnded');
            let oldPostition = {
                x: this.element.getBoundingClientRect().left,
                y: this.element.getBoundingClientRect().top
            };
            this.element.style.transformOrigin = '50% 50%';
            let newPostition = {
                x: this.element.getBoundingClientRect().left,
                y: this.element.getBoundingClientRect().top
            };
            let offset = Points.subtract(oldPostition, newPostition);

            TweenLite.to(this.element, 0, { css: { left: '+=' + offset.x + 'px' } });
            TweenLite.to(this.element, 0, { css: { top: '+=' + offset.y + 'px' } });

            e.target.setAttribute('resizing', 'false');

            this.element.dispatchEvent(event);
        }
    }

    DOMScatter.zIndex = 1000;

    /* eslint-disable no-unused-vars */

    class CardLoader {
        constructor(
            src,
            {
                x = 0,
                y = 0,
                width = 1000,
                height = 800,
                maxWidth = null,
                maxHeight = null,
                scale = 1,
                minScale = 0.5,
                maxScale = 1.5,
                rotation = 0,
            } = {}
        ) {
            this.src = src;
            this.x = x;
            this.y = y;
            this.scale = scale;
            this.rotation = rotation;
            this.maxScale = maxScale;
            this.minScale = minScale;
            this.wantedWidth = width;
            this.wantedHeight = height;
            this.maxWidth = maxWidth != null ? maxWidth : window.innerWidth;
            this.maxHeight = maxHeight != null ? maxHeight : window.innerHeight;
            this.addedNode = null;
        }

        unload() {
            if (this.addedNode) {
                this.addedNode.remove();
                this.addedNode = null;
            }
        }
    }

    class HTMLLoader extends CardLoader {
        load(domNode) {
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.open('GET', this.src, false);
                xhr.onreadystatechange = (e) => {
                    if (xhr.readyState == 4) {
                        domNode.innerHTML = this.prepare(xhr.response);
                        this.addedNode = domNode.firstElementChild;
                        let { width, height } = this.size(this.addedNode);
                        if (width) this.wantedWidth = width || this.wantedWidth;
                        if (height) this.wantedHeight = height || this.wantedHeight;
                        resolve(this);
                    }
                };
                xhr.onerror = (e) => {
                    reject(this);
                };
                xhr.send();
            })
        }

        /**
         * Preoares the html before it is assigned with innerHTML.
         * Can be overwritten in subclasses.
         *
         * @param {*} html
         * @returns
         * @memberof HTMLLoader
         */
        prepare(html) {
            return html
        }

        /**
         * Tries to determine the size of the addedNode.
         * Checks for explicit width and height style attributes.
         *
         * Overwrite this method if you want to extract values from other infos.
         *
         * @returns { width: int, height: int }
         * @memberof HTMLLoader
         */
        size(node) {
            let width = parseInt(node.style.width) || null;
            let height = parseInt(node.style.height) || null;
            return { width, height }
        }
    }

    class ImageReplacer {
        static setImages(img, original, preview) {
            this.setOriginal(img, original);
            this.setPreview(img, preview);
        }

        static getImages(img) {
            return {
                original: this.getOriginal(img),
                preview: this.getPreview(img)
            }
        }

        static get previewAttribute() {
            return 'data-preview-image'
        }

        static get originalAttribute() {
            return 'data-image'
        }

        static getPreview(img) {
            return img.getAttribute(this.previewAttribute)
        }

        static setPreview(img, preview) {
            img.setAttribute(this.previewAttribute, preview);
        }

        static getOriginal(img) {
            return img.getAttribute(this.originalAttribute)
        }

        static setOriginal(img, original) {
            img.setAttribute(this.originalAttribute, original);
        }

        static async replaceWithLowResImage(img, attr = 'src') {
            let preview = this.getPreview(img);
            if (!preview || preview == 'null') {
                console.error('There was no preview image set!', img);
                return null
            } else {
                return this.replaceImage(img, preview, attr)
            }
        }

        static async replaceWithHighResImage(img, attr = 'src') {
            let original = this.getOriginal(img);
            if (!original) {
                console.error('There was no original image set!', img);
                return null
            } else {
                return this.replaceImage(img, original, attr)
            }
        }

        static async replaceImage(img, other, attr = 'src') {
            return new Promise(resolve => {
                let image = new Image();
                image.addEventListener('load', () => {
                    img.setAttribute(attr, image.src);
                    // console.log('replaceImage ready', image.src)
                    resolve();
                });
                image.src = other;
                // console.log('replaceImage', other)
            })
        }

        static replaceWithLowResImageDelayed(img, attr = 'src') {
            let preview = this.getPreview(img);
            if (!preview || preview == 'null') {
                console.error('There was no preview image set!', img);
                return null
            } else {
                return this.replaceImageDelayed(img, preview, attr)
            }
        }

        static replaceWithHighResImageDelayed(img, attr = 'src') {
            let original = this.getOriginal(img);
            if (!original) {
                console.error('There was no original image set!', img);
                return null
            } else {
                return this.replaceImageDelayed(img, original, attr)
            }
        }
        static replaceImageDelayed(img, other, attr = 'src', delay = 500) {
            setTimeout(() => {
                let image = new Image();
                image.addEventListener('load', () => {
                    img.setAttribute(attr, image.src);
                });
                image.src = other;
            }, delay + Math.random() * 1000);
        }
    }

    class Slider {
        constructor(node) {
            this.dragging = false;

            let wrapper = ExtendedCardWrapper.getCardWrapper(node);
            this.constructor.initSlider(wrapper, this);

            this.sliderBase = node.querySelector('.sliderBase');
            this.interaction = new InteractionMapper(this.sliderBase, this);
        }

        capture(event) {
            Events.capturedBy(event, this);
            return false
        }

        findTarget(event, local, global) {
            if (event.target === this.slider) {
                return this
            }
            return null
        }

        onStart(event, interaction) {
            this.dragging = true;
        }

        onMove(event, interaction) {
            //   Event.capturedBy(event, this)
            if (this.dragging) {
                const p = window.convertPointFromPageToNode(
                    this.slider,
                    event.clientX,
                    event.clientY
                );
                const ratio = this.slider.max / this.slider.offsetWidth;
                this.slider.value = parseInt(p.x * ratio);

                Slider.changed(this.cardWrapper, this.slider.value);
            }
        }

        onEnd(event, interaction) {
            this.dragging = false;
        }

        static initSlider(cardWrapper, instance) {
            let slides = this.getSlides(cardWrapper);
            let rangeSlider = this.getSliderInput(cardWrapper);

            const min = 0;
            rangeSlider.setAttribute('min', min);

            const max = (slides.length - 1) * this.steps;
            rangeSlider.setAttribute('max', max);

            cardWrapper.setAttribute('slider-value', 0);
            this.updateSlideFigure(cardWrapper);

            rangeSlider.addEventListener('pointerdown', event => {
                cardWrapper.sliding = 0;
                instance.onStart(event);
            });

            rangeSlider.addEventListener('pointermove', event => {
                cardWrapper.sliding++;
                instance.onMove(event);
            });

            rangeSlider.addEventListener('pointerup', event => {
                instance.onEnd(event);
            });

            rangeSlider.addEventListener('pointercancel', event => {
                instance.onEnd(event);
            });

            /* UO: The highlevel input event works only after the user
            moved the finger along the x-axis. For rotated card sliders
            the low level pointer move events are needed.

            rangeSlider.addEventListener('input', event => {
                cardWrapper.sliding++

                // If the user just taps, we want the slider to be animated.
                // But if he slides the finger on the slider, the input shall
                // be updated right away!
                if (cardWrapper.sliding > 10) {
                    this.stopAnimation(cardWrapper)
                    this.changed(cardWrapper, rangeSlider.value)
                }
            })
            */

            /**
             * If the slider is released near any end, it will be reset
             * to min or max.
             */
            rangeSlider.addEventListener('change', () => {
                // console.log('changed')
                this.snapSliderAtEnds(rangeSlider, min, max);
                this.animate(cardWrapper);
            });

            instance.slider = rangeSlider;
            instance.cardWrapper = cardWrapper;
            ExtendedCardWrapper.setDebugValue(cardWrapper, 'slider', 0);
            this.setValue(cardWrapper, 0);
        }

        static animate(cardWrapper) {
            const value = parseFloat(cardWrapper.getAttribute('slider-value'));
            const target = this.getSliderInput(cardWrapper).value;

            let start = { value };

            this.stopAnimation(cardWrapper);

            cardWrapper.tween = TweenLite.to(start, 0.5, {
                value: target,
                onUpdate: () => {
                    this.changed(cardWrapper, start.value);
                }
            });
        }

        static stopAnimation(cardWrapper) {
            if (cardWrapper.tween) cardWrapper.tween.kill();
        }

        static changed(cardWrapper, value) {
            let oldValue = parseFloat(cardWrapper.getAttribute('slider-value')) || 0;
            if (value != oldValue) {
                cardWrapper.setAttribute('slider-value', value);
                this.updateSlideFigure(cardWrapper, true);
            }
        }

        static snapSliderAtEnds(slider, min, max, threshold = 0.1) {
            const thresholdAsPoints = max * threshold;

            const underThreshold = slider.value < thresholdAsPoints;
            const aboveThreshold = slider.value > max - thresholdAsPoints;

            if (underThreshold || aboveThreshold) {
                slider.value = underThreshold ? 0 : max;
                // var event = new Event('input')
                // slider.dispatchEvent(event)
            }
        }

        static updateSlideFigure(cardWrapper, interactive = false) {
            const slides = this.getSlides(cardWrapper);
            const value = cardWrapper.getAttribute('slider-value');

            let normalizedValue = value / this.steps + 1;

            const visibleIndex = Math.round(normalizedValue);

            this.hideNextImages(visibleIndex, slides);

            /* UO: Is this the better entry to sliding? */
            if (window.config.get('showOriginalAtSliderStart')) {
                let duration = interactive ? 0.5 : 0;
                if (value <= 0.1) {
                    this.showOriginalImage(cardWrapper, duration);
                } else {
                    this.hideOriginalImage(cardWrapper, duration);
                }
            }

            ExtendedCardWrapper.setDebugValue(
                cardWrapper,
                'slider',
                `${visibleIndex} (${normalizedValue.toFixed(2)})`
            );

            this.showPreviousImages(visibleIndex, slides);
            this.fadeImage(normalizedValue, slides);

            if (window.config.get('fadeSliderText')) {
                let txt = this.getSliderTextElement(cardWrapper);
                txt.style.opacity =
                    1 - Math.abs(normalizedValue - visibleIndex) / 0.5;
            }

            this.updateSliderText(cardWrapper, slides[visibleIndex - 1]);
            this.updateSlideVisuals(cardWrapper);
        }

        static updateSlideVisuals(cardWrapper) {
            let input = this.getSliderInput(cardWrapper);
            let slider = cardWrapper.querySelector('.slider');
            let activeTrack = slider.querySelector('.sliderTrackActive');
            let thumb = slider.querySelector('.sliderThumb');

            const min = input.getAttribute('min') || 0;
            const max = input.getAttribute('max') || 100;
            const value = cardWrapper.getAttribute('slider-value');

            let normalizedValue = value / (max - min) - min;

            let percent = parseInt(normalizedValue * 100) + '%';
            activeTrack.style.width = percent;
            thumb.style.left = percent;
        }

        static getSliderTextElement(cardWrapper) {
            return cardWrapper.querySelector('.sliderInfo p')
        }

        static updateSliderText(cardWrapper, slide) {
            if (slide) {
                let text = slide.getAttribute('alt');
                let alignment = slide.getAttribute('data-align');

                if (text) {
                    let info = this.getSliderTextElement(cardWrapper);
                    if (!info)
                        console.error(
                            'Your card has no info element. Could not update text!'
                        );
                    else {
                        if (
                            ObersalzbergCard.isInitialized(cardWrapper) &&
                            info.innerHTML != text
                        ) {
                            let key = ObersalzbergCard.getKey(cardWrapper);
                            Logging.log(
                                `SLIDER_TEXT_CHANGED::CARD:${key},TEXT:${text}`
                            );
                            info.innerHTML = text;

                            if (alignment) {
                                info.parentNode.style.justifyContent = 'flex-start';
                                info.style.textAlign = alignment;
                            } else {
                                info.style.textAlign = 'center';
                                info.parentNode.style.justifyContent = 'center';
                            }
                        }
                    }
                }
            } else {
                const card = cardWrapper.getAttribute('data-key');
                const message = `Could not update slider's text of card ${card}.`;
                if (window.errorWindow) {
                    window.errorWindow.error(message, ['slider']);
                    console.error(message, cardWrapper);
                }
            }
        }

        static get steps() {
            return 100
        }

        static getSliderInput(cardWrapper) {
            return cardWrapper.querySelector('.sliderBase input[type=range]')
        }

        static getSlides(cardWrapper) {
            let slides = cardWrapper.querySelectorAll('.slide');
            slides = Array.from(slides);
            slides.shift();
            return slides
        }

        static getOriginalImage(cardWrapper) {
            return cardWrapper.querySelector('img.original')
        }

        static hideOriginalImage(cardWrapper, animated = 0.0) {
            let original = this.getOriginalImage(cardWrapper);
            let autoAlpha = 0;
            // original.style.opacity = autoAlpha
            //    original.style.visibility = 'hidden'

            if (animated > 0.0) {
                TweenLite.to(original, animated, { autoAlpha });
            } else {
                TweenLite.set(original, { autoAlpha });
            }
        }

        static showOriginalImage(cardWrapper, animated = 0.0) {
            let original = this.getOriginalImage(cardWrapper);
            let autoAlpha = 1;
            //original.style.opacity = autoAlpha
            //    original.style.visibility = 'visible'
            if (animated > 0.0) {
                TweenLite.to(original, animated, { autoAlpha });
            } else {
                TweenLite.set(original, { autoAlpha });
            }
        }

        static setValue(cardWrapper, normalizedValue) {
            let slider = this.getSliderInput(cardWrapper);
            const min = slider.getAttribute('min') || 0;
            const max = slider.getAttribute('max') || 100;
            slider.value = min + (max - min) * normalizedValue;
            this.updateSlideFigure(cardWrapper);
        }

        static showPreviousImages(index, slides) {
            //Show all previous images
            for (let i = 0; i < index && i < slides.length; i++) {
                let slide = slides[i];
                TweenLite.set(slide, { autoAlpha: 1 });
            }
        }

        static fadeImage(value, slides) {
            let next = Math.floor(value);
            if (next < slides.length) {
                let autoAlpha = value % 1;
                TweenLite.set(slides[next], {
                    autoAlpha
                });
            }
        }

        static hideNextImages(index, slides) {
            index++;
            while (index < slides.length) {
                TweenLite.set(slides[index], { opacity: 0 });
                index++;
            }
        }
    }

    function getVariable(name) {
        return getComputedStyle(document.documentElement)
            .getPropertyValue(name)
            .trim()
    }

    getVariable('--highlight-color');
    getVariable('--drawer-width');

    class Menu extends Object {
        static getItems(item, query = '') {
            let wrapper = ExtendedCardWrapper.getCardWrapper(item);
            return Array.from(
                wrapper.querySelectorAll(`.frontFooter .button${query}`)
            )
        }

        // static highlightItem(item) {
        //     let isHighlighted = item.style.color == HIGHLIGHT_COLOR
        //     // Menu.resetMenuColor(item)
        //     if (!isHighlighted) {
        //         item.style.color = HIGHLIGHT_COLOR
        //         return true
        //     }
        //     return false
        // }

        static highlightItemByMode(card, mode) {
            let activeFooterItems = this.getItems(card, '.active');
            activeFooterItems.forEach(item => {
                item.classList.remove('active');
            });

            let menuItem = card.querySelector(`.frontFooter .${mode}`);
            if (menuItem != null) {
                menuItem.classList.add('active');
            }
        }

        static setCardModeToNone(event) {
            let cardWrapper = ObersalzbergCard.getCardWrapper(event.target);
            ObersalzbergCard.changeMode(cardWrapper, 'none');
        }

        static toggleBackground(event) {
            const backgroundMode = 'background';
            this.toggleMode(event, backgroundMode);
        }

        static toggleDetails(event) {
            const detailsMode = 'details';
            this.toggleMode(event, detailsMode);
        }

        static toggleSlider(event) {
            const sliderMode = 'slider';
            this.toggleMode(event, sliderMode);
        }

        static toggleMode(event, mode) {
            let cardWrapper = ObersalzbergCard.getCardWrapper(event.target);
            Drawer.scrollToTop(cardWrapper);
            const resultingMode = ObersalzbergCard.toggleMode(cardWrapper, mode);
            this.highlightItemByMode(cardWrapper, resultingMode);
        }
    }

    class Localization {
        static validate(wrapper) {
            if (
                window.config &&
                window.config.definitions &&
                window.config.definitions.languages
            ) {
                const languages = window.config.definitions.languages;
                this.getAllLocalizables(wrapper).forEach(localizable => {
                    languages.forEach(language => {
                        const attr = this.getLanguageAttribute(language);
                        if (!localizable.getAttribute(attr)) {
                            const message = `Element had missing language attribute ${attr}: ${localizable.outerHTML}.`;
                            if (window.errorWindow) {
                                window.errorWindow.error(message);
                            }

                            console.error(message);
                        }
                    });
                });
            } else {
                console.error(
                    'Could not validate languages, as language config was not found @window.config.definitions.languages.'
                );
            }
        }

        /**
         * Returns the name of the locale file.
         *
         * @param {string} file - Path to the basefile.
         * @param {string} local - Locale identifier.
         */
        static getFileName(file, local, extension) {
            return `${file}_${local}.${extension}`
        }

        static getAllLocalizables(element) {
            return element.querySelectorAll(`[${this.localizableAttribute}]`)
        }

        static makeLocalizable(element, languageObject) {
            element.setAttribute(this.localizableAttribute, true);

            for (let [lang, txt] of Object.entries(languageObject)) {
                this.addLanguageAttribute(element, lang, txt);
            }
        }

        static addLanguageAttribute(element, lang, text) {
            element.setAttribute(this.getLanguageAttribute(lang), text);
        }

        static getLanguageAttribute(lang) {
            return `data-lang-${lang}`
        }

        static get localizableAttribute() {
            return 'data-localizable'
        }

        static changeLanguageOfLocalizables(element, lang) {
            this.getAllLocalizables(element).forEach(localizable => {
                this.localize(localizable, lang);
            });

            let localeElements = element.querySelectorAll('[data-locale-element]');
            localeElements.forEach(localeElement => {
                let locale = localeElement.getAttribute('data-lang');

                if (locale == lang) {
                    localeElement.classList.remove('hidden');
                } else {
                    localeElement.classList.add('hidden');
                }
            });
        }

        static localize(localizable, lang) {
            const langAttr = this.getLanguageAttribute(lang);
            if (!localizable.hasAttribute(langAttr)) {
                console.error(`Could not change language to ${lang}.`, localizable);
            } else {
                localizable.innerText = localizable.getAttribute(langAttr);
            }
        }
    }

    class DetailsDrawerSynch {
        static detailsOpened(highlight, className) {
            let wrapper = ExtendedCardWrapper.getCardWrapper(highlight);
            Details.openAllOfClass(wrapper, className, highlight);

            let collapsible = Details.getCollapsibleClassFromHighlight(highlight);
            if (collapsible) {
                Drawer.openCollapsibleByClassName(wrapper, collapsible);
            } else {
                console.warn('Highlight has no collapsible set. ', highlight);
            }
        }

        static detailsClosed(highlight, className) {
            let wrapper = ExtendedCardWrapper.getCardWrapper(highlight);

            Details.closeAllOfClass(wrapper, className, highlight);

            let collapsible = Details.getCollapsibleClassFromHighlight(highlight);
            if (collapsible) {
                Drawer.closeCollapsibleByClassName(wrapper, collapsible);
            } else {
                console.warn('Highlight has no collapsible set. ', highlight);
            }
        }

        static collapsibleOpened(collapsible, className) {
            let wrapper = ExtendedCardWrapper.getCardWrapper(collapsible);
            Details.closeAll(wrapper);
            Details.openAllOfClass(wrapper, className);
        }

        static collapsibleClosed(collapsible, className) {
            let wrapper = ExtendedCardWrapper.getCardWrapper(collapsible);
            Details.closeAllOfClass(wrapper, className);
        }

        static closeAll(cardWrapper) {
            Details.closeAll(cardWrapper);
            Drawer.closeAllCollapsibles(cardWrapper);
        }
    }

    class PopupContainer {
        static handlePopup(target, position) {
            // Is it an Popup? Then open the popup

            let cardWrapper = ExtendedCardWrapper.getCardWrapper(target);

            if (
                target.tagName.toLowerCase() == 'a' ||
                target.tagName.toLowerCase() == 'circle'
            ) {
                if (target.closest('.drawer') != null) {
                    let href = target.getAttribute('href');
                    if (target.tagName == 'circle') {
                        let a = target.closest('a');
                        if (a) href = a.getAttribute('href');
                    }

                    const openPopup = cardWrapper.getAttribute('data-popup');
                    this.closeAll(cardWrapper, [href]);
                    if (openPopup != href) {
                        this.openPopup(target, position);

                        if (target.tagName == 'circle') {
                            ObersalzbergHighlight.openHighlight(target, {
                                scale: window.config.get('highlightScale')
                            });
                        }
                    } else {
                        this.closeAll(cardWrapper);
                    }
                }
            }
        }

        static closeAll(cardWrapper, exclude = []) {
            this.closeAllHighlights(cardWrapper, exclude);
            return this.closeAllPopups(cardWrapper, exclude)
        }

        static closeAllHighlights(cardWrapper, exclude = []) {
            cardWrapper
                .querySelectorAll('article circle.expanded')
                .forEach(highlight => {
                    console.error(' TODO: IMPLEMENTED EXCLUDE');
                    ObersalzbergHighlight.closeHighlight(highlight);
                });
        }

        static closeAllPopups(wrapper, exclude = []) {
            let closedHrefs = [];
            let popups = wrapper.querySelectorAll('.popup');
            popups.forEach(popup => {
                const href = popup.getAttribute('data-href');
                if (exclude.indexOf(href) == -1) {
                    this.closePopup(wrapper, popup);
                    closedHrefs.push(href);
                }
            });

            return closedHrefs
        }

        static followTarget() {}

        static openPopup(target, position, closedHrefs = []) {
            let href = target.hasAttribute('href')
                ? target.getAttribute('href')
                : target.getAttribute('xlink:href');

            let wrapper = target.closest('.cardWrapper');

            if (wrapper.popupInterval) clearInterval(wrapper.popupInterval);

            // Add a popup if it's not the same as the existing one.
            if (closedHrefs.indexOf(href) == -1) {
                this.load(href)
                    .then(htmlText => {
                        if (wrapper.getAttribute('data-popup') !== href) {
                            let domParser = new DOMParser();
                            let html = domParser.parseFromString(
                                htmlText,
                                'text/html'
                            );

                            html.querySelectorAll('img').forEach(img => {
                                let src = img.getAttribute('src');
                                let cardNum = wrapper.getAttribute('data-key');
                                img.setAttribute(
                                    'src',
                                    `var/cards/${cardNum}/${src}`
                                );
                            });

                            html.querySelectorAll('video').forEach(video => {
                                let src = video.getAttribute('src');
                                let cardNum = wrapper.getAttribute('data-key');
                                video.setAttribute(
                                    'src',
                                    `var/cards/${cardNum}/${src}`
                                );

                                video.setAttribute('autoplay', true);
                                video.setAttribute('muted', true);
                                video.setAttribute('loop', true);
                            });

                            let popup = document.createElement('div');
                            popup.setAttribute('data-href', href);
                            popup.className = 'popup';

                            let popupContent = document.createElement('div');
                            popup.appendChild(popupContent);
                            popupContent.className = 'popup-content';
                            popupContent.innerHTML = html.body.innerHTML;

                            let closeBtn = document.createElement('div');
                            closeBtn.classList.add('button', 'close-btn');
                            closeBtn.addEventListener(
                                'pointerdown',
                                this.closePopup.bind(this, wrapper, popup)
                            );
                            popupContent.appendChild(closeBtn);

                            let closeIcon = document.createElement('div');
                            closeIcon.className = 'close-icon';
                            closeBtn.appendChild(closeIcon);

                            TweenLite.set(popup, { opacity: 0 });
                            TweenLite.to(popup, 0.5, {
                                opacity: 1,
                                delay: 0.1
                            });

                            wrapper.setAttribute('data-popup', href);
                            wrapper.appendChild(popup);

                            const offset = Points.fromPageToNode(target, position);

                            let key = ObersalzbergCard.getKey(wrapper);
                            this.setPopupPosition(wrapper, popup, target, offset);


                            wrapper.popupInterval = setInterval(() => {
                                this.setPopupPosition(
                                    wrapper,
                                    popup,
                                    target,
                                    offset
                                );
                            }, 1);

                            Logging.log(`OPEN_POPUP::CARD:${key},POPUP:${href}`);
                        }
                    })
                    .catch(console.error);
                return true
            } else return false
        }

        static closePopup(cardWrapper, popup) {
            if (cardWrapper.popupInterval) clearInterval(cardWrapper.popupInterval);
            const key = ObersalzbergCard.getKey(cardWrapper);
            const href = popup.getAttribute('data-href');
            Logging.log(`CLOSE_POPUP::CARD:${key},POPUP:${href}`);

            cardWrapper.removeAttribute('data-popup');
            TweenLite.to(popup, 0.5, {
                autoAlpha: 0,
                onComplete: () => {
                    if (popup.parentNode) popup.parentNode.removeChild(popup);
                }
            });
        }

        static setPopupPosition(cardWrapper, popup, target, mouseOffset) {
            let scroll = Drawer.getScrollElement(cardWrapper);
            const drawer = cardWrapper.querySelector('.drawer');

            
          

            let offsetLeft, offsetTop;
            if (target.offsetLeft) {
                offsetLeft = target.offsetLeft + mouseOffset.x;
                offsetTop = target.offsetTop + mouseOffset.y;
            } else if (target.getBBox) {
                let overlayWrap = target.closest('.overlayWrap');

                if (!overlayWrap)
                    console.error('Every svg must be in an overlayWrap!', target);

                const bbox = target.getBBox();
                offsetLeft =
                    overlayWrap.offsetLeft + bbox.x + Math.round(bbox.width / 2) + 5;
                offsetTop = overlayWrap.offsetTop + bbox.y;
            } else {
                console.error('Could not compute offsetPosition of popup.', target);
            }

            let position = {
                x: drawer.offsetWidth + drawer.offsetLeft + offsetLeft  + 15,
                y: offsetTop - scroll.scrollTop
            };

            const popupAboveArticle = position.y < 0;
            const popupBelowArticle = position.y > drawer.offsetHeight;

            position.y = popupAboveArticle
                ? 0
                : popupBelowArticle
                ? drawer.offsetHeight
                : position.y;


               Object.assign(popup.style, {
                position: 'absolute',
                left: position.x + 'px',
                top: position.y + 'px'
            });

            if (popupAboveArticle || popupBelowArticle) {
                this.closePopup(cardWrapper, popup);
            }
        }

        static async load(href) {
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function() {
                    if (this.readyState == 4) {
                        if (this.responseText) {
                            resolve(this.responseText);
                        } else {
                            reject(
                                `Response @${href} did not return anything.`,
                                this
                            );
                        }
                    }
                };

                xhr.onerror = function() {
                    reject('Could not load content: ', this);
                };

                xhr.open('GET', href);
                xhr.send();
            })
        }

        static handlePopupOnClick(event) {
            this.handlePopup(event.target, { x: event.clientX, y: event.clientY });
        }

        static fixPopups(cardWrapper) {
            let popups = cardWrapper.querySelectorAll('article a');
            popups.forEach(popup => {
                popup.setAttribute(
                    'onclick',
                    'PopupContainer.handlePopupOnClick(event)'
                );
                popup.setAttribute('draggable', false);
                popup.addEventListener('click', event => {
                    event.preventDefault();
                });
            });
        }
    }

    const cardWrapperMap = new Map();

    class FrontLoader extends HTMLLoader {
        constructor(src, options = {}, prefixPath = '') {
            super(src, options);
            this.prefixPath = prefixPath;
        }

        // We need a async load function therefore I just overloaded it
        // for simplicity
        async load(domNode) {
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.open('GET', this.src, false);
                xhr.onreadystatechange = async e => {
                    if (xhr.readyState == 4) {
                        domNode.innerHTML = await this.prepare(xhr.response);
                        this.addedNode = domNode.firstElementChild;
                        let { width, height } = this.size(this.addedNode);
                        if (width) this.wantedWidth = width || this.wantedWidth;
                        if (height) this.wantedHeight = height || this.wantedHeight;
                        resolve(this);
                    }
                };
                xhr.onerror = e => {
                    reject(this);
                };
                xhr.send();
            })
        }

        async prepare(html) {
            html = this.addPrefixPathToLinks(html);

            let parser = new DOMParser();
            let parsedHTML = parser.parseFromString(html, 'text/html');
            this.fixSvgImageElement(parsedHTML);
            this.prepareHighlights(parsedHTML);
            ExtendedCardWrapper.prepareLowResolutionPlaceholders(parsedHTML);

            let text = parsedHTML.body.innerHTML;
            return text
        }

        /**
         * Adjusts the relative paths to the project folder.
         *
         * @param {string} html - The html string of the loaded card.
         */
        addPrefixPathToLinks(html) {
            for (let attr of ['href="', 'src="', 'image="']) {
                html = html.replace(new RegExp(attr, 'gi'), attr + this.prefixPath);
            }
            let start = html.indexOf('<body>');
            if (start > -1) {
                let end = html.lastIndexOf('</body>');
                if (end > -1) {
                    html = html.slice(start + 6, end);
                }
            }

            return html
        }

        /**
         * The image is not scaling with the card.
         * This is bad, and it's hard coded within the HTML.
         * Wich is also bad. So I just overwrite it here, which
         * is not ideal, but should do the trick just fine.
         * @param {*} parsedHTML
         */
        fixSvgImageElement(parsedHTML) {
            let image = parsedHTML.querySelector('svg > image');

            if (image == null)
                console.error('Could not find image.', parsedHTML.innerHTML);
            else {
                // The width and height have redundand information
                // with their svg parent. This is removed here.
                image.setAttribute('width', '100%');
                image.setAttribute('height', '100%');

                //The covering we achieve with e.g.:
                // style="background-size: cover; background-position: center;""
                // us mimiced with the following:
                image.setAttribute('preserveAspectRatio', 'xMidYMid slice');

                // We should not need to see the image. As it is just
                // a blueprint for the highlights. And we already have
                // the identical 'original' image that is always shown.
                image.style.display = 'none';
            }
        }

        /**

         *
         * @param {DOMObject} html
         */
        prepareHighlights(parsedHTML) {
            let highlights = Details.getAllHighlights(parsedHTML);
            highlights.forEach(highlight => {
                let name = this.addClassNameToHighlight(highlight);
                highlight.removeAttribute('onclick');

                if (name) {
                    InteractionMapper.on('click', highlight, event => {
                        Details.highlight(event, `.${name}`);
                    });
                }
            });
        }

        /**
         * We need an identifier for the highlights.
         * But there is only the class provided inside a function.
         * So I extract that klass from the function and apply it as
         * attribute.
         *
         * @param {DOMElement} highlight
         */
        addClassNameToHighlight(highlight) {
            let name = null;
            let clickFunction = highlight.getAttribute('onclick');
            if (!clickFunction) {
                console.error('No onclick function was specified!', highlight);
            } else {
                //Find all parameters of the function call.
                let parameters = clickFunction.match(/(?:\().+(?=\))/g);

                if (parameters.length == 1) {
                    // Get the match that we found from the array.
                    parameters = parameters[0];

                    // Split into single parameters.
                    let splitted = parameters.split(',');
                    if (splitted.length < 2) {
                        console.error(
                            'Something is wrong with the onclick function on ',
                            highlight
                        );
                    } else {
                        // Finally remove all quotes and the className dot.
                        let collapsibleClass = splitted[1].replace(/["'.\s]/g, '');
                        highlight.setAttribute('data-collapsible', collapsibleClass);
                        name = collapsibleClass;
                    }
                } else {
                    console.error(
                        'Something is wrong with the onclick function on ',
                        highlight
                    );
                }
            }
            return name
        }

        getImageSize() {
            let width = 0;
            let height = 0;
            if (this.addedNode != null) {
                width = parseInt(this.addedNode.getAttribute('data-width'));
                height = parseInt(this.addedNode.getAttribute('data-height'));
            }
            return { width, height }
        }
    }

    class CardScatter extends DOMScatter {
        constructor(element, container, options) {
            super(element, container, options);
            this.scroll = element.querySelector('.scroll');
            if (!this.scroll) console.error('Scroll element was not found!');
            this.throwScroll = false;
        }

        bringToFront(event) {
            super.bringToFront();
            if (this.tipWrapper) {
                this.tipWrapper.popController();
            }
        }

        onStart(event, interaction) {
            this.throwScroll = false;
            this.scrollMode = event.target.closest('.scroll') ? true : false;
            super.onStart(event, interaction);
        }

        transform(translate, zoom, rotate, anchor) {
            const zooming = this.scalable && zoom != 1;
            const rotating = this.rotatable && rotate != 0;
            if (!zooming && !rotating) {
                this.scrollMove(translate);
            } else {
                super.transform(translate, zoom, rotate, anchor);
            }
        }

        rotateDelta(delta) {
            let alpha = -this.rotation;
            return {
                x: Math.cos(alpha) * delta.x - Math.sin(alpha) * delta.y,
                y: Math.sin(alpha) * delta.x + Math.cos(alpha) * delta.y
            }
        }

        scrollMove(delta) {
            let alpha = -this.rotation;

            // The card may be rotated, so we have to rotate the received x and y
            // coordinates accordingly.
            let rotatedDelta = this.rotateDelta(delta);

            if (this.scrollMode && this.scroll) {
                // Calculate the desired scrolloffset.
                let scroll = this.scroll.scrollTop - rotatedDelta.y / this.scale;
                this.scroll.scrollTop = scroll;

                if (window.config.get('translateCardOnScrollLimit')) {
                    if (scroll < 0) {
                        rotatedDelta.y = -scroll;
                    } else if (
                        this.scroll.scrollHeight -
                            this.scroll.offsetHeight -
                            scroll <
                        0
                    ) {
                        rotatedDelta.y =
                            this.scroll.scrollHeight -
                            this.scroll.offsetHeight -
                            scroll;
                    } else rotatedDelta.y = 0;
                } else rotatedDelta.y = 0;

                rotatedDelta.y *= this.scale;
                rotatedDelta.x = 0;
            }

            let reversedDelta = {
                x:
                    Math.cos(alpha) * rotatedDelta.x +
                    Math.sin(alpha) * rotatedDelta.y,
                y:
                    -1 * Math.sin(alpha) * rotatedDelta.x +
                    Math.cos(alpha) * rotatedDelta.y
            };

            super._move(reversedDelta);
            delta.x = reversedDelta.x;
            delta.y = reversedDelta.y;
        }

        onEnd(event, interaction) {
            super.onEnd(event, interaction);
            this.throwScroll = this.scrollMode && this.scroll;
            this.scrollMode = false;
        }

        _move(delta) {
            if (this.throwScroll) {
                let rotatedDelta = this.rotateDelta(delta);
                // console.log("throw", delta)
                if (this.scroll.scrollTop > 0)
                    this.scroll.scrollTop -= rotatedDelta.y;
                else console.log('Scroll over the top');
                //TweenLite.set(this.scroll, { scrollTop: "-=" + delta.y })
            } else {
                super._move(delta);
            }
        }
    }

    class DOMCardScatterContainer extends DOMScatterContainer {
        //     capture(event) {
        //         let node = event.target
        //         while (node) {
        //             if (node.tagName === 'FIGCAPTION') return false
        //             // if (node.tagName === 'ARTICLE') return false
        //             node = node.parentNode
        //         }
        //         return super.capture(event)
        //     }
    }

    class ExtendedCardWrapper extends CardWrapper {
        constructor(node, data, defaults) {
            super(node);
            this.data = data;
            this.currentUrl = null;
            cardWrapperMap.set(node, this);

            this.interaction = new InteractionMapper(node, this);
        }

        static async create(
            data,
            parent,
            scatterContainer,
            { x, y, startScale, rotation = null } = {}
        ) {
            let element = await this.loadCardTemplate();

            element.style.opacity = 0;
            element.setAttribute('data-key', data.key);
            element.style.position = 'absolute';
            element.classList.add('cardWrapper');
            element.classList.add('mainContent');
            parent.appendChild(element);

            const language = window.config.get('startupLanguage');

            let frontLoader = new FrontLoader(
                data.front,
                {
                    startScale,
                    minScale: 0.25,
                    maxScale: 2
                },
                data.prefixPath
            );

            let legacyFront = document.createElement('div');
            await frontLoader.load(legacyFront);
            let { width, height } = frontLoader.getImageSize();

            /**
             * Create Scatter
             */
            element.classList.add('scatter');
            const padding = 10;
            const footerHeight = 50;

            let scatter = new CardScatter(element, scatterContainer, {
                x,
                y,
                useCapture: true,
                width: width + 2 * padding,
                height: height + footerHeight + padding,
                rotation,
                throwVisibility: 88,
                startScale,
                minScale: 0.5,
                maxScale: 1.5,
                debug: true,
                onTransform: this.transformed
            });

            this.createTitle(element, legacyFront);

            Drawer.init(element);

            this.createSlider(element, legacyFront);
            Details.create(element, legacyFront, { width, height });

            element.querySelectorAll('img').forEach(img => {
                img.setAttribute('draggable', 'false');
            });

            let frontFooter = element.querySelector('.frontFooter');

            let buttons = {};

            this.modes.forEach(mode => {
                if (mode != 'none') {
                    let button = frontFooter.querySelector(`.${mode}`);
                    if (!button) {
                        console.error(
                            `Could not find a menu button for mode ${mode}.`
                        );
                    } else {
                        buttons[mode] = button;
                    }
                }
            });

            for (let [key, button] of Object.entries(buttons)) {
                InteractionMapper.on('tap', button, event => {
                    Menu.toggleMode(event, key);
                });
            }

            await this.loadArticles(element, data);

            PopupContainer.fixPopups(element);

            this.changeLanguage(element, language);
            // It is important to initialize everything late since the interaction manager
            // will not find the slider.
            let wrapper = new ExtendedCardWrapper(element, data);

            this.initOnTouched(element);

            // UO: We need the tapDelegate now in Chrome and Electron
            scatter.tapDelegate = wrapper;
            if (element) {
                this.validate(element);
            } else {
                const message = `Element was not created properly: ${data.key}.`;
                if (window.errorWindow) {
                    window.errorWindow.error(message, ['wrapper']);
                }
                console.error(message, element);
            }

            this.changeMode(element, 'none');

            this.setInitialized(element);
            Logging.log(`CARD_INITIALIZED::CARD:${data.key}`);

            TweenLite.to(element, 1, {
                delay: 0.5,
                opacity: 1
            });
            return element
        }

        static initOnTouched(element) {
            let anyOnTouch =
                window.config.get('titleOnTouch') ||
                window.config.get('menuOnTouch') ||
                window.config.get('languageOnTouch');

            if (window.config.get('titleOnTouch')) {
                element.classList.add('title-on-touch');
            }

            if (window.config.get('menuOnTouch')) {
                element.classList.add('menu-on-touch');
            }

            if (window.config.get('languageOnTouch')) {
                element.classList.add('language-on-touch');
            }

            if (anyOnTouch) {
                let timeout = null;

                let removeTitleTouch = function() {
                    element.classList.remove('touched');
                }.bind(this);

                InteractionMapper.on('pointerdown', element, () => {
                    element.classList.add('touched');
                    if (timeout) clearTimeout(timeout);
                });

                element.scatter.addTransformEventCallback(event => {
                    if (event.type == 'onEnd') {
                        if (timeout) clearTimeout(timeout);
                        timeout = setTimeout(
                            removeTitleTouch,
                            window.config.get('onTouchTime')
                        );
                    }
                });

                element.addEventListener('pointerup', removeTitleTouch.bind(this));
            }
        }

        static validate(wrapper) {
            Localization.validate(wrapper);

            if (window.errorWindow) {
                /**
                 * Test if all collapsibles have correct tagName.
                 */
                const errorWindow = window.errorWindow;
                let collapsibles = wrapper.querySelectorAll('.collapsible');
                collapsibles.forEach(collapsible => {
                    let wrapper = collapsible.closest('.cardWrapper');
                    let card = this.getKey(wrapper);

                    const shouldBe = 'H2';
                    if (collapsible.tagName != shouldBe) {
                        errorWindow.error(
                            `Wrong collapsible tag on ${card} is ${collapsible.tagName} but should be ${shouldBe}: ${collapsible.outerHTML}.`
                        );
                    }
                });

                /** Test if all links are set. */

                let popups = document.querySelectorAll('article a');
                popups.forEach(popup => {
                    let wrapper = popup.closest('.cardWrapper');
                    let card = this.getKey(wrapper);
                    if (
                        !popup.getAttribute('href') &&
                        !popup.getAttribute('xlink:href')
                    ) {
                        errorWindow.error(
                            `Wrong popup on card ${card} - Popup has no href: ${popup.outerHTML}.`
                        );
                    }
                });
            }
        }

        static changeLanguage(cardWrapper, language) {
            let currentLanguage = cardWrapper.getAttribute('data-language');

            if (currentLanguage != language) {
                cardWrapper.setAttribute('data-language', language);

                let languageElements = cardWrapper.querySelectorAll('.language');
                languageElements.forEach(el => {
                    if (el.classList.contains(language)) {
                        el.classList.add('active');
                    } else {
                        el.classList.remove('active');
                    }
                });

                this.changeLanguageOfIndexData(cardWrapper, language);
                Localization.changeLanguageOfLocalizables(cardWrapper, language);

                PopupContainer.closeAll(cardWrapper);

                this.resetCurrentMode(cardWrapper);
                this.articleChanged(cardWrapper);
            }
        }

        /**
         * Resets the current mode to it's initial state.
         *
         * E.g. This is used when changing a language, to reset the details and the collapsables.
         *
         * @param {DOMElement} cardWrapper - Card element.
         */
        static resetCurrentMode(cardWrapper) {
            let mode = this.getMode(cardWrapper);
            switch (mode) {
                case 'none':
                case 'slider':
                    break
                case 'details':
                    DetailsDrawerSynch.closeAll(cardWrapper);
                    Drawer.reset(cardWrapper);
                    break
                case 'background':
                    Drawer.reset(cardWrapper);
            }
        }

        static getKey(cardWrapper) {
            return cardWrapper.getAttribute('data-key')
        }

        /**
         * As the index.html is a special case considering localization,
         * as it has the localization values as attributes instead of separate files.
         * The attributes need to be adjusted manually.
         *
         * @param {*} cardWrapper
         */
        static changeLanguageOfIndexData(cardWrapper, language) {
            let slides = cardWrapper.querySelectorAll('.slide');
            slides.forEach(slide => {
                let languageText = slide.getAttribute(`data-alt-${language}`);
                if (!languageText) {
                    const errorMessage = `Card ${cardWrapper.getAttribute(
                    'data-key'
                )} has missing language attribute "data-${language}" on slide "${
                    slide.classList[1]
                }".`;
                    if (window.errorWindow) {
                        window.errorWindow.error(errorMessage);
                    }

                    console.error(errorMessage, cardWrapper);
                } else {
                    slide.setAttribute('alt', languageText);
                }
            });
            Slider.updateSlideFigure(cardWrapper);
        }

        static createTitle(element, legacyFront) {
            let title = element.querySelector('.title');

            const legacyCard = legacyFront.querySelector('.mainCard');

            title.innerText = legacyCard.getAttribute('data-title');

            let languages = {};
            let attributes = [...legacyCard.attributes];
            attributes.forEach(attr => {
                if (attr.name.startsWith('data-title-')) {
                    const lang = attr.name.replace('data-title-', '');
                    languages[lang] = attr.value;
                }
            });

            Localization.makeLocalizable(title, languages);
        }

        static createSlider(element, indexDom) {
            let slideFigure = element.querySelector('.slideFigure');

            indexDom.querySelectorAll('.slideFigure img').forEach(img => {
                slideFigure.appendChild(img);
                img.setAttribute('draggable', false);
            });

            let wrapper = ExtendedCardWrapper.getCardWrapper(element);

            /*
            We must use the non-static version of the slider as we need the interaction mapper
            that is only working when we overwrite non-static funtions.
            */
            new Slider(wrapper);
        }

        /**
         * Loads all articles from the data object.
         * And prepares that loaded data.
         *
         * @param {DOMElement} element
         */
        static async loadArticles(element, data) {
            let drawerContent = element.querySelector('.scroll');

            let sections = ['background', 'details'];

            for (let sectionName of sections) {
                let section = drawerContent.querySelector(`section.${sectionName}`);

                for (let [language, file] of Object.entries(data[sectionName])) {
                    let html = await this.loadSection(
                        sectionName,
                        file,
                        data.prefixPath
                    ).catch(console.error);

                    if (!section) {
                        console.error(
                            `Section ${sectionName} is not defined in template!`
                        );
                    } else {
                        html.setAttribute(`data-locale-element`, 'true');
                        html.setAttribute(`data-lang`, language);
                        section.appendChild(html);
                    }
                }

                let highlightFigures = section.querySelectorAll('figure svg');
                if (highlightFigures.length > 0) {
                    highlightFigures.forEach(svg => {
                        ObersalzbergHighlight.initHighlightFigure(svg);
                        let popups = svg.querySelectorAll('a');

                        popups.forEach(popup => {
                            const xlink = popup.getAttribute('xlink:href');
                            if (xlink) popup.setAttribute('href', xlink);
                            InteractionMapper.on('tap', popup, event => {
                                PopupContainer.handlePopup(popup, event.center);
                            });
                        });
                    });
                }

                this.prepareLowResolutionPlaceholders(section);
            }

            //Register event handlers for collapsibles.
            let articleSections = drawerContent.querySelectorAll('section');
            articleSections.forEach(section => {
                Drawer.registerEventHandler(section);
            });
        }

        static async loadSection(name, src, prefixPath) {
            return new Promise((resolve, reject) => {
                this.loadHtml(src)
                    .then(text => {
                        let parser = new DOMParser();

                        for (let attr of ['href="', 'src="', 'image="']) {
                            text = text.replace(
                                new RegExp(attr, 'gi'),
                                attr + prefixPath
                            );
                        }

                        let html = parser.parseFromString(text, 'text/html');
                        let article = html.querySelector('article');

                        resolve(article);
                    })
                    .catch(reject);
            })
        }

        static loadCardTemplate(params) {
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();

                setTimeout(() => {
                    reject(`Could not load template.Request timed out.`);
                }, 1000);

                xhr.addEventListener('readystatechange', event => {
                    if (xhr.readyState == 4 && xhr.responseText) {
                        try {
                            let parser = new DOMParser();
                            let dom = parser.parseFromString(
                                xhr.responseText,
                                'text/html'
                            );
                            let card = dom.querySelector('.cardWrapper');
                            resolve(card);
                        } catch (e) {
                            reject(`Could not parse template: ${e} `);
                        }
                    }
                });

                xhr.open('GET', './templates/card/card.html');
                xhr.send();
            })
        }

        /**
         * Replaces all images with a generated low resolution placeholder.
         *
         * @param {DOMObject} element - HTML string of the card content.
         */
        static prepareLowResolutionPlaceholders(element) {
            let lowlyRequiresUpdate = false;
            let attributes = [
                {
                    // Details circles.
                    selector: 'g image',
                    tagName: 'image',
                    urlAttribute: 'href'
                },
                {
                    // Details background.
                    tagName: 'image',
                    urlAttribute: 'xlink:href'
                },
                {
                    // All others
                    tagName: 'img',
                    urlAttribute: 'src'
                }
            ];

            attributes.forEach(attribute => {
                let selector = attribute.selector
                    ? `${attribute.selector}`
                    : `${attribute.tagName}`;

                let elements = element.querySelectorAll(selector);

                elements.forEach(img => {
                    let original = img.getAttribute(attribute.urlAttribute);

                    if (original == null) {
                        console.error(
                            `Could not get attribute '${attribute.urlAttribute}'.`,
                            img
                        );
                    } else {
                        const attributes = ['data-image', 'data-preview-image'];
                        // Save image path.
                        img.setAttribute(attributes[0], original);

                        // Replace original with lowly path.
                        let parts = original.split('.');
                        let extension = parts.pop();
                        let lowlyPath = parts.join('.') + '-lowly.' + extension;
                        //('lowlyPath', lowlyPath)
                        img.setAttribute(attributes[1], lowlyPath);
                        img.setAttribute(attribute.urlAttribute, lowlyPath);

                        let images = [original, lowlyPath];

                        images.forEach(imagePath => {
                            let xhr = new XMLHttpRequest();
                            xhr.open('GET', imagePath);
                            xhr.addEventListener('error', () => {
                                let message = `Image is missing ${imagePath}.`;

                                if (imagePath.includes('-lowly')) {
                                    if (
                                        imagePath.endsWith('.tif') ||
                                        imagePath.endsWith('.tiff')
                                    ) {
                                        message += ` TIFF images cannot be converted by the image script. Convert them manually into jpg or png images.`;
                                        window.errorWindow.error(message);
                                    }

                                    lowlyRequiresUpdate = true;
                                } else {
                                    if (window.errorWindow) {
                                        window.errorWindow.error(message);
                                    }
                                }

                                console.error(message);
                            });
                            xhr.send();
                        });
                    }
                });
            });

            if (lowlyRequiresUpdate && window.errorWindow) {
                window.errorWindow.error(
                    `Not all *-lowly files are generated. Rerun the image script!`
                );
            }

            return element
        }

        /**
         * Creates or updates a debug field on the card.
         *
         * @param {DOMElement} cardWrapper
         * @param {string} name - Name of the attribute.
         * @param {string} value - Value of the text.
         */
        static setDebugValue(cardWrapper, name, value) {
            let debug = cardWrapper.querySelector('.debug');
            if (!debug) {
                console.error(
                    'Your card has no debug element attached. But you try to access it!'
                );
            } else {
                let ul = debug.querySelector('ul');
                if (!ul) {
                    ul = document.createElement('ul');
                    debug.appendChild(ul);
                }

                let element = ul.querySelector(`.${name}`);
                if (!element) {
                    let li = document.createElement('li');
                    ul.appendChild(li);

                    let label = document.createElement('span');
                    label.innerText = `${name[0].toUpperCase()}${name.substr(1)}: `;
                    li.appendChild(label);

                    let valueSpan = document.createElement('span');
                    valueSpan.className = name;
                    valueSpan.innerText = value;
                    li.appendChild(valueSpan);
                } else {
                    element.innerText = value;
                }
            }
        }

        static async loadHtml(src) {
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.open('GET', src, false);
                xhr.onreadystatechange = async e => {
                    if (xhr.readyState == 4) {
                        if (xhr.responseText != '') {
                            resolve(xhr.responseText);
                        } else {
                            reject(`Could not load html: ${src}.`);
                        }
                    }
                };
                xhr.onerror = e => {
                    reject(`Could not load html: ${src}. ${e}`);
                };

                setTimeout(() => {
                    reject(`Loading HTML timed out: ${src}.`);
                }, 3000);

                xhr.send();
            })
        }

        // capture(event) {
        //     // return true
        // }

        // findTarget(event, local, global) {
        //     let target = event.target
        //     let cardWrapper = target.closest('.cardWrapper')

        //     console.log("ASDHIOASDJLIAKSDJLASD")
        //     //TODO: Move this to a proper place!
        //     if (event.type == 'pointerdown') {
        //         PopupContainer.handlePopup(target, local)
        //     }

        //     // if (
        //     //     target.tagName == 'circle' &&
        //     //     target.getAttribute('data-collapsible')
        //     // ) {
        //     //     let collapsibleName = target.getAttribute('data-collapsible')
        //     //     let collapsible = Drawer.getCollapsibleByClassName(
        //     //         cardWrapper,
        //     //         collapsibleName
        //     //     )
        //     //     if (collapsible) {
        //     //         Drawer.toggleCollapsible(collapsible)
        //     //         return collapsible
        //     //     }
        //     // }

        //     //     console.log("target", target)

        //     // let menuItems = Menu.getItems(target)
        //     // if (menuItems.indexOf(target) != -1) return this
        //     if (cardWrapper == this.domNode) {
        //         let collapsible = target.closest('.collapsible')
        //         if (collapsible) {
        //             return this
        //         }
        //     }
        //     return null
        // }

        // onStart(event, interaction) {}

        // onMove(event, interaction) {
        //     console.log('I LIKE TO MOVE IT!')
        // }

        // onEnd(event, interaction) {
        //     let x = event.clientX
        //     console.log("END!!!!!!!!!")
        //     let y = event.clientY
        //     let target = document.elementFromPoint(x, y)
        //     document.elementFromPoint(x, y)
        //     let collapsible = target.closest('.collapsible')
        //     if (collapsible) {
        //         let arr = Array.from(collapsible.classList).slice()

        //         let activeIdx = arr.indexOf('active')
        //         let active = activeIdx != -1 ? 'close' : 'open'
        //         if (active) {
        //             arr.splice(activeIdx, 1)
        //         }

        //         let collapsibleIdx = arr.indexOf('collapsible')
        //         if (collapsibleIdx != -1) {
        //             arr.splice(collapsibleIdx, 1)
        //         }

        //         let wrapper = ObersalzbergCard.getCardWrapper(collapsible)
        //         let key = ObersalzbergCard.getKey(wrapper)

        //         Logging.log(
        //             `COLLAPSIBLE_CLICKED::CARD:${key},NAME:${arr.join(
        //                 '&'
        //             )},ACTIVE:${active}`
        //         )

        //         Drawer.toggleCollapsible(collapsible)
        //         return
        //     }
        // }

        static getCardWrapper(node) {
            return node.closest('.cardWrapper')
        }

        static get(node) {
            return cardWrapperMap.get(node)
        }
    }

    class ObersalzbergCard extends ExtendedCardWrapper {
        static isInitialized(cardWrapper) {
            return cardWrapper.getAttribute('data-initialized') === 'true'
        }

        static setInitialized(cardWrapper) {
            cardWrapper.setAttribute('data-initialized', true);
        }

        static hideNode(node) {
            if (node) {
                node.style.pointerEvents = 'none';
                TweenLite.to(node, 0.5, { autoAlpha: 0 });
            }
        }

        static showNode(node) {
            if (node) {
                node.style.pointerEvents = 'auto';
                TweenLite.to(node, 0.5, { autoAlpha: 1 });
            }
        }

        static showDefault(cardWrapper) {
            let img = cardWrapper.querySelector('img.original');

            cardWrapper.setAttribute('data-mode', 'none');
            Drawer.hideDrawer(cardWrapper);

            if (img) {
                ImageReplacer.replaceWithHighResImage(img);
            } else console.error('Originial image could not be found!', cardWrapper);
        }

        static exitDefault(cardWrapper) {}

        static showArticle(cardWrapper) {
            let wrapper = ExtendedCardWrapper.get(cardWrapper);
            Drawer.ensureContent(cardWrapper, wrapper.data.germanBackground);

            let images = cardWrapper.querySelectorAll('section.background img');
            images.forEach(img => ImageReplacer.replaceWithHighResImage(img));
        }

        static exitArticle(cardWrapper) {
            let images = cardWrapper.querySelectorAll('section.background img');
            images.forEach(img => ImageReplacer.replaceWithLowResImageDelayed(img));
            Drawer.closeAllCollapsibles(cardWrapper);
        }

        static get modes() {
            return ['none', 'slider', 'details', 'background']
        }

        static changeMode(wrapper, mode) {
            if (this.modes.indexOf(mode) == -1) {
                console.warn(
                    "The requested mode is not implemented! 'None' was used instead."
                );
                mode = 'none';
            }

            const currentMode = this.getModeUnfixed(wrapper);
            if (currentMode != mode) {
                // if (currentMode) this.exitMode(wrapper)

                wrapper.setAttribute('data-mode', mode);

                switch (mode) {
                    case 'slider':
                        Details.exit(wrapper);
                        this.exitArticle(wrapper);
                        this.showSlider(wrapper);
                        this.exitDefault(wrapper);
                        break
                    case 'details':
                        this.exitArticle(wrapper);
                        this.exitSlider(wrapper);
                        Details.show(wrapper);
                        this.exitDefault(wrapper);
                        break
                    case 'background':
                        this.exitSlider(wrapper);
                        Details.exit(wrapper);
                        this.showArticle(wrapper);
                        this.exitDefault(wrapper);
                        break
                    default:
                        this.exitArticle(wrapper);
                        this.exitSlider(wrapper);
                        Details.exit(wrapper);
                        this.showDefault(wrapper);
                }

                this.setArticle(wrapper, mode);
                this.modeChanged(wrapper);
            }
        }

        /**
         * Sets the corresponding article visible and
         * hides all others. Has no effect, if the mode
         * has no article.
         *
         * @param {*} wrapper
         * @param {*} mode
         *
         */
        static setArticle(wrapper, mode) {
            let modes = ['background', 'details'];
            //In the loader we call the articlepage 'hintergrund'.
            if (modes.indexOf(mode) != -1) {
                let sections = wrapper.querySelectorAll('.drawerContent section');
                sections.forEach(section => {
                    if (section.classList.contains(mode)) {
                        section.style.display = 'block';
                    } else {
                        section.style.display = 'none';
                    }
                });
            }
        }

        static modeChanged(cardWrapper) {
            const mode = this.getMode(cardWrapper);
            Menu.highlightItemByMode(cardWrapper, mode);

            if (window.config.get('debug')) {
                this.setDebugValue(cardWrapper, 'mode', mode);
            }

            let key = this.getKey(cardWrapper);
            PopupContainer.closeAll(cardWrapper);

            if (ObersalzbergCard.isInitialized(cardWrapper))
                Logging.log(`CHANGED_MODE::CARD:${key},MODE:${mode}`);
        }

        static articleChanged(cardWrapper) {
            cardWrapper.querySelectorAll('.overlayWrap').forEach(wrap => {
                wrap.style.display = 'flex';
                wrap.style.display = 'block';
            });

            // let mode = cardWrapper.getAttribute('data-mode')
            // let language = cardWrapper.getAttribute('data-language')
            // let section = cardWrapper.querySelector(`section.${mode}`)
            // if (section) {
            //     let article = section.querySelector(
            //         `article[data-lang=${language}]`
            //     )
            //     if (article) {
            //         article.querySelectorAll('.overlayWrap').forEach(wrap => {
            //             let bb = wrap.querySelector('img').getBoundingClientRect()
            //             wrap.querySelector('svg').setAttribute(
            //                 'viewbox',
            //                 `0 0 ${bb.width} ${bb.height}`
            //             )
            //         })
            //     }
            // }
        }

        /**
         * Adjusts the svg viewbox for figures that contain
         * image popups.
         *
         * @static
         */
        static async createFigurePopups(wrapper) {
            let svgs = wrapper.querySelectorAll('.drawer figure svg');
            svgs.forEach(svg => {
                let wrapper = svg.closest('.overlayWrap');
                if (wrapper) {
                    let originalImage = wrapper.querySelector('img');

                    svg.setAttribute(
                        'viewbox',
                        `0 0 ${originalImage.clientWidth} ${originalImage.clientHeight}`
                    );
                }
            });
        }

        static exitSlider(cardWrapper) {
            Slider.stopAnimation(cardWrapper);
            Slider.changed(cardWrapper, 0);
            Slider.showOriginalImage(cardWrapper);
            this.hideSlider(cardWrapper);

            this.getSlideImages(cardWrapper).forEach(img => {
                ImageReplacer.replaceWithLowResImageDelayed(img);
                img.style.display = 'none';
            });
        }

        static showSlider(cardWrapper) {
            let sliderBase = cardWrapper.querySelector('.sliderBase');
            sliderBase.style.visibility = 'visible';
            this.showNode(sliderBase);

            Slider.hideOriginalImage(cardWrapper);
            Drawer.hideDrawer(cardWrapper);
            Slider.setValue(cardWrapper, 0);
            // UO: avoid flicker and set to block after loading
            this.getSlideImages(cardWrapper).forEach(img => {
                ImageReplacer.replaceWithHighResImage(img);
            });

            if (window.config.get('showOriginalAtSliderStart')) {
                setTimeout(() => {
                    this.getSlideImages(cardWrapper).forEach(img => {
                        img.style.display = 'block';
                    });
                }, 500);
            } else {
                this.getSlideImages(cardWrapper).forEach(img => {
                    img.style.display = 'block';
                });
            }
        }

        static hideSlider(cardWrapper) {
            let sliderBase = cardWrapper.querySelector('.sliderBase');
            this.hideNode(sliderBase);
        }

        static getSlideImages(cardWrapper) {
            return cardWrapper.querySelectorAll('.slideFigure > img:not(.original)')
        }

        /**
         * Gets the mode of the card wrapper.
         * The mode is not always valid and may be null.
         *
         * @param {HTMLElement} cardWrapper
         * @returns {null | string} - If a mode is set the string is returned. Otherwise false.
         */
        static getModeUnfixed(cardWrapper) {
            return cardWrapper.getAttribute('data-mode')
        }

        /**
         * Returns the mode of the card wrapper.
         * If no mode is set. A mode of 'none' is returned.
         *
         * @param {HTMLElement} cardWrapper
         * @returns {string} - Returns the mode of the card. If none is set the mode 'none' is returned.
         */
        static getMode(cardWrapper) {
            let mode = this.getModeUnfixed(cardWrapper);
            return mode == null ? 'none' : mode
        }

        /**
         * Changes the mode to the provided one. Or resets to default mode
         * when already in this mode.
         *
         * @param {HTMLElement} cardWrapper
         * @param {string} mode
         * @returns {string} - Returns the mode the card is in after the change.
         */
        static toggleMode(cardWrapper, mode) {
            const current_mode = this.getMode(cardWrapper);
            const targetMode = current_mode == mode ? 'none' : mode;
            this.changeMode(cardWrapper, targetMode);
            return targetMode
        }

        /**
         * Changes the language of the card, when a button on the card is clicked.
         *
         * @param {event} event - Click event on the card.
         * @param {string} lang - Language identifier to change the card to.
         */
        static changeLanguageByEvent(event, lang) {
            let wrapper = this.getCardWrapper(event.target);

            const fromLang = wrapper.getAttribute('data-language');
            this.changeLanguage(wrapper, lang);

            let key = ObersalzbergCard.getKey(wrapper);
            Logging.log(`CHANGE_LANGUAGE::CARD:${key},FROM:${fromLang},TO:${lang}`);
        }
    }

    class ObersalzbergHighlight extends Highlight {
        static toggleHighlight(node, options = {}) {
            let wrapper = ObersalzbergCard.getCardWrapper(node);
            let key = ObersalzbergCard.getKey(wrapper);
            Logging.log(
                `TOGGLE_HIGHLIGHT::CARD:${key},HIGHLIGHT:${node.getAttribute(
                'id'
            )}:ACTIVE:${this._isExpanded(node) ? 'close' : 'open'}`
            );
            super.toggleHighlight(node, options);
        }

        static initHighlightFigure(svg) {
            let image = svg.querySelector('image');

            svg.querySelectorAll('circle').forEach(circle => {
                if (window.config.get('highlightStroke') != null) {
                    circle.setAttribute(
                        'stroke-width',
                        window.config.get('highlightStroke')
                    );
                }
            });

            if (image) {
                svg.querySelectorAll('circle').forEach(circle => {
                    this._createSVGMask(circle, { svgRoot: svg, image });

                    // Somehow the mask is not working here,
                    // when the mask is inside the <defs> tag.
                    // So we put it outside! Feel free to fix this, I couldn't. -sopel
                    const id = circle.getAttribute('id').replace(/@/g, '');

                    let mask = svg.querySelector(`#mask${id}`);
                    mask.style.position = 'absolute';
                    svg.appendChild(mask);
                });
                image.parentNode.removeChild(image);
            } else if (window.config.get('debug')) {
                window.errorWindow.error(
                    'There is a svg specified for image highlights, but there is no image attatched to it!'
                );
            }
        }

        static _createSVGMask(
            element,
            { svgRoot = null, image = null, id = null } = {}
        ) {
            let [mask, maskedImage] = Highlight._createSVGMask(element, {
                svgRoot,
                image,
                id
            });

            let lowly = maskedImage.getAttribute('href');
            let original = lowly.replace('-lowly', '');

            /**
             * In the browser the mask also get's clicked.
             * Making the mask bigger than to original image.
             *
             * We remove all click handlers on the mask to prevent that.
             */
            mask.childNodes.forEach(element => {
                element.removeAttribute('onclick');
            });

            maskedImage.setAttribute('width', '100%');
            maskedImage.setAttribute('height', '100%');
            maskedImage.setAttribute('preserveAspectRatio', 'xMidYMid slice');

            ImageReplacer.setImages(maskedImage, original, lowly);
            ImageReplacer.replaceWithHighResImage(maskedImage, 'href');

            return [mask, maskedImage]
        }
    }

    /* eslint-disable no-console */
    class Details extends Object {
        /**
         * Creates the details view on initialization.
         * For the new str
         *
         * @param {DomElement} newElement - Newly created element's cardWrapper.
         * @param {DomElement} loadedElement - The loaded element's cardWrapper.
         */
        static create(
            newElement,
            loadedElement,
            { width = 600, height = 800 } = {}
        ) {
            let overlayHighlight = newElement.querySelector('.overlayHighlight');
            let legacyHighlights = loadedElement.querySelector('.overlayHighlight');
            overlayHighlight.innerHTML = legacyHighlights.innerHTML;

            this.initializeTheCircleImage(newElement, width, height);
        }

        /**
         * We need to create the circles when the card is created to avoid
         * weird long of the image when the image is created on click.
         *
         */
        static initializeTheCircleImage(wrapper, width, height) {
            let svgRoot = this.getSvgRoot(wrapper);
            svgRoot.setAttribute('viewBox', `0 0 ${width} ${height}`);

            let image = this.getOriginalImage(wrapper);

            Details.getAllHighlights(wrapper).forEach(highlight => {
                ObersalzbergHighlight._createSVGMask(highlight, { image, svgRoot });

                highlight.removeAttribute('onclick');

                if (window.config.get('highlightStroke') != null) {
                    highlight.setAttribute(
                        'stroke-width',
                        window.config.get('highlightStroke')
                    );
                }

                InteractionMapper.on('pointerdown', highlight, () => {
                    this.highlightNode(highlight);
                });
            });
        }

        static popupPosition(target, image) {
            let bbox = image.getBBox();
            let width = bbox.width;
            let height = bbox.height;
            let cx = target.getAttribute('cx');
            let cy = target.getAttribute('cy');
            let r = target.getAttribute('r');
            let radius = r.endsWith('%')
                ? (parseFloat(r) / 100) * Math.max(width, height)
                : parseFloat(r);

            let x = cx.endsWith('%') ? (parseFloat(cx) / 100) * width : cx;
            let y = cy.endsWith('%') ? (parseFloat(cy) / 100) * height : cx;
            y -= radius * 1.75;
            return { x, y }
        }

        static get detailsSelector() {
            return 'svg.overlayHighlight'
        }

        static getSvgRoot(cardWrapper) {
            return cardWrapper.querySelector('svg')
        }

        static getOriginalImage(cardWrapper) {
            return cardWrapper.querySelector(`${this.detailsSelector} > image`)
        }

        static getAllHighlights(cardWrapper) {
            return cardWrapper.querySelectorAll(`${this.detailsSelector} circle`)
        }

        static getNonExpandedHighlightsOfClass(cardWrapper, className) {
            return cardWrapper.querySelectorAll(
                `${this.detailsSelector} g circle.${className}:not(.expanded)`
            )
        }

        static getExpandedHighlightsOfClass(cardWrapper, className) {
            return cardWrapper.querySelectorAll(
                `${this.detailsSelector} g circle.${className}.expanded`
            )
        }

        static closeAll(cardWrapper) {
            for (let svgRoot of cardWrapper.querySelectorAll('svg')) {
                for (let expanded of svgRoot.querySelectorAll('.expanded')) {
                    ObersalzbergHighlight.closeHighlight(expanded);
                }
            }
        }

        static closeOthers(wrapper, node) {
            for (let _svgRoot of wrapper.querySelectorAll('svg')) {
                for (let expanded of _svgRoot.querySelectorAll('.expanded')) {
                    if (expanded != node) {
                        ObersalzbergHighlight.closeHighlight(expanded);
                    }
                }
            }
        }

        static openAllOfClass(wrapper, className, excludedHighlight = null) {
            let highlights = this.getNonExpandedHighlightsOfClass(
                wrapper,
                className
            );
            highlights.forEach(highlight => {
                if (highlight != excludedHighlight) {
                    if (window.config.get('highlightScale')) {
                        ObersalzbergHighlight.openHighlight(highlight, {
                            scale: window.config.get('highlightScale')
                        });
                    } else {
                        ObersalzbergHighlight.openHighlight(highlight);
                    }
                }
            });
        }

        static closeAllOfClass(wrapper, className, excludedHighlight = null) {
            let highlights = this.getExpandedHighlightsOfClass(wrapper, className);
            highlights.forEach(highlight => {
                if (highlight != excludedHighlight)
                    ObersalzbergHighlight.closeHighlight(highlight);
            });
        }

        static show(cardWrapper) {
            let overlayHighlight = cardWrapper.querySelector('.overlayHighlight');
            ObersalzbergCard.showNode(overlayHighlight);

            this.getOverlayItems(overlayHighlight).forEach(item => {
                ImageReplacer.replaceWithHighResImage(item, 'href');
            });

            let wrapper = ObersalzbergCard.get(cardWrapper);
            Drawer.ensureContent(cardWrapper, wrapper.data.germanDetails);

            let images = cardWrapper.querySelectorAll('section.details img');
            images.forEach(img => {
                ImageReplacer.replaceWithHighResImage(img).then(() => {
                    img.style.opacity = 1;
                });
            });
        }

        static exit(cardWrapper) {
            this.hideOverlay(cardWrapper);
            DetailsDrawerSynch.closeAll(cardWrapper);

            let images = cardWrapper.querySelectorAll('section.details img');
            images.forEach(img => {
                img.style.opacity = 0;
                ImageReplacer.replaceWithLowResImageDelayed(img);
            });
        }

        static hideOverlay(cardWrapper) {
            let overlayHighlight = cardWrapper.querySelector('.overlayHighlight');
            ObersalzbergCard.hideNode(overlayHighlight);

            this.getOverlayItems(overlayHighlight).forEach(item => {
                ImageReplacer.replaceWithLowResImageDelayed(item, 'href');
            });
        }

        static getOverlayItems(overlayHighlight) {
            let querySelector = ['g image'];
            let results = [];

            querySelector.forEach(selector => {
                let matches = overlayHighlight.querySelectorAll(selector);
                matches.forEach(match => {
                    results.push(match);
                });
            });

            return results
        }

        /**
         * Highlight function that is called from HTML.
         *
         * @param {*} event
         * @param {*} collapsibleSelector
         * @param {*} selector
         */
        // eslint-disable-next-line no-unused-vars
        static highlight(event, collapsibleSelector = null, selector = null) {
            let node = event.target;
            this.highlightNode(node);
        }

        static highlightNode(node) {
            let klass = node.classList[0];
            let container = node.closest('.cardWrapper');

            //Closes all but the clicked highlight
            this.closeOthers(container, node);

            if (ObersalzbergHighlight._isExpanded(node)) {
                DetailsDrawerSynch.detailsClosed(node, klass);
            } else {
                DetailsDrawerSynch.detailsOpened(node, klass);
            }

            /* 
            We toggle the Highlight afterwards.
            Then the state 'IsExpanded' is logically
            correct when clicked!
            */
            if (window.config.get('highlightScale')) {
                ObersalzbergHighlight.toggleHighlight(node, {
                    scale: window.config.get('highlightScale')
                });
            } else {
                ObersalzbergHighlight.toggleHighlight(node);
            }
        }

        /**
         * Returns associated collapsible of a ObersalzbergHighlight.
         * By accessing the 'data-collapsible' attribute.
         *
         * @param {Element} highlight - The highlight element e.g. svg-circle.
         * @returns If set a string of a class, otherwise null.
         */
        static getCollapsibleClassFromHighlight(highlight) {
            return highlight.getAttribute('data-collapsible')
        }

        static openLink(event) {
            let url = event.target.getAttribute('href');
            Events.stop(event);
            let mainCard = event.target.closest('.cardWrapper');
            Drawer.ensureContent(mainCard, url);
        }
    }

    /* global SimpleBar */

    class Drawer extends Object {
        static hideDrawer(drawer) {
            let card = drawer.closest('.cardWrapper');
            card.classList.remove('expanded');
        }

        static showDrawer(drawer) {
            let card = drawer.closest('.cardWrapper');
            card.classList.add('expanded');
        }

        /**
         * This should only be called when the collapsible is clicked.
         * More appropriate name would be 'collapsibleClicked' due to it's called
         * within the HTML files and is therefore hard to change.
         *
         *
         * @param {*} collapsible
         */
        static toggleCollapsible(collapsible) {
            let className = collapsible.classList.item(1);

            if (collapsible.classList.contains('active')) {
                DetailsDrawerSynch.collapsibleClosed(collapsible, className);
                this.closeCollapsible(collapsible);
            } else {
                DetailsDrawerSynch.collapsibleOpened(collapsible, className);
                this.openCollapsible(collapsible);
            }
        }

        static getCollapsibleContainer(collapsible) {
            return collapsible.closest('.drawer article')
        }

        static getCollapsibleContent(collapsible) {
            var content = collapsible.nextElementSibling;
            if (!(content && content.classList.contains('collapsibleContent'))) {
                console.error(
                    `The associated content was either not found or not a collapsibleContent.`,
                    collapsible
                );
                content = null;
            }

            return content
        }

        static init(cardWrapper) {
            // this.initSimplebar(cardWrapper)

            let drawer = Drawer.getDrawer(cardWrapper);

            let closeButton = drawer.querySelector('.close-button');
            closeButton.setAttribute('onclick', 'Menu.setCardModeToNone(event)');
        }

        /**
         * Simplebar overwrites the default scrollbar.
         * Unfortunately, there is no convenient way to get
         * the simplebar element from the simplebar element.
         *
         * So we create it manually and add it to the element itself.
         *
         * @param {DomElement} element
         */
        static initSimplebar(element) {
            let scroll = element.querySelector('.scroll');
            const simplebar = new SimpleBar(scroll, {
                autoHide: false
            });
            scroll.simplebar = simplebar;
        }

        /**
         * Get's a collapsible element by class name.
         *
         * @param {Element} wrapper - Cardwrapper element.
         * @param {string} className - Class name of the collapsible.
         */
        static getCollapsibleByClassName(wrapper, className) {
            let container = this.getDrawer(wrapper);
            let collapsible = container.querySelector(
                `article:not(.hidden) .collapsible.${className}`
            );

            if (!collapsible)
                console.error(
                    `No collapsible with class name of '${className}' was found.`
                );
            return collapsible
        }

        /**
         * Calls the openCollapsible using a wrapper and a class name instead of the
         * collapsible itself.
         *
         * @param {Element} wrapper - Cardwrapper element.
         * @param {string} className - Class name of the collapsible to open.
         */
        static openCollapsibleByClassName(wrapper, className) {
            let collapsible = this.getCollapsibleByClassName(wrapper, className);
            if (collapsible !== null) this.openCollapsible(collapsible);
        }

        /**
         * Calls the closeCollapsible using a wrapper and a class name instead of the
         * collapsible itself.
         *
         * @param {Element} cardWrapper - Cardwrapper element.
         * @param {string} className - Class name of the collapsible to close.
         */
        static closeCollapsibleByClassName(cardWrapper, className) {
            let collapsible = this.getCollapsibleByClassName(cardWrapper, className);
            if (collapsible !== null) this.closeCollapsible(collapsible);
        }

        /**
         *  Closes all collapsibles.
         *
         * @param {DomElement} cardWrapper - card
         */
        static closeAllCollapsibles(cardWrapper) {
            let drawer = this.getDrawer(cardWrapper);
            let collapsibles = drawer.querySelectorAll('.collapsible');
            collapsibles.forEach(collapsible => {
                this.closeCollapsible(collapsible);
            });
        }

        static async openCollapsible(collapsible) {
            let container = this.getCollapsibleContainer(collapsible);
            let content = this.getCollapsibleContent(collapsible);
            await this.unsetActiveCollapsible(container, collapsible);

            collapsible.classList.add('active');
            let height = content.scrollHeight + 'px';

            content.style.pointerEvents = 'all';
            let time = window.config.get('collapsibleScrollDuration') / 2;
            TweenLite.to(content, time, {
                maxHeight: height,
                onUpdate: function() {
                    if (window.config.get('collapsibleScrollIntoView'))
                        collapsible.scrollIntoView({ behavior: 'smooth' });
                }
            });
        }

        static scrollTo(child, scrollTo, time, additionalOptions = {}) {
            const options = Object.assign(additionalOptions, { scrollTo });
            let scroll = child.closest('.scroll');
            if (scroll) {
                let simplebar = scroll.querySelector('.simplebar-content-wrapper');
                if (simplebar != null) {
                    scroll = simplebar;
                }

                TweenLite.to(scroll, time, options);
            } else console.error('Could not find scroll element.');
        }

        /**
         * Resets the drawer by closing all collapsibles and
         * resets the scroll position.
         *
         * @param {DomElement} cardWrapper
         */
        static reset(cardWrapper) {
            this.closeAllCollapsibles(cardWrapper);

            let drawer = cardWrapper.querySelector('.drawer');
            let scroll = drawer.querySelector('.scroll');

            let simplebar = scroll.querySelector('.simplebar-content-wrapper');
            if (simplebar != null) {
                scroll = simplebar;
            }

            scroll.scrollTop = 0;
        }

        static scrollToTop(cardWrapper) {
            let drawer = cardWrapper.querySelector('.drawer');
            let scroll = drawer.querySelector('.scroll');
            scroll.scrollTop = 0;
        }

        static getScrollElement(cardWrapper) {
            const drawer = cardWrapper.querySelector('.drawer');
            const scroll = drawer.querySelector('.scroll');
            return scroll
        }

        /**
         *
         * Removes all active classes from the other collapsible items.
         * If the item is the newActiveCollapsible, it'll stay active.
         *
         * @param {Element} newActiveCollapsible - Collapsible that will be active.
         * @returns {void}
         */
        static async unsetActiveCollapsible(
            container,
            newActiveCollapsible = null
        ) {
            const promises = [];
            for (let collapsible of container.querySelectorAll(
                '.collapsible.active'
            )) {
                if (collapsible !== newActiveCollapsible) {
                    const closePromise = this.closeCollapsible(
                        collapsible,
                        newActiveCollapsible
                    );
                    promises.push(closePromise);
                }
            }
            return Promise.all(promises)
        }

        static closeCollapsible(collapsible, newActiveCollapsible = null) {
            return new Promise((resolve, reject) => {
                collapsible.classList.remove('active');
                let content = this.getCollapsibleContent(collapsible);
                let time = window.config.get('collapsibleScrollDuration') / 2;

                let wrapper = ExtendedCardWrapper.getCardWrapper(collapsible);
                PopupContainer.closeAll(wrapper);

                content.style.pointerEvents = 'none';
                TweenLite.to(content, time, {
                    maxHeight: 0,
                    onComplete: () => {
                        content.style.maxHeight = null;
                        resolve();
                    }
                });

                let scroll = collapsible.closest('.scroll');
                if (scroll) {
                    let simplebar = scroll.querySelector(
                        '.simplebar-content-wrapper'
                    );
                    if (simplebar != null) {
                        scroll = simplebar;
                    }

                    // TweenLite.to(scroll, time, {
                    //     delay: time,
                    //     scrollTo: { y: 0 }
                    // })
                } else console.error('Could not find scroll element.');
            })
        }

        static registerEventHandler(article) {
            let coll = article.getElementsByClassName('collapsible');

            for (let i = 0; i < coll.length; i++) {
                InteractionMapper.on('tap', coll[i], () => {
                    Drawer.toggleCollapsible(coll[i]);
                });
            }
        }

        static back(event) {
            let drawer = event.target.closest('.drawer');
            Drawer.hideDrawer(drawer);
            let cardWrapper = event.target.closest('.cardWrapper');
            let menuItem = cardWrapper.querySelector('.articleMenuItem');

            menuItem.style.color = 'black';
            Details.closeAllDetails(cardWrapper);
            ObersalzbergCard.changeMode(cardWrapper, 'none');
        }

        static ensureContent(mainCard, url, collapsibleSelector, selector) {
            let drawer = this.getDrawer(mainCard);
            if (!drawer) {
                console.error('There is no drawer attatched to the card.');
            } else {
                Drawer.showDrawer(drawer);
            }
        }

        static getDrawer(wrapper) {
            return wrapper.querySelector('.drawer')
        }

        static getCollapsible(wrapper, klass) {
            let drawer = this.getDrawer(wrapper);
            return drawer.querySelector('.' + klass)
        }

        static notImplemented() {
            alert('Funktion noch nicht implementiert');
        }
    }

    const heartbeatIntervall = 5000; // in ms
    const zoomedScatter = [];
    const maxZoomedScatters = 6;
    var heartbeatHandle = null;
    let stressTestRunning = false;
    let preventHeartBeats = false;
    let maxScale = 2;

    function pushZoomedScatter(scatter) {
        let index = zoomedScatter.indexOf(scatter);
        if (index >= 0) {
            zoomedScatter.splice(index, 1);
        }
        zoomedScatter.push(scatter);
        while (zoomedScatter.length > maxZoomedScatters) {
            let first = zoomedScatter.shift();
            TweenMax.to(first, 1.0, {
                scale: 1,
                onComplete: () => {
                    if (first.isOutside()) {
                        first.bouncing();
                    }
                }
            });
        }
    }

    function throwStressTest() {
        let modes = ['slider', 'details', 'background', 'none'];

        function randomMode() {
            return modes[Math.floor(Math.random() * modes.length)]
        }

        for (let scatter of app.scatterContainer.scatter.values()) {
            ObersalzbergCard.changeMode(scatter.element, randomMode());

            let delta = {
                x: (Math.random() - 0.5) * 100,
                y: (Math.random() - 0.5) * 100
            };
            let megaScale = 5;
            let scale =
                Math.random() < 0.5
                    ? 1
                    : Math.min(
                          maxScale * megaScale,
                          1.0 + Math.random() * maxScale * megaScale
                      );
            TweenMax.to(scatter, 0.5, { scale });
            if (scale > 1) {
                pushZoomedScatter(scatter);
            }

            scatter.addTestVelocity(delta);
            scatter.startThrow();
        }
        if (stressTestRunning) {
            setTimeout(throwStressTest, 500);
        } else {
            Logging.log('Stress test finished');
            for (let scatter of app.scatterContainer.scatter.values()) {
                scatter.killAnimation();
            }
            app.resetCards();
            console.log('Stress test finished');
        }
    }

    function toggleStressTest() {
        stressTestRunning = !stressTestRunning;
        if (stressTestRunning) {
            Logging.log('Stress test starts');
            console.log('Stress test starts');
            throwStressTest();
        }
    }

    function startHeartBeat() {
        if (preventHeartBeats) {
            console.log('Heartbeats already prevented');
        } else {
            heartbeatHandle = setInterval(() => {
                Logging.log('heartbeat');
                console.log('heartbeat', heartbeatHandle);
            }, heartbeatIntervall);
            console.log('Started heartbeats', heartbeatHandle);
        }
    }

    function stopHeartbeat() {
        console.log('Stop heartbeats', heartbeatHandle);
        if (heartbeatHandle != null) {
            clearInterval(heartbeatHandle);
            heartbeatHandle = null;
        } else {
            preventHeartBeats = true;
        }
    }

    function registerStressTest() {
        document.addEventListener('keydown', event => {
            switch (event.key.toLowerCase()) {
                case 't':
                    toggleStressTest();
                    break
                case 's':
                    stopHeartbeat();
                    break
                case 'h':
                    startHeartBeat();
                    break
                case 'r':
                    app.resetCards();
                    break
            }
        });
        console.log(
            "Tests registered. Type 't' to toggle stress test, 's' to stop heartbeats, 'h' to start heartbeats, 'r' to reset cards."
        );
    }

    class JSONLoader {
        constructor(file) {
            this.file = file;
        }

        async load() {
            return new Promise((resolve, reject) => {
                let xhr = new XMLHttpRequest();
                xhr.open('GET', this.file);

                xhr.addEventListener('readystatechange', () => {
                    if (xhr.readyState == 4 && xhr.responseText != '') {
                        try {
                            let json = JSON.parse(xhr.responseText);
                            resolve(json);
                        } catch (e) {
                            reject(e);
                        }
                    }
                });
                xhr.addEventListener('error', err => {
                    reject(err);
                });
                setTimeout(() => {
                    reject(`Loading config file timed out....`);
                }, 3000);

                xhr.send();
            })
        }
    }

    /* eslint-disable no-console */

    /**
     * This configuration handler manages the config.
     *
     * Entries can be added using the *define()* function
     * and providing a default value. If the value is not specified
     * inside the config file, the default value is applied.
     *
     * The configuration can be overwritten by using a query parameter
     * named the same as the config parameter (case-sensitive).
     *
     * Arrays can be provided by using the same parameter key multiple times
     * e.g. ...html?cards=01&cards=02&cards=14
     *
     * However, when defining an array the 'applyQueryOverwrites' must be overwritten
     * inside the subclass, as a single array entry would be interpreted as single value
     * and needs to be converted into an array at that point.
     *
     */
    class Config {
        constructor(file) {
            this.file = file;
            this.definitions = {};
            this.loaders = {};
            this.define();
        }

        /**
         * Used to set all necessary defines in childclass.
         */
        define() {}

        async load() {
            return new Promise((resolve, reject) => {
                if (window.config != null) {
                    resolve(window.config);
                } else {
                    let loader = new JSONLoader(this.file);
                    loader
                        .load()
                        .then(obj => {
                            this.apply(obj)
                                .then(() => {
                                    let queryObject = this.queryOverwrites();
                                    this.applyQueryOverwrites(queryObject);
                                    resolve(obj);
                                })
                                .catch(reject);
                        })
                        .catch(reject);
                }
            })
        }

        /**
         * Determines how the query overwrites the definitions.
         * This may be overloaded in subclass to enforce e.g. arrays.
         */
        applyQueryOverwrites(queryObject) {
            Object.assign(this.definitions, queryObject);
        }

        /**
         * Checks the values if they are overwritten inside the query.
         */
        queryOverwrites() {
            let overwriteObject = {};
            let urlSearchParams = new URLSearchParams(window.location.search);
            let processed = [];
            for (let key of urlSearchParams.keys()) {
                //Skip if already processed. This is necessary if handling arrays.
                if (processed.indexOf(key) == -1) {
                    processed.push(key);
                    if (this.definitions.hasOwnProperty(key)) {
                        let results = urlSearchParams.getAll(key);
                        let value = results.length == 1 ? results[0] : results;
                        overwriteObject[key] = value;
                        console.warn(
                            `The query overwrites the key ${key} with: ${value}.`
                        );
                    }
                }
            }

            return overwriteObject
        }

        add(name, defaultValue) {
            if (this.definitions.hasOwnProperty(name)) {
                console.error(
                    `Cannot redeclare '${name}', it has been already declared.`
                );
            } else {
                this.definitions[name] = defaultValue;
            }
        }

        addLoader(name, loadingFunction) {
            if (!this.definitions.hasOwnProperty(name)) {
                console.error(
                    `Config property '${name}' was not yet defined. Can only add a loader to an already defined key.`
                );
            } else {
                this.loaders[name] = loadingFunction;
            }
        }

        get(key) {
            if (!this.definitions.hasOwnProperty(key)) {
                console.error(
                    `Tried to get config of '${key}'. This is not defined yet.`
                );
            } else {
                return this.definitions[key]
            }
        }

        async applyLoader(key) {
            return new Promise((resolve, reject) => {
                if (this.loaders.hasOwnProperty(key)) {
                    this.loaders[key]
                        .call(this)
                        .then(obj => {
                            this.definitions[key] = obj;
                            resolve(obj);
                        })
                        .catch(reject);
                } else {
                    reject(`No loader with key '${key}' was found.`);
                }
            })
        }

        async apply(obj) {
            let copy = Object.assign({}, obj);
            for (let key of Object.keys(this.definitions)) {
                if (copy.hasOwnProperty(key) || this.loaders.hasOwnProperty(key)) {
                    if (this.loaders.hasOwnProperty(key)) {
                        await this.applyLoader(key);
                    } else {
                        this.definitions[key] = copy[key];
                    }
                    delete copy[key];
                } else {
                    console.warn(
                        `Key '${key}' was not set in config file '${this.file}'.`
                    );
                }
            }

            for (let key of Object.keys(copy)) {
                console.warn(
                    `Key '${key}' was set in the config, but not defined inside the definition.`
                );
            }
        }
    }

    class ObersalzbergConfig extends Config {
        define() {
            this.addLocalization();

            //The expanded scale of th highlights.
            this.add('highlightScale', 2);
            this.add('highlightStroke', 2);

            // Enables various debugging utilities.
            this.add('debug', false);

            // Add debug functionality for card sorting. E.g. draws relevant informatio to debug canvas.
            this.add('debugCardSorting', false);

            // Cards array. Is set by the cards loader.
            // The cards json can be generated via: 'Entwickler > Skripte > Kartenkonfiguration aktualisieren'
            this.add('cards', []);
            this.addLoader('cards', () => {
                return new Promise((resolve, reject) => {
                    let loader = new JSONLoader('cards.json');
                    loader
                        .load()
                        .then(resolve)
                        .catch(reject);
                })
            });
            //Sorting style that is applied to the cards.
            this.add('display', 'desktop'); //table or desktop
            this.add('sortingType', 'line'); //random
            this.add('sortingDuration', '5');

            //Reset time in minutes.
            this.add('resetTimer', true);
            this.add('resetTime', 5);

            // Details scrolling.
            this.add('collapsibleScrollIntoView', true);
            this.add('collapsibleScrollDuration', 1);

            this.add('onTouchTime', 3000);

            this.add('titleOnTouch', false);
            this.add('languageOnTouch', false);
            this.add('menuOnTouch', false);

            this.add('fadeSliderText', true); // When set, the text's opacity will be animated, otherwise there will be a hard jump.

            this.add('translateCardOnScrollLimit', false);
            this.add('startScale', 0.5); // Initial scale and reset scale
            this.add('showOriginalAtSliderStart', false); // Show original when switching to slider mode
        }

        addLocalization() {
            this.add('startupLanguage', 'de');
            this.add('languages', ['de', 'en']);
            this.add('useNonLocalizedCardVersions', false);
        }

        applyQueryOverwrites(query) {
            if (query['cards']) {
                if (!Array.isArray(query.cards)) query.cards = [query.cards];
            }
            super.applyQueryOverwrites(query);
        }
    }

    /**
     * The EventHandler class is used to take care of a event based design
     * pattern. Callbacks can subscribe to an event and these unknown sources
     * get notified whenever the event changes.
     *
     * @export
     * @class EventHandler
     */
    class EventHandler {
        /**
         * Creates an instance of EventHandler.
         * @param {any} name
         * @param {any} [{
         *         listeners = [] - With the listnerers parameter the user can specify a function, array of functions or null (no function - useful when used in constructor with optional parameter).
         *     }={}]
         * @memberof EventHandler
         */
        constructor(name, { listeners = [] } = {}) {
            this.name = name;
            this.listeners = [];
            this.onces = [];

            /**
             * One may initialize the eventListener using a parameter
             * that is either passed or null.
             */

            if (listeners == null) ; else if (Array.isArray(listeners)) this.listeners = listeners;
            else if (typeof listeners == 'function') {
                this.listeners = [];
                this.add(listeners);
            } else {
                console.warn(
                    "The provided 'listeners' is neither an Array of functions, nor a function. No eventcallback was added!",
                    listeners,
                    this
                );
            }
        }

        addMultiple(...callbacks) {
            for (let callback of callbacks) {
                this.listeners.push(callback);
            }
        }

        add(callback) {
            this.listeners.push(callback);
            return callback
        }

        once(callback) {
            this.onces.push(callback);
        }

        remove(func) {
            for (const [idx, listener] of this.listeners.entries()) {
                if (listener === func) {
                    this.listeners.splice(idx, 1);
                    return true
                }
            }
            return false
        }

        empty() {
            this.listeners = [];
        }

        call(context, ...args) {
            if (context == null) {
                this.listeners.forEach(listener => listener(...args));
                this.onces.forEach(listener => listener(...args));
            } else {
                this.listeners.forEach(listener => listener.call(context, ...args));
                this.onces.forEach(listener => listener.call(context, ...args));
            }

            this.onces = [];
        }

        get length() {
            return this.listeners.length + this.onces.length
        }
    }

    class ErrorWindow {
        constructor() {
            // Queue that get's filled it the window is not created yet.
            this.queue = [];

            this.requested = false;
        }

        create() {
            this.window = document.createElement('div');
            this.window.id = 'error-window';

            let heading = document.createElement('header');
            this.window.appendChild(heading);

            let icon = document.createElement('i');
            icon.className = 'fas fa-chevron-up';
            heading.appendChild(icon);

            let h3 = document.createElement('h3');
            h3.innerText = 'Error Window';
            heading.appendChild(h3);

            this.counter = document.createElement('span');
            this.counter.className = 'counter';
            heading.appendChild(this.counter);

            let listContainer = document.createElement('div');
            listContainer.className = 'list-wrapper';
            this.window.appendChild(listContainer);

            this.list = document.createElement('div');
            this.list.className = 'list';
            listContainer.appendChild(this.list);

            heading.addEventListener('click', () => {
                this.window.classList.toggle('expanded');
            });

            document.body.appendChild(this.window);
        }

        render() {
            if (this.list) {
                this.list.innerHTML = '';
                this.queue.forEach((element, index) => {
                    let messageBox = document.createElement('p');
                    messageBox.className = element.type;
                    if (this.list) {
                        this.list.appendChild(messageBox);
                        messageBox.innerText = element.message;
                        messageBox.addEventListener('click', () => {
                            this.queue.splice(index, 1);
                            this.render();
                        });
                        this.updateCount();
                    } else console.error('No error window was created.');
                    return messageBox
                });
            }
        }

        changed() {
            if (!this.requested) {
                this.requested = true;
                setTimeout(() => {
                    this.render();
                    this.requested = false;
                }, 500);
            }
        }

        updateCount() {
            this.counter.innerText = this.queue.length;
        }

        add(message, type = 'info', tags = []) {
            let em = new ErrorMessage(message, type, tags);
            this.addErrorMessage(em);
        }

        error(message, tags = []) {
            const em = new ErrorMessage(message, 'error', tags);
            this.addErrorMessage(em);
        }
        addErrorMessage(em) {
            this.queue.push(em);
            this.changed();
        }
    }

    class ErrorMessage {
        constructor(message, type, tags = []) {
            this.message = message;
            this.type = type;
            this.tags = tags;
        }

        hasTag(tag) {
            return this.tags.indexOf(tag) != -1
        }
    }

    /**
     * The time class is based on miliseconds and helps
     * to create more readable time conversions.
     */
    class Time {
        static seconds(seconds) {
            return seconds * 1000
        }

        static minutes(minutes) {
            return this.seconds(60) * minutes
        }

        static hours(hours) {
            return this.minutes(60) * hours
        }
        static days(days) {
            return this.hours(24) * days
        }

        static get({ days = 0, hours = 0, minutes = 0, seconds = 0 } = {}) {
            return [
                this.days(days),
                this.hours(hours),
                this.minutes(minutes),
                this.seconds(seconds)
            ].reduce((sum, val) => sum + val)
        }
    }

    class ResetTimer {
        constructor({ time = Time.minutes(5), resetFunction = () => {} } = {}) {
            this.time = time;
            this.timeout = null;
            this.resetFunction = resetFunction;

            this.resetFunction = this.resetFunction.bind(this);
            this.start = this.start.bind(this);

            this.setup();
        }

        setup() {
            if (this.time > 0) {
                this.start();
                document.body.addEventListener('pointerdown', () => {
                    this.start();
                });
            } else console.error('Reset time was invalid and therefore not set!');

            document.addEventListener('keydown', event => {
                if (event.ctrlKey == true && event.key == 'o') {
                    event.preventDefault();
                    this.resetFunction();
                }
            });
        }

        start() {
            clearTimeout(this.timeout);
            this.timeout = setTimeout(() => {
                this.start();
                this.resetFunction();
            }, this.time);
        }
    }

    /* global main */

    window.ObersalzbergCard = ObersalzbergCard;
    window.Drawer = Drawer;
    window.Details = Details;
    window.Menu = Menu;
    window.PopupContainer = PopupContainer;

    // if (window.ipcRenderer) {
    //     ;[('log', 'warn', 'error')].forEach(messageType => {
    //         console.log(window.Logging)

    //          setTimeout(, 10)

    //         window.Logging[messageType] = message => {
    //             window.ipcRenderer.send(messageType, message)
    //             console.log('SENT SHIT MATE!')
    //         }
    //     })
    // } else {
    //     console.error('Electron logging is really not possible.')
    // }

    window.traceElement = function(query) {
        let el = document.querySelector(query);
        if (el) {
            let rect = el.getBoundingClientRect();
            let d = document.createElement('div');
            Object.assign(d.style, {
                position: 'absolute',
                width: rect.width + 'px',
                height: rect.height + 'px',
                backgroundColor: 'red',
                left: rect.x + 'px',
                top: rect.y + 'px',
                zIndex: 1000000000000000000
            });

            document.body.appendChild(d);

            setTimeout(() => {
                document.body.removeChild(d);
            }, 3000);
        } else throw new Error('Invalid Query!')
    };

    DOMScatter.timeoutThrow();
    let overlayAnimation = null;
    let data = [];

    function pad(num, size) {
        var s = num + '';
        while (s.length < size) s = '0' + s;
        return s
    }

    class ObersalzbergApp {
        constructor() {
            this.cards = [];
            this.progress = 0;
            this.onLoaded = new EventHandler('loaded');
            this.onCardLoaded = new EventHandler('onCardLoaded');
        }

        async setup() {
            await this.loadConfig();
            if (this.config.get('debug')) this.setupDebugging();
            this.loadCardData();
        }

        async loadConfig() {
            let config = new ObersalzbergConfig('config.json');
            await config.load().catch(console.error);
            this.config = window.config = config;
        }

        setupDebugging() {
            let errorWindow = (window.errorWindow = new ErrorWindow());
            window.error = errorWindow.error.bind(errorWindow);

            window.errorWindow.create();
        }

        loadCardData() {
            let dir = './var/cards/';
            //Defined inside the config.js
            const cards = window.config.get('cards');
            console.log(`Load cards: ${cards.join(', ')}`);
            for (let i = 0; i < cards.length; i++) {
                this.createCardData(dir, cards[i]);
            }
        }

        loader(url, prefixPath) {
            return new FrontLoader(url, {}, prefixPath)
        }

        createMemoryLeak() {
            setInterval(() => {
                this.evenHandler = [];
                this.evenHandler.push(
                    new EventHandler('Blah', () => {
                        console.log('Nothing in peticular!');
                    })
                );
            }, 100);
        }

        showProgress() {
            if (typeof loadingBar != 'undefined') {
                let percent = parseInt(this.progress);
                loadingBar.style.width = percent + '%';
                // console.log('showProgress', percent)
                if (percent >= 99) {
                    loadingBar.style.width = '100%';
                    if (overlayAnimation == null) {
                        overlayAnimation = true;
                        TweenMax.to(startOverlay, 1.5, {
                            delay: 3,
                            backgroundColor: 'rgba(0, 0, 0, 0)',
                            onComplete: () => {
                                startOverlay.remove();

                                this.setupResetTimer();
                                {
                                    // window.config.get('display') == 'table') {
                                    startHeartBeat();
                                }
                            }
                        });
                    }
                }
            }
        }

        prepareProgress() {
            let allImages = main.getElementsByTagName('img');
            let allSvgImages = main.getElementsByTagNameNS(
                'http://www.w3.org/2000/svg',
                'image'
            );
            let remaining = 100 - this.progress;
            let totalImages = allImages.length + allSvgImages.length;
            let step = remaining / totalImages;
            console.log('Detected images', totalImages);
            for (let img of allImages) {
                img.onload = () => {
                    this.progress += step;
                    this.showProgress();
                    img.onload = null;
                };
            }
            for (let img of allSvgImages) {
                img.onload = () => {
                    this.progress += step / 2;
                    this.showProgress();
                };
            }
        }

        run() {
            return new Promise((resolve, reject) => {
                let scatterContainer = new DOMCardScatterContainer(main, {
                    // stopEvents: false,
                    debugCanvas: null, // new DebugCanvas(),
                    useCapture: false
                });
                this.scatterContainer = scatterContainer;
                let x = 10;
                let y = 10;
                let rotation = 0;
                let cardPromises = [];
                data.forEach(d => {
                    let promise = this.createCard(d, scatterContainer).then(
                        element => {
                            if (window.config.get('sortingType') == 'random') {
                                let pos = this.sortRandom(
                                    element.scatter.scale,
                                    element.scatter.width,
                                    element.scatter.height
                                );

                                x = pos.x;
                                y = pos.y;

                                if (window.config.get('display') == 'table')
                                    rotation = pos.rotation;
                            } else if (window.config.get('sortingType') == 'line') {
                                x += 100;
                                y += 100;
                            }

                            const scatter = element.scatter;
                            scatter.x = x;
                            scatter.y = y;
                            scatter.rotation = rotation;
                            this.cards.push(element);
                            this.progress += 1;
                            this.showProgress();
                        }
                    );

                    cardPromises.push(promise);
                });

                Promise.all(cardPromises)
                    .then(() => {
                        this.onLoaded.call(this);
                        resolve();
                        console.log('All cards loaded');

                        this.prepareProgress();
                    })
                    .catch(console.error);
                setTimeout(() => {
                    reject('Loading cards timed out!');
                }, 30000);

                if (window.ipcRenderer) {
                    window.ipcRenderer.on('load-layout', txt => {
                        console.log('LOADED LAYOUT', txt);
                    });
                }
                registerStressTest();
            })
        }

        setupResetTimer() {
            if (this.config.get('resetTimer'))
                window.resetTimer = new ResetTimer({
                    time: Time.minutes(this.config.get('resetTime')),
                    resetFunction: () => {
                        this.resetCards();
                    }
                });
        }

        resetCards() {
            let scale = window.config.get('startScale') || 0.5;
            for (let cardScatter of this.scatterContainer.scatter.values()) {
                ObersalzbergCard.changeMode(cardScatter.element, 'none');
                let { x, y, rotation } = this.sortRandom(
                    cardScatter.scale,
                    cardScatter.width,
                    cardScatter.height
                );
                TweenLite.to(cardScatter, window.config.get('sortingDuration'), {
                    x,
                    y,
                    scale,
                    directionalRotation: {
                        rotation: rotation + '_short',
                        useRadians: true
                    }
                });
            }
        }

        createCardData(dir, idx) {
            let cardIndex = idx;
            let key = pad(cardIndex, 2);
            let prefixPath = dir + key + '/';

            let useNonLocalizedCardVersions = window.config.get(
                'useNonLocalizedCardVersions'
            );

            let defaultLanguage = window.config.get('startupLanguage');
            const languages = window.config.get('languages');
            const details = {};
            const background = {};
            const front = prefixPath + 'index.html';
            if (useNonLocalizedCardVersions) {
                details[defaultLanguage] = prefixPath + 'details.html';
                background[defaultLanguage] = prefixPath + 'hintergrund.html';
            } else {
                languages.forEach(lang => {
                    details[lang] =
                        prefixPath +
                        Localization.getFileName('details', lang, 'html');
                    background[lang] =
                        prefixPath +
                        Localization.getFileName('hintergrund', lang, 'html');
                });
            }

            data.push({
                dir,
                details,
                background,
                front,
                key,
                prefixPath
            });
        }

        async createCard(cardData, scatterContainer) {
            const startScale = window.config.get('startScale') || 0.5;

            let cardPromise = ObersalzbergCard.create(
                cardData,
                main,
                scatterContainer,
                {
                    startScale
                }
            );

            return cardPromise
        }

        sortRandom(scale, width, height, { border = 40 } = {}) {
            let left = 0 - ((1 - scale) * width) / 2 + border;
            let top = 0 - ((1 - scale) * height) / 2 + border;

            let right =
                window.innerWidth - width + ((1 - scale) * width) / 2 - border;
            let bottom =
                window.innerHeight - height + ((1 - scale) * height) / 2 - border;

            let x = left + Math.random() * (right - left);
            let y = top + Math.random() * (bottom - top);

            // Calculate screen center.
            let cx = window.innerWidth / 2;
            let cy = window.innerHeight / 2;

            let cardCenterX = x + width / 2;
            let cardCenterY = y + height / 2;

            let dx = cardCenterX - cx;
            let dy = cardCenterY - cy;

            let rotation =
                window.config.get('display') == 'table'
                    ? Math.atan2(dy, dx) - Math.PI / 2
                    : 0;

            if (
                window.config.get('debug') &&
                window.config.get('debugCardSorting')
            ) {
                // this.debugCanvas.clear()
                // this.debugCanvas.fill = 'red'
                // this.debugCanvas.drawPoint(x, y)

                this.debugCanvas.fill = 'blue';
                this.debugCanvas.drawPoint(cx, cy);

                this.debugCanvas.fill = 'yellow';
                this.debugCanvas.drawPoint(cardCenterX, cardCenterY);

                this.debugCanvas.drawLine(cx, cy, cardCenterX, cardCenterY);
            }

            return { x, y, rotation }
        }
    }

    const app$1 = new ObersalzbergApp();
    app$1.setup()
        .then(() => {
            app$1.run()
                .then(() => {
                })
                .catch(console.error);
        })
        .catch(console.error);

    window.app = app$1;

})();
