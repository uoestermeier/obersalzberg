import { ExtendedCardWrapper } from './card.js'
import { Details } from './details.js'
import { Drawer } from './drawer.js'

export class DetailsDrawerSynch {
    static detailsOpened(highlight, className) {
        let wrapper = ExtendedCardWrapper.getCardWrapper(highlight)
        Details.openAllOfClass(wrapper, className, highlight)

        let collapsible = Details.getCollapsibleClassFromHighlight(highlight)
        if (collapsible) {
            Drawer.openCollapsibleByClassName(wrapper, collapsible)
        } else {
            console.warn('Highlight has no collapsible set. ', highlight)
        }
    }

    static detailsClosed(highlight, className) {
        let wrapper = ExtendedCardWrapper.getCardWrapper(highlight)

        Details.closeAllOfClass(wrapper, className, highlight)

        let collapsible = Details.getCollapsibleClassFromHighlight(highlight)
        if (collapsible) {
            Drawer.closeCollapsibleByClassName(wrapper, collapsible)
        } else {
            console.warn('Highlight has no collapsible set. ', highlight)
        }
    }

    static collapsibleOpened(collapsible, className) {
        let wrapper = ExtendedCardWrapper.getCardWrapper(collapsible)
        Details.closeAll(wrapper)
        Details.openAllOfClass(wrapper, className)
    }

    static collapsibleClosed(collapsible, className) {
        let wrapper = ExtendedCardWrapper.getCardWrapper(collapsible)
        Details.closeAllOfClass(wrapper, className)
    }

    static closeAll(cardWrapper) {
        Details.closeAll(cardWrapper)
        Drawer.closeAllCollapsibles(cardWrapper)
    }
}
