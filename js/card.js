import CardWrapper from '../iwmlib/lib/card/wrapper.js'
import { HTMLLoader } from '../iwmlib/lib/flippable.js'
import { DOMScatter, DOMScatterContainer } from '../iwmlib/lib/scatter.js'
import { ImageReplacer } from './imagereplacer.js'
import { Drawer } from './drawer.js'
import Slider from './slider.js'
import { InteractionMapper } from '../iwmlib/lib/interaction.js'
import { Menu } from './menu.js'
import { Details } from './details.js'
import Localization from './localization.js'
import { DetailsDrawerSynch } from './details-drawer-sync.js'
import ObersalzbergHighlight from './highlight.js'
import PopupContainer from './popup.js'
import Logging from '../iwmlib/lib/logging.js'
import { pushZoomedScatter } from './zoomlimit.js'

const cardWrapperMap = new Map()

function transformChanged(scatterEvent) {
    // Scatter transform callback

    let scatter = scatterEvent.target.target
    let scale = scatterEvent.scale

    if (scale > 1) {
        //console.log('pushZoomedScatter')
        pushZoomedScatter(scatter)
    }
}

export class FrontLoader extends HTMLLoader {
    constructor(src, options = {}, prefixPath = '') {
        super(src, options)
        this.prefixPath = prefixPath
    }

    // We need a async load function therefore I just overloaded it
    // for simplicity
    async load(domNode) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest()
            xhr.open('GET', this.src, false)
            xhr.onreadystatechange = async e => {
                if (xhr.readyState == 4) {
                    domNode.innerHTML = await this.prepare(xhr.response)
                    this.addedNode = domNode.firstElementChild
                    let { width, height } = this.size(this.addedNode)
                    if (width) this.wantedWidth = width || this.wantedWidth
                    if (height) this.wantedHeight = height || this.wantedHeight
                    resolve(this)
                }
            }
            xhr.onerror = e => {
                reject(this)
            }
            xhr.send()
        })
    }

    async prepare(html) {
        html = this.addPrefixPathToLinks(html)

        let parser = new DOMParser()
        let parsedHTML = parser.parseFromString(html, 'text/html')
        this.fixSvgImageElement(parsedHTML)
        this.prepareHighlights(parsedHTML)
        ExtendedCardWrapper.prepareLowResolutionPlaceholders(parsedHTML)

        let text = parsedHTML.body.innerHTML
        return text
    }

    /**
     * Adjusts the relative paths to the project folder.
     *
     * @param {string} html - The html string of the loaded card.
     */
    addPrefixPathToLinks(html) {
        for (let attr of ['href="', 'src="', 'image="']) {
            html = html.replace(new RegExp(attr, 'gi'), attr + this.prefixPath)
        }
        let start = html.indexOf('<body>')
        if (start > -1) {
            let end = html.lastIndexOf('</body>')
            if (end > -1) {
                html = html.slice(start + 6, end)
            }
        }

        return html
    }

    /**
     * The image is not scaling with the card.
     * This is bad, and it's hard coded within the HTML.
     * Wich is also bad. So I just overwrite it here, which
     * is not ideal, but should do the trick just fine.
     * @param {*} parsedHTML
     */
    fixSvgImageElement(parsedHTML) {
        let image = parsedHTML.querySelector('svg > image')

        if (image == null)
            console.error('Could not find image.', parsedHTML.innerHTML)
        else {
            // The width and height have redundand information
            // with their svg parent. This is removed here.
            image.setAttribute('width', '100%')
            image.setAttribute('height', '100%')

            //The covering we achieve with e.g.:
            // style="background-size: cover; background-position: center;""
            // us mimiced with the following:
            image.setAttribute('preserveAspectRatio', 'xMidYMid slice')

            // We should not need to see the image. As it is just
            // a blueprint for the highlights. And we already have
            // the identical 'original' image that is always shown.
            image.style.display = 'none'
        }
    }

    /**

     *
     * @param {DOMObject} html
     */
    prepareHighlights(parsedHTML) {
        let highlights = Details.getAllHighlights(parsedHTML)
        highlights.forEach(highlight => {
            let name = this.addClassNameToHighlight(highlight)
            highlight.removeAttribute('onclick')

            if (name) {
                InteractionMapper.on('click', highlight, event => {
                    Details.highlight(event, `.${name}`)
                })
            }
        })
    }

    /**
     * We need an identifier for the highlights.
     * But there is only the class provided inside a function.
     * So I extract that klass from the function and apply it as
     * attribute.
     *
     * @param {DOMElement} highlight
     */
    addClassNameToHighlight(highlight) {
        let name = null
        let clickFunction = highlight.getAttribute('onclick')
        if (!clickFunction) {
            console.error('No onclick function was specified!', highlight)
        } else {
            //Find all parameters of the function call.
            let parameters = clickFunction.match(/(?:\().+(?=\))/g)

            if (parameters.length == 1) {
                // Get the match that we found from the array.
                parameters = parameters[0]

                // Split into single parameters.
                let splitted = parameters.split(',')
                if (splitted.length < 2) {
                    console.error(
                        'Something is wrong with the onclick function on ',
                        highlight
                    )
                } else {
                    // Finally remove all quotes and the className dot.
                    let collapsibleClass = splitted[1].replace(/["'.\s]/g, '')
                    highlight.setAttribute('data-collapsible', collapsibleClass)
                    name = collapsibleClass
                }
            } else {
                console.error(
                    'Something is wrong with the onclick function on ',
                    highlight
                )
            }
        }
        return name
    }

    getImageSize() {
        let width = 0
        let height = 0
        if (this.addedNode != null) {
            width = parseInt(this.addedNode.getAttribute('data-width'))
            height = parseInt(this.addedNode.getAttribute('data-height'))
        }
        return { width, height }
    }
}

export class CardScatter extends DOMScatter {
    constructor(element, container, options) {
        super(element, container, options)
        this.scroll = element.querySelector('.scroll')
        if (!this.scroll) console.error('Scroll element was not found!')
        this.throwScroll = false
    }

    bringToFront(event) {
        super.bringToFront()
        if (this.tipWrapper) {
            this.tipWrapper.popController()
        }
    }

    onStart(event, interaction) {
        this.throwScroll = false
        this.scrollMode = event.target.closest('.scroll') ? true : false
        super.onStart(event, interaction)
    }

    transform(translate, zoom, rotate, anchor) {
        const zooming = this.scalable && zoom != 1
        const rotating = this.rotatable && rotate != 0
        if (!zooming && !rotating) {
            this.scrollMove(translate)
        } else {
            super.transform(translate, zoom, rotate, anchor)
        }
    }

    rotateDelta(delta) {
        let alpha = -this.rotation
        return {
            x: Math.cos(alpha) * delta.x - Math.sin(alpha) * delta.y,
            y: Math.sin(alpha) * delta.x + Math.cos(alpha) * delta.y
        }
    }

    scrollMove(delta) {
        let alpha = -this.rotation

        // The card may be rotated, so we have to rotate the received x and y
        // coordinates accordingly.
        let rotatedDelta = this.rotateDelta(delta)

        if (this.scrollMode && this.scroll) {
            // Calculate the desired scrolloffset.
            let scroll = this.scroll.scrollTop - rotatedDelta.y / this.scale
            this.scroll.scrollTop = scroll

            if (window.config.get('translateCardOnScrollLimit')) {
                if (scroll < 0) {
                    rotatedDelta.y = -scroll
                } else if (
                    this.scroll.scrollHeight -
                        this.scroll.offsetHeight -
                        scroll <
                    0
                ) {
                    rotatedDelta.y =
                        this.scroll.scrollHeight -
                        this.scroll.offsetHeight -
                        scroll
                } else rotatedDelta.y = 0
            } else rotatedDelta.y = 0

            rotatedDelta.y *= this.scale
            rotatedDelta.x = 0
        }

        let reversedDelta = {
            x:
                Math.cos(alpha) * rotatedDelta.x +
                Math.sin(alpha) * rotatedDelta.y,
            y:
                -1 * Math.sin(alpha) * rotatedDelta.x +
                Math.cos(alpha) * rotatedDelta.y
        }

        super._move(reversedDelta)
        delta.x = reversedDelta.x
        delta.y = reversedDelta.y
    }

    onEnd(event, interaction) {
        super.onEnd(event, interaction)
        this.throwScroll = this.scrollMode && this.scroll
        this.scrollMode = false
    }

    _move(delta) {
        if (this.throwScroll) {
            let rotatedDelta = this.rotateDelta(delta)
            // console.log("throw", delta)
            if (this.scroll.scrollTop > 0)
                this.scroll.scrollTop -= rotatedDelta.y
            else console.log('Scroll over the top')
            //TweenLite.set(this.scroll, { scrollTop: "-=" + delta.y })
        } else {
            super._move(delta)
        }
    }
}

export class DOMCardScatterContainer extends DOMScatterContainer {
    //     capture(event) {
    //         let node = event.target
    //         while (node) {
    //             if (node.tagName === 'FIGCAPTION') return false
    //             // if (node.tagName === 'ARTICLE') return false
    //             node = node.parentNode
    //         }
    //         return super.capture(event)
    //     }
}

export class ExtendedCardWrapper extends CardWrapper {
    constructor(node, data, defaults) {
        super(node)
        this.data = data
        this.currentUrl = null
        cardWrapperMap.set(node, this)

        this.interaction = new InteractionMapper(node, this)
    }

    static async create(
        data,
        parent,
        scatterContainer,
        { x, y, startScale, rotation = null } = {}
    ) {
        let element = await this.loadCardTemplate()

        element.style.opacity = 0
        element.setAttribute('data-key', data.key)
        element.style.position = 'absolute'
        element.classList.add('cardWrapper')
        element.classList.add('mainContent')
        parent.appendChild(element)

        const language = window.config.get('startupLanguage')

        let frontLoader = new FrontLoader(
            data.front,
            {
                startScale,
                minScale: 0.25,
                maxScale: 2
            },
            data.prefixPath
        )

        let legacyFront = document.createElement('div')
        await frontLoader.load(legacyFront)
        let { width, height } = frontLoader.getImageSize()

        /**
         * Create Scatter
         */
        element.classList.add('scatter')
        const padding = 10
        const footerHeight = 50

        let scatter = new CardScatter(element, scatterContainer, {
            x,
            y,
            useCapture: true,
            width: width + 2 * padding,
            height: height + footerHeight + padding,
            rotation,
            throwVisibility: 88,
            startScale,
            minScale: 0.5,
            maxScale: 1.5,
            overdoScaling: 1.25, // 25% saves space
            debug: true,
            onTransform: this.transformed
        })

        this.createTitle(element, legacyFront)

        Drawer.init(element)

        this.createSlider(element, legacyFront)
        Details.create(element, legacyFront, { width, height })

        element.querySelectorAll('img').forEach(img => {
            img.setAttribute('draggable', 'false')
        })

        let frontFooter = element.querySelector('.frontFooter')

        let buttons = {}

        this.modes.forEach(mode => {
            if (mode != 'none') {
                let button = frontFooter.querySelector(`.${mode}`)
                if (!button) {
                    console.error(
                        `Could not find a menu button for mode ${mode}.`
                    )
                } else {
                    buttons[mode] = button
                }
            }
        })

        for (let [key, button] of Object.entries(buttons)) {
            InteractionMapper.on('tap', button, event => {
                Menu.toggleMode(event, key)
            })
        }

        await this.loadArticles(element, data)

        PopupContainer.fixPopups(element)

        this.changeLanguage(element, language)
        // It is important to initialize everything late since the interaction manager
        // will not find the slider.
        let wrapper = new ExtendedCardWrapper(element, data)

        this.initOnTouched(element)

        // UO: We need the tapDelegate now in Chrome and Electron
        scatter.tapDelegate = wrapper
        if (element) {
            this.validate(element)
        } else {
            const message = `Element was not created properly: ${data.key}.`
            if (window.errorWindow) {
                window.errorWindow.error(message, ['wrapper'])
            }
            console.error(message, element)
        }

        this.changeMode(element, 'none')

        this.setInitialized(element)
        Logging.log(`CARD_INITIALIZED::CARD:${data.key}`)

        TweenLite.to(element, 1, {
            delay: 0.5,
            opacity: 1
        })
        return element
    }

    static initOnTouched(element) {
        let anyOnTouch =
            window.config.get('titleOnTouch') ||
            window.config.get('menuOnTouch') ||
            window.config.get('languageOnTouch')

        if (window.config.get('titleOnTouch')) {
            element.classList.add('title-on-touch')
        }

        if (window.config.get('menuOnTouch')) {
            element.classList.add('menu-on-touch')
        }

        if (window.config.get('languageOnTouch')) {
            element.classList.add('language-on-touch')
        }

        if (anyOnTouch) {
            let timeout = null

            let removeTitleTouch = function() {
                element.classList.remove('touched')
            }.bind(this)

            InteractionMapper.on('pointerdown', element, () => {
                element.classList.add('touched')
                if (timeout) clearTimeout(timeout)
            })

            // uo: disable this to test the effect of the zoom limit
            element.scatter.addTransformEventCallback(transformChanged)

            element.scatter.addTransformEventCallback(event => {
                if (event.type == 'onEnd') {
                    if (timeout) clearTimeout(timeout)
                    timeout = setTimeout(
                        removeTitleTouch,
                        window.config.get('onTouchTime')
                    )
                }
            })

            element.addEventListener('pointerup', removeTitleTouch.bind(this))
        }
    }

    static validate(wrapper) {
        Localization.validate(wrapper)

        if (window.errorWindow) {
            /**
             * Test if all collapsibles have correct tagName.
             */
            const errorWindow = window.errorWindow
            let collapsibles = wrapper.querySelectorAll('.collapsible')
            collapsibles.forEach(collapsible => {
                let wrapper = collapsible.closest('.cardWrapper')
                let card = this.getKey(wrapper)

                const shouldBe = 'H2'
                if (collapsible.tagName != shouldBe) {
                    errorWindow.error(
                        `Wrong collapsible tag on ${card} is ${collapsible.tagName} but should be ${shouldBe}: ${collapsible.outerHTML}.`
                    )
                }
            })

            /** Test if all links are set. */

            let popups = document.querySelectorAll('article a')
            popups.forEach(popup => {
                let wrapper = popup.closest('.cardWrapper')
                let card = this.getKey(wrapper)
                if (
                    !popup.getAttribute('href') &&
                    !popup.getAttribute('xlink:href')
                ) {
                    errorWindow.error(
                        `Wrong popup on card ${card} - Popup has no href: ${popup.outerHTML}.`
                    )
                }
            })
        }
    }

    static changeLanguage(cardWrapper, language) {
        let currentLanguage = cardWrapper.getAttribute('data-language')

        if (currentLanguage != language) {
            cardWrapper.setAttribute('data-language', language)

            let languageElements = cardWrapper.querySelectorAll('.language')
            languageElements.forEach(el => {
                if (el.classList.contains(language)) {
                    el.classList.add('active')
                } else {
                    el.classList.remove('active')
                }
            })

            this.changeLanguageOfIndexData(cardWrapper, language)
            Localization.changeLanguageOfLocalizables(cardWrapper, language)

            PopupContainer.closeAll(cardWrapper)

            this.resetCurrentMode(cardWrapper)
            this.articleChanged(cardWrapper)
        }
    }

    /**
     * Resets the current mode to it's initial state.
     *
     * E.g. This is used when changing a language, to reset the details and the collapsables.
     *
     * @param {DOMElement} cardWrapper - Card element.
     */
    static resetCurrentMode(cardWrapper) {
        let mode = this.getMode(cardWrapper)
        'none', 'slider', 'details', 'background'
        switch (mode) {
            case 'none':
            case 'slider':
                break
            case 'details':
                DetailsDrawerSynch.closeAll(cardWrapper)
                Drawer.reset(cardWrapper)
                break
            case 'background':
                Drawer.reset(cardWrapper)
        }
    }

    static getKey(cardWrapper) {
        return cardWrapper.getAttribute('data-key')
    }

    /**
     * As the index.html is a special case considering localization,
     * as it has the localization values as attributes instead of separate files.
     * The attributes need to be adjusted manually.
     *
     * @param {*} cardWrapper
     */
    static changeLanguageOfIndexData(cardWrapper, language) {
        let slides = cardWrapper.querySelectorAll('.slide')
        slides.forEach(slide => {
            let languageText = slide.getAttribute(`data-alt-${language}`)
            if (!languageText) {
                const errorMessage = `Card ${cardWrapper.getAttribute(
                    'data-key'
                )} has missing language attribute "data-${language}" on slide "${
                    slide.classList[1]
                }".`
                if (window.errorWindow) {
                    window.errorWindow.error(errorMessage)
                }

                console.error(errorMessage, cardWrapper)
            } else {
                slide.setAttribute('alt', languageText)
            }
        })
        Slider.updateSlideFigure(cardWrapper)
    }

    static createTitle(element, legacyFront) {
        let title = element.querySelector('.title')

        const legacyCard = legacyFront.querySelector('.mainCard')

        title.innerText = legacyCard.getAttribute('data-title')

        let languages = {}
        let attributes = [...legacyCard.attributes]
        attributes.forEach(attr => {
            if (attr.name.startsWith('data-title-')) {
                const lang = attr.name.replace('data-title-', '')
                languages[lang] = attr.value
            }
        })

        Localization.makeLocalizable(title, languages)
    }

    static createSlider(element, indexDom) {
        let slideFigure = element.querySelector('.slideFigure')

        indexDom.querySelectorAll('.slideFigure img').forEach(img => {
            slideFigure.appendChild(img)
            img.setAttribute('draggable', false)
        })

        let wrapper = ExtendedCardWrapper.getCardWrapper(element)

        /*
        We must use the non-static version of the slider as we need the interaction mapper
        that is only working when we overwrite non-static funtions.
        */
        new Slider(wrapper)
    }

    /**
     * Loads all articles from the data object.
     * And prepares that loaded data.
     *
     * @param {DOMElement} element
     */
    static async loadArticles(element, data) {
        let drawerContent = element.querySelector('.scroll')

        let sections = ['background', 'details']

        for (let sectionName of sections) {
            let section = drawerContent.querySelector(`section.${sectionName}`)

            for (let [language, file] of Object.entries(data[sectionName])) {
                let html = await this.loadSection(
                    sectionName,
                    file,
                    data.prefixPath
                ).catch(console.error)

                if (!section) {
                    console.error(
                        `Section ${sectionName} is not defined in template!`
                    )
                } else {
                    html.setAttribute(`data-locale-element`, 'true')
                    html.setAttribute(`data-lang`, language)
                    section.appendChild(html)
                }
            }

            let highlightFigures = section.querySelectorAll('figure svg')
            if (highlightFigures.length > 0) {
                highlightFigures.forEach(svg => {
                    ObersalzbergHighlight.initHighlightFigure(svg)
                    let popups = svg.querySelectorAll('a')

                    popups.forEach(popup => {
                        const xlink = popup.getAttribute('xlink:href')
                        if (xlink) popup.setAttribute('href', xlink)
                        InteractionMapper.on('tap', popup, event => {
                            PopupContainer.handlePopup(popup, event.center)
                        })
                    })
                })
            }

            this.prepareLowResolutionPlaceholders(section)
        }

        //Register event handlers for collapsibles.
        let articleSections = drawerContent.querySelectorAll('section')
        articleSections.forEach(section => {
            Drawer.registerEventHandler(section)
        })
    }

    static async loadSection(name, src, prefixPath) {
        return new Promise((resolve, reject) => {
            this.loadHtml(src)
                .then(text => {
                    let parser = new DOMParser()

                    for (let attr of ['href="', 'src="', 'image="']) {
                        text = text.replace(
                            new RegExp(attr, 'gi'),
                            attr + prefixPath
                        )
                    }

                    let html = parser.parseFromString(text, 'text/html')
                    let article = html.querySelector('article')

                    resolve(article)
                })
                .catch(reject)
        })
    }

    static loadCardTemplate(params) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest()

            setTimeout(() => {
                reject(`Could not load template.Request timed out.`)
            }, 1000)

            xhr.addEventListener('readystatechange', event => {
                if (xhr.readyState == 4 && xhr.responseText) {
                    try {
                        let parser = new DOMParser()
                        let dom = parser.parseFromString(
                            xhr.responseText,
                            'text/html'
                        )
                        let card = dom.querySelector('.cardWrapper')
                        resolve(card)
                    } catch (e) {
                        reject(`Could not parse template: ${e} `)
                    }
                }
            })

            xhr.open('GET', './templates/card/card.html')
            xhr.send()
        })
    }

    /**
     * Replaces all images with a generated low resolution placeholder.
     *
     * @param {DOMObject} element - HTML string of the card content.
     */
    static prepareLowResolutionPlaceholders(element) {
        let lowlyRequiresUpdate = false
        let attributes = [
            {
                // Details circles.
                selector: 'g image',
                tagName: 'image',
                urlAttribute: 'href'
            },
            {
                // Details background.
                tagName: 'image',
                urlAttribute: 'xlink:href'
            },
            {
                // All others
                tagName: 'img',
                urlAttribute: 'src'
            }
        ]

        attributes.forEach(attribute => {
            let selector = attribute.selector
                ? `${attribute.selector}`
                : `${attribute.tagName}`

            let elements = element.querySelectorAll(selector)

            elements.forEach(img => {
                let original = img.getAttribute(attribute.urlAttribute)

                if (original == null) {
                    console.error(
                        `Could not get attribute '${attribute.urlAttribute}'.`,
                        img
                    )
                } else {
                    const attributes = ['data-image', 'data-preview-image']
                    // Save image path.
                    img.setAttribute(attributes[0], original)

                    // Replace original with lowly path.
                    let parts = original.split('.')
                    let extension = parts.pop()
                    let lowlyPath = parts.join('.') + '-lowly.' + extension
                    //('lowlyPath', lowlyPath)
                    img.setAttribute(attributes[1], lowlyPath)
                    img.setAttribute(attribute.urlAttribute, lowlyPath)

                    let images = [original, lowlyPath]

                    images.forEach(imagePath => {
                        let xhr = new XMLHttpRequest()
                        xhr.open('GET', imagePath)
                        xhr.addEventListener('error', () => {
                            let message = `Image is missing ${imagePath}.`

                            if (imagePath.includes('-lowly')) {
                                if (
                                    imagePath.endsWith('.tif') ||
                                    imagePath.endsWith('.tiff')
                                ) {
                                    message += ` TIFF images cannot be converted by the image script. Convert them manually into jpg or png images.`
                                    window.errorWindow.error(message)
                                }

                                lowlyRequiresUpdate = true
                            } else {
                                if (window.errorWindow) {
                                    window.errorWindow.error(message)
                                }
                            }

                            console.error(message)
                        })
                        xhr.send()
                    })
                }
            })
        })

        if (lowlyRequiresUpdate && window.errorWindow) {
            window.errorWindow.error(
                `Not all *-lowly files are generated. Rerun the image script!`
            )
        }

        return element
    }

    /**
     * Creates or updates a debug field on the card.
     *
     * @param {DOMElement} cardWrapper
     * @param {string} name - Name of the attribute.
     * @param {string} value - Value of the text.
     */
    static setDebugValue(cardWrapper, name, value) {
        let debug = cardWrapper.querySelector('.debug')
        if (!debug) {
            console.error(
                'Your card has no debug element attached. But you try to access it!'
            )
        } else {
            let ul = debug.querySelector('ul')
            if (!ul) {
                ul = document.createElement('ul')
                debug.appendChild(ul)
            }

            let element = ul.querySelector(`.${name}`)
            if (!element) {
                let li = document.createElement('li')
                ul.appendChild(li)

                let label = document.createElement('span')
                label.innerText = `${name[0].toUpperCase()}${name.substr(1)}: `
                li.appendChild(label)

                let valueSpan = document.createElement('span')
                valueSpan.className = name
                valueSpan.innerText = value
                li.appendChild(valueSpan)
            } else {
                element.innerText = value
            }
        }
    }

    static async loadHtml(src) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest()
            xhr.open('GET', src, false)
            xhr.onreadystatechange = async e => {
                if (xhr.readyState == 4) {
                    if (xhr.responseText != '') {
                        resolve(xhr.responseText)
                    } else {
                        reject(`Could not load html: ${src}.`)
                    }
                }
            }
            xhr.onerror = e => {
                reject(`Could not load html: ${src}. ${e}`)
            }

            setTimeout(() => {
                reject(`Loading HTML timed out: ${src}.`)
            }, 3000)

            xhr.send()
        })
    }

    // capture(event) {
    //     // return true
    // }

    // findTarget(event, local, global) {
    //     let target = event.target
    //     let cardWrapper = target.closest('.cardWrapper')

    //     console.log("ASDHIOASDJLIAKSDJLASD")
    //     //TODO: Move this to a proper place!
    //     if (event.type == 'pointerdown') {
    //         PopupContainer.handlePopup(target, local)
    //     }

    //     // if (
    //     //     target.tagName == 'circle' &&
    //     //     target.getAttribute('data-collapsible')
    //     // ) {
    //     //     let collapsibleName = target.getAttribute('data-collapsible')
    //     //     let collapsible = Drawer.getCollapsibleByClassName(
    //     //         cardWrapper,
    //     //         collapsibleName
    //     //     )
    //     //     if (collapsible) {
    //     //         Drawer.toggleCollapsible(collapsible)
    //     //         return collapsible
    //     //     }
    //     // }

    //     //     console.log("target", target)

    //     // let menuItems = Menu.getItems(target)
    //     // if (menuItems.indexOf(target) != -1) return this
    //     if (cardWrapper == this.domNode) {
    //         let collapsible = target.closest('.collapsible')
    //         if (collapsible) {
    //             return this
    //         }
    //     }
    //     return null
    // }

    // onStart(event, interaction) {}

    // onMove(event, interaction) {
    //     console.log('I LIKE TO MOVE IT!')
    // }

    // onEnd(event, interaction) {
    //     let x = event.clientX
    //     console.log("END!!!!!!!!!")
    //     let y = event.clientY
    //     let target = document.elementFromPoint(x, y)
    //     document.elementFromPoint(x, y)
    //     let collapsible = target.closest('.collapsible')
    //     if (collapsible) {
    //         let arr = Array.from(collapsible.classList).slice()

    //         let activeIdx = arr.indexOf('active')
    //         let active = activeIdx != -1 ? 'close' : 'open'
    //         if (active) {
    //             arr.splice(activeIdx, 1)
    //         }

    //         let collapsibleIdx = arr.indexOf('collapsible')
    //         if (collapsibleIdx != -1) {
    //             arr.splice(collapsibleIdx, 1)
    //         }

    //         let wrapper = ObersalzbergCard.getCardWrapper(collapsible)
    //         let key = ObersalzbergCard.getKey(wrapper)

    //         Logging.log(
    //             `COLLAPSIBLE_CLICKED::CARD:${key},NAME:${arr.join(
    //                 '&'
    //             )},ACTIVE:${active}`
    //         )

    //         Drawer.toggleCollapsible(collapsible)
    //         return
    //     }
    // }

    static getCardWrapper(node) {
        return node.closest('.cardWrapper')
    }

    static get(node) {
        return cardWrapperMap.get(node)
    }
}

export class ObersalzbergCard extends ExtendedCardWrapper {
    static isInitialized(cardWrapper) {
        return cardWrapper.getAttribute('data-initialized') === 'true'
    }

    static setInitialized(cardWrapper) {
        cardWrapper.setAttribute('data-initialized', true)
    }

    static hideNode(node) {
        if (node) {
            node.style.pointerEvents = 'none'
            TweenLite.to(node, 0.5, { autoAlpha: 0 })
        }
    }

    static showNode(node) {
        if (node) {
            node.style.pointerEvents = 'auto'
            TweenLite.to(node, 0.5, { autoAlpha: 1 })
        }
    }

    static showDefault(cardWrapper) {
        let img = cardWrapper.querySelector('img.original')

        cardWrapper.setAttribute('data-mode', 'none')
        Drawer.hideDrawer(cardWrapper)

        if (img) {
            ImageReplacer.replaceWithHighResImage(img)
        } else console.error('Originial image could not be found!', cardWrapper)
    }

    static exitDefault(cardWrapper) {}

    static showArticle(cardWrapper) {
        let wrapper = ExtendedCardWrapper.get(cardWrapper)
        Drawer.ensureContent(cardWrapper, wrapper.data.germanBackground)

        let images = cardWrapper.querySelectorAll('section.background img')
        images.forEach(img => ImageReplacer.replaceWithHighResImage(img))
    }

    static exitArticle(cardWrapper) {
        let images = cardWrapper.querySelectorAll('section.background img')
        images.forEach(img => ImageReplacer.replaceWithLowResImage(img))
        Drawer.closeAllCollapsibles(cardWrapper)
    }

    static get modes() {
        return ['none', 'slider', 'details', 'background']
    }

    static changeMode(wrapper, mode) {
        if (this.modes.indexOf(mode) == -1) {
            console.warn(
                "The requested mode is not implemented! 'None' was used instead."
            )
            mode = 'none'
        }

        const currentMode = this.getModeUnfixed(wrapper)
        if (currentMode != mode) {
            // if (currentMode) this.exitMode(wrapper)

            wrapper.setAttribute('data-mode', mode)

            switch (mode) {
                case 'slider':
                    Details.exit(wrapper)
                    this.exitArticle(wrapper)
                    this.showSlider(wrapper)
                    this.exitDefault(wrapper)
                    break
                case 'details':
                    this.exitArticle(wrapper)
                    this.exitSlider(wrapper)
                    Details.show(wrapper)
                    this.exitDefault(wrapper)
                    break
                case 'background':
                    this.exitSlider(wrapper)
                    Details.exit(wrapper)
                    this.showArticle(wrapper)
                    this.exitDefault(wrapper)
                    break
                default:
                    this.exitArticle(wrapper)
                    this.exitSlider(wrapper)
                    Details.exit(wrapper)
                    this.showDefault(wrapper)
            }

            this.setArticle(wrapper, mode)
            this.modeChanged(wrapper)
        }
    }

    /**
     * Sets the corresponding article visible and
     * hides all others. Has no effect, if the mode
     * has no article.
     *
     * @param {*} wrapper
     * @param {*} mode
     *
     */
    static setArticle(wrapper, mode) {
        let modes = ['background', 'details']
        //In the loader we call the articlepage 'hintergrund'.
        if (modes.indexOf(mode) != -1) {
            let sections = wrapper.querySelectorAll('.drawerContent section')
            sections.forEach(section => {
                if (section.classList.contains(mode)) {
                    section.style.display = 'block'
                } else {
                    section.style.display = 'none'
                }
            })
        }
    }

    static modeChanged(cardWrapper) {
        const mode = this.getMode(cardWrapper)
        Menu.highlightItemByMode(cardWrapper, mode)

        if (window.config.get('debug')) {
            this.setDebugValue(cardWrapper, 'mode', mode)
        }

        let key = this.getKey(cardWrapper)
        PopupContainer.closeAll(cardWrapper)

        if (ObersalzbergCard.isInitialized(cardWrapper))
            Logging.log(`CHANGED_MODE::CARD:${key},MODE:${mode}`)
    }

    static articleChanged(cardWrapper) {
        cardWrapper.querySelectorAll('.overlayWrap').forEach(wrap => {
            wrap.style.display = 'flex'
            wrap.style.display = 'block'
        })

        // let mode = cardWrapper.getAttribute('data-mode')
        // let language = cardWrapper.getAttribute('data-language')
        // let section = cardWrapper.querySelector(`section.${mode}`)
        // if (section) {
        //     let article = section.querySelector(
        //         `article[data-lang=${language}]`
        //     )
        //     if (article) {
        //         article.querySelectorAll('.overlayWrap').forEach(wrap => {
        //             let bb = wrap.querySelector('img').getBoundingClientRect()
        //             wrap.querySelector('svg').setAttribute(
        //                 'viewbox',
        //                 `0 0 ${bb.width} ${bb.height}`
        //             )
        //         })
        //     }
        // }
    }

    /**
     * Adjusts the svg viewbox for figures that contain
     * image popups.
     *
     * @static
     */
    static async createFigurePopups(wrapper) {
        let svgs = wrapper.querySelectorAll('.drawer figure svg')
        svgs.forEach(svg => {
            let wrapper = svg.closest('.overlayWrap')
            if (wrapper) {
                let originalImage = wrapper.querySelector('img')

                svg.setAttribute(
                    'viewbox',
                    `0 0 ${originalImage.clientWidth} ${originalImage.clientHeight}`
                )
            }
        })
    }

    static exitSlider(cardWrapper) {
        Slider.stopAnimation(cardWrapper)
        Slider.changed(cardWrapper, 0)
        Slider.showOriginalImage(cardWrapper)
        this.hideSlider(cardWrapper)

        this.getSlideImages(cardWrapper).forEach(img => {
            ImageReplacer.replaceWithLowResImage(img)
            img.style.display = 'none'
        })
    }

    static showSlider(cardWrapper) {
        let sliderBase = cardWrapper.querySelector('.sliderBase')
        sliderBase.style.visibility = 'visible'
        this.showNode(sliderBase)

        Slider.hideOriginalImage(cardWrapper)
        Drawer.hideDrawer(cardWrapper)
        Slider.setValue(cardWrapper, 0)
        // UO: avoid flicker and set to block after loading
        this.getSlideImages(cardWrapper).forEach(img => {
            ImageReplacer.replaceWithHighResImage(img)
        })

        if (window.config.get('showOriginalAtSliderStart')) {
            setTimeout(() => {
                this.getSlideImages(cardWrapper).forEach(img => {
                    img.style.display = 'block'
                })
            }, 500)
        } else {
            this.getSlideImages(cardWrapper).forEach(img => {
                img.style.display = 'block'
            })
        }
    }

    static hideSlider(cardWrapper) {
        let sliderBase = cardWrapper.querySelector('.sliderBase')
        this.hideNode(sliderBase)
    }

    static getSlideImages(cardWrapper) {
        return cardWrapper.querySelectorAll('.slideFigure > img:not(.original)')
    }

    /**
     * Gets the mode of the card wrapper.
     * The mode is not always valid and may be null.
     *
     * @param {HTMLElement} cardWrapper
     * @returns {null | string} - If a mode is set the string is returned. Otherwise false.
     */
    static getModeUnfixed(cardWrapper) {
        return cardWrapper.getAttribute('data-mode')
    }

    /**
     * Returns the mode of the card wrapper.
     * If no mode is set. A mode of 'none' is returned.
     *
     * @param {HTMLElement} cardWrapper
     * @returns {string} - Returns the mode of the card. If none is set the mode 'none' is returned.
     */
    static getMode(cardWrapper) {
        let mode = this.getModeUnfixed(cardWrapper)
        return mode == null ? 'none' : mode
    }

    /**
     * Changes the mode to the provided one. Or resets to default mode
     * when already in this mode.
     *
     * @param {HTMLElement} cardWrapper
     * @param {string} mode
     * @returns {string} - Returns the mode the card is in after the change.
     */
    static toggleMode(cardWrapper, mode) {
        const current_mode = this.getMode(cardWrapper)
        const targetMode = current_mode == mode ? 'none' : mode
        this.changeMode(cardWrapper, targetMode)
        return targetMode
    }

    /**
     * Changes the language of the card, when a button on the card is clicked.
     *
     * @param {event} event - Click event on the card.
     * @param {string} lang - Language identifier to change the card to.
     */
    static changeLanguageByEvent(event, lang) {
        let wrapper = this.getCardWrapper(event.target)

        const fromLang = wrapper.getAttribute('data-language')
        this.changeLanguage(wrapper, lang)

        let key = ObersalzbergCard.getKey(wrapper)
        Logging.log(`CHANGE_LANGUAGE::CARD:${key},FROM:${fromLang},TO:${lang}`)
    }
}
