export default class Localization {
    static validate(wrapper) {
        if (
            window.config &&
            window.config.definitions &&
            window.config.definitions.languages
        ) {
            const languages = window.config.definitions.languages
            this.getAllLocalizables(wrapper).forEach(localizable => {
                languages.forEach(language => {
                    const attr = this.getLanguageAttribute(language)
                    if (!localizable.getAttribute(attr)) {
                        const message = `Element had missing language attribute ${attr}: ${localizable.outerHTML}.`
                        if (window.errorWindow) {
                            window.errorWindow.error(message)
                        }

                        console.error(message)
                    }
                })
            })
        } else {
            console.error(
                'Could not validate languages, as language config was not found @window.config.definitions.languages.'
            )
        }
    }

    /**
     * Returns the name of the locale file.
     *
     * @param {string} file - Path to the basefile.
     * @param {string} local - Locale identifier.
     */
    static getFileName(file, local, extension) {
        return `${file}_${local}.${extension}`
    }

    static getAllLocalizables(element) {
        return element.querySelectorAll(`[${this.localizableAttribute}]`)
    }

    static makeLocalizable(element, languageObject) {
        element.setAttribute(this.localizableAttribute, true)

        for (let [lang, txt] of Object.entries(languageObject)) {
            this.addLanguageAttribute(element, lang, txt)
        }
    }

    static addLanguageAttribute(element, lang, text) {
        element.setAttribute(this.getLanguageAttribute(lang), text)
    }

    static getLanguageAttribute(lang) {
        return `data-lang-${lang}`
    }

    static get localizableAttribute() {
        return 'data-localizable'
    }

    static changeLanguageOfLocalizables(element, lang) {
        this.getAllLocalizables(element).forEach(localizable => {
            this.localize(localizable, lang)
        })

        let localeElements = element.querySelectorAll('[data-locale-element]')
        localeElements.forEach(localeElement => {
            let locale = localeElement.getAttribute('data-lang')

            if (locale == lang) {
                localeElement.classList.remove('hidden')
            } else {
                localeElement.classList.add('hidden')
            }
        })
    }

    static localize(localizable, lang) {
        const langAttr = this.getLanguageAttribute(lang)
        if (!localizable.hasAttribute(langAttr)) {
            console.error(`Could not change language to ${lang}.`, localizable)
        } else {
            localizable.innerText = localizable.getAttribute(langAttr)
        }
    }
}
