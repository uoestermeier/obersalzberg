import Time from './time.js'

export default class ResetTimer {
    constructor({ time = Time.minutes(5), resetFunction = () => {} } = {}) {
        this.time = time
        this.timeout = null
        this.resetFunction = resetFunction

        this.resetFunction = this.resetFunction.bind(this)
        this.start = this.start.bind(this)

        this.setup()
    }

    setup() {
        if (this.time > 0) {
            this.start()
            document.body.addEventListener('pointerdown', () => {
                this.start()
            })
        } else console.error('Reset time was invalid and therefore not set!')

        document.addEventListener('keydown', event => {
            if (event.ctrlKey == true && event.key == 'o') {
                event.preventDefault()
                this.resetFunction()
            }
        })
    }

    start() {
        clearTimeout(this.timeout)
        this.timeout = setTimeout(() => {
            this.start()
            this.resetFunction()
        }, this.time)
    }
}
