/* global SimpleBar */
/* eslint-disable no-console */

import { Details } from './details.js'
import { ExtendedCardWrapper, ObersalzbergCard } from './card.js'
import { DRAWER_WIDTH } from './globals.js'
// import * as SimpleBar from '../node_modules/simplebar/dist/simplebar.min.js'
import { DetailsDrawerSynch } from './details-drawer-sync.js'
import { InteractionMapper } from '../iwmlib/lib/interaction.js'
import PopupContainer from './popup.js'
export const RIGHT = DRAWER_WIDTH
export const RIGHT_NEG = '-' + RIGHT

export class Drawer extends Object {
    static hideDrawer(drawer) {
        let card = drawer.closest('.cardWrapper')
        card.classList.remove('expanded')
    }

    static showDrawer(drawer) {
        let card = drawer.closest('.cardWrapper')
        card.classList.add('expanded')
    }

    /**
     * This should only be called when the collapsible is clicked.
     * More appropriate name would be 'collapsibleClicked' due to it's called
     * within the HTML files and is therefore hard to change.
     *
     *
     * @param {*} collapsible
     */
    static toggleCollapsible(collapsible) {
        let className = collapsible.classList.item(1)

        if (collapsible.classList.contains('active')) {
            DetailsDrawerSynch.collapsibleClosed(collapsible, className)
            this.closeCollapsible(collapsible)
        } else {
            DetailsDrawerSynch.collapsibleOpened(collapsible, className)
            this.openCollapsible(collapsible)
        }
    }

    static getCollapsibleContainer(collapsible) {
        return collapsible.closest('.drawer article')
    }

    static getCollapsibleContent(collapsible) {
        var content = collapsible.nextElementSibling
        if (!(content && content.classList.contains('collapsibleContent'))) {
            console.error(
                `The associated content was either not found or not a collapsibleContent.`,
                collapsible
            )
            content = null
        }

        return content
    }

    static init(cardWrapper) {
        // this.initSimplebar(cardWrapper)

        let drawer = Drawer.getDrawer(cardWrapper)

        let closeButton = drawer.querySelector('.close-button')
        closeButton.setAttribute('onclick', 'Menu.setCardModeToNone(event)')
    }

    /**
     * Simplebar overwrites the default scrollbar.
     * Unfortunately, there is no convenient way to get
     * the simplebar element from the simplebar element.
     *
     * So we create it manually and add it to the element itself.
     *
     * @param {DomElement} element
     */
    static initSimplebar(element) {
        let scroll = element.querySelector('.scroll')
        const simplebar = new SimpleBar(scroll, {
            autoHide: false
        })
        scroll.simplebar = simplebar
    }

    /**
     * Get's a collapsible element by class name.
     *
     * @param {Element} wrapper - Cardwrapper element.
     * @param {string} className - Class name of the collapsible.
     */
    static getCollapsibleByClassName(wrapper, className) {
        let container = this.getDrawer(wrapper)
        let collapsible = container.querySelector(
            `article:not(.hidden) .collapsible.${className}`
        )

        if (!collapsible)
            console.error(
                `No collapsible with class name of '${className}' was found.`
            )
        return collapsible
    }

    /**
     * Calls the openCollapsible using a wrapper and a class name instead of the
     * collapsible itself.
     *
     * @param {Element} wrapper - Cardwrapper element.
     * @param {string} className - Class name of the collapsible to open.
     */
    static openCollapsibleByClassName(wrapper, className) {
        let collapsible = this.getCollapsibleByClassName(wrapper, className)
        if (collapsible !== null) this.openCollapsible(collapsible)
    }

    /**
     * Calls the closeCollapsible using a wrapper and a class name instead of the
     * collapsible itself.
     *
     * @param {Element} cardWrapper - Cardwrapper element.
     * @param {string} className - Class name of the collapsible to close.
     */
    static closeCollapsibleByClassName(cardWrapper, className) {
        let collapsible = this.getCollapsibleByClassName(cardWrapper, className)
        if (collapsible !== null) this.closeCollapsible(collapsible)
    }

    /**
     *  Closes all collapsibles.
     *
     * @param {DomElement} cardWrapper - card
     */
    static closeAllCollapsibles(cardWrapper) {
        let drawer = this.getDrawer(cardWrapper)
        let collapsibles = drawer.querySelectorAll('.collapsible')
        collapsibles.forEach(collapsible => {
            this.closeCollapsible(collapsible)
        })
    }

    static async openCollapsible(collapsible) {
        let container = this.getCollapsibleContainer(collapsible)
        let content = this.getCollapsibleContent(collapsible)
        await this.unsetActiveCollapsible(container, collapsible)

        collapsible.classList.add('active')
        let height = content.scrollHeight + 'px'

        content.style.pointerEvents = 'all'
        let time = window.config.get('collapsibleScrollDuration') / 2
        TweenLite.to(content, time, {
            maxHeight: height,
            onUpdate: function() {
                if (window.config.get('collapsibleScrollIntoView'))
                    collapsible.scrollIntoView({ behavior: 'smooth' })
            }
        })
    }

    static scrollTo(child, scrollTo, time, additionalOptions = {}) {
        const options = Object.assign(additionalOptions, { scrollTo })
        let scroll = child.closest('.scroll')
        if (scroll) {
            let simplebar = scroll.querySelector('.simplebar-content-wrapper')
            if (simplebar != null) {
                scroll = simplebar
            }

            TweenLite.to(scroll, time, options)
        } else console.error('Could not find scroll element.')
    }

    /**
     * Resets the drawer by closing all collapsibles and
     * resets the scroll position.
     *
     * @param {DomElement} cardWrapper
     */
    static reset(cardWrapper) {
        this.closeAllCollapsibles(cardWrapper)

        let drawer = cardWrapper.querySelector('.drawer')
        let scroll = drawer.querySelector('.scroll')

        let simplebar = scroll.querySelector('.simplebar-content-wrapper')
        if (simplebar != null) {
            scroll = simplebar
        }

        scroll.scrollTop = 0
    }

    static scrollToTop(cardWrapper) {
        let drawer = cardWrapper.querySelector('.drawer')
        let scroll = drawer.querySelector('.scroll')
        scroll.scrollTop = 0
    }

    static getScrollElement(cardWrapper) {
        const drawer = cardWrapper.querySelector('.drawer')
        const scroll = drawer.querySelector('.scroll')
        return scroll
    }

    /**
     *
     * Removes all active classes from the other collapsible items.
     * If the item is the newActiveCollapsible, it'll stay active.
     *
     * @param {Element} newActiveCollapsible - Collapsible that will be active.
     * @returns {void}
     */
    static async unsetActiveCollapsible(
        container,
        newActiveCollapsible = null
    ) {
        const promises = []
        for (let collapsible of container.querySelectorAll(
            '.collapsible.active'
        )) {
            if (collapsible !== newActiveCollapsible) {
                const closePromise = this.closeCollapsible(
                    collapsible,
                    newActiveCollapsible
                )
                promises.push(closePromise)
            }
        }
        return Promise.all(promises)
    }

    static closeCollapsible(collapsible, newActiveCollapsible = null) {
        return new Promise((resolve, reject) => {
            collapsible.classList.remove('active')
            let content = this.getCollapsibleContent(collapsible)
            let time = window.config.get('collapsibleScrollDuration') / 2

            let wrapper = ExtendedCardWrapper.getCardWrapper(collapsible)
            PopupContainer.closeAll(wrapper)

            content.style.pointerEvents = 'none'
            TweenLite.to(content, time, {
                maxHeight: 0,
                onComplete: () => {
                    content.style.maxHeight = null
                    resolve()
                }
            })

            let scroll = collapsible.closest('.scroll')
            if (scroll) {
                let simplebar = scroll.querySelector(
                    '.simplebar-content-wrapper'
                )
                if (simplebar != null) {
                    scroll = simplebar
                }

                // TweenLite.to(scroll, time, {
                //     delay: time,
                //     scrollTo: { y: 0 }
                // })
            } else console.error('Could not find scroll element.')
        })
    }

    static registerEventHandler(article) {
        let coll = article.getElementsByClassName('collapsible')

        for (let i = 0; i < coll.length; i++) {
            InteractionMapper.on('tap', coll[i], () => {
                Drawer.toggleCollapsible(coll[i])
            })
        }
    }

    static back(event) {
        let drawer = event.target.closest('.drawer')
        Drawer.hideDrawer(drawer)
        let cardWrapper = event.target.closest('.cardWrapper')
        let menuItem = cardWrapper.querySelector('.articleMenuItem')

        menuItem.style.color = 'black'
        Details.closeAllDetails(cardWrapper)
        ObersalzbergCard.changeMode(cardWrapper, 'none')
    }

    static ensureContent(mainCard, url, collapsibleSelector, selector) {
        let drawer = this.getDrawer(mainCard)
        if (!drawer) {
            console.error('There is no drawer attatched to the card.')
        } else {
            Drawer.showDrawer(drawer)
        }
    }

    static getDrawer(wrapper) {
        return wrapper.querySelector('.drawer')
    }

    static getCollapsible(wrapper, klass) {
        let drawer = this.getDrawer(wrapper)
        return drawer.querySelector('.' + klass)
    }

    static notImplemented() {
        alert('Funktion noch nicht implementiert')
    }
}
