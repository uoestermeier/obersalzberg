function getVariable(name) {
    return getComputedStyle(document.documentElement)
        .getPropertyValue(name)
        .trim()
}

export const HIGHLIGHT_COLOR = getVariable('--highlight-color')
export const DRAWER_WIDTH = getVariable('--drawer-width')
