/* eslint-disable no-console */
import { JSONLoader } from './loader.js'

/**
 * This configuration handler manages the config.
 *
 * Entries can be added using the *define()* function
 * and providing a default value. If the value is not specified
 * inside the config file, the default value is applied.
 *
 * The configuration can be overwritten by using a query parameter
 * named the same as the config parameter (case-sensitive).
 *
 * Arrays can be provided by using the same parameter key multiple times
 * e.g. ...html?cards=01&cards=02&cards=14
 *
 * However, when defining an array the 'applyQueryOverwrites' must be overwritten
 * inside the subclass, as a single array entry would be interpreted as single value
 * and needs to be converted into an array at that point.
 *
 */
export class Config {
    constructor(file) {
        this.file = file
        this.definitions = {}
        this.loaders = {}
        this.define()
    }

    /**
     * Used to set all necessary defines in childclass.
     */
    define() {}

    async load() {
        return new Promise((resolve, reject) => {
            if (window.config != null) {
                resolve(window.config)
            } else {
                let loader = new JSONLoader(this.file)
                loader
                    .load()
                    .then(obj => {
                        this.apply(obj)
                            .then(() => {
                                let queryObject = this.queryOverwrites()
                                this.applyQueryOverwrites(queryObject)
                                resolve(obj)
                            })
                            .catch(reject)
                    })
                    .catch(reject)
            }
        })
    }

    /**
     * Determines how the query overwrites the definitions.
     * This may be overloaded in subclass to enforce e.g. arrays.
     */
    applyQueryOverwrites(queryObject) {
        Object.assign(this.definitions, queryObject)
    }

    /**
     * Checks the values if they are overwritten inside the query.
     */
    queryOverwrites() {
        let overwriteObject = {}
        let urlSearchParams = new URLSearchParams(window.location.search)
        let processed = []
        for (let key of urlSearchParams.keys()) {
            //Skip if already processed. This is necessary if handling arrays.
            if (processed.indexOf(key) == -1) {
                processed.push(key)
                if (this.definitions.hasOwnProperty(key)) {
                    let results = urlSearchParams.getAll(key)
                    let value = results.length == 1 ? results[0] : results
                    overwriteObject[key] = value
                    console.warn(
                        `The query overwrites the key ${key} with: ${value}.`
                    )
                }
            }
        }

        return overwriteObject
    }

    add(name, defaultValue) {
        if (this.definitions.hasOwnProperty(name)) {
            console.error(
                `Cannot redeclare '${name}', it has been already declared.`
            )
        } else {
            this.definitions[name] = defaultValue
        }
    }

    addLoader(name, loadingFunction) {
        if (!this.definitions.hasOwnProperty(name)) {
            console.error(
                `Config property '${name}' was not yet defined. Can only add a loader to an already defined key.`
            )
        } else {
            this.loaders[name] = loadingFunction
        }
    }

    get(key) {
        if (!this.definitions.hasOwnProperty(key)) {
            console.error(
                `Tried to get config of '${key}'. This is not defined yet.`
            )
        } else {
            return this.definitions[key]
        }
    }

    async applyLoader(key) {
        return new Promise((resolve, reject) => {
            if (this.loaders.hasOwnProperty(key)) {
                this.loaders[key]
                    .call(this)
                    .then(obj => {
                        this.definitions[key] = obj
                        resolve(obj)
                    })
                    .catch(reject)
            } else {
                reject(`No loader with key '${key}' was found.`)
            }
        })
    }

    async apply(obj) {
        let copy = Object.assign({}, obj)
        for (let key of Object.keys(this.definitions)) {
            if (copy.hasOwnProperty(key) || this.loaders.hasOwnProperty(key)) {
                if (this.loaders.hasOwnProperty(key)) {
                    await this.applyLoader(key)
                } else {
                    this.definitions[key] = copy[key]
                }
                delete copy[key]
            } else {
                console.warn(
                    `Key '${key}' was not set in config file '${this.file}'.`
                )
            }
        }

        for (let key of Object.keys(copy)) {
            console.warn(
                `Key '${key}' was set in the config, but not defined inside the definition.`
            )
        }
    }
}

export class ObersalzbergConfig extends Config {
    define() {
        this.addLocalization()

        //The expanded scale of th highlights.
        this.add('highlightScale', 2)
        this.add('highlightStroke', 2)

        // Enables various debugging utilities.
        this.add('debug', false)

        // Add debug functionality for card sorting. E.g. draws relevant informatio to debug canvas.
        this.add('debugCardSorting', false)

        // Cards array. Is set by the cards loader.
        // The cards json can be generated via: 'Entwickler > Skripte > Kartenkonfiguration aktualisieren'
        this.add('cards', [])
        this.addLoader('cards', () => {
            return new Promise((resolve, reject) => {
                let loader = new JSONLoader('cards.json')
                loader
                    .load()
                    .then(resolve)
                    .catch(reject)
            })
        })
        //Sorting style that is applied to the cards.
        this.add('display', 'desktop') //table or desktop
        this.add('sortingType', 'line') //random
        this.add('sortingDuration', '5')

        //Reset time in minutes.
        this.add('resetTimer', true)
        this.add('resetTime', 5)

        // Details scrolling.
        this.add('collapsibleScrollIntoView', true)
        this.add('collapsibleScrollDuration', 1)

        this.add('onTouchTime', 3000)

        this.add('titleOnTouch', false)
        this.add('languageOnTouch', false)
        this.add('menuOnTouch', false)

        this.add('fadeSliderText', true) // When set, the text's opacity will be animated, otherwise there will be a hard jump.

        this.add('translateCardOnScrollLimit', false)
        this.add('startScale', 0.5) // Initial scale and reset scale
        this.add('showOriginalAtSliderStart', false) // Show original when switching to slider mode

        this.add('maxZoomedScatters', 6)
    }

    addLocalization() {
        this.add('startupLanguage', 'de')
        this.add('languages', ['de', 'en'])
        this.add('useNonLocalizedCardVersions', false)
    }

    applyQueryOverwrites(query) {
        if (query['cards']) {
            if (!Array.isArray(query.cards)) query.cards = [query.cards]
        }
        super.applyQueryOverwrites(query)
    }
}
