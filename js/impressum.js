import Logging from '../iwmlib/lib/logging.js'

const infoSVGBlue = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet" width="48">
<rect x="0" y="0" width="100" height="100" style="fill:#3366FF; stroke:rgba(0,0,0,0)" />
<circle cx="50" cy="33" r="2.5" stroke="white" fill="white"  stroke-width="5"/>
<line x1="50" y1="44" x2="50" y2="75" stroke="white" stroke-width="8" />
</svg>`

const infoSVG = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet" width="48">
<circle cx="50" cy="50" r="40" stroke="white" fill="white" stroke-width="8"/>
<circle cx="50" cy="33" r="2.5" stroke="white" fill="white"  stroke-width="5"/>
<line x1="50" y1="44" x2="50" y2="75" stroke="white" stroke-width="8" />
</svg>`

const closeSVG = `<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid meet" width="48" height="48">
<g stroke-width="8" stroke="black">
    <line stroke="white" fill="white" x1="25" y1="25" x2="75" y2="75" />
    <line stroke="white" fill="white" x1="25" y1="75" x2="75" y2="25" />
</g>
</svg>`

export default class Impressum {
    constructor(position) {
        let imp = document.createElement('div')
        imp.classList.add('impressum')

        let buttonContainer = document.createElement('div')
        buttonContainer.classList.add('buttonContainer')
        buttonContainer.innerHTML = infoSVG

        let impContainer = document.createElement('div')
        impContainer.classList.add('impContainer')

        let gmbhContainer = document.createElement('div')
        gmbhContainer.classList.add('gmbhContainer')
        let museumContainer = document.createElement('div')
        museumContainer.classList.add('museumContainer')
        let cssDict = { position: 'absolute', ...position }
        for (var property in cssDict) {
            imp.style[property] = cssDict[property]
        }

        buttonContainer.addEventListener('pointerdown', () => {
            if (imp.classList.contains('impActive')) {
                Logging.log(`Hide Legal Notice`)
                Impressum.deactivate(imp)
            } else {
                Logging.log(`Show Legal Notice`)
                Impressum.activate(imp)
            }
        })
        impContainer.appendChild(museumContainer)
        impContainer.appendChild(gmbhContainer)

        impContainer.append(impressum.content.cloneNode(true))
        imp.appendChild(impContainer)
        imp.appendChild(buttonContainer)

        document.body.appendChild(imp)
    }

    static deactivate(impressum, animationDuration = 0.25) {
        impressum.classList.remove('impActive')
        impressum.querySelector('.buttonContainer').innerHTML = infoSVG
        let impContainer = impressum.querySelector('.impContainer')
        TweenMax.to(impressum, animationDuration, {
            backgroundColor: 'rgba(0, 0, 0, 0.0)'
        })
        TweenMax.to(impContainer, animationDuration, {
            autoAlpha: 0,
            onComplete: () => {
                impContainer.style.display = 'none'
            }
        })
    }

    static activate(impressum, animationDuration = 0.25) {
        impressum.classList.add('impActive')
        impressum.querySelector('.buttonContainer').innerHTML = closeSVG
        let impContainer = impressum.querySelector('.impContainer')
        impContainer.style.display = 'grid'
        TweenMax.to(impressum, animationDuration, {
            backgroundColor: 'rgba(0, 0, 0, 0.5)'
        })
        TweenMax.to(impContainer, animationDuration, {
            autoAlpha: 1
        })
    }
}
