export class ImageReplacer {
    static setImages(img, original, preview) {
        this.setOriginal(img, original)
        this.setPreview(img, preview)
    }

    static getImages(img) {
        return {
            original: this.getOriginal(img),
            preview: this.getPreview(img)
        }
    }

    static get previewAttribute() {
        return 'data-preview-image'
    }

    static get originalAttribute() {
        return 'data-image'
    }

    static getPreview(img) {
        return img.getAttribute(this.previewAttribute)
    }

    static setPreview(img, preview) {
        img.setAttribute(this.previewAttribute, preview)
    }

    static getOriginal(img) {
        return img.getAttribute(this.originalAttribute)
    }

    static setOriginal(img, original) {
        img.setAttribute(this.originalAttribute, original)
    }

    static async replaceWithLowResImage(img, attr = 'src') {
        let preview = this.getPreview(img)
        if (!preview || preview == 'null') {
            console.error('There was no preview image set!', img)
            return null
        } else {
            return this.replaceImage(img, preview, attr)
        }
    }

    static async replaceWithHighResImage(img, attr = 'src') {
        let original = this.getOriginal(img)
        if (!original) {
            console.error('There was no original image set!', img)
            return null
        } else {
            return this.replaceImage(img, original, attr)
        }
    }

    static async replaceImage(img, other, attr = 'src') {
        return new Promise(resolve => {
            let image = new Image()
            image.addEventListener('load', () => {
                img.setAttribute(attr, image.src)
                // console.log('replaceImage ready', image.src)
                resolve()
            })
            image.src = other
            // console.log('replaceImage', other)
        })
    }

    /** 
    static replaceWithLowResImageDelayed(img, attr = 'src') {
        let preview = this.getPreview(img)
        if (!preview || preview == 'null') {
            console.error('There was no preview image set!', img)
            return null
        } else {
            return this.replaceImageDelayed(img, preview, attr)
        }
    }

    static replaceImageDelayed(img, other, attr = 'src', delay = 500) {
        setTimeout(() => {
            let image = new Image()
            image.addEventListener('load', () => {
                img.setAttribute(attr, image.src)
            })
            image.src = other
        }, delay + Math.random() * 1000)
    } */
}
