/**
 * The time class is based on miliseconds and helps
 * to create more readable time conversions.
 */
export default class Time {
    static seconds(seconds) {
        return seconds * 1000
    }

    static minutes(minutes) {
        return this.seconds(60) * minutes
    }

    static hours(hours) {
        return this.minutes(60) * hours
    }
    static days(days) {
        return this.hours(24) * days
    }

    static get({ days = 0, hours = 0, minutes = 0, seconds = 0 } = {}) {
        return [
            this.days(days),
            this.hours(hours),
            this.minutes(minutes),
            this.seconds(seconds)
        ].reduce((sum, val) => sum + val)
    }
}
