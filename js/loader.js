export class JSONLoader {
    constructor(file) {
        this.file = file
    }

    async load() {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest()
            xhr.open('GET', this.file)

            xhr.addEventListener('readystatechange', () => {
                if (xhr.readyState == 4 && xhr.responseText != '') {
                    try {
                        let json = JSON.parse(xhr.responseText)
                        resolve(json)
                    } catch (e) {
                        reject(e)
                    }
                }
            })
            xhr.addEventListener('error', err => {
                reject(err)
            })
            setTimeout(() => {
                reject(`Loading config file timed out....`)
            }, 3000)

            xhr.send()
        })
    }
}
