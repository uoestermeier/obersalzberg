import Logging from '../iwmlib/lib/logging.js'
import { ObersalzbergCard } from './card.js'

const heartbeatIntervall = 5000 // in ms
const zoomedScatter = []
const maxZoomedScatters = 6
var heartbeatHandle = null
let stressTestRunning = false
let preventHeartBeats = false
let maxScale = 1.5 * 1.25 // uo: maxScale times overdoScale

function pushZoomedScatter(scatter) {
    let index = zoomedScatter.indexOf(scatter)
    if (index >= 0) {
        zoomedScatter.splice(index, 1)
    }
    zoomedScatter.push(scatter)
    while (zoomedScatter.length > maxZoomedScatters) {
        let first = zoomedScatter.shift()
        TweenMax.to(first, 1.0, {
            scale: 1,
            onComplete: () => {
                if (first.isOutside()) {
                    first.bouncing()
                }
            }
        })
    }
}

function throwStressTest() {
    let modes = ['slider', 'details', 'background', 'none']

    function randomMode() {
        return modes[Math.floor(Math.random() * modes.length)]
    }

    for (let scatter of app.scatterContainer.scatter.values()) {
        ObersalzbergCard.changeMode(scatter.element, randomMode())

        let delta = {
            x: (Math.random() - 0.5) * 100,
            y: (Math.random() - 0.5) * 100
        }
        let megaScale = 1.0 // former 5.0
        let scale =
            Math.random() < 0.5
                ? 1
                : Math.min(
                      maxScale * megaScale,
                      1.0 + Math.random() * maxScale * megaScale
                  )
        TweenMax.to(scatter, 0.5, { scale })
        if (scale > 1) {
            pushZoomedScatter(scatter)
        }

        scatter.addTestVelocity(delta)
        scatter.startThrow()
    }
    if (stressTestRunning) {
        setTimeout(throwStressTest, 500)
    } else {
        Logging.log('Stress test finished')
        for (let scatter of app.scatterContainer.scatter.values()) {
            scatter.killAnimation()
        }
        app.resetCards()
        console.log('Stress test finished')
    }
}

function toggleStressTest() {
    stressTestRunning = !stressTestRunning
    if (stressTestRunning) {
        Logging.log('Stress test starts')
        console.log('Stress test starts')
        throwStressTest()
    }
}

export function startHeartBeat() {
    if (preventHeartBeats) {
        console.log('Heartbeats already prevented')
    } else {
        heartbeatHandle = setInterval(() => {
            Logging.log('heartbeat')
            console.log('heartbeat', heartbeatHandle)
        }, heartbeatIntervall)
        console.log('Started heartbeats', heartbeatHandle)
    }
}

function stopHeartbeat() {
    console.log('Stop heartbeats', heartbeatHandle)
    if (heartbeatHandle != null) {
        clearInterval(heartbeatHandle)
        heartbeatHandle = null
    } else {
        preventHeartBeats = true
    }
}

function simulateMemoryCrash() {
    console.log('Simulate crash test with infinite loop')
    let txt = 'a'
    while (true) {
        txt += 'a'
    }
}

export function registerStressTest() {
    document.addEventListener('keydown', event => {
        switch (event.key.toLowerCase()) {
            case 'c':
                simulateMemoryCrash()
                break
            case 't':
                toggleStressTest()
                break
            case 's':
                stopHeartbeat()
                break
            case 'h':
                startHeartBeat()
                break
            case 'r':
                app.resetCards()
                break
        }
    })
    console.log(
        "Tests registered. Type 't' to toggle stress test, 's' to stop heartbeats, 'h' to start heartbeats, 'r' to reset cards, 'c' to simulate a crash."
    )
}
