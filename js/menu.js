import { ExtendedCardWrapper, ObersalzbergCard } from './card.js'
import { HIGHLIGHT_COLOR } from './globals.js'
import { Drawer } from './drawer.js'

export class Menu extends Object {
    static getItems(item, query = '') {
        let wrapper = ExtendedCardWrapper.getCardWrapper(item)
        return Array.from(
            wrapper.querySelectorAll(`.frontFooter .button${query}`)
        )
    }

    // static highlightItem(item) {
    //     let isHighlighted = item.style.color == HIGHLIGHT_COLOR
    //     // Menu.resetMenuColor(item)
    //     if (!isHighlighted) {
    //         item.style.color = HIGHLIGHT_COLOR
    //         return true
    //     }
    //     return false
    // }

    static highlightItemByMode(card, mode) {
        let activeFooterItems = this.getItems(card, '.active')
        activeFooterItems.forEach(item => {
            item.classList.remove('active')
        })

        let menuItem = card.querySelector(`.frontFooter .${mode}`)
        if (menuItem != null) {
            menuItem.classList.add('active')
        }
    }

    static setCardModeToNone(event) {
        let cardWrapper = ObersalzbergCard.getCardWrapper(event.target)
        ObersalzbergCard.changeMode(cardWrapper, 'none')
    }

    static toggleBackground(event) {
        const backgroundMode = 'background'
        this.toggleMode(event, backgroundMode)
    }

    static toggleDetails(event) {
        const detailsMode = 'details'
        this.toggleMode(event, detailsMode)
    }

    static toggleSlider(event) {
        const sliderMode = 'slider'
        this.toggleMode(event, sliderMode)
    }

    static toggleMode(event, mode) {
        let cardWrapper = ObersalzbergCard.getCardWrapper(event.target)
        Drawer.scrollToTop(cardWrapper)
        const resultingMode = ObersalzbergCard.toggleMode(cardWrapper, mode)
        this.highlightItemByMode(cardWrapper, resultingMode)
    }
}
