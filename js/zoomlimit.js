const zoomedScatter = []
let maxZoomedScatters = 6

export function setZoomedLimit(limit) {
    console.log('Set max zoomed scatters to', limit)
    maxZoomedScatters = limit
}

export function pushZoomedScatter(scatter) {
    let index = zoomedScatter.indexOf(scatter)
    if (index >= 0) {
        zoomedScatter.splice(index, 1)
    }
    zoomedScatter.push(scatter)
    while (zoomedScatter.length > maxZoomedScatters) {
        let first = zoomedScatter.shift()
        TweenMax.to(first, 1.0, {
            scale: 1,
            onComplete: () => {
                if (first.isOutside()) {
                    first.bouncing()
                }
            }
        })
    }
}
