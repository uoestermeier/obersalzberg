import { ExtendedCardWrapper, ObersalzbergCard } from './card.js'
import { InteractionMapper } from '../iwmlib/lib/interaction.js'
import Events from '../iwmlib/lib/events.js'
import Logging from '../iwmlib/lib/logging.js'

export default class Slider {
    constructor(node) {
        this.dragging = false

        let wrapper = ExtendedCardWrapper.getCardWrapper(node)
        this.constructor.initSlider(wrapper, this)

        this.sliderBase = node.querySelector('.sliderBase')
        this.interaction = new InteractionMapper(this.sliderBase, this)
    }

    capture(event) {
        Events.capturedBy(event, this)
        return false
    }

    findTarget(event, local, global) {
        if (event.target === this.slider) {
            return this
        }
        return null
    }

    onStart(event, interaction) {
        this.dragging = true
    }

    onMove(event, interaction) {
        //   Event.capturedBy(event, this)
        if (this.dragging) {
            const p = window.convertPointFromPageToNode(
                this.slider,
                event.clientX,
                event.clientY
            )
            const ratio = this.slider.max / this.slider.offsetWidth
            this.slider.value = parseInt(p.x * ratio)

            Slider.changed(this.cardWrapper, this.slider.value)
        }
    }

    onEnd(event, interaction) {
        this.dragging = false
    }

    static initSlider(cardWrapper, instance) {
        let slides = this.getSlides(cardWrapper)
        let rangeSlider = this.getSliderInput(cardWrapper)

        const min = 0
        rangeSlider.setAttribute('min', min)

        const max = (slides.length - 1) * this.steps
        rangeSlider.setAttribute('max', max)

        cardWrapper.setAttribute('slider-value', 0)
        this.updateSlideFigure(cardWrapper)

        rangeSlider.addEventListener('pointerdown', event => {
            cardWrapper.sliding = 0
            instance.onStart(event)
        })

        rangeSlider.addEventListener('pointermove', event => {
            cardWrapper.sliding++
            instance.onMove(event)
        })

        rangeSlider.addEventListener('pointerup', event => {
            instance.onEnd(event)
        })

        rangeSlider.addEventListener('pointercancel', event => {
            instance.onEnd(event)
        })

        /* UO: The highlevel input event works only after the user
        moved the finger along the x-axis. For rotated card sliders
        the low level pointer move events are needed.

        rangeSlider.addEventListener('input', event => {
            cardWrapper.sliding++

            // If the user just taps, we want the slider to be animated.
            // But if he slides the finger on the slider, the input shall
            // be updated right away!
            if (cardWrapper.sliding > 10) {
                this.stopAnimation(cardWrapper)
                this.changed(cardWrapper, rangeSlider.value)
            }
        })
        */

        /**
         * If the slider is released near any end, it will be reset
         * to min or max.
         */
        rangeSlider.addEventListener('change', () => {
            // console.log('changed')
            this.snapSliderAtEnds(rangeSlider, min, max)
            this.animate(cardWrapper)
        })

        instance.slider = rangeSlider
        instance.cardWrapper = cardWrapper
        ExtendedCardWrapper.setDebugValue(cardWrapper, 'slider', 0)
        this.setValue(cardWrapper, 0)
    }

    static animate(cardWrapper) {
        const value = parseFloat(cardWrapper.getAttribute('slider-value'))
        const target = this.getSliderInput(cardWrapper).value

        let start = { value }

        this.stopAnimation(cardWrapper)

        cardWrapper.tween = TweenLite.to(start, 0.5, {
            value: target,
            onUpdate: () => {
                this.changed(cardWrapper, start.value)
            }
        })
    }

    static stopAnimation(cardWrapper) {
        if (cardWrapper.tween) cardWrapper.tween.kill()
    }

    static changed(cardWrapper, value) {
        let oldValue = parseFloat(cardWrapper.getAttribute('slider-value')) || 0
        if (value != oldValue) {
            cardWrapper.setAttribute('slider-value', value)
            this.updateSlideFigure(cardWrapper, true)
        }
    }

    static snapSliderAtEnds(slider, min, max, threshold = 0.1) {
        const thresholdAsPoints = max * threshold

        const underThreshold = slider.value < thresholdAsPoints
        const aboveThreshold = slider.value > max - thresholdAsPoints

        if (underThreshold || aboveThreshold) {
            slider.value = underThreshold ? 0 : max
            // var event = new Event('input')
            // slider.dispatchEvent(event)
        }
    }

    static updateSlideFigure(cardWrapper, interactive = false) {
        const slides = this.getSlides(cardWrapper)
        const value = cardWrapper.getAttribute('slider-value')

        let normalizedValue = value / this.steps + 1

        const visibleIndex = Math.round(normalizedValue)

        this.hideNextImages(visibleIndex, slides)

        /* UO: Is this the better entry to sliding? */
        if (window.config.get('showOriginalAtSliderStart')) {
            let duration = interactive ? 0.5 : 0
            if (value <= 0.1) {
                this.showOriginalImage(cardWrapper, duration)
            } else {
                this.hideOriginalImage(cardWrapper, duration)
            }
        }

        ExtendedCardWrapper.setDebugValue(
            cardWrapper,
            'slider',
            `${visibleIndex} (${normalizedValue.toFixed(2)})`
        )

        this.showPreviousImages(visibleIndex, slides)
        this.fadeImage(normalizedValue, slides)

        if (window.config.get('fadeSliderText')) {
            let txt = this.getSliderTextElement(cardWrapper)
            txt.style.opacity =
                1 - Math.abs(normalizedValue - visibleIndex) / 0.5
        }

        this.updateSliderText(cardWrapper, slides[visibleIndex - 1])
        this.updateSlideVisuals(cardWrapper)
    }

    static updateSlideVisuals(cardWrapper) {
        let input = this.getSliderInput(cardWrapper)
        let slider = cardWrapper.querySelector('.slider')
        let activeTrack = slider.querySelector('.sliderTrackActive')
        let thumb = slider.querySelector('.sliderThumb')

        const min = input.getAttribute('min') || 0
        const max = input.getAttribute('max') || 100
        const value = cardWrapper.getAttribute('slider-value')

        let normalizedValue = value / (max - min) - min

        let percent = parseInt(normalizedValue * 100) + '%'
        activeTrack.style.width = percent
        thumb.style.left = percent
    }

    static getSliderTextElement(cardWrapper) {
        return cardWrapper.querySelector('.sliderInfo p')
    }

    static updateSliderText(cardWrapper, slide) {
        if (slide) {
            let text = slide.getAttribute('alt')
            let alignment = slide.getAttribute('data-align')

            if (text) {
                let info = this.getSliderTextElement(cardWrapper)
                if (!info)
                    console.error(
                        'Your card has no info element. Could not update text!'
                    )
                else {
                    if (
                        ObersalzbergCard.isInitialized(cardWrapper) &&
                        info.innerHTML != text
                    ) {
                        let key = ObersalzbergCard.getKey(cardWrapper)
                        Logging.log(
                            `SLIDER_TEXT_CHANGED::CARD:${key},TEXT:${text}`
                        )
                        info.innerHTML = text

                        if (alignment) {
                            info.parentNode.style.justifyContent = 'flex-start'
                            info.style.textAlign = alignment
                        } else {
                            info.style.textAlign = 'center'
                            info.parentNode.style.justifyContent = 'center'
                        }
                    }
                }
            }
        } else {
            const card = cardWrapper.getAttribute('data-key')
            const message = `Could not update slider's text of card ${card}.`
            if (window.errorWindow) {
                window.errorWindow.error(message, ['slider'])
                console.error(message, cardWrapper)
            }
        }
    }

    static get steps() {
        return 100
    }

    static getSliderInput(cardWrapper) {
        return cardWrapper.querySelector('.sliderBase input[type=range]')
    }

    static getSlides(cardWrapper) {
        let slides = cardWrapper.querySelectorAll('.slide')
        slides = Array.from(slides)
        slides.shift()
        return slides
    }

    static getOriginalImage(cardWrapper) {
        return cardWrapper.querySelector('img.original')
    }

    static hideOriginalImage(cardWrapper, animated = 0.0) {
        let original = this.getOriginalImage(cardWrapper)
        let autoAlpha = 0
        // original.style.opacity = autoAlpha
        //    original.style.visibility = 'hidden'

        if (animated > 0.0) {
            TweenLite.to(original, animated, { autoAlpha })
        } else {
            TweenLite.set(original, { autoAlpha })
        }
    }

    static showOriginalImage(cardWrapper, animated = 0.0) {
        let original = this.getOriginalImage(cardWrapper)
        let autoAlpha = 1
        //original.style.opacity = autoAlpha
        //    original.style.visibility = 'visible'
        if (animated > 0.0) {
            TweenLite.to(original, animated, { autoAlpha })
        } else {
            TweenLite.set(original, { autoAlpha })
        }
    }

    static setValue(cardWrapper, normalizedValue) {
        let slider = this.getSliderInput(cardWrapper)
        const min = slider.getAttribute('min') || 0
        const max = slider.getAttribute('max') || 100
        slider.value = min + (max - min) * normalizedValue
        this.updateSlideFigure(cardWrapper)
    }

    static showPreviousImages(index, slides) {
        //Show all previous images
        for (let i = 0; i < index && i < slides.length; i++) {
            let slide = slides[i]
            TweenLite.set(slide, { autoAlpha: 1 })
        }
    }

    static fadeImage(value, slides) {
        let next = Math.floor(value)
        if (next < slides.length) {
            let autoAlpha = value % 1
            TweenLite.set(slides[next], {
                autoAlpha
            })
        }
    }

    static hideNextImages(index, slides) {
        index++
        while (index < slides.length) {
            TweenLite.set(slides[index], { opacity: 0 })
            index++
        }
    }
}
