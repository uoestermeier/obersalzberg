# OBERSALZBERG

System & Bildschirmauflösung

    Statt 8K Auflösung und 500% Skalierung müssen 4K und 200% eingestellt sein. Sonst werden die Bilder nicht richtig dargestellt.

Software

    node.js v16.14.0   v20.5.0
    electron v16.0.5   v24.3.1

    python > 3.7.5
    pip install Pillow

Mac

    brew install wine-stable

    Wine Stable.app von Hand öffnen um Sicherheitswarnung zu umgehen
