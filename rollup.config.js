// rollup.config.js (building more than one bundle)
export default [
    {
        input: './js/main.js',
        output: {
            file: './js/all.js',
            format: 'iife',
            sourcemap: false
        },
        watch: {
            clearScreen: false
        }
    }
]
