# OBERSALZBERG

## Konzeption

"Um erfolgreich zu manipulieren gab es für nationalsozialistische Propaganda fünf Grundsätze, die ebenfalls von Hitler in seinem Werk festgelegt wurden: Erstens, Propaganda soll sich auf so wenig Themen und Schlagworte wie möglich beschränken. Zweitens, sie darf einen nur geringen geistigen Anspruch haben. Drittens, sie muss auf das gefühlsmäßige Empfinden der Massen abzielen. Viertens, sie muss Differenzierungen vermeiden. Fünftens, die zu transportierenden Glaubenssätze müssen fortwährend wiederholt werden."

_Source: https://www.mdr.de/lexi-tv/ns_propaganda100.html_

Die NS-Propaganda folgte diesen Grundsätzen zwar, läßt sich aber nicht ausschließlich darauf reduzieren. Insbesondere die Bildsprache des langjährigen Hitler-Fotografen Hofmann läßt sich in ihren Vielfalt an Themen und Inszenierungen nicht auf die immer gleichen Muster reduzieren. Viele Fotostrecken folgten einer jeweils thematisch bestimmten Ikonographie. Um diese Bildsprache zu dekonstruieren, verwendet die Applikation konsistent drei verschiedene Informationskanäle bzw. Aspekte, zwischen denen die Benutzerinnen und Benutzer auswählen können.

Dabei stehen die Bilder im Vordergrund, die erst einmal ohne kontextuelle Einbettung erscheinen und somit Interesse am Dargestellten wecken sollen. Die einheitliche Navigation lädt dann aber dazu ein, sich vertieft den propagandistischen Stilmitteln zu widmen. Ein erster schneller Einstieg über den Bildaufbau macht deutlich, dass die Bilder keineswegs nur spontane Schnappschüsse sind, sondern meistens sehr bewußt einen Bildaufbau wählten, der die Rolle Hitlers hervorhebt. Des weiteren führt die Detailansicht zu vertiefenden Details mit Links zu Hintergrundinformationen, die schließlich den historischen Kontext und die übergreifenden propagandistischen Prinzipien thematisieren.

Aufgrund der Platzierung des Multimedia-Tisches in der Daueraustellung ergab sich zudem die Anforderung, das jedes Bild für sich steht und verschiedene Bilder gleichzeitig von verschiedenen Personen mit verschiedenen Perspektiven gelesen werden können. Wird die Applikation längere Zeit nicht benutzt, gehen die Karten in ihre Ausgangspositionen zurück.

## Benutzerführung

Die Benutzer üben jederzeit die volle Kontrolle über die Karte aus, die sie für sich ausgewählt haben. Sie können die Karte sich selbst überlassen, wenn sie ihre Interaktion beendet haben. Beim ersten Berühren wird lediglich unten der Titel eingeblendet, der mit einem Schlagwort angibt, worum es bei dem Bild geht. Damit wird das Auge automatisch auch in Richtung der drei Aspekte gelenkt, die für die Propagandadekonstruktion für alle Bilder gleichermaßen aubereitet wurden.

Durch Auswahl des Menüpunktes "Bildaufbau" wird ein Slider eingeblendet, mit dem im Schnelldurchlauf die verschienden Bildebenen ein- und ausgeblendet werden können. Auch hier können die Benutzerinnen und Benutzer selbst Tempo und Ebene bestimmen. Damit wird ein schneller und leicht zugänglicher Ein- und Überblick über den Inszenierungscharakter möglich.

Der Menüpunkt "Details" hebt Bildelemente hervor und fährt gleichzeitig seitlich erläuternde Texte aus. Diese Texte beziehen sich gezielt auf die gezeigten Bildelemente, wobei die Selektion der Bild- oder Textelemente durch farbliche Hervorhebung immer einen eindeutigen Text-Bild-Bezug herstellt. Durch die konsequente Aufbereitung der Text-Bild-Bezüge soll das Verständnis und die Lesbarkeit erleichtert werden.

Der Menüpunkt "Hintergründe" schließlich führt zu vertiefenden Informationen über die Entstehungsgeschichte und die Verwendung der Aufnahmen, um den über die einzelnen Bildelemente hinausgehende historischen Kontext verstehen zu können. Dabei wird besonderer Wert auf vertiefenden Popups und Glossareinträge gelegt, die je nach Interesse unterschiedliche Verarbeitungstiefen zulassen.

Die Sprache kann jederzeit oben-rechts für die jeweilige Karte gewechselt werden.

### Online Version

https://inbegriff-design.de/projects/obersalzberg/index.html

## Systemvoraussetzungen

Die Applikation wurde für einen 8K Multitouchtisch mit einem Windows 10 Rechner, der über eine moderne leistungsfähige Graphikkarte verfügt, entwickelt.

Leider kam es bei bestimmten Einstellungen auf unserem Entwicklungssystem zu Darstellungsproblemen, bei denen die Bilder nicht richtig geladen wurden und beim Zoomen stark flackerten. Wenn dieses Problem auftritt, sollte die Bildschirmauflösung überprüft werden. Auf unserem System trat das Problem bei einer 8K Auflösung mit 500% Skalierung auf. Das Problem konnte durch Einstellung der Bildschirmauflösung auf 4096 x 2160 und einer Textskalierung von 200% behoben werden.

### Empfohlene Systemeinrichtung

Wir empfehlen für die Installation auf einem Multitouchtisch einen eigenen Benutzer, z.B. "Tisch", einzurichten. Die Applikation kann dann über die autoexec.bat automatisch gestartet werden. Damit keine weiteren Eingaben erforderlich sind, sollte dieser Benutzer als Default Login Account konfiguriert werden.

Wenn Sie Unterstützung über TeamViewer erhalten wollen, installieren Sie für diesen Tisch-Benutzer TeamViewer QuickSupport und legen sie auf den Desktop.

https://www.teamviewer.com/de/download/windows/

Beim Starten von TeamViewer Quicksupport werden Zugangsdaten angezeigt, die uns erlauben über TeamViewer auf Ihr System zuzugreifen. Bitte vereinbaren Sie für Supportanfragen einen Termin mit uns, da wir keine festen Supportzeiten haben.

## Entwicklungsumgebung

Die Software kann für die Anpassung und Weiterentwicklung der Inhalte unter folgender URL heruntergeladen werden:

```
git clone https://uoestermeier@bitbucket.org/uoestermeier/obersalzberg.git
```

Wir empfehlen Visual Studio Code (https://code.visualstudio.com) als Entwicklungsumgebung. Daneben sind folgende Softwarevoraussetzungen zu beachten:

```
node.js >=  v20.2.0
electron >=  v24.3.1
python > 3.7.5
```

Nach dem Auschecken sind folgenden Schritte auszuführen:

```
pip install -r requirements.txt         # für das bin/images.py Skript
npm install -g electron                 # Globale Installation von Electron
npm install                             # Installation der Node.js Module
```

Nachdem die Änderungen durchgeführt wurden, kann die Applikation mit

```
npm run package-win
```

neu gebaut werden. Die neue Applikation findet sich danach im dist-Verzeichnis.

## Setup

Die für die Aufbereitung der Materialien notwendigen Schritte werden mit folgender Funktion ausgeführt:

```
npm run setup
```

Damit werden mehrere Skripte ausgeführt, die im Weiteren genauer beschrieben werden.

### Kartenskript

Karten werden mit einer Datein 'cards.json' geladen. Diese muss entweder manuell modifiziert werden oder alle verfügbaren Karten werden mit dem Kartenskript,automatisiert in die Datei geladen. Das Kartenskript kann über

```
npm run update-cards
```

gestartet werden.

### Bilderskript

Um die Performanz der Anwendung zu erhöhen, müssen alle Bilder in der Auflösung reduziert werden und für alle Bilder kleiner Vorschauversionen erstellt werden. Sehr große Bilder, die den Speicherbedarf zu stark wachsen lassen, werden auf eine geringere Auflösung reduziert. Dieser Prozess ist vollautomatisiert und kann über

```
npm run create-images
```

gestartet werden. Dieser Prozess kann einige Minuten in Anspruch nehmen.

## Inhalte ändern und hinzufügen

Alle Inhalte finden sich im Verzeichnis

```
var/cards
```

Pro Karte gibt es ein Unterverzeichnis, das der Namenskonvention 01, 02, ... 21, ... folgt. Jedes Kartenverzeichnis sollte die folgenden Elemente enthalten:

index.html -- Die Haupteinstiegsseite mit der Spezifikation der Bildregionen
layer -- Ein Verzeichnis mit Bildern, die sukzessive den Bildaufbau demonstrieren.
details_de.html -- Die Beschreibung der Bilddetails auf Deutsch
details_en.html -- Die englischsprachigen Bilddetails

Änderungen können in Visual Studio Code und mit Graphik-Editoren vorgenommen werden. Wichtig ist, dass die HTMl Header und Scripte beibehalten werden, damit die Dateien in Visual Studio Code direkt über "Open In Browser" angeschaut werden können.

Wenn eine neue Karte hinzugefügt werden soll, ist es am einfachsten, eine existierende Karte als Vorlage zu nutzen und zu kopieren, um dann alle Inhalte bis auf die genannten zu löschen.

Nach dem Anlegen einer neuen Karte muss

```
npm run update-cards
```

aufgerufen werden. Jede Änderung oder Hinzufügung eines Bildes erfodert den erneuten Aufruf von

```
npm run create-images
```

bevor die Hauptapplikation mit

```
npm run elctron
```

neu getestet wird.

## Logging

Bei der Interaktion werden Log-files generiert. Diese befinden sich im Nutzerpfad unter: [NUTZERPFAD]/propaganda/log/

Unter Windows ist der [NUTZERPFAD]: C:\Users\\%USERNAME%\AppData\Roaming\

Bei jedem Start der Anwendung wird eine neue Logdatei angelegt. Bestimmte Ereignisse werden in die Datei geschrieben. Diese sind wie folgt aufgebaut:

<span style="color: green">18:04:35,277</span><span style="color: blue">\_\_\_\_</span></span><span style="color: red">CARD_INITIALIZED</span><span style="color: magenta">::CARD:05</span>

1. <span style="color: green">Zeit als HH:MM:SS,sss</span>
1. <span style="color: blue">4-Leerzeichen</span>
1. <span style="color: red">Ereignisname</span>
1. <span style="color: magenta">Optionale Schlüssel-Wert-Paare, getrennt durch '::'</span> |

### Initialisierung Karte

Bei der Initilialisierung wird ein Wert geloggt, der als Parameter den Wert 'CARD' enthält. Hiermit kann sichergestellt werden, dass bei einem Versuch alle geforderten Karten vorhanden waren.

#### Beispiel

```

18:04:35,277 CARD_INITIALIZED::CARD:05

```

### Sprachänderung

Es wird gespeichert, wenn der Nutzer die Sprache auf einer Karte umstellt. Es werden die Parameter 'CARD', 'FROM' und 'TO' angegeben.

#### Beispiel

```

18:05:47,864 CHANGE_LANGUAGE::CARD:05,FROM:de,TO:en

```

### Wechsel des Kartenmodus

Wenn der Nutzer auf die Knöpfe der Menüleiste drückt, wechselt er den Kartenmodus. Mögliche Zustände sind: "slider", "background", "details" und "none".

#### Beispiel

```

18:32:32,668 CHANGED_MODE::CARD:05,MODE:none

```

### Popup Interaktion

Wenn der Nutzer ein Popup im Artikel anwählt (OPEN_POPUP) oder es schließt (CLOSE_POPUP).
Es wird bei beiden Ereignissen der Dateipfad der Popup-Datei angegeben.

**HINWEIS: Bisher wird kein Unterschied zwischen Artikel- und Highlightpopups gemacht.**

### Details Highlight

Wenn ein Nutzer ein Details-Highlight öffnet, wird das Ereignis geloggt. Wählt er ein anderes Highlight an, schließt das erste und das neue wird geöffnet. Wobei das Öffnen geloggt und das Schließen **nicht** geloggt wird. Wird das aktive Highlight durch erneutes anklicken geschlossen, wird das Schließevent geloggt. Damit kann unterschieden werden, ob der Nutzer ein anderes Highlight angeklickt hat oder ein Highlight explizit geschlossen hat.

```

18:41:29,989 TOGGLE_HIGHLIGHT::CARD:05,HIGHLIGHT:@@1:ACTIVE:open
18:41:31,821 TOGGLE_HIGHLIGHT::CARD:05,HIGHLIGHT:@@2:ACTIVE:open
18:41:35,325 TOGGLE_HIGHLIGHT::CARD:05,HIGHLIGHT:@@2:ACTIVE:close

```

```

18:39:13,052 OPEN_POPUP::CARD:05,POPUP:./var/cards/05/anekdoten.html
18:39:17,838 CLOSE_POPUP::CARD:05,POPUP:./var/cards/05/anekdoten.html

```

## Configuration

## collapsibleScrollIntoView

Werte: [**true**, false]

Gibt an, ob der Titel des Collapsible in das Bild animiert wird.

## collapsibleScrollDuration

Werte: t, t >= 0, [**1**]

Die Zeit in Sekunden, die die Animation benötigt um vollständig aufzuklappen.

## debug

Werte: [true,**false**]

Gibt an, ob die Applikation im Debug-Modus startet.

## display

Werte: [**desktop**, table]

Ändert das Verhalten der Anwendung in Abhängigkeit des Display Typs. Zum Beispiel wird beim Zurücksetzen der Karten
die Karte im _Tisch-Modus_ rotiert, wohingegen die rotation im _Desktop-Modus_ nicht verändert wird.

### fadeSliderText

Werte: [**true**,false]

Animiert die Transparenz des Slidertextes. Wenn

### highlightScale

Werte: Kleitkommazahl k, k > 0, [**2.0**]

Beschreibt um welchen Faktor das Highlight skaliert werden soll.

### highlightStroke

Werte: Ganzzahlen i, i > 0 [**2**]

Gibt die Strichstärke der Highlight-Kreise an.

### titleOnTouch

Werte: [true,**false**]

Der Titel wird nur angezeigt, wenn die Karte bewegt wird und zudem kein Modus der Karte aktiv ist.

### translateCardOnScrollLimit

Werte: [true,**false**]

Erlaubt es die Karte in der Scrolleiste zu verschieben, wenn das Scrolllimit erreicht wurde.

### languageOnTouch

Werte: [true,**false**]

Der Sprachknöpfe werden nur angezeigt, wenn die Karte bewegt wird und zudem kein Modus der Karte aktiv ist.

### menuOnTouch

Werte: [true,**false**]

Das Menu wird nur angezeigt, wenn die Karte bewegt wird und zudem kein Modus der Karte aktiv ist.

### onTouchTime

Werte: Ganzzahlen i, i >= 0 [**3000**]

Zeit in Millisekunden, wie lange die Elemente nach dem Touch sichtbar sind.

### resetTimer"

Werte: [**true**,false]

Gibt an, ob die Karten nach längerer Zeit, in der sie nicht berührt werden, wieder an den Ausgangspunkt zurückbewegt werden.

### resetTime

Werte: Kleitkommazahl k, k > 0, [**5.0**]

Der Zeitraum an Inaktivität in Minuten, der verstreichen muss, bevor die Karten wieder in ihre Ausgangsposition zurück gehen.

### showOriginalAtSliderStart

Werte: [true,**false**]

Gibt an, ob das Original angezeigt werden soll, sobald der Slider betätigt wird.

### sortingType

Werte: ["line",**"random"**]

Gibt die Sortierung an, mit der die Karten beim Start angeordnet werden. Die Zufallsanordnung "random" verteilt die Karten willkürlich über den ganzen Tisch, wobei allerdings die Leserichtung nach außen berücksichtigt wird. Die lineare Anordnung "line" reiht die Karten von links oben nach revchts unten an.

### sortingDuration

Werte: Kleitkommazahl k, k > 0, [**5.0**]

Der Zeitraum in Sekunden, in der die Karten sortiert werden.

### startupLanguage

Werte: [**"de"**, "en"]

Die Sprache mit der die Karten zu Beginn angezeigt werden.

### startScale

Werte: Kleitkommazahl k, k > 0, [**0.5**]

Der Skalierungsfaktor der Karten mit der die Applikation startet und der benutzt wird, um die Karten in ihren Ausgangszustand zurück zu versetzen.

### useNonLocalizedCardVersions

Werte: [true,**false**]

Gibt an, ob die nicht-lokalisierten Kartenversionen benutzt werden sollen. Normalerweise werden die lokalisierten Karten benutzt.

```

```
