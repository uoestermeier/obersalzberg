mkdir -p www
mkdir -p www/obersalzberg/node_modules
npm run build
cp -r css www/obersalzberg
cp -r js www/obersalzberg
cp -r var www/obersalzberg
cp -r iwmlib www/obersalzberg
cp -r lib www/obersalzberg
cp -r icons www/obersalzberg
cp -r templates www/obersalzberg

cp index.html www/obersalzberg
cp index2.html www/obersalzberg
cp config.json www/obersalzberg
cp cards.json www/obersalzberg
perl -0pe 's/<script>.*?<\/script>/<script src="js\/all.js"><\/script>/gs' index.html > www/obersalzberg/index.html
perl -0pe 's/<script>.*?<\/script>/<script src="js\/all.js"><\/script>/gs' index2.html > www/obersalzberg/index2.html

# https://inbegriff-design.de/projects/obersalzberg/index.html
sshpass -p '' scp -r /Users/uo/bitbucket/obersalzberg/www/obersalzberg u97339241@access783756419.webspace-data.io:./www/projects