from PIL import Image 
import os
import os.path
import shutil

# ORIGINAL_SIZE = 4096
# LAYER_SIZE = 2048
# DRAWER_SIZE = 1024

#ORIGINAL_SIZE = 1536
#LAYER_SIZE = 768
#DRAWER_SIZE = 768   # 512 leads to problems with text in images


# uo: Original 1.2 version currently in production
ORIGINAL_SIZE = 2048
LAYER_SIZE = 1024
DRAWER_SIZE = 1024   # 512 leads to problems with text in images


LOW_SIZE = 32     # Needed to ensure fast loading, 64 is too slow
MAXCOLORCOUNT = 1024


supported = ['.png', '.jpg', '.jpeg', '.gif']
here = os.path.dirname(__file__)
var = os.path.join(os.path.dirname(here), 'var', 'cards')


def save_quantized(input, path):
    try:
        colsbefore = input.getcolors(maxcolors=MAXCOLORCOUNT)
        colsbefore = len(colsbefore) if colsbefore is not None else f"{MAXCOLORCOUNT}<"
        im = input.quantize(colors=256)
        colsafter = len(im.getcolors(maxcolors=MAXCOLORCOUNT))
        print(f"  colors {colsbefore} -> {colsafter}")
        im.save(path) 
    except ValueError:
        print("   cannot quantize", path)
        input.save(path)
    except OSError:
        print("   cannot quantize", path)
        input.save(path) 


def resize_width(image, width, height, final_width, path, quantize=True):
    final_height = round((final_width / width) * height)
    resized = image.resize((final_width, final_height))
    if quantize:
        save_quantized(resized, path)
    else:
        resized.save(path)
    return final_width, final_height

def resize_height(image, width, height, final_height, path, quantize=True):
    final_width = round((final_height / height) * width)
    resized = image.resize((final_width, final_height))
    if quantize:
        save_quantized(resized, path)
    else:
        resized.save(path)
    return final_width, final_height

def reset():
    for root, dirs, files in sorted(os.walk(var, topdown=False)):
        for name in files:
            for ext in supported:
                if name.endswith('-lowly'+ext):
                    continue
                original_ext = '-original'+ext
                if name.endswith(original_ext):
                    path = os.path.join(root, name)
                    target_name = name[:-len(original_ext)] + ext
                    target_path = os.path.join(root, target_name)
                    os.unlink(target_path)
                    print("reset ", name, "->", target_name)
                    shutil.move(path, target_path)

def main(quantize=True):
    count = 0
    originals = 0
    for root, dirs, files in sorted(os.walk(var, topdown=False)):
        for name in files:
            for ext in supported:
                if name.endswith('-lowly'+ext):
                    continue
                if name.endswith('-original'+ext):
                    continue
                if name.endswith(ext):
                    dst = name[:-len(ext)] + '-lowly' + ext
                    path = os.path.join(root, name)
                    print(path, "->", dst)
                
                    input = Image.open(path)
                    width, height = input.size
                    resize_width(input, width, height, LOW_SIZE, os.path.join(root, dst), quantize)
                    MAX_SIZE = DRAWER_SIZE
                    if '/layer' in path:
                        MAX_SIZE = LAYER_SIZE
                    if name.startswith('original.'):
                        MAX_SIZE = ORIGINAL_SIZE
                    original = name[:-len(ext)] + '-original' + ext
                    original_path = os.path.join(root, original)    
                    if width > MAX_SIZE or height > MAX_SIZE:
                        if not os.path.exists(original_path):
                            shutil.move(path, original_path)
                        if width > height:
                            size = resize_width(input, width, height, MAX_SIZE, path, quantize)
                        else:
                            size = resize_height(input, width, height, MAX_SIZE, path, quantize)
                        print(path, "->", original, size)
                        originals += 1
                    else:
                        if quantize:
                            if not os.path.exists(original_path):
                                shutil.move(path, original_path)
                            save_quantized(input, path)
                            originals += 1
                        print(path, 'ok', input.size)
                    count += 1
    print("Resized", count, "images, reduced", originals)
 
if __name__ == '__main__':  
    reset()
    main(quantize=True)
