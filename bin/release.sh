rm -r www/obersalzberg
mkdir -p www/obersalzberg
cp -r css www/obersalzberg
cp -r fonts www/obersalzberg
cp -r js www/obersalzberg
cp -r var www/obersalzberg
cp -r iwmlib www/obersalzberg
cp -r lib www/obersalzberg
cp -r icons www/obersalzberg
cp -r templates www/obersalzberg
cp index.html www/obersalzberg
cp config-www.json www/obersalzberg/config.json
cp cards.json www/obersalzberg