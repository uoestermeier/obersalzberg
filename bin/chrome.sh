#!/bin/bash

# Starts Chrome on Mac with the root index.html

app="/Applications/Google Chrome.app"
url="index.html"

open -a "${app}" "${url}" --args --allow-file-access-from-files --auto-open-devtools-for-tabs --enable-precise-memory-info --ignore-certificate-errors
