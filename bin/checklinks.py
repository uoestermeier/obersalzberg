import os
import os.path

from bs4 import BeautifulSoup, SoupStrainer
import requests

here = os.path.dirname(__file__)
base = os.path.dirname(here)
var = os.path.join(base, 'var', 'cards')
supported = ['.png', '.jpg', '.jpeg', '.gif']

def main():
    a_hrefs = dict()
    img_srcs = dict()
    image_hrefs = dict()
    circle_classes = dict()
    detail_classes = dict()
    print("Collecting links")
    problems = 0
    for root, dirs, files in sorted(os.walk(var, topdown=False)):
        for name in files:
            if name.endswith('.html'):
                path = os.path.join(root, name)
                short_path = path[len(base)+1:]
                with open(path, 'r') as f:
                    soup = BeautifulSoup(f.read(), 
                                         features="lxml")
                    for link in soup.find_all('a'):
                        try:
                            href = requests.compat.urljoin(short_path, link['href'])
                            a_hrefs.setdefault(short_path, set()).add(href)
                        except KeyError:
                            try:
                                href = requests.compat.urljoin(short_path, link['xlink:href'])
                                a_hrefs.setdefault(short_path, set()).add(href)
                            except KeyError:
                                print("missing href attr in <a> tag in file", short_path)
                                problems += 1
                    for img in soup.find_all('img'):
                        try:
                            src = requests.compat.urljoin(short_path, img['src'])
                            img_srcs.setdefault(short_path, set()).add(src)
                        except KeyError:
                            print("missing src attr in <img> tag in file", short_path)
                            problems += 1
                    # <image xlink:href="layer/original.png" ...
                    for image in soup.find_all('image'):
                        try:
                            href = requests.compat.urljoin(short_path, image['xlink:href'])
                            image_hrefs.setdefault(short_path, set()).add(href)
                        except KeyError:
                            print("missing href attr in <svg:image> tag in file", short_path)
                            problems += 1

                    if name.startswith('index'):
                        for circle in soup.find_all('circle'):
                            for klass in circle['class']:
                                circle_classes.setdefault(short_path, set()).add(klass)

                    if name.startswith('details'):
                        for h2 in soup.find_all('h2'):
                            for klass in h2['class']:
                                detail_classes.setdefault(short_path, set()).add(klass)


    print("Checking links")
    for path, values in sorted(a_hrefs.items()):
        for value in sorted(values):
            if not os.path.exists(value):
                print("a href", value, "in", path, "does not exist")
                problems += 1
            
    print("Checking html img tags")
    all_img_srcs = set()
    for path, values in sorted(img_srcs.items()):
        for value in sorted(values):
            all_img_srcs.add(value)
            if not os.path.exists(value):
                print("img src", value, "in", path, "does not exist")
                problems += 1

    print("Checking svg image tags")
    for path, values in sorted(image_hrefs.items()):
        for value in sorted(values):
            all_img_srcs.add(value)
            if not os.path.exists(value):
                print("imgage href", value, "in", path, "does not exist")
                problems += 1

    print("Checking images without references")  
    existing_srcs = set()
    for root, dirs, files in sorted(os.walk(var, topdown=False)):
        for name in files:
            for ext in supported:
                if name.endswith('-lowly'+ext):
                    continue
                if name.endswith('-original'+ext):
                    continue
                if name.endswith(ext):
                    path = os.path.join(root, name)
                    short_path = path[len(base)+1:]
                    existing_srcs.add(short_path)
    diff = existing_srcs - all_img_srcs
    for value in sorted(diff):
        print("img file", value, "without src reference")
        problems += 1   

    print("Checking highlight circles")
    for path, values in sorted(circle_classes.items()):
        details_de = detail_classes.get(path.replace('index.html', 'details_de.html'))
        details_en = detail_classes.get(path.replace('index.html', 'details_en.html'))
        for klass in values:
            if klass not in details_de:
                print("circle class", klass, "in", path, "not found in details_de.html h2 classes")
                problems += 1  
            if klass not in details_en:
                print("circle class", klass, "in", path, "not found in details_en.html h2 classes")
                problems += 1  

   

    print("found", len(existing_srcs), "images and", len(all_img_srcs), "src links")
    print("—" * 80)
    print(problems, "problems")

if __name__ == '__main__':  
    main()
