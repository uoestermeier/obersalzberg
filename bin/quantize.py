from PIL import Image 
import os
import os.path
import shutil

ORIGINAL_SIZE = 4096
LAYER_SIZE = 2048
DRAWER_SIZE = 1024
LOW_SIZE = 32     # Needed to ensure fast loading, 64 is too slow

supported = ['.png', '.jpg', '.jpeg', '.gif']
here = os.path.dirname(__file__)
var = os.path.join(os.path.dirname(here), 'var', 'cards')
target = os.path.join(os.path.dirname(here), 'var', 'quantized')


MAXCOLORCOUNT = 1024

def quantize(input, path):
    try:
        colsbefore = input.getcolors(maxcolors=MAXCOLORCOUNT)
        colsbefore = len(colsbefore) if colsbefore is not None else f"{MAXCOLORCOUNT}<"
        im = input.quantize(colors=256)
        colsafter = len(im.getcolors(maxcolors=MAXCOLORCOUNT))
        print(f"colors {colsbefore} -> {colsafter}")
        dir = os.path.dirname(path)
        os.makedirs(dir, exist_ok=True)
        im.save(path) 
        return True
    except ValueError:
        print("cannot quanatize")
        return False
    
def reset():
    for root, dirs, files in sorted(os.walk(var, topdown=False)):
        for name in files:
            for ext in supported:
                if name.endswith('-lowly'+ext):
                    continue
                original_ext = '-original'+ext
                if name.endswith(original_ext):
                    path = os.path.join(root, name)
                    target_name = name[:-len(original_ext)] + ext
                    target_path = os.path.join(root, target_name)
                    os.unlink(target_path)
                    print("reset ", name, "->", target_name)
                    shutil.move(path, target_path)

def main():
    count = 0
    originals = 0
    for root, dirs, files in sorted(os.walk(var, topdown=False)):
        for name in files:
            for ext in supported:
                if name.endswith('-lowly'+ext):
                    continue
                if name.endswith('-original'+ext):
                    continue
                if name.endswith(ext):
                    path = os.path.join(root, name)
                    dst = target + path[len(var):]
                    print(path, "->", dst)
                    input = Image.open(path)
                    if quantize(input, dst):
                        count += 1
                    if count > 10:
                        return
    print("Quantized", count, "images", originals)
 
if __name__ == '__main__':  
    reset()
    main()
