#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import os
import unicodedata
import sys
import argparse

here = os.path.abspath(os.path.dirname(__file__))
content = os.path.join(here, 'var', 'cards')
print content
map = {u' ': u'_',
        u'ß': u'ss',
        u'ê': u'e',
        u'à': u'a',
        u'ä': u'ae',
        u'ö': u'oe',
        u'ü': u'ue',
        u'Ä': u'Ae',
        u'Ö': u'oe',
        u'Ü': u'ue'}

def ascii_hammer(content, encoding='utf-8'):
    for key, value in map.items():
        content = content.replace(key.encode(encoding), value.encode(encoding))
    return content.replace('\xcc\x88', 'e')


def normalize(path):
    """Normalize a filename to normalization form C.

    Linux and (most?) other Unix-like operating systems use the normalization
    form C (NFC) for UTF-8 encoding by default but do not enforce this.
    Darwin, the base of Macintosh OSX, enforces normalization form D (NFD),
    where a few characters are encoded in a different way.

    Returns the path as an unicode string.

    """
    if isinstance(path, unicode):
        path = unicodedata.normalize("NFC", path)
    else:
        path = unicode(path, encoding=sys.getfilesystemencoding())
        path = unicodedata.normalize("NFC", path)
    return path

def main(dryrun=False):
    umlauts = dict()
    normalized = dict()
    xmlfiles = set()
    rename = dict()
    for dirpath, dnames, fnames in os.walk(content):
        for f in fnames:
            path = os.path.join(dirpath, f)
            if f.endswith('.xml'):
                xmlfiles.add(path)
            ascii = ascii_hammer(f)
            if f != ascii :
                nf = normalize(f)
                normalized[nf] = ascii
                umlauts[f] = ascii
                rename[path] = os.path.join(dirpath, ascii)
    if dryrun:
        print "Dry run on", content
        for path in xmlfiles:
            modified = dict()
            fp = open(path, 'r')
            xml = fp.read()
            fp.close()
            for key, value in normalized.items():
                if key.encode('utf-8') in xml:
                    modified[key] = value
            if modified:
                print "Replace in", path[len(content):]
                for key, value in modified.items():
                    print "   ", key, "->", value
        if rename:
            print "Renaming"
            for key, value in sorted(rename.items()):
                print 'Move %s %s' % (key[len(content):], value[len(content):])
        else:
            print "Nothing to rename"
        return

    print "Replace"
    for path in xmlfiles:
        fp = open(path, 'r')
        xml = fp.read()
        fp.close()

        for key, value in normalized.items():
            xml = xml.replace(key.encode('utf-8'), value)

        fp = open(path, 'w')
        fp.write(xml)
        fp.close()

    print "Renaming"
    for key, value in sorted(rename.items()):
        print [key, value]
        cmd = 'mv "%s" "%s"' % (key, value)
        print cmd
        os.system(cmd)

if __name__ == '__main__':
    """Run the script

        python rename.py

    with the options

        python rename.py --dry-run

    or

        python rename.py --no-dry-run
    """
    parser = argparse.ArgumentParser(description='Rename files with umlauts.')
    parser.add_argument('--dry-run', dest='dry_run', action='store_true')
    parser.add_argument('--no-dry-run', dest='dry_run', action='store_false')
    parser.set_defaults(dry_run=True)
    args = parser.parse_args()
    main(dryrun=args.dry_run)
