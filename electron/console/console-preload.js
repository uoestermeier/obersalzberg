const { ipcRenderer } = require('electron')
const { BrowserWindow } = require('electron').remote

window.BrowserWindow = BrowserWindow
window.ipcRenderer = ipcRenderer
