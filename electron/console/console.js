const path = require('path')

class Console {
    constructor({ devTools = false } = {}) {
        this.instance = null
        this.devTools = devTools
    }

    async open() {
        return new Promise((resolve, reject) => {
            if (!this.instance) {
                const { BrowserWindow } = require('electron')

                const preloadScript = path.join(__dirname, `console-preload.js`)

                this.instance = new BrowserWindow({
                    height: 250,
                    width: 400,
                    show: false,
                    frame: false,
                    transparent: true,
                    webPreferences: {
                        preload: preloadScript
                    }
                })

                this.instance.on('ready-to-show', () => {
                    this.instance.show()
                    resolve(this.instance)
                })

                this.instance
                    .loadURL(`file://${__dirname}/console.html`)
                    .catch(console.error)

                if (this.devTools == true)
                    this.instance.webContents.openDevTools()
            } else reject(`Console was already opened!`)
        })
    }

    send(message, className = '') {
        if (!this.instance.isDestroyed())
            this.instance.webContents.send('message', {
                message,
                className
            })
    }
}

module.exports = Console
