/* eslint-disable no-unused-vars */
/* eslint-disable no-console */

const {
    app,
    BrowserWindow,
    ipcMain,
    dialog,
    shell,
    powerSaveBlocker
} = require('electron')
const electronLocalshortcut = require('electron-localshortcut')
//const electron = require('electron')
const os = require('os')
const fs = require('fs')
const fse = require('fs-extra')
const path = require('path')
const { URL } = require('url')
const Store = require('./store.js')
const dateFormat = require('dateformat')
const appName = 'Obersalzberg'
// Use this constant to start the application in development mode
const DEVELOPMENT = true
global.errorCount = 0
global.menu = null

const id = powerSaveBlocker.start('prevent-display-sleep')
console.log(powerSaveBlocker.isStarted(id))

//app.disableHardwareAcceleration()
const logFile = createLogFile()

if (DEVELOPMENT) {
    let electron = path.join(
        __dirname,
        '..',
        'node_modules',
        '.bin',
        'electron'
    )
    // Add the electron reloader to reload the page when the code changes.
    require('electron-reload')(path.join(__dirname, '..'), {
        electron,
        ignored: /node_modules|[\/\\]\.|var/
    })
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win
let heartbeatTimeout = 10 // in seconds
let lastHeartBeat = null
let reloading = false

const store = new Store({
    // We'll call our data file 'user-preferences'
    configName: 'user-preferences',
    defaults: {
        devTools: DEVELOPMENT,
        multiUserBrowser: true,
        storedBounds: { x: 32, y: 32, width: 1600, height: 1200 }
    }
})

function createWindow() {
    let { screen } = require('electron')
    // Cannot develop properly with the store always highjacking all paths
    // Therefore the url is set absolutely here!
    let url = `file://${__dirname}/../index.html`
    let bounds = store.get('storedBounds')
        ? store.get('storedBounds')
        : screen.getPrimaryDisplay().bounds
    console.log('createWindow', bounds)
    win = new BrowserWindow({
        x: 0,
        y: 0,
        width: bounds.width,
        height: bounds.height,
        fullscreenable: true,
        fullscreen: true,
        title: appName,
        show: false,
        kiosk: DEVELOPMENT ? false : true,
        acceptFirstMouse: true,
        webPreferences: {
            webSecurity: true,
            allowRunningInsecureContent: false,
            nodeIntegration: true, // uo: is this critical? Seems to be needed in preload.js
            webviewTag: true,
            nativeWindowOpen: true,
            devTools: true,
            preload: path.join(__dirname, './preload.js')
        },
        icon: path.join(__dirname, 'assets/icons/png/64x64.png')
    })
    module.exports.win = win

    // BAD: All other methods don't work (like ensureFileSync, fileExists...)
    try {
        let settings = require('./settings.json')
        console.log('Using settings', `file://${__dirname}/${settings.url}`)
        win.loadURL(`file://${__dirname}/${settings.url}`)
    } catch (ex) {
        win.loadURL(url)
    }

    const { webContents } = win

    webContents.focus()

    // Add the app menu
    let menu = require('./menu.js')
    global.menu = menu

    // Add global shortcuts
    // Esc quits the app
    electronLocalshortcut.register('Esc', () => {
        app.quit()
    })

    // Command (Mac) or Control (Win) + K toggles the Kiosk mode
    electronLocalshortcut.register('CommandOrControl+K', () => {
        if (win) {
            win.setKiosk(!win.isKiosk())
        }
    })

    // Show if its ready.
    win.once('ready-to-show', () => {
        webContents.send('preparePads')
        win.show()
    })

    // Clear cache
    webContents.session.clearCache(() => console.log('Cache cleared'))

    // Open dev tools when in development mode
    if (store.get('devTools')) {
        webContents.openDevTools({ mode: 'right' })
    } else {
        webContents.closeDevTools()
    }

    webContents.on('devtools-opened', () => {
        store.set('devTools', true)
    })

    webContents.on('devtools-closed', () => {
        store.set('devTools', false)
    })

    webContents.on('did-navigate', (event, url) => {
        menu.setHistoryStatus()
    })

    webContents.on('render-process-gone', (e, details) => {
        console.log('render-process-gone', details)
        const arg = `Render process has ${details.reason}. Reloading page.`
        console.error(arg)
        writeLogLine(arg)
        win.reload()
    })

    // WORKAROUND: On windows, if the app was set to fullscreen, the menubar is not hidden
    if (win.isKiosk()) {
        win.setMenuBarVisibility(false)
    }

    win.on('focus', event => {
        menu.focus()
    })

    win.on('blur', event => {
        menu.blur()
    })

    win.on('enter-full-screen', () => {
        win.setMenuBarVisibility(false)
    })

    win.on('leave-full-screen', () => {
        win.setMenuBarVisibility(true)
    })

    win.on('enter-html-full-screen', () => {
        win.setMenuBarVisibility(false)
    })

    win.on('leave-html-full-screen', () => {
        win.setMenuBarVisibility(true)
    })

    win.on('close', () => {
        console.log('closing', win.getBounds())
        store.set('storedBounds', win.getBounds())
        app.quit()
    })
}

function forceRestart() {
    app.relaunch()
    app.exit()
}

ipcMain.on('directoryListing', (e, opts = {}) => {
    let { directory, files, folders } = opts
    console.log('directoryListing', opts)
    try {
        let listing = fs.readdirSync(directory)
        let result = { directory, files: [], folders: [] }
        for (let name of listing) {
            if (name.startsWith('.')) continue
            let fullPath = path.join(directory, name)
            let stat = fs.lstatSync(fullPath)
            if (files && stat.isFile()) {
                if (typeof files == 'string' && !files.endsWith(files)) continue
                result.files.push(name)
            }
            if (folders && stat.isDirectory()) result.folders.push(name)
        }
        e.sender.send('directoryListing', result)
    } catch (err) {
        let args = { directory, errorMessage: err.message }
        e.sender.send('directoryListingError', args)
    }
})

ipcMain.on('createTextfile', (e, opts = {}) => {
    opts = Object.assign(
        {},
        {
            name: `iwmbrowser-${new Date()
                .toISOString()
                .replace(/:/g, '-')}.txt`,
            path: os.tmpdir(),
            text: ''
        },
        opts
    )
    let file = path.join(opts.path, opts.name)
    fs.writeFile(file, opts.text, err => {
        if (err) {
            //throw err
        } else {
            console.log(`Textfile saved: ${file}`)
        }
    })
})

ipcMain.on('asynchronous-message', (event, arg) => {
    console.log(arg) // prints "ping"
    event.reply('asynchronous-reply', 'pong')
})

ipcMain.on('log', (event, arg) => {
    if (arg.startsWith('heartbeat')) {
        let now = new Date()
        lastHeartBeat = now.getSeconds()
        reloading = false
    } else {
        writeLogLine(arg)
    }
})

ipcMain.on('error', e => {
    console.log('Received error notification')
    global.errorCount += 1
})

ipcMain.on('openExternal', (e, url = null) => {
    console.log('Received openExternal', url)
    if (url) {
        shell.openExternal(url)
    }
})

ipcMain.on('save', (e, opts = {}) => {
    let { url, html, saveAs, action } = opts
    // url must absolute URL
    let urlObj = new URL(url)
    let pathname = urlObj.pathname
    if (saveAs) {
        pathname = dialog.showSaveDialog(win, {
            title: 'Save as:',
            defaultPath: pathname
        })
        if (typeof pathname == 'undefined') return
    }
    try {
        console.log('Saving', pathname, action)
        html = prettyPrint(html, { indent_size: 4 })
        fs.writeFileSync(pathname, html, 'utf-8')
        if (saveAs) {
            let normalized = pathname.replace(/\\/g, '/')
            e.sender.send('savedAs', { url: `file://${normalized}`, action })
        }
    } catch (e) {
        console.warn('Failed to save the file', pathname)
        e.sender.send('saveFailed', pathname)
    }
})

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q

    powerSaveBlocker.stop(id)
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
})

function createLogFile() {
    const fileName =
        dateFormat(new Date(), 'yyyy-mm-dd__HH"h"MM"m"ss"s"') + '.txt'
    const appData = app.getPath('appData')
    const filepath = path.join(appData, appName, 'log', fileName)

    fse.ensureFileSync(filepath)
    console.log('Created logfile: ', filepath)
    return filepath
}

function writeLogLine(arg) {
    const time = dateFormat(new Date(), 'HH:MM:ss","l')
    const message = time + '    ' + arg + '\n'
    fs.appendFile(logFile, message, err => {
        if (err) console.error(err)
    })
}

setInterval(() => {
    if (lastHeartBeat != null) {
        let now = new Date()
        let seconds = now.getSeconds()
        if (seconds > lastHeartBeat + heartbeatTimeout) {
            if (!reloading) {
                reloading = true
                const arg = `Waited for heartbeat ${heartbeatTimeout} s. Restarting application.`
                console.error(arg)
                writeLogLine(arg)
                forceRestart()
            }
        }
    }
}, 1000)

module.exports = {
    store: store
}
