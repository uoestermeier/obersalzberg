// UO: Disable unintended zoom of fullscreen page if user wants to zoom
// only parts like Eyevisit info cards.
// window.nodeDirname = __dirname

/* According to https://electron.atom.io/docs/faq/
"I can not use jQuery/RequireJS/Meteor/AngularJS in Electron" we
have to rename the symbols in the page before including other libraries.
Remember to use nodeRequire after this point.
*/
window.nodeRequire = require
delete window.require
delete window.exports
delete window.module

/* Special error handling if the rendere process sends a error notification
log this error on the main console.
*/
let { Console } = require('console')

let debugConsole = new Console(process.stdout, process.stderr)

let { contextBridge, ipcRenderer } = require('electron')

contextBridge.exposeInMainWorld('ipcRenderer', {
    send: (msg, args) => {
        debugConsole.log('received send', msg, args)
        ipcRenderer.send(msg, args)
    },

    on: (event, args) => {
        debugConsole.log('received on event', event, args)
    }
})

window.ipcRenderer = ipcRenderer
window.addEventListener('error', event => {
    debugConsole.error('PAGE ERROR', event.error, event.filename)
    debugConsole.error('open', window.location.href)
    ipcRenderer.send('error', 1)
})

/**
 * Load config and cardjs.
 */
let options = {
    debug: true,
    existing: [],
    highlightScale: 1.5
}

try {
    let config = require('../config.json')
    Object.assign(options, config)
} catch (e) {
    debugConsole.error(`Could not find 'config json', default values are used.`)
}

options.existing = []
try {
    options.existing = require('../cards.json')
} catch (e) {
    debugConsole.error(
        `Could not find 'cards.json'. Run the cards script to create or upgrade the 'cards.json' scripts.`
    )
}

for (let [key, val] of Object.entries(options)) {
    if (window[key])
        debugConsole.error(`Config tries to overwrite a window element ${key}.`)
    else {
        window[key] = val
    }
}
