const { app } = require('electron')
const path = require('path')
const fs = require('fs')
const { dialog } = require('electron')

class Snapshot {
    static getAbsolutePath() {
        return path.join(app.getPath('userData'), this.directory)
    }

    static async loadSnapshot(main) {
        let result = await dialog.showOpenDialog({
            defaultPath: this.getAbsolutePath(),
            filters: ['.json'],
            properties: ['openFile']
        })
        if (!result.canceled) {
            const file = result.filePaths[0]
            fs.readFile(file, { encoding: 'utf8' }, (error, data) => {
                if (error) {
                    console.error(error)
                } else {
                    const javascript = `window.app.applyCardSnapshot(${data})`
                    console.log('EXECUTE:', javascript)
                    main.win.webContents.executeJavaScript(javascript)
                }
            })
        }
    }

    static takeSnapshot(main) {
        console.log('EXECUTE: window.app.takeCardSnapshot()')
        try {
            main.win.webContents
                .executeJavaScript(`window.app.takeCardSnapshot()`)
                .then(result => {
                    console.log(result)

                    let date = new Date(Date.now())
                    let calenderDay = [
                        date.getFullYear(),
                        date.getMonth(),
                        date.getDate()
                    ].join('-')
                    let weekdayIdx = date.getDay()

                    let days = [
                        'Montag',
                        'Dienstag',
                        'Mittwoch',
                        'Donnerstag',
                        'Freitag',
                        'Samstag',
                        'Sonntag'
                    ]

                    let time = `${date.getHours()}h ${date.getMinutes()}m ${date.getSeconds()}s`
                    const filename =
                        ['snapshot', calenderDay, days[weekdayIdx], time].join(
                            ' '
                        ) + '.json'

                    let directory = this.getAbsolutePath()

                    if (!fs.existsSync(directory)) {
                        fs.mkdirSync(directory)
                    }

                    let savePath = path.join(directory, filename)

                    console.log(savePath, JSON.stringify(result))
                    try {
                        fs.writeFileSync(savePath, JSON.stringify(result), {
                            encoding: 'utf8'
                        })
                    } catch (e) {
                        console.error(e)
                    }
                })
                .catch(console.error)
        } catch (e) {
            console.error(e)
        }
    }
}
Snapshot.directory = 'card-snapshots'

module.exports = Snapshot
