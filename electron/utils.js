function fileURL(src) {
    let dir = __dirname.replace(/\\/g, '/')
    return `file://${dir}/${src}`
}

function loadScript(src, callback) {
    let url = fileURL(src)
    let script = document.createElement('script')
    script.onload = () => {
        if (callback) {
            callback.call(this, script)
        }
    }
    script.src = url
    document.head.appendChild(script)
}

function thumbnail(screenshot) {
    return screenshot.resize({ width: 1024 }).toPNG()
}

let hiddenCursor = fileURL('../assets/cursor/cur0000.cur')
let defaultCursor = fileURL('../assets/cursor/cur1054.cur')

function hideCursor() {
    document.body.style.cursor = `url('${hiddenCursor}'), default`
}

function showCursor() {
    document.body.style.cursor = `url('${defaultCursor}'), default`
}

module.exports = {
    fileURL,
    loadScript,
    thumbnail,
    hideCursor,
    showCursor
}
