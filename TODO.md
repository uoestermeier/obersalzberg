# OBERSALZBERG

## Installation des Tisches

Auto Login: Registrierung-Editor aufrufen

Computer\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon

AutoAdminLogon REG_SZ 1
DefaultUserName REG_SZ Medientisch
DefaultPassword REG_SZ iwm

Autostart Ordner anpassen: Rechtsklck Ausführen

1. Rufe das Ausführen-Fenster auf, indem du Windows-Taste + R betätigst.
2. Gib im erschienenen Fenster „shell:startup“ (ohne Anführungszeichen) ein. Es öffnet sich der Autostart-Ordner.
3. Erstelle eine Verknüpfung des Programms, das du zum Autostart hinzufügen willst. Klicke dazu mit einem Rechtsklick auf die .exe-Datei des Programms und auf den Menü-Punkt „Verknüpfung erstellen“.
4. Lege die Verknüpfung per Drag-and-Drop oder mit dem Ausschneiden-Befehl in den geöffneten Autostart-Ordner.
5. Schließe den Ordner. Die dort abgelegten Programme starten nun automatisch beim Hochfahren deines PCs oder Laptops.

Kiosk Mode aktivieren: Obersalzberg.win32.x64 \ resources \ app \ doc \ Kiosk-Infos \ .txt Dateien

## Sonstiges

Vorbereiten des Filmes

## Obssolet?

ffmpeg -i movie.mov -vf scale=960:540 -preset slow -crf 18 output.mov

Beim Wechsel von gescrollten Details auf den Hintergrund fängt der Text nicht oben bündig an. Beim Wechsel immer nach oben gehen?

Kart 8: Komisches Verhalten beim Aufklappen der Zwischenüberschriften

-   Die Breite der Popups würde ich etwas Größer als die Textseite darunter programmieren. Zu schmal ist auch wieder schwierig, weil wir öfter mal eine komplette Seite dort einfügen und diese dann entweder transkribieren oder zumindest im Englischen überstezen müssen. Da wäre zu schmal dann auch blöd.

Breite der Popups

Popups in der Breite anpassen. Die sind mir zu breit. Erst wenn ich weiß, wie breit die werden kann ich alle Popups mit Test und Transkript endgültig bearbeiten. Erst dann entscheide ich mich, welche Transkripe übereinander oder untereinander vorkommen. Das Format variiert da leider sehr.

Quellenpopups noch schmaler als normale Popups.

BU`s nicht kursiv? Hier ist mir erst bei den Üebrsetzungen ins Englische aufgefallen, dass die Titel dort immer kursiv gesetzt sind. Das geht aktuell nicht, weil die kompletten BU#s bereits kursiv sind. Wie sehen die aus, wenn sie nur kleiner sind, als der restliche Text?

Definitiver Projektname?

Breite der Popups von 1080 auf 840 ok bei Quellenangaben muss verwendet werden

<article class="article-small">
...
</article>

Splashscreen mit Logo Obersalzberg

E-Mail Versand von Log-Dateien?
Mail der Log-Dateien?
Dokumentation?

Sauberer Übergang zwischen Kartenmodi ohne Flackern und Unschärfen (verbessern der replace low and high res methoden)
